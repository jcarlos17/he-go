// information basic
$(document).on('click', '[data-update]', function () {
    var urlUpdatePassword = $(this).data('update');
    var currentPassword = $('#current_password');
    var password = $('#password');
    var passwordConfirmation = $('#password_confirmation');

    $.ajax({
        type: 'POST',
        url: urlUpdatePassword,
        data:{
            '_token':$('input[name=_token]').val(),
            'current_password':currentPassword.val(),
            'password':password.val(),
            'password_confirmation':passwordConfirmation.val()
        },
        success:function (data){
            $('.alert-success').remove();
            $('.alert-warning').remove();

            var $alert = $('#alert-update');

            $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>La contraseña se cambió correctamente</strong></span>' +
                '</div>');
            currentPassword.val("");
            password.val("");
            passwordConfirmation.val("");
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-update');

            if ($data.current_password)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.current_password +'</strong></span>' +
                    '</div>');

            if ($data.password_confirmation)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.password_confirmation +'</strong></span>' +
                    '</div>');

            if ($data.password)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.password +'</strong></span>' +
                    '</div>');
        }
    });
});