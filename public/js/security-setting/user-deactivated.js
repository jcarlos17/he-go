// information basic
$(document).on('click', '[data-deactivated]', function () {
    var urlDeactivated = $(this).data('deactivated');
    var password = $('#psw_deactivated');
    var passwordConfirmation = $('#psw_confirmation_deactivated');
    var selectReason = $('#selectReason');
    var justification = $('#justificationDeactivated');

    $.ajax({
        type: 'GET',
        url: urlDeactivated,
        data:{
            'password':password.val(),
            'password_confirmation':passwordConfirmation.val(),
            'reason': selectReason.val(),
            'justification': justification.val()
        },
        success:function (){
            location.reload();
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-deactivated');

            $('#deactivatedModal').modal('hide');
            if ($data.reason)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.reason +'</strong></span>' +
                    '</div>');

            if ($data.password)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.password +'</strong></span>' +
                    '</div>');

            if ($data.password_confirmation)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.password_confirmation +'</strong></span>' +
                    '</div>');

            if ($data.justification)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.justification +'</strong></span>' +
                    '</div>');
        }
    });
});