// benefit
$(document).on('click', '[data-updateoffer]', function () {
    let urlOffer = $(this).data('updateoffer');
    let formOfferJob = $('#formOfferJob');
    let title = $('#titleOffer');
    let type = $('input[name=type]:checked');
    let expire_date = $('#expireDateOffer');
    let description = $('#descriptionOffer');
    let abilities = $('#abilitiesOffer');
    let type_image = $('#image_option:checked');
    let category_id = $('#select-category');
    let subcategory_id = $('#select-subcategory');
    let priority_type = $('#priorityType');
    let schedule = $('#schedule');
    let job_type = $('#jobType');
    let geographical_mobility = $('#geographicalMobility');
    let experience = $('#experience');
    let language = $('#language');
    let limit = $('#select-limit');
    let department_id = $('#select_department');
    let province_id = $('#select_province');
    let district_id = $('#select_district');
    let suggested_payment = $('#suggestedPayment');


    $.ajax({
        type: 'POST',
        url: urlOffer,
        data:{
            '_token':formOfferJob.find('[name=_token]').val(),
            'title': title.val(),
            'type': type.val(),
            'expire_date': expire_date.val(),
            'description': description.val(),
            'abilities': abilities.val(),
            'attached_images': type_image.val(),
            'category_id': category_id.val(),
            'subcategory_id': subcategory_id.val(),
            'priority_type': priority_type.val(),
            'schedule': schedule.val(),
            'job_type': job_type.val(),
            'geographical_mobility': geographical_mobility.val(),
            'experience': experience.val(),
            'language': language.val(),
            'department_id': department_id.val(),
            'limit': limit.val(),
            'province_id': province_id.val(),
            'district_id': district_id.val(),
            'suggested_payment': suggested_payment.val()

        },
        success:function (){
            window.location = "/gestionar-trabajos"
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            let $data = data.responseJSON;

            let $alert_description = $('#alert-description');
            let $alert_abilities = $('#alert-abilities');
            let $alert_detail = $('#alert-detail');
            let $alert_type = $('#alert-type');

            if ($data.type)
                $alert_type.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.type +'</strong></span>' +
                    '</div>');

            if ($data.title)
                $alert_description.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.expire_date)
                $alert_description.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.expire_date +'</strong></span>' +
                    '</div>');

            if ($data.description)
                $alert_description.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.description +'</strong></span>' +
                    '</div>');

            if ($data.abilities)
                $alert_abilities.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.abilities +'</strong></span>' +
                    '</div>');

            if ($data.category_id)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.category_id +'</strong></span>' +
                    '</div>');

            if ($data.subcategory_id)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.subcategory_id +'</strong></span>' +
                    '</div>');

            if ($data.priority_type)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.priority_type +'</strong></span>' +
                    '</div>');

            if ($data.schedule)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.schedule +'</strong></span>' +
                    '</div>');

            if ($data.job_type)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.job_type +'</strong></span>' +
                    '</div>');

            if ($data.geographical_mobility)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.geographical_mobility +'</strong></span>' +
                    '</div>');

            if ($data.experience)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.experience +'</strong></span>' +
                    '</div>');

            if ($data.language)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.language +'</strong></span>' +
                    '</div>');

            if ($data.department_id)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.department_id +'</strong></span>' +
                    '</div>');

            if ($data.province_id)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.province_id +'</strong></span>' +
                    '</div>');

            if ($data.district_id)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.district_id +'</strong></span>' +
                    '</div>');

            if ($data.suggested_payment)
                $alert_detail.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.suggested_payment +'</strong></span>' +
                    '</div>');
        }
    });
});