// benefit
$(document).on('click', '[data-benefit]', function () {
    var urlBenefit = $(this).data('benefit');

    $.ajax({
        type: 'POST',
        url: urlBenefit,
        data:{
            '_token':$('input[name=_token]').val(),
            'name':$('#nameBenefit').val()
        },
        success:function (data){
            var $benefitList = $('#add-benefits');

            $benefitList.append('<li>' +
                '<span class="tg-tagdashboard">' +
                '<a data-deletebenefit="/publicar-oferta-trabajo/benefit/'+ data.id +'/delete"><i class="fa fa-close"></i></a>' +
                '<em>'+ data.name +'</em>' +
                '</span>' +
                '</li>');
            $('#nameBenefit').val("");
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-benefit');

            if ($data.name)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.name +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-deletebenefit]', function () {
    var $li=$(this).parent().parent();
    var urlDeleteAddress = $(this).data('deletebenefit');
    $.ajax({
        type: 'GET',
        url: urlDeleteAddress,

        success:function (data){
            $li.remove();
        }
    });
});