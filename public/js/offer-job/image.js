$(function () {
    var  $formPhoto, $inputPhoto, $templatePhoto,$uploadedPhotos, $offerId;

    $formPhoto = $('#formPhoto');
    $inputPhoto = $('#inputPhoto');
    $templatePhoto = $('#templatePhoto');
    $uploadedPhotos = $('#uploadedPhotos');
    $offerId = $('#offerId').val();

    $inputPhoto.on('change', uploadPhotos);
    $(document).on('click', '[data-selectphoto]', selectPhoto);
    $(document).on('click', '[data-deletephoto]', removePhoto);

    function uploadPhotos() {
        var filesData = $inputPhoto.prop("files");
        for (var i=0; i<filesData.length; ++i) {
            var url = URL.createObjectURL(filesData[i]);
            var templatePhotoLoading = $('#templatePhotoLoading').html();
            templatePhotoLoading = templatePhotoLoading.replace('id=""', 'id="photo'+i+'"');
            templatePhotoLoading = templatePhotoLoading.replace('src=""', 'src="'+url+'"');
            $('#uploadedPhotos').append(templatePhotoLoading);
            postAjaxPhotoData(filesData[i], i);
        }
    }
    function postAjaxPhotoData(fileData, i) {
        var sizeKB = fileData.size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            $('#photo'+i).remove();
            var $alert = $('#alert-images');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
        var formData = new FormData();
        formData.append('image', fileData);
        formData.append('_token', $formPhoto.find('[name=_token]').val());

        $.ajax({
            url: '/publicar-oferta-trabajo/'+$offerId+'/image/add',
            type: 'POST',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.alert-warning').remove();
                $('#photo'+i).remove();
                var uploadedPhoto = $templatePhoto.html();
                uploadedPhoto = uploadedPhoto.replace('src=""', 'src="'+data.image_url+'"');
                uploadedPhoto = uploadedPhoto.replace('data-selectphoto=""', 'data-selectphoto="/publicar-oferta-trabajo/'+data.offer_job_id+'/image/'+data.id+'/select"');
                uploadedPhoto = uploadedPhoto.replace('data-deletephoto=""', 'data-deletephoto="/publicar-oferta-trabajo/image/'+data.id+'/delete"');
                $uploadedPhotos.append(uploadedPhoto);
            },
            error: function (data) {
                $('.alert-warning').remove();
                $('#banner'+i).remove();
                var $data = data.responseJSON;
                var $alert = $('#alert-images');

                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.image +'</strong></span>' +
                    '</div>');
            }
        });
    }

    function selectPhoto() {
        var urlSelectPhoto = $(this).data('selectphoto');

        $.ajax({
            url: urlSelectPhoto,
            type: 'GET',
            success: function (data) {
                alert('se cambió correctamente la selección')
            },
            error: function () {
                alert('Ha ocurrido un error de conexión')
            }
        });
    }

    function removePhoto() {
        // request al servidor
        var $div = $(this).closest('#itemPhoto');
        var urlDeletePhoto = $(this).data('deletephoto');
        $.ajax({
            type: 'GET',
            url: urlDeletePhoto,

            success:function (){
                $div.remove();
            }
        });
        // y una vez que confirma
    }
});