// Sent proposal
$(document).on('click', '[data-proposal]', function () {
    var btnSentProposal = $('#btnSentProposal');
    btnSentProposal.hide();
    var urlExperience = $(this).data('proposal');

    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('geographical_mobility', $('input[name=geographical_mobility]:checked').val());
    formData.append('necessary_material', $('input[name=necessary_material]:checked').val());
    formData.append('minimum_guarantee', $('input[name=minimum_guarantee]:checked').val());
    formData.append('insurance', $('input[name=insurance]:checked').val());
    formData.append('advancement', $('input[name=advancement]').val());
    formData.append('final_price', $('input[name=final_price]').val());
    formData.append('service_detail', $('textarea[name=service_detail]').val());
    formData.append('personal_interview', $('input[name=personal_interview]:checked').val());

    $.ajax({
        url: urlExperience,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            location.reload();
            var alert = $('#alert-sent-proposal');
            alert.append('<div class="alert alert-success tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>Se envió correctamente su propuesta</strong></span>' +
                '</div>');
        },
        error:function (data) {
            btnSentProposal.show();
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-proposal');

            if ($data.geographical_mobility)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.geographical_mobility +'</strong></span>' +
                    '</div>');

            if ($data.necessary_material)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.necessary_material +'</strong></span>' +
                    '</div>');

            if ($data.minimum_guarantee)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.minimum_guarantee +'</strong></span>' +
                    '</div>');

            if ($data.insurance)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.insurance +'</strong></span>' +
                    '</div>');
            if ($data.advancement)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.advancement +'</strong></span>' +
                    '</div>');
            if ($data.final_price)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.final_price +'</strong></span>' +
                    '</div>');
            if ($data.service_detail)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.service_detail +'</strong></span>' +
                    '</div>');
        }
    });
});