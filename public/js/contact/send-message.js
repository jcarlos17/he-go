// information basic
$(document).on('click', '[data-sendmessage]', function () {
    var urlSendMessage = $(this).data('sendmessage');
    var name = $('#name');
    var email = $('#email');
    var phone = $('#phone');
    var description = $('#description');
    var department_id = $('select[name=department_id]');
    var province_id = $('select[name=province_id]');
    var district_id = $('select[name=district_id]');
    var theme = $('select[name=theme]');
    $('#btnSendMessage').prop('disabled', true);

    $.ajax({
        type: 'POST',
        url: urlSendMessage,
        data:{
            '_token':$('input[name=_token]').val(),
            'name':name.val(),
            'email':email.val(),
            'phone':phone.val(),
            'theme':theme.val(),
            'description':description.val(),
            'department_id':department_id.val(),
            'province_id':province_id.val(),
            'district_id':district_id.val()
        },
        success:function (){
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $alert = $('#alert-sendmessage');
            $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>Se envió correctamente su mensaje</strong></span>' +
                '</div>');
            name.val("");
            email.val("");
            phone.val("");
            theme.val("");
            description.val("");
            department_id.val("");
            province_id.val("");
            district_id.val("");

            $('#btnSendMessage').prop('disabled', false);
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-sendmessage');

            if ($data.name)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.name +'</strong></span>' +
                    '</div>');

            if ($data.email)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.email +'</strong></span>' +
                    '</div>');

            if ($data.phone)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.phone +'</strong></span>' +
                    '</div>');

            if ($data.department_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.department_id +'</strong></span>' +
                    '</div>');

            if ($data.province_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.province_id +'</strong></span>' +
                    '</div>');

            if ($data.theme)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.theme +'</strong></span>' +
                    '</div>');

            if ($data.description)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.description +'</strong></span>' +
                    '</div>');

            $('#btnSendMessage').prop('disabled', false);
        }
    });
});