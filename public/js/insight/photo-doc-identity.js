$(function () {
    var  $formDocIdentity, $inputDocIdentity, $templateDocIdentity, $uploadedDocIdentities;

    $formDocIdentity = $('#formDocIdentity');
    $inputDocIdentity = $('#inputDocIdentity');
    $templateDocIdentity = $('#templateDocIdentity');
    $uploadedDocIdentities = $('#uploadedDocIdentity');

    $inputDocIdentity.on('change', uploadDocIdentities);
    $(document).on('click', '[data-deleteidentity]', removeDocIdentity);

    function uploadDocIdentities() {
        var filesData = $inputDocIdentity.prop("files");
        for (var i=0; i<filesData.length; ++i) {
            postAjaxDocIdenttyData(filesData[i]);
        }
    }
    function postAjaxDocIdenttyData(fileData) {
        var sizeKB = fileData.size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            var $alert = $('#alert-identity');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
        var formData = new FormData();
        formData.append('file', fileData);
        formData.append('_token', $formDocIdentity.find('[name=_token]').val());

        $.ajax({
            url: '/doc-identity/update',
            type: 'POST',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.alert-warning').remove();
                var uploadedIdentity = $templateDocIdentity.html();
                uploadedIdentity = uploadedIdentity.replace('src=""', 'src="'+data.doc_identity_url+'"');
                uploadedIdentity = uploadedIdentity.replace('href=""', 'href="'+data.view_doc_url+'"');
                uploadedIdentity = uploadedIdentity.replace('data-deleteidentity=""', 'data-deleteidentity="/doc-identity/'+data.id+'/delete"');
                $uploadedDocIdentities.append(uploadedIdentity);

                updateProgressBarStatus();
            },
            error: function (data) {
                $('.alert-warning').remove();
                var $data = data.responseJSON;
                var $alert = $('#alert-identity');

                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.file +'</strong></span>' +
                    '</div>');
            }
        });
    }

    function removeDocIdentity() {
        // request al servidor
        var $div = $(this).closest('#itemDocIdentity');
        var urlDeleteIdentity = $(this).data('deleteidentity');
        $.ajax({
            type: 'GET',
            url: urlDeleteIdentity,

            success:function (){
                $('.alert-warning').remove();
                $div.remove();
                updateProgressBarStatus();
            }
        });
    }
});