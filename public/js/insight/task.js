// tasks
$(document).on('click', '[data-task]', function () {
    var urlTask = $(this).data('task');
    var description = $('input[id=new_task]');
    var uploadedTasks = $('#mCSB_3_container');
    var templateTask = $('#templateTask');
    $.ajax({
        type: 'POST',
        url: urlTask,
        data:{
            '_token':$('input[name=_token]').val(),
            'description':description.val()
        },
        success:function (data){
            $('.alert-warning').remove();

            var uploadTask = templateTask.html();
            uploadTask = uploadTask.replace('id=""', 'id="'+data.id+'"');
            uploadTask = uploadTask.replace('value=""', 'value="'+data.id+'"');
            uploadTask = uploadTask.replace('for=""', 'for="'+data.id+'"');
            uploadTask = uploadTask.replace('<span id="checked"></span>', '<span id="checked'+data.id+'">'+data.description+'</span>');
            uploadTask = uploadTask.replace('data-deletetask=""', 'data-deletetask="/tasks/'+data.id+'/delete"');
            uploadedTasks.append(uploadTask);

            $('#new_task').val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-task');
            $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>'+ $data.description +'</strong></span>' +
                '</div>');
        }
    });
});

function save(value, checked) {
    $.ajax({
        type: 'POST',
        url: '/tasks/'+value+'/completed',
        data:{
            '_token':$('input[name=_token]').val(),
            'checked': checked
        },
        success:function (data){
            if (checked === true){
                $('#checked'+data.id).replaceWith('<span id="checked'+data.id+'"><del>'+data.description+'</del></span>');
            }
            else{
                $('#checked'+data.id).replaceWith('<span id="checked'+data.id+'">'+data.description+'</span>');
            }
        },
        error:function () {
        }
    });
}

$(document).on('click', '[data-deletetask]', function () {
    var $li = $(this).closest('#item-task');
    var urlDeleteTask = $(this).data('deletetask');
    $.ajax({
        type: 'GET',
        url: urlDeleteTask,

        success:function (){
            $li.remove();
        }
    });
});