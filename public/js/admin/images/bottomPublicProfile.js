$(function () {
    var $formImage, $inputImage, $uploadedImages, $templateImage;

    $formImage = $('#formBottomPublicProfile');
    $inputImage = $('#inputBottomPublicProfile');
    $uploadedImages = $('#uploadedBottomPublicProfile');
    $templateImage = $('#templateBottomPublicProfile');

    $inputImage.on('change', uploadImages);

    function uploadImages() {
        var fileData = $inputImage.prop("files");

        var url = URL.createObjectURL(fileData[0]);
        var templateGalleryLoading = $('#templateBottomPublicProfileLoading').html();
        templateGalleryLoading = templateGalleryLoading.replace('id=""', 'id="gallery0"');
        templateGalleryLoading = templateGalleryLoading.replace('src=""', 'src="'+url+'"');
        $uploadedImages.append(templateGalleryLoading);

        var sizeKB = fileData[0].size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            $('#gallery0').remove();
            var $alert = $('#alert-gallery');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
        var formData = new FormData();
        formData.append('image', fileData[0]);
        formData.append('type', $formImage.find('[name=type]').val());
        formData.append('_token', $formImage.find('[name=_token]').val());

        $.ajax({
            url: '/admin/images/add',
            type: 'POST',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.alert-warning').remove();
                $('#gallery0').remove();
                $('#itemBottomPublicProfile').remove();
                var uploadedImage = $templateImage.html();
                uploadedImage = uploadedImage.replace('src=""', 'src="'+data.image_url+'"');
                uploadedImage = uploadedImage.replace('alt=""', 'value="'+data.type+'"');
                uploadedImage = uploadedImage.replace('data-delete-image=""', 'data-delete-image="/admin/images/'+data.id+'/delete"');
                $uploadedImages.append(uploadedImage);
            },
            error: function (data) {
                $('.alert-warning').remove();
                $('#gallery0').remove();
                var $data = data.responseJSON;
                var $alert = $('#alertImgBottomPublicProfile');

                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.image +'</strong></span>' +
                    '</div>');
            }
        });

    }
});