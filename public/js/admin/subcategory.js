// Category
$(document).on('click', '[data-category]', function () {
    var fileData = $('#inputCategory').prop("files");

    if (fileData[0]){
        var sizeKB = fileData[0].size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            var $alert = $('#alert-category');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
    }
    var urlCategory = $(this).data('category');
    var $formCategory = $('#formCategory');
    var name = $('#name');
    var slug = $('#slug');
    var icon_id = $('input[name=icon_id]:checked');
    var formData = new FormData();
    formData.append('_token', $formCategory.find('[name=_token]').val());
    formData.append('name', name.val());
    formData.append('slug', slug.val());
    formData.append('image', fileData[0]);
    formData.append('icon_id', icon_id.val());

    $.ajax({
        url: urlCategory,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            var uploadedCertificate = $('#templateCategory').html();
            uploadedCertificate = uploadedCertificate.replace('<figure id=""><a href="#"><img src="" style="height: 40px"></a></figure>', '<figure id="ImageUrlText'+data.id+'"><a href="#"><img src="'+data.image_url+'" style="height: 40px"></a></figure>');
            uploadedCertificate = uploadedCertificate.replace('<h3 id=""><a href="javascript:void(0);"></a></h3>', '<h3 id="nameCategoryText'+data.id+'"><a href="javascript:void(0);">'+data.name+'</a></h3>');
            uploadedCertificate = uploadedCertificate.replace('<span id=""></span>', '<span id="iconText'+data.id+'"><i class="'+data.icon_code+'"></i></span>');
            uploadedCertificate = uploadedCertificate.replace('data-show=""', 'data-show="/admin/categories/subcategories/'+data.id+'/show"');
            uploadedCertificate = uploadedCertificate.replace('data-delete=""', 'data-delete="/admin/categories/subcategories/'+data.id+'/delete"');
            $('#uploadedCategories').append(uploadedCertificate);
            $('#categoryModal').modal('hide');
            name.val("");
            slug.val("");
            icon_id.prop('checked',false);
            $('.tg-uploading').remove();
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-category');

            if ($data.name)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.name +'</strong></span>' +
                    '</div>');
            if ($data.slug)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.slug +'</strong></span>' +
                    '</div>');
            if ($data.image)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.image +'</strong></span>' +
                    '</div>');
            if ($data.icon_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.icon_id +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-show]', function () {
    // request al servidor
    var urlShowCertificate = $(this).data('show');
    $.ajax({
        type: 'GET',
        url: urlShowCertificate,

        success:function (data){
            document.getElementById('nameEdit').value=data.name;
            document.getElementById('slugEdit').value=data.slug;
            $('#edit'+data.icon_id).prop('checked', true);
            $('#imageView').replaceWith('<img id="imageView" src="'+data.image_url+'" alt="'+data.name+'" style="height: 80px">');
            $('#btnCategoryEdit').data('edit', '/admin/categories/subcategories/'+data.id+'/update');
            $('#galleryEdit').removeClass("tg-uploading");
            $('#loaderEdit').replaceWith('<span id="loaderEdit"></span>');
            $('#inputImgEdit').val('');

            $('#categoryEditModal').modal('show');
        }
    });

});

$(document).on('click', '[data-edit]', function () {
    var fileData = $('#inputImgEdit').prop("files");

    if (fileData[0]){
        var sizeKB = fileData[0].size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            var $alert = $('#alert-category-edit');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
    }
    var urlCategoryEdit = $(this).data('edit');
    var $formCategory = $('#formEditCategory');
    var name = $('#nameEdit');
    var slug = $('#slugEdit');
    var icon_id = $('input[name=iconIdEdit]:checked');
    var formData = new FormData();
    formData.append('_token', $formCategory.find('[name=_token]').val());
    formData.append('name', name.val());
    formData.append('slug', slug.val());
    formData.append('image', fileData[0]);
    formData.append('icon_id', icon_id.val());

    $.ajax({
        url: urlCategoryEdit,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            $('#ImageUrlText'+data.id).replaceWith('<figure id="ImageUrlText'+data.id+'"><a href="#"><img src="'+data.image_url+'" style="height: 40px"></a></figure>');
            $('#nameCategoryText'+data.id).replaceWith('<h3 id="nameCategoryText'+data.id+'"><a href="javascript:void(0);">'+data.name+'</a></h3>');
            $('#iconText'+data.id).replaceWith('<span id="iconText'+data.id+'"><i class="'+data.icon_code+'"></i></span>');
            $('.alert-warning').remove();
            $('#categoryEditModal').modal('hide');
            name.val("");
            slug.val("");
            icon_id.prop('checked',false);
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-category-edit');

            if ($data.name)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.name +'</strong></span>' +
                    '</div>');
            if ($data.slug)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.slug +'</strong></span>' +
                    '</div>');
            if ($data.image)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.image +'</strong></span>' +
                    '</div>');
            if ($data.icon_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.icon_id +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-delete]', function () {
    // request al servidor
    var $div = $(this).closest('.tg-certificatesaward');
    var urlDelete = $(this).data('delete');
    $.ajax({
        type: 'GET',
        url: urlDelete,

        success:function (){
            $div.remove();
        }
    });

});