// Service
$(document).on('click', '[data-service]', function () {
    var urlService,token, title, type_value, price, description, appointment;

    urlService = $(this).data('service');
    token = $('input[name=_token]');
    title = $('#title');
    type_value = $('#type_value');
    price = $('#price');
    description = $('#description');
    appointment = $('#appointment:checked');

    var formData = new FormData();
    formData.append('_token', token.val());
    formData.append('title', title.val());
    formData.append('type_value', type_value.val());
    formData.append('price', price.val());
    formData.append('description', description.val());
    formData.append('appointment', appointment.val());

    $.ajax({
        url: urlService,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            var uploadedService = $('#templateService').html();
            uploadedService = uploadedService.replace('<h2 id=""><a href="javascript:void(0);"></a> <span></span></h2>', '<h2 id="titleText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a> <span>'+data.by_appointment+'</span></h2>');
            uploadedService = uploadedService.replace('<span id=""></span>', '<span id="priceText'+data.id+'">'+data.price_value+'</span>');
            uploadedService = uploadedService.replace('data-showservice=""', 'data-showservice="/service/'+data.id+'/show"');
            uploadedService = uploadedService.replace('data-delete=""', 'data-delete="/services/'+data.id+'/delete"');
            $('#uploadedService').append(uploadedService);
            $('#serviceModal').modal('hide');
            $('.alert-warning').remove();
            title.val("");
            description.val("");
            type_value.val("");
            price.val("");
            appointment.removeAttr('checked');
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-service');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.description)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.description +'</strong></span>' +
                    '</div>');

            if ($data.type_value)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.type_value +'</strong></span>' +
                    '</div>');

            if ($data.price)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.price +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-showservice]', function () {
    // request al servidor
    var urlShowService = $(this).data('showservice');
    $.ajax({
        type: 'GET',
        url: urlShowService,

        success:function (data){
            document.getElementById('titleEdit').value=data.title;
            document.getElementById('typeValueEdit').value=data.type_value;
            if(data.type_value === 'G')
                $("#NEdit").css({ display: "none" });
            else
                $("#NEdit").css("display", "");

            document.getElementById('priceEdit').value=data.price;
            document.getElementById('descriptionEdit').value=data.description;
            $('#btnEdit').data('edit', '/services/'+data.id+'/update');
            if (data.appointment === 0)
                $('#appointmentEdit').removeAttr('checked');

            $('#serviceEditModal').modal('show');
        }
    });

});

$(document).on('click', '[data-edit]', function () {
    var urlEditService,token, title, type_value, price, description, appointment;

    urlEditService = $(this).data('edit');
    token = $('input[name=_token]');
    title = $('#titleEdit');
    type_value = $('#typeValueEdit');
    price = $('#priceEdit');
    description = $('#descriptionEdit');
    appointment = $('#appointmentEdit:checked');

    var formData = new FormData();
    formData.append('_token', token.val());
    formData.append('title', title.val());
    formData.append('type_value', type_value.val());
    formData.append('price', price.val());
    formData.append('description', description.val());
    formData.append('appointment', appointment.val());

    $.ajax({
        url: urlEditService,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            $('#titleText'+data.id).replaceWith('<h2 id="titleText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a> <span>'+data.by_appointment+'</span></h2>');
            $('#priceText'+data.id).replaceWith('<span id="priceText'+data.id+'">'+data.price_value+'</span>');
            $('#serviceEditModal').modal('hide');
            $('.alert-warning').remove();
            title.val("");
            description.val("");
            type_value.val("");
            price.val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-service');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.description)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.description +'</strong></span>' +
                    '</div>');

            if ($data.type_value)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.type_value +'</strong></span>' +
                    '</div>');

            if ($data.price)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.price +'</strong></span>' +
                    '</div>');
        }
    });
});


$(document).on('click', '[data-delete]', function () {
    // request al servidor
    var $div = $(this).closest('#item-service');
    var urlDeleteService = $(this).data('delete');
    $.ajax({
        type: 'GET',
        url: urlDeleteService,

        success:function (){
            $div.remove();
        }
    });

});