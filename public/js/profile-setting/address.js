// address
$(document).on('click', '[data-address]', function () {
    var urlAddress = $(this).data('address');

    $.ajax({
        type: 'POST',
        url: urlAddress,
        data:{
            '_token':$('input[name=_token]').val(),
            'department_id':$('select[name=department_id]').val(),
            'province_id':$('select[name=province_id]').val(),
            'district_id':$('select[name=district_id]').val(),
            'description':$('input[name=description]').val()
        },
        success:function (data){
            var $languageList = $('#add-address');

            $languageList.append('<li>' +
                '<span class="tg-tagdashboard">' +
                '<a data-deleteaddress="/address/'+ data.id +'/delete"><i class="fa fa-close"></i></a>' +
                '<em>'+ data.description_complete +'</em>' +
                '</span>' +
                '</li>');

            $('#select_department').val("");
            $('#select_province').val("");
            $('#select_district').val("");
            $('#description').val("");
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-address');

            if ($data.department_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.department_id +'</strong></span>' +
                    '</div>');

            if ($data.province_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.province_id +'</strong></span>' +
                    '</div>');

            if ($data.description)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.description +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-deleteaddress]', function () {
    var $li=$(this).parent().parent();
    var urlDeleteAddress = $(this).data('deleteaddress');
    $.ajax({
        type: 'GET',
        url: urlDeleteAddress,

        success:function (data){
            $li.remove();
        }
    });
});