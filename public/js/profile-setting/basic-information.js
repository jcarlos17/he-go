// information basic
$(document).on('click', '[data-updateinformation]', function () {
    var urlUpdateInformation = $(this).data('updateinformation');

    $.ajax({
        type: 'POST',
        url: urlUpdateInformation,
        data:{
            '_token':$('input[name=_token]').val(),
            'name':$('input[name=name]').val(),
            'last_name':$('input[name=last_name]').val(),
            'phone':$('input[name=phone]').val(),
            'mobile':$('input[name=mobile]').val(),
            'company_name':$('input[name=company_name]').val(),
            'email':$('input[name=email]').val(),
            'website':$('input[name=website]').val(),
            'identity_type':$('select[name=identity_type]').val(),
            'identity_document':$('input[name=identity_document]').val()
        },
        success:function (data){
            $('.alert-success').remove();
            $('.alert-warning').remove();

            var $alert = $('#alert-basicinformation');

            $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>Se guardó correctamente los cambios</strong></span>' +
                '</div>');

            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            console.log($data);
            var $alert = $('#alert-basicinformation');

            if ($data.identity_document)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.identity_document +'</strong></span>' +
                    '</div>');

            if ($data.identity_type)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.identity_type +'</strong></span>' +
                    '</div>');

            if ($data.website)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.website +'</strong></span>' +
                    '</div>');
            if ($data.email)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.email +'</strong></span>' +
                    '</div>');
        }
    });
});