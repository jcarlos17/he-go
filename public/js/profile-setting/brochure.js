$(function () {
    var $formBrochure, $inputBrochure, $inputId;
    var $uploadedBrochures, $templateBrochure;

    $formBrochure = $('#formBrochure');
    $inputBrochure = $('#inputBrochure');
    $inputId = $('#inputUserId');
    $uploadedBrochures = $('#uploadedBrochures');
    $templateBrochure = $('#templateBrochure');

    $inputBrochure.on('change', uploadBrochures);
    $(document).on('click', '[data-deletebrochure]', removeBrochure);

    function uploadBrochures() {
        var filesData = $inputBrochure.prop("files");
        for (var i=0; i<filesData.length; ++i) {
            var url = URL.createObjectURL(filesData[i]);
            var name = filesData[i].name;
            var extArray = name.split(".");
            var ext = extArray[1].toLowerCase();
            var templateBrochureLoading = $('#templateBrochureLoading').html();
            templateBrochureLoading = templateBrochureLoading.replace('id=""', 'id="brochure'+i+'"');
            if (ext === 'pdf')
                templateBrochureLoading = templateBrochureLoading.replace('src=""', 'src="/images/brochureFiles/icono_pdf.png"');
            if (ext === 'docx' || ext === 'doc')
                templateBrochureLoading = templateBrochureLoading.replace('src=""', 'src="/images/brochureFiles/icono_word.png"');
            if (ext === 'jpg' || ext === 'jpeg' || ext === 'png' || ext === 'svg')
                templateBrochureLoading = templateBrochureLoading.replace('src=""', 'src="'+url+'"');

            $('#uploadedBrochures').append(templateBrochureLoading);
            postAjaxBrochureData(filesData[i], i);
        }
    }

    function postAjaxBrochureData(fileData, i) {
        var userId = $inputId.val();
        var sizeKB = fileData.size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            $('#brochure'+i).remove();
            var $alert = $('#alert-brochure');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
        var formData = new FormData();
        formData.append('file', fileData);
        formData.append('_token', $formBrochure.find('[name=_token]').val());

        $.ajax({
            url: '/brochures/'+userId+'/add',
            type: 'POST',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.alert-warning').remove();
                $('#brochure'+i).remove();
                var uploadedBrochure = $templateBrochure.html();
                uploadedBrochure = uploadedBrochure.replace('src=""', 'src="'+data.brochure_url+'"');
                uploadedBrochure = uploadedBrochure.replace('href=""', 'href="/brochures/'+data.view_url+'"');
                uploadedBrochure = uploadedBrochure.replace('data-deletebrochure=""', 'data-deletebrochure="/brochures/'+data.id+'/delete"');
                $uploadedBrochures.append(uploadedBrochure);

                updateProgressBarStatus();
            },
            error: function (data) {
                $('.alert-warning').remove();
                $('#brochure'+i).remove();
                var $data = data.responseJSON;
                var $alert = $('#alert-brochure');

                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.file +'</strong></span>' +
                    '</div>');
            }
        });
    }

    function removeBrochure() {
        // request al servidor
        var $div = $(this).closest('#itemBrochure');
        var urlDeleteBrochure = $(this).data('deletebrochure');
        $.ajax({
            type: 'GET',
            url: urlDeleteBrochure,

            success:function (){
                $div.remove();

                updateProgressBarStatus();
            }
        });

    }
});