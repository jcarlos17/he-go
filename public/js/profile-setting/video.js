// address
$(document).on('click', '[data-video]', function () {
    var urlVideo = $(this).data('video');

    $.ajax({
        type: 'POST',
        url: urlVideo,
        data:{
            '_token':$('input[name=_token]').val(),
            'link':$('input[name=link]').val()
        },
        success:function (data){
            var $languageList = $('#add-video');

            $languageList.append('<li>' +
                '<div class="tg-videobox">' +
                '<span class="tg-tagdashboard">'+
                '<a data-deleteaddress="/videos/'+ data.id +'/delete"><i class="fa fa-close"></i></a>' +
                '<figure>' +
                '<a href="'+ data.link +'" target="_blank">' +
                '<img src="http://mydesignhoard.com/HTML/html/service_provider/images/placeholder/img-03.jpg">' +
                '</a>' +
                '</figure>' +
                '</span>' +
                '</div>' +
                '</li>');

            $('#link').val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-video');
            $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>'+ $data.link[0] +'</strong></span>' +
                '</div>');
        }
    });
});

$(document).on('click', '[data-deletevideo]', function () {
    var $li=$(this).parent().parent().parent();
    var urlDeleteAddress = $(this).data('deletevideo');
    $.ajax({
        type: 'GET',
        url: urlDeleteAddress,

        success:function (){
            $li.remove();
        }
    });
});