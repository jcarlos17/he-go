// category
$(document).on('click', '[data-category]', function () {
    var urlCategory = $(this).data('category');
    $.ajax({
        type: 'POST',
        // dataType: 'json',
        url: urlCategory,
        data:{
            '_token':$('input[name=_token]').val(),
            'category_id':$('select[name=category_id]').val()
        },
        success:function (data){
            $('.alert-warning').remove();
            var $categoryList = $('#container-categories');

            var uploadedCategory = $('#templateNewCategory').html();
            uploadedCategory = uploadedCategory.replace('id="category"', 'id="category'+data.category_id+'"');
            uploadedCategory = uploadedCategory.replace('<span></span>', '<span>'+data.category_name+'</span>');
            uploadedCategory = uploadedCategory.replace('id="select-subcategory"', 'id="select-subcategory'+data.category_id+'"');
            uploadedCategory = uploadedCategory.replace('data-deletecategory=""', 'data-deletecategory="/categories/'+data.category_id+'/delete"');
            uploadedCategory = uploadedCategory.replace('data-id=""', 'data-id="'+data.category_id+'"');
            uploadedCategory = uploadedCategory.replace('data-subcategory=""', 'data-subcategory="/categories/subcat/'+data.user_id+'/add"');
            uploadedCategory = uploadedCategory.replace('id="list-subcategory"', 'id="list-subcategory'+data.category_id+'"');
            $categoryList.append(uploadedCategory);

            showSubcategories(data.category_id);

            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-category');
            if ($data.category_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.category_id +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-deletecategory]', function () {
    var $li=$(this).parent().parent();
    var urlDelete = $(this).data('deletecategory');
    $.ajax({
        type: 'GET',
        url: urlDelete,

        success:function (data){
            var $alert = $('#alert-category');
            if (data.success){
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ data.message +'</strong></span>' +
                    '</div>');
            }
            else {
                $li.remove();
                updateProgressBarStatus();
            }
        }
    });
});

// subcategory
$(document).on('click', '[data-subcategory]', function () {
    var urlCategory = $(this).data('subcategory');
    var id = $(this).data('id');

    var subcategory_id = $('#category'+id+' select[name=subcategory_id]');
    $.ajax({
        type: 'POST',
        // dataType: 'json',
        url: urlCategory,
        data:{
            '_token':$('input[name=_token]').val(),
            'category_id':subcategory_id.val()
        },
        success:function (data){
            $('.alert-warning').remove();
            var $categoryList = $('#list-subcategory'+data.r_category_id);

            $categoryList.append('<li>' +
                '<span class="tg-tagdashboard">' +
                '<a data-deletesubcategory="/categories/subcat/'+ data.category_id +'/delete"><i class="fa fa-close"></i></a>' +
                '<em>'+ data.category_name +'</em>' +
                '</span>' +
                '</li>');

            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-category');
            if ($data.category_id)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.category_id +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-deletesubcategory]', function () {
    var $li=$(this).parent().parent();
    var urlDelete = $(this).data('deletesubcategory');
    $.ajax({
        type: 'GET',
        url: urlDelete,

        success:function (data){
            $li.remove();
        }
    });
});

function showSubcategories(id) {
    // AJAX
    $.get('/api/category/'+id+'/subcategories', function (data) {
        var html_select = '<option value="">Seleccione subcategoría</option>';
        for (var i=0; i<data.length; ++i)
            html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';

        $('#select-subcategory'+id).html(html_select);
    });
}