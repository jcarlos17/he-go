// information basic
$(document).on('click', '[data-updatepresentation]', function () {
    var urlUpdatePresentation = $(this).data('updatepresentation');

    $.ajax({
        type: 'POST',
        url: urlUpdatePresentation,
        data:{
            '_token':$('input[name=_token]').val(),
            'presentation':$('textarea[name=presentation]').val()
        },
        success:function (){
            $('.alert-success').remove();
            $('.alert-warning').remove();

            var $alert = $('#alert-presentation');

            $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>Se guardó correctamente los cambios</strong></span>' +
                '</div>');

            updateProgressBarStatus();
        },
        error:function (data, response) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            console.log($data);
            var $alert = $('#alert-presentation');
            $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>'+ $data.presentation +'</strong></span>' +
                '</div>');
        }
    });
});