// Qualification
$(document).on('click', '[data-qualification]', function () {
    var urlQualification = $(this).data('qualification');

    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('title', $('input[id=title-qualification]').val());
    formData.append('institution', $('input[id=institution-qualification]').val());
    formData.append('start_date', $('input[id=start_date]').val());
    formData.append('end_date', $('input[id=end_date]').val());

    $.ajax({
        url: urlQualification,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            console.log(data);
            var uploadedQualification = $('#templateQualification').html();
            uploadedQualification = uploadedQualification.replace('<h3 id=""><a href="javascript:void(0);"></a></h3><br>', '<h3 id="titleQualificationText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a></h3><br>');
            uploadedQualification = uploadedQualification.replace('<h3 id="" class="text-muted"></h3>', '<h3 id="institutionQualificationText'+data.id+'" class="text-muted">'+data.institution+'</h3>');
            uploadedQualification = uploadedQualification.replace('<span id=""></span>', '<span id="dateIntervalQualificationText'+data.id+'">'+data.date_interval+'</span>');
            uploadedQualification = uploadedQualification.replace('data-showqualification=""', 'data-showqualification="/qualification/'+data.id+'/show"');
            uploadedQualification = uploadedQualification.replace('data-deletequalification=""', 'data-deletequalification="/qualifications/'+data.id+'/delete"');
            $('#uploadedQualification').append(uploadedQualification);
            $('#qualificationModal').modal('hide');
            $('.alert-warning').remove();
            $('#title-qualification').val("");
            $('#institution-qualification').val("");
            $('#start_date').val("");
            $('#end_date').val("");
            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            console.log($data);
            var $alert = $('#alert-qualification');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.institution)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.institution +'</strong></span>' +
                    '</div>');

            if ($data.start_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.start_date +'</strong></span>' +
                    '</div>');

            if ($data.end_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.end_date +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-showqualification]', function () {
    // request al servidor
    var urlShowQualification = $(this).data('showqualification');
    $.ajax({
        type: 'GET',
        url: urlShowQualification,

        success:function (data){
            document.getElementById('titleQualificationEdit').value=data.title;
            document.getElementById('institutionQualificationEdit').value=data.institution;
            document.getElementById('startDateQualificationEdit').value=data.start_date_format;
            document.getElementById('endDateQualificationEdir').value=data.end_date_format;
            $('#btnQualificationEdit').data('editqualification', '/qualifications/'+data.id+'/update');

            $('#qualificationEditModal').modal('show');
        }
    });

});

$(document).on('click', '[data-editqualification]', function () {
    var urlEditExperience,token, title, institution, startDate, endDate;
    urlEditExperience = $(this).data('editqualification');
    token = $('input[name=_token]');
    title = $('#titleQualificationEdit');
    institution = $('#institutionQualificationEdit');
    startDate = $('#startDateQualificationEdit');
    endDate = $('#endDateQualificationEdir');

    var formData = new FormData();
    formData.append('_token', token.val());
    formData.append('title', title.val());
    formData.append('institution', institution.val());
    formData.append('start_date', startDate.val());
    formData.append('end_date', endDate.val());

    $.ajax({
        url: urlEditExperience,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            $('#titleQualificationText'+data.id).replaceWith('<h3 id="titleQualificationText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a></h3>');
            $('#institutionQualificationText'+data.id).replaceWith('<h3 id="institutionQualificationText'+data.id+'" class="text-muted">'+data.institution+'</h3>');
            $('#dateIntervalQualificationText'+data.id).replaceWith('<span id="dateIntervalQualificationText'+data.id+'">'+data.date_interval+'</span>');
            $('#qualificationEditModal').modal('hide');
            $('.alert-warning').remove();
            title.val("");
            institution.val("");
            startDate.val("");
            endDate.val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-qualification-edit');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.institution)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.company +'</strong></span>' +
                    '</div>');

            if ($data.start_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.start_date +'</strong></span>' +
                    '</div>');

            if ($data.end_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.end_date +'</strong></span>' +
                    '</div>');
        }
    });
});


$(document).on('click', '[data-deletequalification]', function () {
    // request al servidor
    var $div = $(this).closest('#new-qualification');
    var urlDeleteQualification = $(this).data('deletequalification');
    $.ajax({
        type: 'GET',
        url: urlDeleteQualification,

        success:function (){
            $div.remove();

            updateProgressBarStatus();
        }
    });

});