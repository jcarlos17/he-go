// languages
$(document).on('click', '[data-language]', function () {
    var urlCreate = $(this).data('language');
    $.ajax({
        type: 'POST',
        // dataType: 'json',
        url: urlCreate,
        data:{
            '_token':$('input[name=_token]').val(),
            'language':$('select[name=language]').val()
        },
        success:function (data){
            var $languageList = $('#add-language');

            $languageList.append('<li>' +
                '<span class="tg-tagdashboard">' +
                '<a data-delete="/languages/'+ data.id +'/delete"><i class="fa fa-close"></i></a>' +
                '<em>'+ data.language +'</em>' +
                '</span>' +
                '</li>');

            $('#select_language').val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;

            var $alert = $('#alert-language');
            $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>'+ $data.language[0] +'</strong></span>' +
                '</div>');
        }
    });
});

$(document).on('click', '[data-delete]', function () {
    var $li=$(this).parent().parent();
    var urlDelete = $(this).data('delete');
    $.ajax({
        type: 'GET',
        url: urlDelete,

        success:function (data){
            $li.remove();
        }
    });
});