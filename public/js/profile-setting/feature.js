// information basic
$(document).on('click', '[data-feature]', function () {
    var urlFeature = $(this).data('feature');
    var serializedServices = $('.services:checked').serialize();
    console.log(serializedServices);
    $.ajax({
        type: 'POST',
        url: urlFeature+'?'+serializedServices,
        data:{
            '_token':$('input[name=_token]').val()
        },
        success:function (){
            $('.alert-success').remove();

            var $alert = $('#alert-feature');

            $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>Se guardó correctamente los cambios</strong></span>' +
                '</div>');
        },
        error:function (data) {
            alert('error al cargar');
        }
    });
});