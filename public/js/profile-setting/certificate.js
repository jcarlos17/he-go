// Certificate
$(document).on('click', '[data-certificate]', function () {
    var fileData = $('#inputCertificate').prop("files");
    var sizeKB = fileData[0].size / 1024;
    if (sizeKB > 4096){
        $('.alert-warning').remove();
        var $alert = $('#alert-certificate');

        return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
            '<i class="lnr lnr-bug"></i>' +
            '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
            '</div>');
    }
    var $formImage = $('#formImage');
    var urlCertificate = $(this).data('certificate');
    var formData = new FormData();
    formData.append('_token', $formImage.find('[name=_token]').val());
    formData.append('title', $('input[id=new-title]').val());
    formData.append('description', $('textarea[id=new-description]').val());
    formData.append('file', fileData[0]);

    $.ajax({
        url: urlCertificate,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            var uploadedCertificate = $('#templateCertificate').html();
            uploadedCertificate = uploadedCertificate.replace('<figure id=""><a href="#"><img src="" align=""></a></figure>', '<figure id="ImageUrlText'+data.id+'"><a href="#"><img src="'+data.image_url+'" align=""></a></figure>');
            uploadedCertificate = uploadedCertificate.replace('<h3 id=""><a href="javascript:void(0);"></a></h3>', '<h3 id="titleCertificateText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a></h3>');
            uploadedCertificate = uploadedCertificate.replace('<span id=""></span>', '<span id="dateCertificateText'+data.id+'">'+data.date+'</span>');
            uploadedCertificate = uploadedCertificate.replace('data-showcertificate=""', 'data-showcertificate="/certificate/'+data.id+'/show"');
            uploadedCertificate = uploadedCertificate.replace('data-deletecertificate=""', 'data-deletecertificate="/certificates/'+data.id+'/delete"');
            $('#uploadedCertificates').append(uploadedCertificate);
            $('#certificateModal').modal('hide');
            $('#new-title').val("");
            $('#new-description').val("");

            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-certificate');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.file)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.file +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-showcertificate]', function () {
    // request al servidor
    var urlShowCertificate = $(this).data('showcertificate');
    $.ajax({
        type: 'GET',
        url: urlShowCertificate,

        success:function (data){
            document.getElementById('titleCertificateEdit').value=data.title;
            document.getElementById('descriptionCertificateEdit').value=data.description;
            $('#imageViewCertificate').replaceWith('<img id="imageViewCertificate" src="'+data.image_url+'" alt="'+data.title+'" style="height: 80px">');
            $('#btnCertificateEdit').data('editcertificate', '/certificates/'+data.id+'/update');

            $('#certificateEditModal').modal('show');
        }
    });

});

$(document).on('click', '[data-editcertificate]', function () {
    var fileData = $('#inputCertificateImgEdit').prop("files");
    var sizeKB = fileData[0].size / 1024;
    if (sizeKB > 4096){
        $('.alert-warning').remove();
        var $alert = $('#alert-certificate');

        return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
            '<i class="lnr lnr-bug"></i>' +
            '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
            '</div>');
    }

    var $formImage = $('#formImage');
    var urlEditCertificate = $(this).data('editcertificate');
    var title = $('input[id=titleCertificateEdit]');
    var description = $('textarea[id=descriptionCertificateEdit]');
    var formData = new FormData();
    formData.append('_token', $formImage.find('[name=_token]').val());
    formData.append('title', title.val());
    formData.append('description', description.val());
    formData.append('file', fileData[0]);
    $.ajax({
        url: urlEditCertificate,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            $('#ImageUrlText'+data.id).replaceWith('<figure id="ImageUrlText'+data.id+'"><a href="#"><img src="'+data.image_url+'" align=""></a></figure>');
            $('#titleCertificateText'+data.id).replaceWith('<h3 id="titleCertificateText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a></h3>');
            $('#dateCertificateText'+data.id).replaceWith('<span id="dateCertificateText'+data.id+'">'+data.date+'</span>');
            $('.alert-warning').remove();
            $('#certificateEditModal').modal('hide');
            title.val("");
            description.val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-certificate-edit');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.image)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.image +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-deletecertificate]', function () {
    // request al servidor
    var $div = $(this).closest('.tg-certificatesaward');
    var urlDeleteCertificate = $(this).data('deletecertificate');
    $.ajax({
        type: 'GET',
        url: urlDeleteCertificate,

        success:function (){
            $div.remove();

            updateProgressBarStatus();
        }
    });

});