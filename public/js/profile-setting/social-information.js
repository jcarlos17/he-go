// information basic
$(document).on('click', '[data-updatesocial]', function () {
    var urlUpdateSocial = $(this).data('updatesocial');

    $.ajax({
        type: 'POST',
        url: urlUpdateSocial,
        data:{
            '_token':$('input[name=_token]').val(),
            'facebooklink':$('input[name=facebooklink]').val(),
            'twitterlink':$('input[name=twitterlink]').val(),
            'linkedinlink':$('input[name=linkedinlink]').val(),
            'skypelink':$('input[name=skypelink]').val(),
            'googlepluslink':$('input[name=googlepluslink]').val(),
            'pinterestlink':$('input[name=pinterestlink]').val(),
        },
        success:function (data){
            $('.alert-success').remove();
            $('.alert-danger').remove();

            var $alert = $('#alert-socialinformation');

            $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-star"></i>' +
                '<span><strong>Se guardó correctamente los cambios</strong></span>' +
                '</div>');

            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-success').remove();
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            console.log($data);
            var $alert = $('#alert-socialinformation');

            if ($data.facebooklink)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.facebooklink +'</strong></span>' +
                    '</div>');

            if ($data.twitterlink)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.twitterlink +'</strong></span>' +
                    '</div>');

            if ($data.linkedinlink)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.linkedinlink +'</strong></span>' +
                    '</div>');

            if ($data.twitterlink)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.twitterlink +'</strong></span>' +
                    '</div>');

            if ($data.googlepluslink)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.googlepluslink +'</strong></span>' +
                    '</div>');

            if ($data.pinterestlink)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.pinterestlink +'</strong></span>' +
                    '</div>');
        }
    });
});