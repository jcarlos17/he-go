$(function () {
    var $formImage, $inputImage, $inputId;
    var $uploadedImages, $templateImage;

    $formImage = $('#formImage');
    $inputImage = $('#inputImage');
    $inputId = $('#inputId');
    $uploadedImages = $('#uploadedImages');
    $templateImage = $('#templateImage');

    $inputImage.on('change', uploadImages);
    $(document).on('click', '[data-deletegallery]', removeImage);

    function uploadImages() {
        var filesData = $inputImage.prop("files");
        for (var i=0; i<filesData.length; ++i) {
            var url = URL.createObjectURL(filesData[i]);
            var templateGalleryLoading = $('#templateImageLoading').html();
            templateGalleryLoading = templateGalleryLoading.replace('id=""', 'id="gallery'+i+'"');
            templateGalleryLoading = templateGalleryLoading.replace('src=""', 'src="'+url+'"');
            $('#uploadedImages').append(templateGalleryLoading);

            postAjaxImageData(filesData[i], i);
        }
    }

    function postAjaxImageData(fileData, i) {
        var sizeKB = fileData.size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            $('#gallery'+i).remove();
            var $alert = $('#alert-gallery');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
        var userId = $inputId.val();
        var formData = new FormData();
        formData.append('file', fileData);
        formData.append('_token', $formImage.find('[name=_token]').val());

        $.ajax({
            url: '/galleries/'+userId+'/add',
            type: 'POST',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.alert-warning').remove();
                $('#gallery'+i).remove();
                var uploadedImage = $templateImage.html();
                uploadedImage = uploadedImage.replace('src=""', 'src="'+data.image_url+'"');
                uploadedImage = uploadedImage.replace('value=""', 'value="'+data.id+'"');
                uploadedImage = uploadedImage.replace('data-deletegallery=""', 'data-deletegallery="/galleries/'+data.id+'/delete"');
                $uploadedImages.append(uploadedImage);

                updateProgressBarStatus();
            },
            error: function (data) {
                $('.alert-warning').remove();
                $('#gallery'+i).remove();
                var $data = data.responseJSON;
                var $alert = $('#alert-gallery');

                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.file +'</strong></span>' +
                    '</div>');
            }
        });
    }

    function removeImage() {
        // request al servidor
        var $div = $(this).closest('.tg-galleryimg');
        var urlDeleteGallery = $(this).data('deletegallery');
        $.ajax({
            type: 'GET',
            url: urlDeleteGallery,

            success:function (){
                $div.remove();
                updateProgressBarStatus();
            }
        });
        // y una vez que confirma
    }
});