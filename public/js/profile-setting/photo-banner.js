$(function () {
    var  $formBanner, $inputBanner, $menuBanner, $templateBanner, $uploadedBanners;

    $formBanner = $('#formBanner');
    $inputBanner = $('#inputBanner');
    $menuBanner = $('#menuBanner');
    $templateBanner = $('#templateBanner');
    $uploadedBanners = $('#uploadedBanners');

    $inputBanner.on('change', uploadBanners);
    $(document).on('click', '[data-selectbanner]', selectBanner);
    $(document).on('click', '[data-deletebanner]', removeBanner);

    function uploadBanners() {
        var filesData = $inputBanner.prop("files");
        for (var i=0; i<filesData.length; ++i) {
            var url = URL.createObjectURL(filesData[i]);
            var templateBannerLoading = $('#templateBannerLoading').html();
            templateBannerLoading = templateBannerLoading.replace('id=""', 'id="banner'+i+'"');
            templateBannerLoading = templateBannerLoading.replace('src=""', 'src="'+url+'"');
            $('#uploadedBanners').append(templateBannerLoading);

            postAjaxBannerData(filesData[i], i);
        }
    }
    function postAjaxBannerData(fileData, i) {
        var sizeKB = fileData.size / 1024;
        if (sizeKB > 4096){
            $('.alert-warning').remove();
            $('#banner'+i).remove();
            var $alert = $('#alert-banner');

            return $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                '<i class="lnr lnr-bug"></i>' +
                '<span><strong>El tamaño del archivo debe ser máximo 4MB</strong></span>' +
                '</div>');
        }
        var formData = new FormData();
        formData.append('image', fileData);
        formData.append('_token', $formBanner.find('[name=_token]').val());

        $.ajax({
            url: '/banner/update',
            type: 'POST',
            data: formData,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $('.alert-warning').remove();
                $('#banner'+i).remove();
                var uploadedBanner = $templateBanner.html();
                uploadedBanner = uploadedBanner.replace('src=""', 'src="'+data.banner_url+'"');
                uploadedBanner = uploadedBanner.replace('data-selectbanner=""', 'data-selectbanner="/banner/'+data.id+'/select"');
                uploadedBanner = uploadedBanner.replace('data-deletebanner=""', 'data-deletebanner="/banner/'+data.id+'/delete"');
                $uploadedBanners.append(uploadedBanner);

                updateProgressBarStatus();
            },
            error: function (data) {
                $('.alert-warning').remove();
                $('#banner'+i).remove();
                var $data = data.responseJSON;
                var $alert = $('#alert-banner');

                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.image +'</strong></span>' +
                    '</div>');
            }
        });
    }

    function selectBanner() {
        var urlSelectBanner = $(this).data('selectbanner');

        $.ajax({
            url: urlSelectBanner,
            type: 'GET',
            success: function (data) {
                // $menuBanner.attr('src', data.banner_selected);
            },
            error: function () {
                alert('Ha ocurrido un error de conexión')
            }
        });
    }

    function removeBanner() {
        // request al servidor
        var $div = $(this).closest('#itemBanner');
        var urlDeleteBanner = $(this).data('deletebanner');
        $.ajax({
            type: 'GET',
            url: urlDeleteBanner,

            success:function (data){
                $div.remove();
                // $menuBanner.attr('src', data.banner_selected);
            }
        });
    }
});