// Experience
$(document).on('click', '[data-experience]', function () {
    var urlExperience = $(this).data('experience');

    var formData = new FormData();
    formData.append('_token', $('input[name=_token]').val());
    formData.append('title', $('input[id=title-experience]').val());
    formData.append('company', $('input[id=company-experience]').val());
    formData.append('description', $('textarea[id=description-experience]').val());
    formData.append('start_date', $('input[id=startDate]').val());
    formData.append('end_date', $('input[id=endDate]').val());

    $.ajax({
        url: urlExperience,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            var uploadedExperience = $('#templateExperience').html();
            uploadedExperience = uploadedExperience.replace('<h3 id=""><a href="javascript:void(0);"></a></h3><br>', '<h3 id="titleExperienceText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a></h3><br>');
            uploadedExperience = uploadedExperience.replace('<h3 id="" class="text-muted"></h3>', '<h3 id="companyExperienceText'+data.id+'" class="text-muted">'+data.company+'</h3>');
            uploadedExperience = uploadedExperience.replace('<span id=""></span>', '<span id="dateIntervalText'+data.id+'">'+data.date_interval+'</span>');
            uploadedExperience = uploadedExperience.replace('data-showexperience=""', 'data-showexperience="/experience/'+data.id+'/show"');
            uploadedExperience = uploadedExperience.replace('data-deleteexperience=""', 'data-deleteexperience="/experiences/'+data.id+'/delete"');
            $('#uploadedExperience').append(uploadedExperience);
            $('#experienceModal').modal('hide');
            $('.alert-warning').remove();
            $('#title-experience').val("");
            $('#company-experience').val("");
            $('#description-experience').val("");
            $('#startDate').val("");
            $('#endDate').val("");

            updateProgressBarStatus();
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            console.log($data);
            var $alert = $('#alert-experience');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.company)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.company +'</strong></span>' +
                    '</div>');

            if ($data.start_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.start_date +'</strong></span>' +
                    '</div>');

            if ($data.end_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.end_date +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-showexperience]', function () {
    // request al servidor
    var urlShowExperience = $(this).data('showexperience');
    $.ajax({
        type: 'GET',
        url: urlShowExperience,

        success:function (data){
            document.getElementById('titleExperienceEdit').value=data.title;
            document.getElementById('companyExperienceEdit').value=data.company;
            document.getElementById('descriptionExperienceEdit').value=data.description;
            document.getElementById('startDateEdit').value=data.start_date_format;
            document.getElementById('endDateEdit').value=data.end_date_format;
            $('#btnExperienceEdit').data('editexperience', '/experiences/'+data.id+'/update');

            $('#experienceEditModal').modal('show');
        }
    });

});

$(document).on('click', '[data-editexperience]', function () {
    var urlEditExperience,token, title, company, description, startDate, endDate;
    urlEditExperience = $(this).data('editexperience');
    token = $('input[name=_token]');
    title = $('#titleExperienceEdit');
    company = $('#companyExperienceEdit');
    description = $('#descriptionExperienceEdit');
    startDate = $('#startDateEdit');
    endDate = $('#endDateEdit');

    var formData = new FormData();
    formData.append('_token', token.val());
    formData.append('title', title.val());
    formData.append('company', company.val());
    formData.append('description', description.val());
    formData.append('start_date', startDate.val());
    formData.append('end_date', endDate.val());

    $.ajax({
        url: urlEditExperience,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: formData,
        success:function (data){
            $('#titleExperienceText'+data.id).replaceWith('<h3 id="titleExperienceText'+data.id+'"><a href="javascript:void(0);">'+data.title+'</a></h3>');
            $('#companyExperienceText'+data.id).replaceWith('<h3 id="companyExperienceText'+data.id+'" class="text-muted">'+data.company+'</h3>');
            $('#dateIntervalText'+data.id).replaceWith('<span id="dateIntervalText'+data.id+'">'+data.date_interval+'</span>');
            $('#experienceEditModal').modal('hide');
            $('.alert-warning').remove();
            title.val("");
            company.val("");
            description.val("");
            startDate.val("");
            endDate.val("");
        },
        error:function (data) {
            $('.alert-warning').remove();
            var $data = data.responseJSON;
            var $alert = $('#alert-experience-edit');

            if ($data.title)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.title +'</strong></span>' +
                    '</div>');

            if ($data.company)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.company +'</strong></span>' +
                    '</div>');

            if ($data.start_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.start_date +'</strong></span>' +
                    '</div>');

            if ($data.end_date)
                $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>'+ $data.end_date +'</strong></span>' +
                    '</div>');
        }
    });
});

$(document).on('click', '[data-deleteexperience]', function () {
    // request al servidor
    var $div = $(this).closest('#new-experience');
    var urlDeleteExperience = $(this).data('deleteexperience');
    $.ajax({
        type: 'GET',
        url: urlDeleteExperience,

        success:function (){
            $div.remove();

            updateProgressBarStatus();
        }
    });

});