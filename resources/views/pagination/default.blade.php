@if ($paginator->lastPage() > 1)
    <nav class="tg-pagination">
        <ul>
            <li class="tg-prevpage" @if($paginator->currentPage() == 1) style="pointer-events: none; display: inline-block;" @endif>
                <a href="{{ $paginator->url(1) }}">
                    <i class="fa fa-angle-left"></i>
                </a>
            </li>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' tg-active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            <li class="tg-nextpage" @if($paginator->currentPage() == $paginator->lastPage()) style="pointer-events: none; display: inline-block;" @endif>
                <a href="{{ $paginator->url($paginator->currentPage()+1) }}">
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </nav>
@endif

    {{--<ul>--}}
        {{--<li class="tg-prevpage"><a href="#"><i class="fa fa-angle-left"></i></a></li>--}}
        {{--<li><a href="#">1</a></li>--}}
        {{--<li><a href="#">2</a></li>--}}
        {{--<li><a href="#">3</a></li>--}}
        {{--<li><a href="#">4</a></li>--}}
        {{--<li class="tg-active"><a href="#">5</a></li>--}}
        {{--<li>...</li>--}}
        {{--<li><a href="#">10</a></li>--}}
        {{--<li class="tg-nextpage"><a href="#"><i class="fa fa-angle-right"></i></a></li>--}}
    {{--</ul>--}}
{{--</nav>--}}