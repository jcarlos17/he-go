@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Contacto</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Contacto</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Contacto</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <!--Contact Us Start-->
        <div style="width: 100%; height: 100%">
            <img src="{{ asset('/images/banner-menu-contacto.png') }}" alt="">
        </div>
        <div class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2">
                        <div class="tg-sectionhead">
                            <div class="tg-sectiontitle">
                                <h2>Es mejor estar informado que estar en la luna</h2>
                            </div>
                            <div class="tg-description">
                                <p>La transparencia en todos nuestros servicios es nuestra razón de ser,
                                    y por ese mismo motivo no queremos negarte ninguna información. Desde
                                    aquí puedes hacernos llegar todas tus dudas y preguntas que tengas.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tg-contactusarea">
                        <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-10 col-lg-push-1">
                            <div id="alert-sendmessage"></div>
                            <div class="tg-themeform">
                                {{ csrf_field() }}
                                <fieldset>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" id="name" class="form-control" placeholder="Nombres y Apellidos">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" id="email" class="form-control" placeholder="Correo">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" id="phone" class="form-control" placeholder="Teléfono">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <span class="tg-select">
                                                    <select name="department_id" id="select_department" required>
                                                        <option value="">Seleccione departamento</option>
                                                        @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <span class="tg-select">
                                                    <select name="province_id" id="select_province" required>
                                                        <option value="">Seleccione provincia</option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <span class="tg-select">
                                                    <select name="district_id" id="select_district" required>
                                                        <option value="">Seleccione distrito</option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <span class="tg-select">
                                                    <select name="theme" id="theme" required>
                                                        <option value="">Seleccione un asunto</option>
                                                        <option value="información sobre el servicio">Información sobre el servicio</option>
                                                        <option value="pagos y facturas">Pagos y facturas</option>
                                                        <option value="mediación y reclamos">Mediación y reclamos</option>
                                                        <option value="departamento legal">Departamento legal</option>
                                                        <option value="departamento comercial">Departamento comercial</option>
                                                        <option value="auspicios y patrocinios">Auspicios y patrocinios</option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <textarea placeholder="Mensaje" id="description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <button id="btnSendMessage" data-sendmessage="{{ url('contact/message/send') }}" class="tg-btn">Enviar</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Contact Us End-->

        <!--Secure & Reliable Start-->
        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <figure><img src="{{ asset('images/recuerda-contacto.png') }}" alt="image description"></figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <h2>¡Descubre que es He Go!</h2>
                                <h3>Es seguro, es real, es posible, es ilimitado</h3>
                                <div class="tg-description">
                                    <p>
                                        Bienvenido a la primera plataforma más completa e ilimitada
                                        del mercado, un lugar en el que se complementan personas que
                                        prestan servicios con personas que ofrecen servicios.
                                    </p>
                                    <p>
                                        Regístrate y empieza a descubrir todas las ventajas que tenemos para tí.
                                    </p>
                                </div>
                            </div>
                            <a class="tg-btn" href="#">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Secure & Reliable End-->
    </main>
@endsection

@section('scripts')
    <script src="{{ asset('js/contact/send-message.js') }}"></script>
    <script>
        $(function() {

            $('#select_department').on('change', onSelectDepartmentChange);
            $('#select_province').on('change', onSelectProvinceChange);

        });

        function onSelectDepartmentChange() {
            var department_id = $(this).val();
            if (! department_id) {
                $('#select_province').html('<option value="">Seleccione provincia</option>');
                return;
            }

            // AJAX
            $.get('/api/department/'+department_id+'/provinces', function (data) {
                var html_select = '<option value="">Seleccione provincia</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_province').html(html_select);
            });
        }

        function onSelectProvinceChange() {
            var province_id = $(this).val();

            if (! province_id) {
                $('#select-district').html('<option value="">Seleccione distrito</option>');
                return;
            }

            // AJAX
            $.get('/api/province/'+province_id+'/districts', function (data) {
                var html_select = '<option value="">Seleccione distrito</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_district').html(html_select);
            });
        }
    </script>
@endsection
