@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Soy un buscador de servicios</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Soy un buscador de servicios</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Soy un buscador de servicios</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <!--Secure & Reliable Start-->
        <section class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <figure><img src="{{ asset('images/alcancias.png') }}" alt="image description"></figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-description">
                                    <p>
                                        He Go te ofrece este servicio para tí completamente gratis,
                                        sin letras pequeñas, ni cobros futuros.
                                    </p>
                                    <br>
                                    <p>
                                        Úsalo con total seguridad y sabiduría.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Secure & Reliable End-->
        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Cuéntanos sobre tu trabajo</h3>
                                </div>
                                <div class="tg-description">
                                    <p>
                                        Bríndanos algunos datos con los detalles de tu trabajo
                                        y empezarás a recibir propuestas de los prestadores de
                                        servicios locales.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <figure style="margin: 0"><img src="{{ asset('images/mobile-icons.png') }}" alt="image description"></figure>
                    </div>
                </div>
            </div>
        </section>

        <section class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <figure><img src="{{ asset('images/icono-presupuesto.png') }}" alt="image description"></figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Compara presupuestos</h3>
                                </div>
                                <div class="tg-description">
                                    <p>
                                        Analiza de manera pausada todas las propuestas
                                        recibidas; incluyendo los materiales a utilizar, la duración
                                        del trabajo, opiniones de otros clientes, y revisa la ficha técnica
                                        del prestador de servicios y/o empresa.
                                    </p>
                                    <br>
                                    <p>
                                        Esta es una gran manera de ayudarlo a elegir al mejor
                                        prestador de servicios de la zona.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Elegir con sabiduría</h3>
                                </div>
                                <div class="tg-description">
                                    <p>
                                        Elige al candidato que cumpla y mejor se adapte
                                        a tus requerimientos, si aún no lo has encontrado, no te preocupes
                                        que nosotros lo encontraremos lo más pronto posible y
                                        completamente gratis.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <figure style="padding-left: 20px; padding-right: 20px"><img src="{{ asset('images/wise-men.png') }}" alt="image description"></figure>
                    </div>
                </div>
            </div>
        </section>

        <section class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2">
                        <div class="tg-sectionhead">
                            <div class="tg-sectiontitle" style="margin: 0 0 10px;">
                                <h3 style="margin: 0;color: #389bff">¿No has encontrado al prestador de servicios que buscabas?</h3>
                            </div>
                            <div class="tg-sectiontitle">
                                <h2 style="margin: 0;color: #5dc560">¡Tranquilo!</h2>
                            </div>
                            <div class="tg-description">
                                <p>Nosotros lo hacemos por tí completamente gratis</p>
                            </div>
                        </div>
                    </div>
                    <div class="tg-brands">
                        <div class="text-center">
                            <a class="tg-btn" href="#">¡CONTÁCTANOS!</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <figure><img src="{{ asset('images/doctor.png') }}" alt="image description"></figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-description">
                                    <p>
                                        Porque estamos a favor de un trabajo digno en igual de condiciones para todas y todos
                                        y nuestro valor principal es mejorar la calidad de vida y la unión familiar.
                                    </p>
                                    <br>
                                    <p>
                                        Desde He Go queremos informarte que todos nuestros profesionales disponibles
                                        en nuestra plataforma cuentan con una póliza de seguros contra accidentes laborales,
                                        hospitalización y defunción.
                                    </p>
                                    <br>
                                    <p>
                                        Porque tú nos importas.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
