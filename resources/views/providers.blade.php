@extends('layouts.app')

@section('styles')
    <link href="{{ asset('/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Proveedores</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Proveedores</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Proveedores</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Inner Page Banner Start-->
    <div class="tg-mapinnerbanner">
        <div class="tg-searchbox">
            <div class="container">

            </div>
        </div>
        <div id="tg-map" class="tg-map"></div>
        <div class="tg-mapcontrols">
            <span id="doc-mapplus"><i class="fa fa-plus"></i></span>
            <span id="doc-mapminus"><i class="fa fa-minus"></i></span>
            <span id="doc-lock"><i class="fa fa-lock"></i></span>
            <span id="tg-geolocation"><i class="fa fa-crosshairs"></i></span>
        </div>
        <a id="tg-btnmapview" class="tg-btn tg-btnmapview" href="#">
            <span>Map View</span>
            <i class="lnr lnr-chevron-down"></i>
        </a>
    </div>
    <!--Inner Page Banner End-->

    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout" style="padding-bottom: 80px">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 pull-right">
                        <div class="tg-widget tg-widgetcontact" style="padding: 40px 0">
                            <div class="tg-widgettitle">
                                <h3>¿Qué Prestador de servicios o comercio local de la zona estas buscando?</h3>
                            </div>
                            <div class="tg-widgetcontent">
                                <form class="tg-themeform" method="GET">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                <span class="tg-select" style="text-transform: none">
                                                    <input type="hidden" name="subcategory_id" class="form-control" id="select-subcategory">
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                <span class="tg-select">
                                                    <select name="category_id" id="select-category">
                                                        @if ($subcategoryId)
                                                            <option value="{{ $subcategory->category->id }}">{{ $subcategory->category->name }}</option>
                                                        @else
                                                            <option value="">Categoría</option>
                                                        @endif
                                                    </select>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12" style="padding-bottom: 10px">
                                                <a class="pull-right" id="buttonShow" href="#!" onclick="showContent(true)" style="display: {{ $username || $id ? 'none' : '' }};">Búsqueda avanzada</a>
                                                <a class="pull-right" id="buttonHide" href="#!" onclick="showContent(false)" style="display: {{ $username || $id ? '' : 'none' }};">
                                                    Búsqueda avanzada
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row pull-right" id="content-search-advance" style="display: {{ $username || $id ? '' : 'none' }};">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="id" name="id" placeholder="ID" value="{{ $id }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="username" name="username" placeholder="Nombre de usuario" value="{{ $username }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="tg-btn pull-right" id="btnSearch" type="submit">Buscar</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 20px">
                                            <span class="tg-privacysetting">{{ $providers->count() }} Profesionales encontrados en esta zona y alrededores</span>
                                        </div>
                                    </div>
                                    <div class="tg-latestserviceproviders">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="tg-sortfilters pull-left" style="padding: 10px 0">
                                                <div class="tg-sortfilter tg-sortby">
                                                    <span>Ordenar por:</span>
                                                    <div class="tg-select">
                                                        <select>
                                                            <option>Nombre</option>
                                                            <option>Tipo</option>
                                                            <option>Fecha</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="tg-sortfilter tg-arrange">
                                                    <span>Organizar:</span>
                                                    <div class="tg-select">
                                                        <select>
                                                            <option>Des</option>
                                                            <option>Asc</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="border-right: 1px solid #ddd;">
                                            <div class="tg-privacysetting">
                                                <span>Atienden Urgencias las 24H</span>
                                                <div class="tg-iosstylcheckbox">
                                                    <input name="emergency" type="checkbox" id="emergency" {{ $emergency == 'on' ? 'checked' : '' }} onchange="$('#btnSearch').click()">
                                                    <label for="emergency"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="tg-privacysetting">
                                                <span>Mostrar Profesionales Top </span>
                                                <div class="tg-iosstylcheckbox">
                                                    <input type="checkbox" id="2" value="1" onchange="">
                                                    <label for="2"></label>
                                                </div>
                                            </div>
                                        </div>
                                        @if($providers->count() == 0)
                                            <div style="width: 100%; text-align: center">
                                                <img style="width: 45%; margin-top: 5em" src="{{ asset('images/oops.png') }}" alt="">
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-listing">

                                    <div class="tg-latestserviceproviders">
                                        @foreach($providers as $provider)
                                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                                <div class="tg-serviceprovider tg-automotive">
                                                    <figure class="tg-featuredimg">
                                                        <img src="http://mydesignhoard.com/HTML/html/service_provider/images/serviceproviders/img-01.jpg" alt="image description">
                                                        <figcaption>
                                                            <a class="tg-themetag tg-categorytag" href="javascript:void(0);">Automotive</a>
                                                            <a class="tg-heart tg-liked" href="javascript:void(0);"><i class="fa fa-heart"></i></a>
                                                        </figcaption>
                                                    </figure>
                                                    <div class="tg-serviceprovidercontent">
                                                        <div class="tg-companylogo"><a href="#"><img src="{{ $provider->photo_selected }}" alt="image description"></a></div>
                                                        <div class="tg-companycontent">
                                                            <ul class="tg-tags">
                                                                {{--<li><a class="tg-tag tg-featuredtag" href="#">featured</a></li>--}}
                                                                {{--<li><a class="tg-tag tg-verifiedtag" href="#">verified</a></li>--}}
                                                            </ul>
                                                            <div class="tg-title">
                                                                <h3><a href="{{ url('perfil-publico/'.$provider->id) }}">{{ $provider->name }}</a></h3>
                                                            </div>
                                                            <ul class="tg-matadata">
                                                                <li><span class="tg-stars"><span></span></span></li>
                                                                <li>
                                                                    <i class="fa fa-thumbs-o-up"></i>
                                                                    <em>99% (1009 votes)</em>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @include('pagination.default', ['paginator' => $providers])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 pull-left">
                        <aside id="tg-sidebar" class="tg-sidebar" style="padding: 40px 0">
                            <form class="tg-themeform tg-formrefinesearch" method="GET">
                                <fieldset>
                                    <h4>Subcategorías</h4>
                                    <div class="tg-checkboxgroup">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#subCategoryModal">Mostrar todo</a>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtro por locación</h4>
                                    <div class="tg-checkboxgroup">
                                        <div class="form-group">
                                            <span class="tg-select">
                                                <select name="department_id" id="select_department">
                                                    <option value="">Seleccione departamento</option>
                                                    @foreach ($departments as $department)
                                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                    @endforeach
                                                </select>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="tg-select">
                                                <select name="province_id" id="select_province">
                                                    <option value="">Seleccione provincia</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="tg-select">
                                                <select name="district_id" id="select_district">
                                                    <option value="">Seleccione distrito</option>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por idioma</h4>
                                    <div class="tg-checkboxgroup">
                                        <div class="form-group tg-inpuicon">
                                            <i class="lnr lnr-magnifier"></i>
                                            <input type="text" name="language" class="form-control" placeholder="Idioma">
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por clasificación</h4>
                                    <div class="tg-checkboxgroup tg-ratingcheckbox">
											<span class="tg-checkbox">
												<input type="checkbox" id="nostar" name="ratingfilter" value="nostar">
												<label for="nostar"><span class="tg-stars"><span></span></span></label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="onestar" name="ratingfilter" value="onestar">
												<label for="onestar"><span class="tg-stars"><span></span></span></label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="twostar" name="ratingfilter" value="twostar">
												<label for="twostar"><span class="tg-stars"><span></span></span></label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="threestar" name="ratingfilter" value="threestar">
												<label for="threestar"><span class="tg-stars"><span></span></span></label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="fourstar" name="ratingfilter" value="fourstar">
												<label for="fourstar"><span class="tg-stars"><span></span></span></label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="fivestar" name="ratingfilter" value="fivestar">
												<label for="fivestar"><span class="tg-stars"><span></span></span></label>
											</span>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Misc</h4>
                                    <div class="tg-checkboxgroup">
											<span class="tg-checkbox">
												<input type="checkbox" id="offerfreeinspection" name="misc" value="Offer Free Inspection">
												<label for="offerfreeinspection">Offer Free Inspection</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="onlineappointment" name="misc" value="Online Appointment">
												<label for="onlineappointment">Online Appointment</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="withprofilephoto" name="misc" value="With Profile Photo">
												<label for="withprofilephoto">With Profile Photo</label>
											</span>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <button class="tg-btn" type="submit">Aplicar</button>
                                    <button class="tg-btn" type="reset">Reiniciar</button>
                                </fieldset>
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <div class="modal fade tg-categoryModal" id="subCategoryModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Subcategorías</h2>
                    {{--<span class="tg-selecteditems">05 Items Selected</span>--}}
                </div>
                <form class="tg-themeform tg-formrefinesearch" method="GET">
                    <div class="tg-modalbody">
                        <div class="col-sm-6">
                            <h3>Categorías</h3>
                            <div class="tg-checkboxgroup">
                                @foreach($categories as $category)
                                    <span class="tg-radio">
                                        <input type="radio" id="{{ $category->id }}" name="category_id" value="{{ $category->id }}">
                                        <label for="{{ $category->id }}">{{ $category->name }}</label>
                                    </span>
                                @endforeach
                            </div>
                        </div>
                        <div class="tg-column">
                            <h3>Subcategorías</h3>
                            <div class="tg-checkboxgroup" id="subCategories">
                            </div>
                        </div>
                    </div>
                    <div class="tg-modalfoot">
                        <button class="tg-btn" type="submit">Aplicar</button>
                        <button class="tg-btn" type="button" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function showContent(status) {
            const $btnShow = $('#buttonShow');
            const $btnHide = $('#buttonHide');
            const $content = $('#content-search-advance');
            const $id = $('#id');
            const $username = $('#username');

            if(status) {
                $btnShow.hide();
                $content.show();
                $btnHide.show();
            } else {
                $btnShow.show();
                $content.hide();
                $btnHide.hide();
                $id.val('');
                $username.val('');
            }
        }
    </script>
    <script src="{{ asset('/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
    <script>
        $('#select-subcategory').select2({
            placeholder: 'Busca aquí: pintor, niñera, limpieza de casa…',
            ajax: {
                url: '/api/subcategories',
                dataType: 'json',
                delay: 250,

                data: function (query) {
                    return {
                        queryText: query
                    };
                },

                results: function (data) {
                    return {
                        results: data
                    };
                },

                cache: true
            }
        });
        @if ($subcategoryId)
        $('#select-subcategory').select2('data', {id:{{$subcategory->id}}, text:'{{$subcategory->name}}'});
        @endif
    </script>
    <script>
        $(function() {

            $('#select_department').on('change', onSelectDepartmentChange);
            $('#select_province').on('change', onSelectProvinceChange);
            $('#select-subcategory').on('change', onSelectCategory);
            $('input[name=category_id]').on('change', onCheckSubCategory);
        });

        function onSelectDepartmentChange() {
            var department_id = $(this).val();
            if (! department_id) {
                $('#select_province').html('<option value="">Seleccione provincia</option>');
                return;
            }

            // AJAX
            $.get('/api/department/'+department_id+'/provinces', function (data) {
                var html_select = '<option value="">Seleccione provincia</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_province').html(html_select);
            });
        }

        function onSelectProvinceChange() {
            var province_id = $(this).val();

            if (! province_id) {
                $('#select-district').html('<option value="">Seleccione distrito</option>');
                return;
            }

            // AJAX
            $.get('/api/province/'+province_id+'/districts', function (data) {
                var html_select = '<option value="">Seleccione distrito</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_district').html(html_select);
            });
        }

        function onSelectCategory() {
            var subcategory_id = $(this).val();
            // AJAX
            $.get('/api/subcategories/'+subcategory_id+'/category', function (data) {
                var html_select = '<option value="'+data.id+'" selected>'+data.name+'</option>';
                $('#select-category').html(html_select);
            });
        }

        function onCheckSubCategory() {
            var category_id = $(this).val();
            // AJAX
            $.get('/api/category/'+category_id+'/subcategories', function (data) {
                var html_select = '';
                for (var i=0; i<data.length; ++i)
                    html_select += '<span class="tg-radio">' +
                        '<input type="radio" id="'+data[i].id+'" name="subcategory_id" value="'+data[i].id+'">' +
                    '<label for="'+data[i].id+'">'+data[i].name+'</label>' +
                    '</span>';

                $('#subCategories').html(html_select);
            });
        }
    </script>
    <script>
        jQuery(document).ready(function(e) {
            sp_init_map_script('tg-map');
        });
    </script>
@endsection