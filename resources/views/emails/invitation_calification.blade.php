<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
<style>
    @media  only screen and (max-width: 600px) {
        .inner-body {
            width: 100% !important;
        }
    }
</style>
<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
    <tr>
        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
            <table class="content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                <tr>
                    <table>
                        <tr>
                            <td class="header" style="width: 50%; font-family: Avenir, Helvetica, sans-serif; text-align: center; box-sizing: border-box; padding: 25px 0;">
                                <img src="{{asset('images/email/header.png')}}" style="width: 200px">
                            </td>
                            <td style="width: 50%; text-align: center">
                                <h1 style="width: 90%; font-size: 25px">
                                    Estamos donde tú estas
                                </h1>
                                <p style="width: 90%; font-size: 15px">
                                    La Red más completa y a nivel nacional de expertos dispuestos a atender todas tus consultas
                                    y solicitudes de servicios y urgencias
                                </p>
                            </td>
                        </tr>
                    </table>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                        <table class="inner-body" align="center" width="690" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 690px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 690px;">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">
                                        Buenas {{$name}}!
                                    </h1>
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                        Ha sido invitado para dar una valoración en <span style="color: #5dc560;font-weight: bold;">He Go</span>. Ingresa <a href="{{ url('perfil-publico/'.$id) }}" target="_blank" style="color: #5dc560;font-weight: bold; text-decoration: none">aquí</a>
                                    </p>
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                        Si tienes cualquier duda acerca del funcionamiento de <span style="color: #5dc560;font-weight: bold;">He Go</span>, puedes usar cualquiera de los medios disponibles.
                                    </p>
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
                                        Saludos.
                                    </p>
                                    <table class="subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-top: 5px dashed #EDEFF2; margin-top: 25px; padding-top: 25px;">
                                        <tr>
                                            <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                <p style="width: 100%; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; line-height: 1.5em; margin-top: 0; text-align: center; font-size: 28px;">
                                                    Conecta con tus clientes y empieza a recibir tus primeros trabajos.
                                                </p>
                                                <h1 style="width: 100%; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 20px; font-weight: bold; margin-top: 0; text-align: center;">
                                                    Haz crecer tu negocio
                                                </h1>
                                                <table class="" align="center" cellpadding="0" cellspacing="0" style="width: 100%; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 0; -premailer-cellpadding: 0; -premailer-cellspacing: 0">
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <a href="" target="_blank">
                                                                <img src="{{asset('images/email/app-store.png')}}" style="width: 150px;padding: 1em">
                                                            </a>
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <a href="" target="_blank">
                                                                <img src="{{asset('images/email/Play-store.png')}}" style="width: 150px;padding: 1em">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; line-height: 1.5em; text-align: center; font-size: 18px;">
                                                    Disponible para IOS y Android
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                        <table class="" align="center" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 0; text-align: center; -premailer-cellpadding: 0; -premailer-cellspacing: 0">
                            <tr>
                                <td style="width: 35%; border-right: 3px solid #5dc560;">
                                    <h1 style="font-family: Avenir, Helvetica, sans-serif; font-style: italic; color: #888888; font-size: 20px; text-align: center; margin-right: 10px">
                                        Gracias por usar He Go
                                    </h1>
                                </td>
                                <td class="content-cell" align="center" style="width: 65%; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px;">
                                    <p style="text-align: left">
                                        Puedes ponerte en contacto con nosotros de una forma mas sencilla:
                                    </p>
                                    <table align="center" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 0; -premailer-cellpadding: 0; -premailer-cellspacing: 0">
                                        <tr>
                                            <td>
                                                <img src="{{ asset('images/email/envelope.png') }}" alt="Correo" width="20px">
                                                <span style="font-weight: bold;margin-right: 10px">Orientacion@redhego.com</span>
                                            </td>
                                            <td>
                                                <img src="{{ asset('images/email/phone.png') }}" alt="Teléfono" width="20px">
                                                <span style="font-weight: bold;">699 699 699</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>