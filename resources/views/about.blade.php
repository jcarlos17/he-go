@extends('layouts.app')

@section('styles')
    <style>
        .img-man {
            height: 550px !important;
        }

        @media (min-width: 992px) {
            .img-man {
                height: 450px !important;
            }
        }
    </style>
@endsection

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Nosotros</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Nosotros</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Nosotros</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <!--Services Facilities Start-->
        <section class="tg-main-section tg-haslayout">
            <figure style="margin: 0;"><img style="max-height: 200px !important; position: absolute; top: 25px" src="{{ asset('images/about/circulos.png') }}" alt="Directora"></figure>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="tg-secureandreliable">
                                    <div style="margin-bottom: 10px">
                                        <h4 style="margin: 0;color: #5dc560">¿Qué es He Go?</h4>
                                    </div>
                                    <div class="tg-description">
                                        <p>
                                            He Go es la plataforma web más completa, segura, rápida y fácil de usar para conectar a cualquier
                                            persona, profesional o empresa que ofrezca un producto o servicio con personas que buscan
                                            a estos para atender cualquier problema urgente o realizar cualquier problema urgente
                                            o realizar cualquier tipo de proyecto o más completa, y a nivel nacional, de profesionales y
                                            empresas de todos los rubros y sectores listas para atender cualquier tipo de requetimiento.
                                            Ten la seguridad de que aquí podrás dar con una solución.
                                        </p>
                                        <br>
                                        <p>
                                            Nuestra lista es muy amplia y diversa, aquí te damos algunos ejemplos:
                                            Desde servicios de limpieza, pasando por electricistas, albañiles, empresa de
                                            mudanza, pintores, constructoras y también locales como peluquerías, veterinaria,
                                            lavanderías, también incluimos oficios como: niñeras, profesores particulares, traductores.
                                            También incluimos profesionales especializados como: abogados, psicólogos, arquitectos,
                                            ingenieros y muchísimos más, tu imaginación es el limite.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <figure style="margin: 0"><img src="{{ asset('images/about/directora.png') }}" alt="Directora"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-lg-offset-1 text-center" id="showMore">
                        <p style="color: #5dc560;cursor: pointer;">
                            <i class="fa fa-angle-double-down"></i> <br>continua
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1" id="content" style="display: none">
                        <p>Contamos con la tecnología adecuada y de libre acceso para asegurarte de que encuentres al
                            prestador de servicios que más se adapte a tus necesidades. Para lograr esto debemos proporcionar
                            una plataforma fácil de usar y que atienda a todos, desde una ama de casa hasta grandes organizaciones corporativas.</p>
                        <br>
                        <p>
                            Estamos convencidos que el éxito del marketing digital para una Prestador de Servicios no debería costarle un
                            brazo y una pierna ni arruinarlo en el intento. Queremos ser tu compañero de trabajo, tu socio y amigo
                            ya que nuestra principal preocupación es que tú ganes mas clientes y mejores tu economía porque es nuestra motivación
                            para seguir trabajando muy duro y ofrecerte mas herramientas para que cumplas tus objetivos, “vender más”.
                            Creemos en la mejora continua por ello hemos puesto nuestro servicio de forma ilimitada, en pocas palabras
                            no te costará dinero real ver los datos de contácto de las ofertas de trabajo ni responder a las solicitudes
                            de trabajo ni usar alguna herramienta disponible en tu panel de control, todo esta ahí para que lo uses de forma ílimitada
                            las veces que quieras, tu pones el limite. Prestador de servicios, Bienvenido a la Tarifa Plana
                        </p>
                        <br>
                        <p>
                            Si lo que necesitas es una solución ya sea en forma de carpintero, albañil, abogado, la farmacia que atiende 24h,
                            agencia de viajes o cualquier otro profesional, comercio local de la zona e incluso grandes empresas,
                            estas en el lugar correcto. Aquí podrás ver su perfil profesional, sus datos de contacto, los trabajos/servicios
                            que ofrece y ha echo, las opiniones y valoraciones de otros clientes.
                        </p>
                        <br>
                        <p>
                            Sabemos que ya es muy complicado y estresante buscar a un técnico, persona o empresa capacitada para brindarte el servicios que necesitas.
                            Por eso queremos facilitarte las cosas poniendo a tu disposición este servicio completamente gratis para ti, ahorrando tiempo y dinero.
                            Solo debes proporcionado unos pocos datos de lo que te ocurre y necesitas y empezarás a recibir propuestas de solución de
                            Prestadores de servicios. Tranquilo, puedes publicar solicitudes tantas veces quieras, los Prestadores de servicios siempre estarán ahi
                            para asesorarte y ayudarte, solo debes indicar el tipo de prioridad de tu solicitud. Este servicio es ilimitado para ti y
                            para los Prestadores de Servicio, por esta razón no se veran afectados económicamente el solo hecho de responder a tus consultas.
                        </p>
                        <h4>Nuestro objetivo y misión de existir</h4>
                        <p>
                            Convertirnos en “la forma más fácil, segura y rápida de encontrar un prestador de servicios” independientemente de quien lo requiera
                            o el tamaño del proyecto o servicio y en donde se valla a realizar.
                        </p>
                        <br>
                        <p>
                            Continuaremos mejorando nuestra plataforma para que nuestros usuarios tengan un producto online atractivo, que ofrezca marketing continuo,
                            soporte comercial y un medio para mejorar su presencia profesional online.
                        </p>
                        <h4 class="text-right" style="margin: 10px 0">Dirección General</h4>
                        <p class="text-center" style="color: #5dc560;cursor: pointer;" id="showLess">
                            <i class="fa fa-angle-double-top"></i> <br>Mostrar menos
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2" style="margin-top: 10px">
                        <div class="tg-sectionhead">
                            <div class="tg-sectiontitle">
                                <h3>Nuestros Principios y Valores que Nos Hacen Único</h3>
                            </div>
                            <div class="tg-description">
                                <p>En He Go estamos firmemente comprometidos con nuestros usuarios y trabajamos para ofrecerles
                                    un servicio y una experiencia excelente. A medida que nuestra Red crece, nos
                                    aseguramos que estos valores estén siempre en el centro y se reflejen en todo lo que hacemos</p>
                            </div>
                        </div>
                    </div>
                    <div class="tg-servicesfacilities">
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-hasslefreelife"><i class="lnr lnr-magic-wand"></i></span>
                                <div class="tg-title">
                                    <h3>Transparencia</h3>
                                </div>
                                <div class="tg-description">
                                    <p>No usamos letras pequeñas. <br>
                                        No mentimos</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-allprofessionals"><i class="lnr lnr-heart"></i></span>
                                <div class="tg-title">
                                    <h3>Pasión con Proposito</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Poner a nuestros usuarios en el corazón de todo lo que hacemos
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-fiskfreeservices"><i class="lnr lnr-thumbs-up"></i></span>
                                <div class="tg-title">
                                    <h3>Clientes de por vida</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Somos una comunidad constante
                                        y de confianza todos los días
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-securepayments"><i class="lnr lnr-license"></i></span>
                                <div class="tg-title">
                                    <h3>Identidad</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Saber quienes somos y a donde
                                        queremos llegar</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-savingyourtime"><i class="lnr lnr-clock"></i></span>
                                <div class="tg-title">
                                    <h3>Puntualidad</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Valoramos tu tiempo y la de tus
                                        clientes </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-savingyourcost"><i class="lnr lnr-tag"></i></span>
                                <div class="tg-title">
                                    <h3>Principio de Oportunidad</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Tener información clara y veras te
                                        da ventaja de elección</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-premiumexperience"><i class="lnr lnr-star"></i></span>
                                <div class="tg-title">
                                    <h3>Excelencia</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Forja tu propio camino y generaras
                                        nuevas oportunidades</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <div class="tg-servicefacility">
                                <span class="tg-servicefacilityicon tg-servicetime"><i class="lnr lnr-cart"></i></span>
                                <div class="tg-title">
                                    <h3>Disponibilidad de Servicio 24/7</h3>
                                </div>
                                <div class="tg-description">
                                    <p>Un cliente llega cuando menos te lo
                                        esperas, estate atento y servicial </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Services Facilities End-->

        <!--Company Video Start-->
        <section class="tg-haslayout tg-bglight">
            <div class="tg-main-section tg-companyvideo">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-right">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h2>Company Video</h2>
                                </div>
                                <div class="tg-description">
                                    <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat aute irure dolor in reprehenderit in voluptate sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium dolor emque laudantium totam rem aperiam.</p>
                                </div>
                                <a class="tg-btn" href="javascript:void(0);">View More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-videoshortcode">
                    <figure>
                        <img src="{{ asset('images/placeholder/img-01.jpg') }}" alt="image description">
                        <a class="tg-btnplay" href="https://youtu.be/iC9CpnSj-MU" data-rel="prettyPhoto[video]"><i class="fa fa-play"></i></a>
                    </figure>
                </div>
            </div>
        </section>
        <!--Company Video End-->
        <section class="tg-haslayout">
            <div class="tg-main-section tg-companyvideo" style="background: url(../images/about/fondo_banner.png) no-repeat; background-size: 100%; padding: 30px 0">
                <div class="container">
                    <div class="row">
                        <span>Electricistas I abogados I manicura I agentes de seguridad I clases particulares I decoradores I lavanderías...</span>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <img class="img-man" src="{{ asset('images/about/hombre_banner_inicio.png') }}" alt="Imagen descriptiva">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <div class="tg-textshortcode">
                                <div style="margin-top: 40px">
                                    <h1>Únete hoy y empieza a
                                        recibir más trabajo.</h1>
                                </div>
                                <div class="tg-description">
                                    <p>Forma parte de la plataforma web de negocios y servicios líder
                                        en el sector y conéctate con miles de nuevos clientes.
                                    </p>
                                    <p>Pondremos tu negocio frente a cientos de clientes que buscan tu servicios
                                    </p>
                                    <h3>
                                        ¿Listo para unirte? Registrate hoy <br>
                                        y crea tu <span style="color: #5dc560">Cuenta Gratuita.</span>

                                    </h3>
                                </div>
                                <a class="tg-btn" href="javascript:void(0);">Registrate gratis *</a><br>
                                <small>*El registro y acceso ilimitado a todos los recursos sin cobro alguno</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Secure & Reliable Start-->
        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <figure class="tg-noticeboard"><img src="{{ asset('images/noticeboard.png') }}" alt="image description"></figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <h2>Start Earning Today!</h2>
                                <h3>It’s Secure, Its Reliable</h3>
                                <div class="tg-description">
                                    <p>Consectetur adipisicing elit sed eiusmod tempor incididunt labore et dolore magna aliqua enim adia minim veniam quis nostrud exercitation ullamco laboris atuiuip extea commo consequat aute irure dolor reprehenderit.</p>
                                </div>
                            </div>
                            <a class="tg-btn" href="#">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Secure & Reliable End-->
    </main>
    <!--Main End-->
@endsection
@section('scripts')
    <script>
        $('#showMore').on('click', function() {
            $('#content').show();
            $(this).hide();
        });
        $('#showLess').on('click', function() {
            $('#content').hide();
            $('#showMore').show();
        });
    </script>
@endsection
