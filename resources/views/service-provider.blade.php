@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Soy un prestador de servicios</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Soy un prestador de servicios</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Soy un prestador de servicios</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <!--Secure & Reliable Start-->
        <section class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Cuéntanos quién eres y qué haces</h3>
                                </div>
                                <div class="tg-description">
                                    <p>
                                        Los clientes vendrán hasta aquí para saber más sobre los
                                        servicios que ofreces, ver información de trabajos anteriores
                                        y por supuesto ver las opiniones y valoraciones de los mismos.
                                    </p>
                                    <br>
                                    <p>
                                        Este es tu escaparate y como tal debes de complementar toda
                                        la información lo más detallada posible añadiendo pruebas como
                                        fotos, videos, certificados de capacitaciones y cursos.
                                    </p>
                                    <br>
                                    <p>
                                        Esto ayudará a que el cliente valore positivamente tu perfil y tome una
                                        decisión en base a información real.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <figure style="margin: 0"><img src="{{ asset('images/thinking-man.png') }}" alt="image description"></figure>
                    </div>
                </div>
            </div>
        </section>
        <!--Secure & Reliable End-->
        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <figure style="margin: 0 30px;"><img src="{{ asset('images/check-list-job.png') }}" alt="image description"></figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Tú decides qué hacer y cuándo hacerlo</h3>
                                </div>
                                <div class="tg-description">
                                    <p>
                                        Creemos en la libertad e independencia laboral, como dueño
                                        de tu tiempo y recursos, tú puedes aceptar, rechazar, postegar
                                        e incluso ceder trabajos a colegas de tu red de colaboradores.
                                    </p>
                                    <br>
                                    <p>
                                        Todo esto sin preocuparte de consumir, créditos, ni saldos. Porque
                                        ser libre es eso, libre de hacer todas las ofertas y usar todos los servicios
                                        de forma ilimitada las veces que tú quieras sin temor de arruinarte en
                                        el proceso.
                                    </p>
                                    <br>
                                    <p>
                                        Tú tienes el control de tu trabajo.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Responde a los clientes</h3>
                                </div>
                                <div class="tg-description">
                                    <p>
                                        Cuando recibas una solicitud u oferta de trabajo a través de
                                        la plataforma puedes optar por aceptarla o rechazarla. Esta acción
                                        no se reflejará de cara al cliente.
                                    </p>
                                    <br>
                                    <p>
                                        Recuerda que todas las ofertas y solicitudes que recibas debes verlas
                                        como un potencial cliente y es por ello que debes tener en cuenta las siguientes
                                        recomendaciones:
                                    </p>
                                    <br>
                                    <ul>
                                        <li>Toma la iniciativa enviando un mensaje personalizado o una llamada amistosa.</li>
                                        <li>Asegúrate que toda la información de tu perfil sea profesional, fácil de entender y
                                            actualizada.</li>
                                        <li>Entendemos que es difícil dar un precio real con poca información, por ello tú
                                            puedes proponer una visita al sitio, datos más precisos y recuerda, que al enviar
                                            el presupuesto se recomienda que desglose los precios para el cliente evitando
                                            gastos ocultos y sorpresas inesperadas al finalizar el servicio.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <figure style="margin: 0"><img src="{{ asset('images/reply-quest.png') }}" alt="image description"></figure>
                    </div>
                </div>
            </div>
        </section>

        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2">
                        <div class="tg-sectionhead">
                            <div class="tg-sectiontitle">
                                <h2 style="margin: 0;color: #5dc560">¿Quieres registrarte? ¡Hazlo visible hoy!</h2>
                            </div>
                            <div class="tg-description">
                                <p>Forma parte de esta gran red de profesionales a nivel nacional</p>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="tg-brands">
                        <div class="text-center">
                            <a class="tg-btn" href="#">¡REGÍSTRATE YA!</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
