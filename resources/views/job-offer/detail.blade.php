@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Vista individual de trabajo</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Vista individual de trabajo</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <div class="container" style="width: 1270px !important;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-loginsubtitle pull-right" style=" padding-top: 20px">
                    <a href="{{ url('gestionar-trabajos') }}" >
                        Volver atrás
                    </a>
                    |
                    <a href="{{ url('oferta-trabajo/'.$offerJob->id.'/pdf') }}" target="_blank">
                        Descargar PDF
                    </a>
                    |
                    <a href="{{ url('oferta-trabajo/'.$offerJob->id.'/edit') }}">
                        Editar
                    </a>
                </div>
            </div>
        </div>
    </div>
    <main id="tg-main" class="tg-main tg-haslayout" style="padding: 40px 0">
        <div class="tg-detailpage tg-jobdetail">
            <div class="tg-detailpagehead">
                <div class="container" style="width: 1270px !important;">
                    <div class="row">
                        @include('user.include.menu')
                        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                            <div class="tg-detailpageheadcontent">
                                <div class="tg-companylogo"><a href="{{ url('perfil-publico/'.$offerJob->user_id) }}"><img src="{{ $offerJob->user->photo_selected }}" alt="image description"></a></div>
                                <div class="tg-companycontent">
                                    <ul class="tg-tags">
                                        <li><a class="tg-tag" href="#">{{ $offerJob->job_type }}</a></li>
                                    </ul>
                                    <div class="tg-title">
                                        <h1>{{ $offerJob->title }}</h1>
                                        <span class="tg-jobpostedby"><a href="{{ url('/perfil-publico/'.$offerJob->user_id) }}">{{ $offerJob->user->name }}</a></span>
                                    </div>
                                </div>
                                <a class="tg-btn-info" href="#!">Iniciar chat</a>
                                @if($offerJob->status != 'Cerrado')
                                    @if($proposalExists)
                                        <a class="tg-btn" href="javascript:void(0);">{{ $offerJob->type_description }}</a>
                                    @else
                                        <a class="tg-btn" href="javascript:void(0);"
                                           {{--data-toggle="modal" data-target="#showProposal"--}}
                                        >{{ $offerJob->type_description }}</a>
                                    @endif
                                @endif
                                {{--<a class="tg-btn" href="{{ url('/proposal') }}">Enviar propuesta</a>--}}
                            </div>
                            <ul class="tg-jobmatadata">
                                <li>
                                    <div class="tg-box tg-posteddate" style="padding: 15px !important;">
                                        <span class="tg-jobmataicon" style="margin: 0 5px 0 0;"><i class="lnr lnr-calendar-full"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Fecha publicación</strong>
                                            <span>{{ $offerJob->publication_date }}</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="tg-box tg-applicantapplied">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-book"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Solicitantes</strong>
                                            <span>3479  solicitantes</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="tg-box tg-views">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-eye"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Vistas</strong>
                                            <span>Total vistas: 22508</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="tg-box tg-expiredate">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-warning"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Fecha de cierre</strong>
                                            <span>{{ $offerJob->expire_date_format }}</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div id="tg-twocolumns" class="tg-twocolumns">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 pull-left">
                                        <div id="tg-content" class="tg-content">
                                            <div class="tg-companyfeatures">
                                                <div class="tg-companyfeaturebox tg-introduction">
                                                    <div class="tg-companyfeaturetitle">
                                                        <h3>Descripción de la Solicitud</h3>
                                                    </div>
                                                    <div class="tg-description">
                                                        <span style="color: #919191 !important;">{!! nl2br($offerJob->description) !!}</span>
                                                    </div>
                                                </div>
                                                <div class="tg-companyfeaturebox tg-jobdetails">
                                                    <div class="tg-companyfeaturetitle">
                                                        <h3>Detalles del trabajo</h3>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span>Categoría profesional:</span>
                                                            <span>{{ $offerJob->category->name }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Profesional en:</span>
                                                            <span>{{ $offerJob->subcategory->name }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Experiencia:</span>
                                                            <span>{{ $offerJob->experience }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Pago sugerido:</span>
                                                            <span>{{ $offerJob->suggested_payment }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Tipo de proridad:</span>
                                                            <span>{{ $offerJob->priority_type }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Tipo de empleo:</span>
                                                            <span>{{ $offerJob->job_type }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Horario preferible:</span>
                                                            <span>{{ $offerJob->schedule }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Movilidad geográfica:</span>
                                                            <span>{{ $offerJob->geographical_mobility }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Idioma preferible:</span>
                                                            <span>{{ $offerJob->language }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Departamento:</span>
                                                            <span>{{ $offerJob->department->name }}</span>
                                                        </li>
                                                        <li>
                                                            <span>Provincia:</span>
                                                            <span>{{ $offerJob->province->name }}</span>
                                                        </li>
                                                        @if($offerJob->district_id)
                                                            <li>
                                                                <span>Distrito:</span>
                                                                <span>{{ $offerJob->district->name }}</span>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="tg-companyfeaturebox tg-jobrequirments">
                                                    <div class="tg-companyfeaturetitle">
                                                        <h3>Requisitos del prestador de servicios</h3>
                                                    </div>
                                                    <div class="tg-description">
                                                        <p style="color: #919191 !important;">{!! nl2br($offerJob->abilities) !!}</p>
                                                    </div>
                                                </div>
                                                @if($offerJob->benefits->count() > 0)
                                                    <div class="tg-companyfeaturebox tg-benefitsallowances">
                                                        <div class="tg-companyfeaturetitle">
                                                            <h3>Beneficios que se incluyen</h3>
                                                        </div>
                                                        <ul class="tg-themeliststyle tg-themeliststylecircletick">
                                                            @foreach($offerJob->benefits as $key => $benefit)
                                                                <li style="color: #919191 !important;">{{ $benefit->name }}</li>
                                                                @if(($key+1)%3 == 0)
                                                                    <div class="clearfix"></div>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                @if($offerJob->images->count() > 0)
                                                    <div class="tg-companyfeaturebox tg-gallery">
                                                        <div class="tg-companyfeaturetitle">
                                                            <h3>Imágenes</h3>
                                                        </div>
                                                        <ul>
                                                            @foreach($offerJob->images as $image)
                                                                <li>
                                                                    <div class="tg-galleryimgbox">
                                                                        <figure>
                                                                            <img src="{{ $image->image_url }}" alt="image description">
                                                                            {{--<a class="tg-btngallery" href="{{ $gallery->image_url }}" data-rel="prettyPhoto[gallery]"><i class="lnr lnr-magnifier"></i></a>--}}
                                                                        </figure>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pull-right">
                                        <aside id="tg-sidebar" class="tg-sidebar">
                                            <div class="tg-widget tg-widgetlocationandcontactinfo">
                                                @include('job-offer.status', ['offerJob' => $offerJob, 'detail' => 1])
                                                <div class="tg-mapbox">
                                                    <div id="tg-joblocationmap" class="tg-locationmap"></div>
                                                </div>
                                                <div class="tg-contactinfobox">
                                                    <ul class="tg-contactinfo" style="color: #919191">
                                                        @foreach($offerJob->user->addresses as $address)
                                                            <li>
                                                                <address>{{ $address->description_complete }}</address>
                                                            </li>
                                                        @endforeach
                                                        <li>
                                                            <i class="lnr lnr-phone-handset"></i>
                                                            <span>{{ $offerJob->user->mobile }}</span>
                                                        </li>
                                                        <li>
                                                            <i class="lnr lnr-phone"></i>
                                                            <span>{{ $offerJob->user->phone }}</span>
                                                        </li>
                                                        <li>
                                                            <i class="lnr lnr-apartment"></i>
                                                            <span>{{ $offerJob->user->company_name }}</span>
                                                        </li>
                                                        <li>
                                                            <i class="lnr lnr-envelope"></i>
                                                            <span><a href="mailto:{{ $offerJob->user->email }}">{{ $offerJob->user->email }}</a></span>
                                                        </li>
                                                        @if($offerJob->user->website)
                                                            <li>
                                                                <i class="lnr lnr-laptop"></i>
                                                                <span><a href="{{ $offerJob->user->website }}" target="_blank">{{ $offerJob->user->website }}</a></span>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                    <ul class="tg-socialicons">
                                                        <li class="tg-facebook"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->facebooklink : '' }}"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="tg-twitter"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->twitterlink : '' }}"><i class="fa fa-twitter"></i></a></li>
                                                        <li class="tg-linkedin"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->linkedinlink : '' }}"><i class="fa fa-linkedin"></i></a></li>
                                                        <li class="tg-skype"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->skypelink : '' }}"><i class="fa fa-skype"></i></a></li>
                                                        <li class="tg-googleplus"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->googlepluslink : '' }}"><i class="fa fa-google-plus"></i></a></li>
                                                        <li class="tg-pinterestp"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->pinterestlink : '' }}"><i class="fa fa-pinterest-p"></i></a></li>
                                                    </ul>
                                                    {{--<a class="tg-btn tg-btn-lg" href="javascript:void(0);">get direction</a>--}}
                                                </div>
                                            </div>
                                            <div class="tg-widget tg-widgetshare">
                                                <div class="tg-widgettitle">
                                                    <h3>Comparte este trabajo</h3>
                                                </div>
                                                <div class="tg-widgetcontent">
                                                    <ul class="tg-socialicons">
                                                        <li class="tg-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li class="tg-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li class="tg-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        <li class="tg-googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                        <li class="tg-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                                                        <li class="tg-vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                                        <li class="tg-tumblr"><a href="#"><i class="fa fa-tumblr"></i></a></li>
                                                        <li class="tg-yahoo"><a href="#"><i class="fa fa-yahoo"></i></a></li>
                                                        <li class="tg-yelp"><a href="#"><i class="fa fa-yelp"></i></a></li>
                                                        <li class="tg-pinterestp"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                                        <li class="tg-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                                        <li class="tg-stumbleupon"><a href="#"><i class="fa fa-stumbleupon"></i></a></li>
                                                        <li class="tg-reddit"><a href="#"><i class="fa fa-reddit"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tg-widget tg-widgetrelatedjobs">
                                                <div class="tg-widgettitle">
                                                    <h3>Trabajos relacionados</h3>
                                                </div>
                                                <div class="tg-widgetcontent">
                                                    <ul>
                                                        <li>
                                                            <div class="tg-serviceprovidercontent">
                                                                <div class="tg-companylogo"><a href="#"><img src="images/logos/img-01.png" alt="image description"></a></div>
                                                                <div class="tg-companycontent">
                                                                    <a class="tg-tag tg-tagjobtype" href="#">fix</a>
                                                                    <div class="tg-title">
                                                                        <h3><a href="#">Carpenter Required</a></h3>
                                                                    </div>
                                                                    <span class="tg-jobpostedby">By: <a href="javascript:void(0);">Blue Bird Organization</a></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="tg-serviceprovidercontent">
                                                                <div class="tg-companylogo"><a href="#"><img src="images/logos/img-02.png" alt="image description"></a></div>
                                                                <div class="tg-companycontent">
                                                                    <a class="tg-tag tg-tagjobtype" href="#">Internship</a>
                                                                    <div class="tg-title">
                                                                        <h3><a href="#">Electrician For Fixing Required</a></h3>
                                                                    </div>
                                                                    <span class="tg-jobpostedby">By: <a href="javascript:void(0);">On Fleek Spa &amp; Beauty Saloon</a></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="tg-serviceprovidercontent">
                                                                <div class="tg-companylogo"><a href="#"><img src="images/logos/img-03.png" alt="image description"></a></div>
                                                                <div class="tg-companycontent">
                                                                    <a class="tg-tag tg-tagjobtype" href="#">Temporary</a>
                                                                    <div class="tg-title">
                                                                        <h3><a href="#">Car Fixing Mechanic Required</a></h3>
                                                                    </div>
                                                                    <span class="tg-jobpostedby">By: <a href="javascript:void(0);">Eldridge Hemenway</a></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

