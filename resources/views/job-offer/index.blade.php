@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Gestionar trabajos</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Gestionar trabajos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout" style="padding: 40px 0">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 pull-right">
                        <div class="row">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-joblisting">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="tg-dashboardhead">
                                            <div class="tg-dashboardtitle">
                                                <h2>Mis ofertas de trabajo</h2>
                                            </div>
                                        </div>
                                        <div class="tg-sortfilters">
                                            <div class="tg-sortfilter tg-sortby">
                                                <span>Ordenar por:</span>
                                                <div class="tg-select">
                                                    <select>
                                                        <option>Nombre</option>
                                                        <option>Tipo</option>
                                                        <option>Fecha</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="tg-sortfilter tg-arrange">
                                                <span>Organizar:</span>
                                                <div class="tg-select">
                                                    <select>
                                                        <option>Des</option>
                                                        <option>Asc</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="tg-sortfilter tg-show">
                                                <span>Mostrar:</span>
                                                <div class="tg-select">
                                                    <select>
                                                        <option>12</option>
                                                        <option>24</option>
                                                        <option>Todo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="border-bottom: 1px solid #ddd; padding-bottom: 3em">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="tg-checkbox">
                                                        <input type="checkbox" id="all" name="check">
                                                        <label for="all">Seleccionar todas las filas</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <form action="">
                                                <div class="col-sm-4">
                                                    <div class="tg-select">
                                                        <select style="height: 30px !important;padding: 5px 15px;" name="status" onchange="$('#btnSearch').click()">
                                                            <option value="">Seleccionar estado</option>
                                                            <option value="5" {{ $status == 5 ? 'selected' : '' }}>Solicitud completada</option>
                                                            <option value="4" {{ $status == 4 ? 'selected' : '' }}>Solicitud cancelada</option>
                                                            <option value="3" {{ $status == 3 ? 'selected' : '' }}>Solicitud pausada</option>
                                                            <option value="2" {{ $status == 2 ? 'selected' : '' }}>Estudiando candidatos</option>
                                                            <option value="1" {{ $status == 1 ? 'selected' : '' }}>Solicitud activa</option>
                                                            <option value="0" {{ $status == '0' ? 'selected' : '' }}>Borrador</option>
                                                        </select>
                                                        <button id="btnSearch" style="display: none" type="submit"></button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="col-sm-4 text-right">
                                                <a href="#">Editar</a>
                                                |
                                                <a href="#" style="margin: 0 10px"><i class="fa fa-cloud-download"></i></a>

                                                <a href="#"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        @if($offerJobs->count() == 0)
                                            <img src="{{ asset('/images/oops-page.png') }}" alt="">
                                        @else
                                            <table class="tg-tablejoblidting">
                                                <tbody>
                                                @foreach($offerJobs as $offerJob)
                                                    <tr>
                                                        <td class="col-md-1 text-center">
                                                            <div class="form-group">
                                                                <div class="tg-checkbox">
                                                                    <input type="checkbox" id="{{ $offerJob->id }}" name="check">
                                                                    <label for="{{ $offerJob->id }}"></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="col-md-5">
                                                            <figure class="tg-companylogo">
                                                                <img src="{{ $offerJob->user->photo_selected }}" alt="image description">
                                                            </figure>
                                                            <div class="tg-contentbox">
                                                                <a class="tg-tag tg-featuredtag" href="#!">{{ $offerJob->job_type_name }}</a>
                                                                <div class="tg-title">
                                                                    <h3><a href="
{{--                                                                         @if($offerJob->status != 0) {{ url('/oferta-trabajo/'.$offerJob->id.'/detalle') }} @else --}}
                                                                                #!
                                                                            {{--@endif--}}
                                                                                ">{{ $offerJob->title }}</a></h3>
                                                                </div>
                                                                <span>{{ $offerJob->user->name }}</span>
                                                            </div>
                                                            <div class="tg-contentbox">
                                                                <div class="tg-btnactions" style="float: none; margin-top: 10px">
                                                                    <a class="tg-btnreply" href="{{ url('/oferta-trabajo/'.$offerJob->id.'/edit') }}" title="Editar" style="margin: 0 10px"><i class="fa fa-edit"></i></a>
                                                                    <a class="tg-btndelemail" href="javascript:void(0);" data-delete="{{ url('/oferta-trabajo/'.$offerJob->id.'/delete') }}" title="Eliminar" style="margin: 0 10px"><i class="fa fa-trash"></i></a>
                                                                    @if($offerJob->status != 3 && $offerJob->status != 0 && $offerJob->status != 5)
                                                                        <a class="tg-btntime" href="javascript:void(0);" title="Pausar oferta de trabajo" onclick="showModalPause({{$offerJob->id}})" style="margin: 0 10px"><i class="fa fa-hourglass-end"></i></a>
                                                                    @elseif($offerJob->status == 3)
                                                                        <a class="tg-btntime" href="javascript:void(0);" title="Reanudar oferta de trabajo" data-show="{{ url('/oferta-trabajo/'.$offerJob->id.'/show-pause') }}" style="margin: 0 10px"><i class="fa fa-hourglass-start"></i></a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="col-md-2">
                                                        <span>
                                                            {{ $offerJob->subcategory ? $offerJob->subcategory->name : '' }}
                                                        </span>
                                                        </td>
                                                        <td class="col-md-2">
                                                        <span>
                                                            {{ $offerJob->dep_prov }}
                                                        </span>
                                                        </td>
                                                        <td class="col-md-2" style="position: relative;">
                                                            @include('job-offer.status', ['offerJob' => $offerJob, 'detail' => 0])
                                                            <span>Caduca:</span><br>
                                                            <span>{{ $offerJob->expire_date_format }}</span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{--{{ $offerJobs->links() }}--}}
                                        @endif
                                    </div>
                                    {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">--}}
                                        {{--<nav class="tg-pagination">--}}
                                            {{--<ul>--}}
                                                {{--<li class="tg-prevpage"><a href="#"><i class="fa fa-angle-left"></i></a></li>--}}
                                                {{--<li><a href="#">1</a></li>--}}
                                                {{--<li><a href="#">2</a></li>--}}
                                                {{--<li><a href="#">3</a></li>--}}
                                                {{--<li><a href="#">4</a></li>--}}
                                                {{--<li class="tg-active"><a href="#">5</a></li>--}}
                                                {{--<li>...</li>--}}
                                                {{--<li><a href="#">10</a></li>--}}
                                                {{--<li class="tg-nextpage"><a href="#"><i class="fa fa-angle-right"></i></a></li>--}}
                                            {{--</ul>--}}
                                        {{--</nav>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                </div>
            </div>
            <div class="modal fade tg-categoryModal" id="showPauseJob" tabindex="-1">
                <div class="modal-dialog tg-modaldialog" role="document" style="max-width: 990px">
                    <div class="modal-content tg-modalcontent">
                        <div class="tg-modalhead">
                            <h2>Pausar Oferta trabajo</h2>
                        </div>
                        <form id="formPause" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="tg-modalbody">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 20px 0">
                                    <span class="col-sm-2" style="color: black; font-size: 15px">Pausar oferta:</span>
                                    <div class="form-horizontal col-sm-2">
                                        <div class="tg-radio">
                                            <input type="radio" id="1_semana" name="duration" value="7" required>
                                            <label for="1_semana">1 semana</label>
                                        </div>
                                    </div>
                                    <div class="form-horizontal col-sm-2">
                                        <div class="tg-radio">
                                            <input type="radio" id="2_semana" name="duration" value="14">
                                            <label for="2_semana">2 semanas</label>
                                        </div>
                                    </div>
                                    <div class="form-horizontal col-sm-2">
                                        <div class="tg-radio">
                                            <input type="radio" id="1_mes" name="duration" value="30">
                                            <label for="1_mes">1 mes</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tg-modalfoot">
                                {{--<button class="tg-btn-danger" type="button">Cancelar</button>--}}
                                <button class="tg-btn pull-right" type="submit">Aceptar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade tg-categoryModal" id="showRestoreJob" tabindex="-1">
                <div class="modal-dialog tg-modaldialog" role="document" style="max-width: 990px">
                    <div class="modal-content tg-modalcontent">
                        <div class="tg-modalhead">
                            <h2>Reanudar Oferta trabajo</h2>
                        </div>
                        <form id="formRestore" action="" method="GET">
                            <div class="tg-modalbody">
                                <div class="tg-pkgexpireyandcounter">
                                    <div class="tg-pkgexpirey">
                                        <img src="{{ asset('images/reloj_arena.svg') }}" alt="" style="max-width: 60%">
                                    </div>
                                    <div class="tg-timecounter tg-expireytimecounter">
                                        <div id="tg-countdown" class="tg-countdown">
                                            <div id="tg-note" class="tg-note"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tg-modalfoot">
                                {{--<button class="tg-btn-danger" type="button">Cancelar</button>--}}
                                <button class="tg-btn pull-right" type="submit">Quitar pausa</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        function showModalPause(id) {
            $('#formPause').attr('action', "oferta-trabajo/"+id+"/pause");
            $('#showPauseJob').modal('show');
        }

        $(document).on('click', '[data-show]', function () {
            // request al servidor
            let urlShowOfferJob = $(this).data('show');
            $.ajax({
                type: 'GET',
                url: urlShowOfferJob,

                success: function (data) {
                    $('#tg-note').remove();
                    $('#tg-countdown').append('<div id="tg-note" class="tg-note"></div>');
                    $('#tg-countdown').show();
                    $('.countDiv').remove();
                    $('.countDays').remove();
                    $('.countHours').remove();
                    $('.countMinutes').remove();
                    $('.countSeconds').remove();
                    $('#formRestore').attr('action', "oferta-trabajo/"+data.offer_job_id+"/restore");
                    // console.log(`${data.year} -  ${data.month} - ${data.day} - ${data.hour} - ${data.minute} - ${data.second}`);
                    expireyCounter(data.year, data.month-1, data.day, data.hour, data.minute, data.second);
                    $('#showRestoreJob').modal('show');
                }
            });

        });
        /* --------------------------------------
			COUNTER
        -------------------------------------- */
            function expireyCounter(year, month, day, hour, minute, second){
                const note = jQuery('#tg-note');
                // 0 January
                // 7-1 July
                // 8-1 August
                const ts = new Date(year, month, day, hour, minute, second);

                jQuery('#tg-countdown').countdown({
                    timestamp: ts,
                    callback: function(days, hours, minutes, seconds) {
                        if (days === 0 && hours === 0 && minutes === 0 && seconds === 0) {
                            $('.tg-countdown').hide();
                        }
                        let message = "";

                        message += days + " día" + ( days==1 ? '':'s' ) + ", ";
                        message += hours + " hora" + ( hours==1 ? '':'s' ) + ", ";
                        message += minutes + " minuto" + ( minutes==1 ? '':'s' ) + " y ";
                        message += seconds + " segundo" + ( seconds==1 ? '':'s' ) + " <br />";

                        note.html(message);
                    }
                });
            }
            // expireyCounter();
    </script>
    <!-- Sweet alert 2 -->
    <script src="https://unpkg.com/sweetalert2@7.3.0/dist/sweetalert2.all.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-delete]').on('click', onClickEvaluatorDelete);
        });

        function onClickEvaluatorDelete() {
            let urlDelete = $(this).data('delete');
            swal({
                title: '¡Atención!',
                text: "Usted está por eliminar una oferta/solicitud de servicio. con esta acción se borrará todo el contenido de la misma",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Eliminar'
            }).then((result) => {
                if (result.value) {
                location.href = urlDelete;
            }
        });
        }
    </script>
@endsection
