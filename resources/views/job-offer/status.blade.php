@if($detail)
    @switch($offerJob->status)
        @case(5)
        <span class="status-detail-info">{{ $offerJob->status_description }}</span>
        @break

        @case(4)
        <span class="status-detail-danger">{{ $offerJob->status_description }}</span>
        @break

        @case(3)
        <span class="status-detail-cyan">{{ $offerJob->status_description }}</span>
        @break

        @case(2)
        <span class="status-detail-purple">{{ $offerJob->status_description }}</span>
        @break

        @case(1)
        <span class="status-detail-success">{{ $offerJob->status_description }}</span>
        @break

        @default
        <span class="status-detail-secondary">{{ $offerJob->status_description }}</span>
    @endswitch
@else
    @switch($offerJob->status)
        @case(5)
        <span class="status-info">{{ $offerJob->status_description }}</span>
        @break

        @case(4)
        <span class="status-danger">{{ $offerJob->status_description }}</span>
        @break

        @case(3)
        <span class="status-cyan">{{ $offerJob->status_description }}</span>
        @break

        @case(2)
        <span class="status-purple">{{ $offerJob->status_description }}</span>
        @break

        @case(1)
        <span class="status-success">{{ $offerJob->status_description }}</span>
        @break

        @default
        <span class="status-secondary">{{ $offerJob->status_description }}</span>
    @endswitch
@endif