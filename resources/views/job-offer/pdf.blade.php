<!doctype html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>He Go</title>
    <link rel="stylesheet" href="{{ asset('css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <style type="text/css">
        @page { margin: 1em;}
        @font-face {
            font-family: Arial;
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
        }
        body {
            color: #919191;
            margin: 0;
            font-size: 14px;
            background: #fff;
        }
        .tg-jobdetail .tg-detailpagehead .tg-companylogo {
            margin: 15px 15px 15px 0;
        }
        .tg-jobdetail .tg-detailpageheadcontent {
            padding: 0 185px 0 0;
        }
        .tg-detailpageheadcontent {
            width: 100%;
            padding: 9px 185px 9px 0;
        }
        .tg-companylogo {
            float: left;
            overflow: hidden;
            margin: 0 8px 0 0;
            width: 100px;
            height: 100px;
            position: relative;
            border-radius: 50px;
            border: 1px solid #ddd;
        }
        .tg-serviceprovidercontent .tg-companylogo {
            float: left;
            overflow: hidden;
            margin: 0 8px 0 0;
            width: 70px;
            height: 70px;
            position: relative;
            border-radius: 35px;
            border: 1px solid #ddd;
        }
        .tg-companycontent {
            overflow: hidden;
        }
        .tg-tags {
            width: 100%;
            list-style: none;
            font-size: 12px;
            line-height: 20px;
            margin: 0;
        }
        .tg-tags span {
            font-size: inherit;
            line-height: inherit;
            list-style-type: none;
        }
        li {
            line-height: 28px;
            list-style: disc inside none;
        }
        .tg-tag {
            color: #999;
            padding: 10px;
            font-size: 12px;
            border-radius: 3px;
            line-height: 20px;
            background: #ffffff;
            border: 1px solid #ddd;
        }
        .tg-jobdetail .tg-detailpagehead .tg-title {
            padding: 15px 0 0;
        }
        .tg-detailpageheadcontent .tg-title {
            padding: 15px 0 10px;
        }
        .tg-companycontent .tg-title {
            padding: 5px 0;
        }
        .tg-title {
            width: 100%;
        }
        .tg-detailpageheadcontent .tg-title h1 {
            margin: 0 !important;
            font-size: 30px;
            font-weight: 300;
            line-height: 20px;
        }
        .tg-detailpageheadcontent .tg-title span {
            display: block;
            font-size: 16px;
            line-height: 16px;
            padding: 5px 0 0;
        }
        .tg-jobpostedby {
            color: #999;
            width: 100%;
            font-size: 13px;
            line-height: 13px;
        }
        .tg-jobmatadata {
            clear: both;
            color: #919191;
            padding: 15px 0;
            font-size: 13px;
            line-height: 16px;
            width: 100%;
        }
        .tg-jobmatadata .tg-box {
            width: 100%;
            height: 50px;
            padding: 15px;
            border-radius: 5px;
            background: #f7f7f7;
        }
        .tg-jobmataicon {
            color: #fff;
            width: 40px;
            height: 40px;
            float: left;
            background: #333;
            font-size: 20px;
            line-height: 32px;
            margin: 0 5px 0 0;
            border-radius: 20px;
            text-align: center;
        }
        .tg-posteddate .tg-jobmataicon {
            background: #5c6bc0;
        }
        .tg-applicantapplied .tg-jobmataicon {
            background: #26a69a;
        }
        .tg-views .tg-jobmataicon{
            background: #78909c;
        }
        .tg-expiredate .tg-jobmataicon {
            background: #ef5350;
        }
        .tg-jobmataicon i {
            line-height: inherit;
        }
        .tg-posteddate strong {
            color: #5c6bc0;
        }
        .tg-applicantapplied strong {
            color: #26a69a;
        }
        .tg-jobmatacontent strong, .tg-jobmatacontent span {
            display: block;
        }
        .tg-jobmatacontent strong {
            font: 400 16px/16px 'Work Sans', Arial, Helvetica, sans-serif;
        }
        .tg-applicantapplied strong {
            color: #26a69a;
        }
        .tg-views strong, .alert-danger strong {
            color: #78909c;
        }
        .tg-expiredate strong, .alert-warning strong {
            color: #ef5350;
        }
        .status-detail-success, .status-detail-secondary, .status-detail-danger, .status-detail-info{
            color: white;
            padding: 10px 15px;
        }

        .status-detail-success{
            background-color: #5dc560;
        }
        .status-detail-secondary{
            background-color: #808285;
        }
        .status-detail-danger{
            background-color: #ee1d23;
        }
        .status-detail-info{
            background-color: #8dd7f7;
        }
        .tg-companyfeaturetitle {
            width: 100%;
            padding: 0 0 2px;
        }
        .tg-companyfeaturetitle h3 {
            margin: 0;
            font-size: 18px;
            line-height: 14px;
        }
        .tg-description {
            width: 100%;
            margin: 10px 0;
            max-height: none;
        }
        .tg-companyfeaturebox {
            width: 100%;
            padding: 10px 0 0;
        }
        .tg-companyfeaturebox table tr:nth-child(even) {
            background: #f7f7f7;
        }
        .tg-companyfeaturebox.tg-jobdetails table tr {
            margin: 0;
            padding: 10px 20px;
        }
        .tg-galleries {
            width: 100%;
            clear: both;
            margin: -5px;
        }
        .tg-gallery {
            margin: 0;
            padding: 5px;
        }
        .tg-galleryimgbox figure {
            margin: 0;
            width: auto;
            background: #333;
        }
        .row{
            margin-right: -5px;
            margin-left: -5px;
        }
        .row:after, .row:before {
            display: table;
            content: " ";
            clear: both;
        }
        .col-sm-3 {
            width: 20%;
        }
        .col-sm-4 {
            width: 33.33%;
        }
        .col-sm-4, .col-sm-3{
            float: left;
            position: relative;
            min-height: 1px;
            padding-right: 5px;
            padding-left: 5px;
        }
        .tg-contactinfobox .tg-contactinfo div {
            padding: 10px 0 0;
        }

    </style>
</head>
<body class="tg-login">
<!--************************************
        Wrapper Start
*************************************-->
<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout" style="padding: 10px 0">
        <div class="tg-detailpage tg-jobdetail">
            <div class="tg-detailpagehead">
                <div class="container">
                    <div style="width: 100%">
                        <div class="tg-detailpageheadcontent">
                            <div class="tg-companylogo">
                                <img src="{{ $offerJob->user->photo_selected_pdf }}" alt="image description">
                            </div>
                            <div class="tg-companycontent">
                                <div class="tg-tags">
                                    <span class="tg-tag">{{ $offerJob->job_type }}</span>
                                </div>
                                <div class="tg-title">
                                    <h1 style="font-size: 30px; font-family: 'Work Sans', Arial, Helvetica, sans-serif;">{{ $offerJob->title }}</h1>
                                    <span class="tg-jobpostedby">{{ $offerJob->user->name }}</span>
                                </div>
                            </div>
                        </div>
                        <table class="tg-jobmatadata">
                            <tr>
                                <td class="tg-box tg-posteddate">
                                    <span class="tg-jobmataicon"><i class="lnr lnr-calendar-full"></i></span>
                                    <div class="tg-jobmatacontent">
                                        <strong>Fecha publicación</strong>
                                        <span>{{ $offerJob->publication_date }}</span>
                                    </div>
                                </td>
                                <td class="tg-box tg-applicantapplied">
                                    <span class="tg-jobmataicon"><i class="lnr lnr-book"></i></span>
                                    <div class="tg-jobmatacontent">
                                        <strong>Solicitantes</strong>
                                        <span>3479  solicitantes</span>
                                    </div>
                                </td>
                                <td class="tg-box tg-views">
                                    <span class="tg-jobmataicon"><i class="lnr lnr-eye"></i></span>
                                    <div class="tg-jobmatacontent">
                                        <strong>Vistas</strong>
                                        <span>Total vistas: 22508</span>
                                    </div>
                                </td>
                                <td class="tg-box tg-expiredate">
                                    <span class="tg-jobmataicon"><i class="lnr lnr-warning"></i></span>
                                    <div class="tg-jobmatacontent">
                                        <strong>Fecha de cierre</strong>
                                        <span>{{ $offerJob->expire_date_format }}</span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div id="tg-twocolumns" class="tg-twocolumns">
                <div class="container">
                    <table>
                        <tbody>
                            <tr>
                               <td style="width: 60%;">
                                   <div id="tg-content" class="tg-content">
                                       <div class="tg-companyfeatures">
                                           <div class="tg-companyfeaturebox">
                                               <div class="tg-companyfeaturetitle">
                                                   <h3>Descripción de la Solicitud</h3>
                                               </div>
                                               <div class="tg-description">
                                                   <span>{{ $offerJob->description }}</span>
                                               </div>
                                           </div>
                                           <div class="tg-companyfeaturebox">
                                               <div class="tg-companyfeaturetitle">
                                                   <h3>Detalles del trabajo</h3>
                                               </div>
                                               <table style="width: 100%; padding-bottom: 10px">
                                                   <tbody style="font-size: 14px;">
                                                   <tr>
                                                       <td style="width: 50%;"><span>Categoría profesional:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->category->name }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Profesional en:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->subcategory->name }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Experiencia:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->experience }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Pago sugerido:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->suggested_payment }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Tipo de proridad:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->priority_type }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Tipo de empleo:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->job_type }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Horario preferible:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->schedule }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Movilidad geográfica:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->geographical_mobility }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Idioma preferible:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->language }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Departamento:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->department->name }}</span></td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 50%;"><span>Provincia:</span></td>
                                                       <td style="width: 50%;"><span>{{ $offerJob->province->name }}</span></td>
                                                   </tr>
                                                   @if($offerJob->district_id)
                                                       <tr>
                                                           <td style="width: 50%;"><span>Distrito:</span></td>
                                                           <td style="width: 50%;"><span>{{ $offerJob->district->name }}</span></td>
                                                       </tr>
                                                   @endif
                                                   </tbody>
                                               </table>
                                           </div>
                                           <div class="tg-companyfeaturebox">
                                               <div class="tg-companyfeaturetitle">
                                                   <h3>Requisitos del prestador de servicios</h3>
                                               </div>
                                               <div class="tg-description">
                                                   <span>{{ $offerJob->abilities }}</span>
                                               </div>
                                           </div>
                                           @if($offerJob->benefits->count() > 0)
                                               <div class="tg-companyfeaturebox tg-benefitsallowances">
                                                   <div class="tg-companyfeaturetitle">
                                                       <h3>Beneficios que se incluyen</h3>
                                                   </div>
                                                   @foreach ($offerJob->benefits->chunk(3) as $chunk)
                                                       <div class="row">
                                                           @foreach ($chunk as $benefit)
                                                               <div class="col-sm-4">{{ $benefit->name }}</div>
                                                           @endforeach
                                                       </div>
                                                   @endforeach
                                               </div>
                                           @endif
                                           @if($offerJob->images->count() > 0)
                                               <div class="tg-companyfeaturebox">
                                                   <div class="tg-companyfeaturetitle" style="margin-bottom: 15px">
                                                       <h3>Imágenes</h3>
                                                   </div>
                                                   <div class="tg-galleries">
                                                       @foreach ($offerJob->images->chunk(4) as $chunk)
                                                           <div class="row">
                                                               @foreach ($chunk as $image)
                                                                   <div class="col-sm-3">
                                                                       <div class="tg-gallery">
                                                                           <div class="tg-galleryimgbox">
                                                                               <figure>
                                                                                   <img src="{{ $image->image_url_pdf }}" alt="image description">
                                                                               </figure>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               @endforeach
                                                           </div>
                                                       @endforeach
                                                   </div>
                                               </div>
                                           @endif
                                       </div>
                                   </div>
                               </td>
                                <td style="width: 40%; vertical-align: top">
                                    <aside id="tg-sidebar" class="tg-sidebar">
                                        <div class="tg-widget tg-widgetlocationandcontactinfo">
                                            @switch($offerJob->status)
                                                @case(5)
                                                <span class="status-detail-info">{{ $offerJob->status_description }}</span>
                                                @break

                                                @case(4)
                                                <span class="status-detail-danger">{{ $offerJob->status_description }}</span>
                                                @break

                                                @case(3)
                                                <span class="status-detail-cyan">{{ $offerJob->status_description }}</span>
                                                @break

                                                @case(2)
                                                <span class="status-detail-purple">{{ $offerJob->status_description }}</span>
                                                @break

                                                @case(1)
                                                <span class="status-detail-success">{{ $offerJob->status_description }}</span>
                                                @break

                                                @default
                                                <span class="status-detail-secondary">{{ $offerJob->status_description }}</span>
                                            @endswitch
                                            <div class="tg-contactinfobox">
                                                <div class="tg-contactinfo">
                                                    @foreach($offerJob->user->addresses as $address)
                                                        <div>
                                                            <address>{{ $address->description_complete }}</address>
                                                        </div>
                                                    @endforeach
                                                    <div>
                                                        <i class="lnr lnr-phone-handset"></i>
                                                        <span>{{ $offerJob->user->mobile }}</span>
                                                    </div>
                                                    <div>
                                                        <i class="lnr lnr-phone"></i>
                                                        <span>{{ $offerJob->user->phone }}</span>
                                                    </div>
                                                    <div>
                                                        <i class="lnr lnr-apartment"></i>
                                                        <span>{{ $offerJob->user->company_name }}</span>
                                                    </div>
                                                    <div>
                                                        <i class="lnr lnr-envelope"></i>
                                                        <span>{{ $offerJob->user->email }}</span>
                                                    </div>
                                                    @if($offerJob->user->website)
                                                        <div>
                                                            <i class="lnr lnr-laptop"></i>
                                                            <span>{{ $offerJob->user->website }}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                {{--<div class="tg-socialicons">--}}
                                                    {{--<div class="tg-facebook"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->facebooklink : '' }}"><i class="fa fa-facebook"></i></a></div>--}}
                                                    {{--<div class="tg-twitter"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->twitterlink : '' }}"><i class="fa fa-twitter"></i></a></div>--}}
                                                    {{--<div class="tg-linkedin"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->linkedinlink : '' }}"><i class="fa fa-linkedin"></i></a></div>--}}
                                                    {{--<div class="tg-skype"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->skypelink : '' }}"><i class="fa fa-skype"></i></a></div>--}}
                                                    {{--<div class="tg-googleplus"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->googlepluslink : '' }}"><i class="fa fa-google-plus"></i></a></div>--}}
                                                    {{--<div class="tg-pinterestp"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->pinterestlink : '' }}"><i class="fa fa-pinterest-p"></i></a></div>--}}
                                                {{--</div>--}}
                                                {{--<a class="tg-btn tg-btn-lg" href="javascript:void(0);">get direction</a>--}}
                                            </div>
                                        </div>
                                        {{--<div class="tg-widget tg-widgetshare">--}}
                                            {{--<div class="tg-widgettitle">--}}
                                                {{--<h3>Comparte este trabajo</h3>--}}
                                            {{--</div>--}}
                                            {{--<div class="tg-widgetcontent">--}}
                                                {{--<ul class="tg-socialicons">--}}
                                                    {{--<li class="tg-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
                                                    {{--<li class="tg-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
                                                    {{--<li class="tg-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
                                                    {{--<li class="tg-googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                                                    {{--<li class="tg-rss"><a href="#"><i class="fa fa-rss"></i></a></li>--}}
                                                    {{--<li class="tg-vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li>--}}
                                                    {{--<li class="tg-tumblr"><a href="#"><i class="fa fa-tumblr"></i></a></li>--}}
                                                    {{--<li class="tg-yahoo"><a href="#"><i class="fa fa-yahoo"></i></a></li>--}}
                                                    {{--<li class="tg-yelp"><a href="#"><i class="fa fa-yelp"></i></a></li>--}}
                                                    {{--<li class="tg-pinterestp"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>--}}
                                                    {{--<li class="tg-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>--}}
                                                    {{--<li class="tg-stumbleupon"><a href="#"><i class="fa fa-stumbleupon"></i></a></li>--}}
                                                    {{--<li class="tg-reddit"><a href="#"><i class="fa fa-reddit"></i></a></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="tg-widget tg-widgetrelatedjobs">
                                            <div class="tg-widgettitle">
                                                <h3>Trabajos relacionados</h3>
                                            </div>
                                            <div class="tg-widgetcontent">
                                                <div>
                                                    <div style="width: 100%; margin-bottom: 35px">
                                                        <div class="tg-serviceprovidercontent">
                                                            <div class="tg-companylogo"><img src="images/logos/img-01.png" alt="image description"></a></div>
                                                            <div class="tg-companycontent">
                                                                <div class="tg-tags">
                                                                    <span class="tg-tag">fix</span>
                                                                </div>
                                                                <div class="tg-title">
                                                                    <span class="tg-jobpostedby">Carpenter Required</span>
                                                                    <span class="tg-jobpostedby">By: Blue Bird Organization</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%; margin-bottom: 35px">
                                                        <div class="tg-serviceprovidercontent">
                                                            <div class="tg-companylogo"><img src="images/logos/img-02.png" alt="image description"></a></div>
                                                            <div class="tg-companycontent">
                                                                <div class="tg-tags">
                                                                    <span class="tg-tag">Internship</span>
                                                                </div>
                                                                <div class="tg-title">
                                                                    <span class="tg-jobpostedby">Electrician For Fixing Required</span>
                                                                    <span class="tg-jobpostedby">By: On Fleek Spa &amp; Beauty Saloon</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%; margin-bottom: 35px">
                                                        <div class="tg-serviceprovidercontent">
                                                            <div class="tg-companylogo"><img src="images/logos/img-03.png" alt="image description"></a></div>
                                                            <div class="tg-companycontent">
                                                                <div class="tg-tags">
                                                                    <span class="tg-tag">Temporary</span>
                                                                </div>
                                                                <div class="tg-title">
                                                                    <span class="tg-jobpostedby">Car Fixing Mechanic Required</span>
                                                                    <span class="tg-jobpostedby">By: Eldridge Hemenway</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
<!--Main End-->
</div>
<!--Wrapper End-->
</body>
</html>