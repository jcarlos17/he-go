@extends('layouts.app')

@section('styles')
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Editar oferta de trabajo</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Editar oferta de trabajo</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                @include('user.include.menu')
                <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprofilesetting">
                                <div id="formOfferJob" class="tg-themeform">
                                    {{ csrf_field() }}
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-basicinformation">
                                            <div class="tg-dashboardtitle">
                                                <h2>Nueva solicitud de trabajo</h2>
                                            </div>
                                            <div id="alert-type"></div>
                                            <div class="tg-basicinformationbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="tg-radio" style="padding-bottom: 5px">
                                                            <input class="services" type="radio" id="1" name="type" value="Presupuesto" {{ $offer->type == 'Presupuesto' ? 'checked' : '' }}>
                                                            <label for="1">Necesito presupuestos</label>
                                                        </div>
                                                        <p style="width: 60%">Si su solicitud esta basado en un proyecto o idea que requiera mano de obra, materiales, herramientas y maquinaria. debe marcar esta opción </p>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="tg-radio" style="padding-bottom: 5px">
                                                            <input class="services" type="radio" id="2" name="type" value="Carta" {{ $offer->type == 'Carta' ? 'checked' : '' }}>
                                                            <label for="2">Necesito carta de presentación</label>
                                                        </div>
                                                        <p style="width: 70%">Si su solicitud consiste en buscar personal para cubrir un puesto vacante o el desarrollo de alguna actividad o servicio. Debe marcar esta opción.</p>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="form-group">
                                                            <span style="margin: auto 0">Indicar un límite de respuestas a recibir (opcional):</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="limit" id="select-limit">
                                                                <option value="">Seleccione un límite</option>
                                                                @for($i=1; $i<=20; $i++)
                                                                    <option value="{{$i}}" {{ $i == $offer->limit ? 'selected' : ''}}>{{$i}}</option>
                                                                @endfor
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-basicinformation">
                                            <div class="tg-dashboardtitle">
                                                <h2>Descripción de la Solicitud</h2>
                                            </div>
                                            <div id="alert-description"></div>
                                            <div class="tg-basicinformationbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <label for="title">Título del trabajo</label>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="titleOffer" name="title" value="{{ $offer->title }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <label for="expireDateOffer">Fecha caducidad</label>
                                                            <div class="tg-inpuicon">
                                                                <i class="lnr lnr-calendar-full"></i>
                                                                <input id="expireDateOffer" type="date" name="expire_date" class="form-control" value="{{ $offer->expire_date }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <label for="end_date">Descripción</label>
                                                        <div class="form-group">
                                                            <textarea class="form-control" id="descriptionOffer" name="description" rows="2"
                                                          placeholder="Explica brevemente el objetivo del trabajo o servicio que queremos alcanzar"
                                                            >{{ $offer->description }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-introduction">
                                            <div class="tg-dashboardtitle">
                                                <h2>Habilidades del Prestador de Servicios</h2>
                                            </div>
                                            <div id="alert-abilities"></div>
                                            <div class="form-group">
                                                    <textarea class="form-control" id="abilitiesOffer" name="abilities" rows="2"
                                                              placeholder="Aquí debes explicar brevemente qué habilidades, experiencia, equipo o conocimientos debe cumplir el profesional"
                                                    >{{ $offer->abilities }}</textarea>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-uploadphotos">
                                            <div class="tg-dashboardtitle">
                                                <h2>Cargar imagen (Una imagen vale más que mil palabras)</h2>
                                            </div>
                                            <div class="tg-checkbox">
                                                <input class="services" type="checkbox" id="image_option" name="attached_images" {{ $offer->attached_images ? 'checked' : '' }}>
                                                <label for="image_option">Quiero adjuntar imágenes a esta solicitud</label>
                                            </div>
                                            <div class="tg-uploadbox" id="content-images" style="display: {{ $offer->attached_images ? '' : 'none' }}">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="alert-images"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
																	<span>
																		<h3>Cargar imagen (Una imagen vale más que mil palabras)</h3>
																		<i class="fa fa-exclamation-circle"></i>
																	</span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputPhoto">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formPhoto" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputPhoto" class="tg-fileinput" type="file" name="image" accept="image/*" multiple>
                                                                        <input type="hidden" name="id" id="offerId" value="{{ $offer->id }}">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedPhotos" class="tg-galleryimages">
                                                                        @foreach ($offer->images as $image)
                                                                            <div id="itemPhoto" class="tg-galleryimg">
                                                                                <figure>
                                                                                    <img src="{{ $image->image_url }}" alt="{{ $offer->id }}" style="width: 80px; height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-selectphoto="{{ url('publicar-oferta-trabajo/'.$offer->id.'/image/'.$image->id.'/select') }}"><i class="fa fa-check"></i></a>
                                                                                        <a data-deletephoto="{{ url('publicar-oferta-trabajo/image/'.$image->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                    <template id="templatePhoto">
                                                                        <div id="itemPhoto" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="width: 80px; height: 80px">
                                                                                <figcaption>
                                                                                    <a data-selectphoto=""><i class="fa fa-check"></i></a>
                                                                                    <a data-deletephoto=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templatePhotoLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-basicinformation">
                                            <div class="tg-dashboardtitle">
                                                <h2>Detalles del trabajo</h2>
                                            </div>
                                            <div id="alert-detail"></div>
                                            <div class="tg-basicinformationbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="category_id" id="select-category">
                                                                <option value="">Seleccione categoría profesional</option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{ $category->id }}" {{ $offer->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <span class="tg-select">
                                                            <select name="subcategory_id" id="select-subcategory">
                                                                <option value="">Profesional en...</option>
                                                                @if($offer->category_id)
                                                                    @foreach($offer->category->subcategories as $subcategory)
                                                                        <option value="{{ $subcategory->id }}" {{ $offer->subcategory_id == $subcategory->id ? 'selected' : '' }}>{{ $subcategory->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="priority_type" id="priorityType">
                                                                <option value="">Seleccione tipo de prioridad</option>
                                                                <option value="Busco informacion de precios" {{ $offer->priority_type == 'Busco informacion de precios' ? 'selected' : '' }}>Busco informacion de precios</option>
                                                                <option value="Esta en proyecto" {{ $offer->priority_type == 'Esta en proyecto' ? 'selected' : '' }}>Esta en proyecto</option>
                                                                <option value="Proyecto iniciado" {{ $offer->priority_type == 'Proyecto iniciado' ? 'selected' : '' }}>Proyecto iniciado</option>
                                                                <option value="Para una fecha concreta" {{ $offer->priority_type == 'Para una fecha concreta' ? 'selected' : '' }}>Para una fecha concreta</option>
                                                                <option value="Muy urgente" {{ $offer->priority_type == 'Muy urgente' ? 'selected' : '' }}>Muy urgente</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="schedule" id="schedule">
                                                                <option value="">Seleccione horario preferible</option>
                                                                <option value="Mañana" {{ $offer->schedule == 'Mañana' ? 'selected' : '' }}>Mañana</option>
                                                                <option value="Tarde" {{ $offer->schedule == 'Tarde' ? 'selected' : '' }}>Tarde</option>
                                                                <option value="Noche" {{ $offer->schedule == 'Noche' ? 'selected' : '' }}>Noche</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="job_type" id="jobType">
                                                                <option value="">Seleccione tipo de empleo</option>
                                                                <option value="Freelance" {{ $offer->job_type == 'Freelance' ? 'selected' : '' }}>Freelance</option>
                                                                <option value="Eventual" {{ $offer->job_type == 'Eventual' ? 'selected' : '' }}>Eventual</option>
                                                                <option value="Por horas" {{ $offer->job_type == 'Por horas' ? 'selected' : '' }}>Por horas</option>
                                                                <option value="Bajo modalidad" {{ $offer->job_type == 'Bajo modalidad' ? 'selected' : '' }}>Bajo modalidad</option>
                                                                <option value="Por proyecto" {{ $offer->job_type == 'Por proyecto' ? 'selected' : '' }}>Por proyecto</option>
                                                                <option value="A convenir" {{ $offer->job_type == 'A convenir' ? 'selected' : '' }}>A convenir</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="geographical_mobility" id="geographicalMobility">
                                                                <option value="">Seleccione movilidad geográfica</option>
                                                                <option value="Se requiere desplazamiento nacional" {{ $offer->geographical_mobility == 'Se requiere desplazamiento nacional' ? 'selected' : '' }}>Se requiere desplazamiento nacional</option>
                                                                <option value="En una única zona" {{ $offer->geographical_mobility == 'En una única zona' ? 'selected' : '' }}>En una única zona</option>
                                                                <option value="No" {{ $offer->geographical_mobility == 'No' ? 'selected' : '' }}>No</option>
                                                                <option value="A convenir" {{ $offer->geographical_mobility == 'A convenir' ? 'selected' : '' }}>A convenir</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="experience" id="experience">
                                                                <option value="">Seleccione experiencia</option>
                                                                <option value="Estudiante" {{ $offer->experience == 'Estudiante' ? 'selected' : '' }}>Estudiante</option>
                                                                <option value="No se requiere experiencia" {{ $offer->experience == 'No se requiere experiencia' ? 'selected' : '' }}>No se requiere experiencia</option>
                                                                <option value="Menos de 1 año" {{ $offer->experience == 'Menos de 1 año' ? 'selected' : '' }}>Menos de 1 año</option>
                                                                <option value="Menos de 2 años" {{ $offer->experience == 'Menos de 2 años' ? 'selected' : '' }}>Menos de 2 años</option>
                                                                <option value="Menos de 4 años" {{ $offer->experience == 'Menos de 4 años' ? 'selected' : '' }}>Menos de 4 años</option>
                                                                <option value="Más de 5 años" {{ $offer->experience == 'Más de 5 años' ? 'selected' : '' }}>Más de 5 años</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="language" id="language">
                                                                <option value="">Idioma preferible</option>
                                                                <option value="Español" {{ $offer->language == 'Español' ? 'selected' : '' }}>Español</option>
                                                                <option value="Inglés" {{ $offer->language == 'Inglés' ? 'selected' : '' }}>Inglés</option>
                                                                <option value="Árabe" {{ $offer->language == 'Árabe' ? 'selected' : '' }}>Árabe</option>
                                                                <option value="Portugués" {{ $offer->language == 'Portugués' ? 'selected' : '' }}>Portugués</option>
                                                                <option value="Ruso" {{ $offer->language == 'Ruso' ? 'selected' : '' }}>Ruso</option>
                                                                <option value="Japonés" {{ $offer->language == 'Japonés' ? 'selected' : '' }}>Japonés</option>
                                                                <option value="Alemán" {{ $offer->language == 'Alemán' ? 'selected' : '' }}>Alemán</option>
                                                                <option value="Coreano" {{ $offer->language == 'Coreano' ? 'selected' : '' }}>Coreano</option>
                                                                <option value="Francés" {{ $offer->language == 'Francés' ? 'selected' : '' }}>Francés</option>
                                                                <option value="Turco" {{ $offer->language == 'Turco' ? 'selected' : '' }}>Turco</option>
                                                                <option value="Italiano" {{ $offer->language == 'Italiano' ? 'selected' : '' }}>Italiano</option>
                                                                <option value="Persa" {{ $offer->language == 'Persa' ? 'selected' : '' }}>Persa</option>
                                                                <option value="Rumano" {{ $offer->language == 'Rumano' ? 'selected' : '' }}>Rumano</option>
                                                                <option value="Holandés" {{ $offer->language == 'Holandés' ? 'selected' : '' }}>Holandés</option>
                                                                <option value="Griego" {{ $offer->language == 'Griego' ? 'selected' : '' }}>Griego</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <span class="tg-select">
                                                                <select name="department_id" id="select_department" required>
                                                                    <option value="">Seleccione departamento</option>
                                                                    @foreach ($departments as $department)
                                                                        <option value="{{ $department->id }}" {{ $offer->department_id == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <span class="tg-select">
                                                                <select name="province_id" id="select_province" required>
                                                                    <option value="">Seleccione provincia</option>
                                                                    @if($offer->department_id)
                                                                        @foreach ($offer->department->provinces as $province)
                                                                            <option value="{{ $province->id }}" {{ $offer->province_id == $province->id ? 'selected' : '' }}>{{ $province->name }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <span class="tg-select">
                                                                <select name="district_id" id="select_district" required>
                                                                    <option value="">Seleccione distrito</option>
                                                                    @if($offer->district_id)
                                                                        @foreach ($offer->province->districts as $district)
                                                                            <option value="{{ $district->id }}" {{ $offer->district_id == $district->id ? 'selected' : '' }}>{{ $district->name }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="suggested_payment" id="suggestedPayment">
                                                                <option value="">Pago sugerido</option>
                                                                <option value="Cantidad fija" {{ $offer->suggested_payment == 'Cantidad fija' ? 'selected' : '' }}>Cantidad fija</option>
                                                                <option value="A convenir" {{ $offer->suggested_payment == 'A convenir' ? 'selected' : '' }}>A convenir</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-amenitiesfeatures">
                                            <div class="tg-dashboardtitle">
                                                <h2>Beneficios que se incluyen</h2>
                                            </div>
                                            <div id="alert-benefit"></div>
                                            <div class="tg-amenitiesfeaturesbox">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="nameBenefit" name="name" placeholder="Ingrese sus beneficios" value="">
                                                    <button class="tg-btn" data-benefit="{{ url('/publicar-oferta-trabajo/'.$offer->id.'/benefit/add') }}">Agregar</button>
                                                </div>
                                                <ul id="add-benefits" class="tg-tagdashboardlist">
                                                    @foreach($offer->benefits as $benefit)
                                                        <li>
                                                            <span class="tg-tagdashboard">
                                                                <a data-delete="{{ url('publicar-oferta-trabajo/benefit/'.$benefit->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                <em>{{ $benefit->name }}</em>
                                                            </span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-socialinformation">
                                            <div class="form-group text-center">
                                                <button data-updateoffer="{{ url('oferta-trabajo/'.$offer->id.'/edit?save=1') }}" class="tg-btn">Guardar cambios</button>
                                                @if($offer->status == 'Borrador')
                                                    <button data-updateoffer="{{ url('oferta-trabajo/'.$offer->id.'/edit?save=0') }}" class="tg-btn" style="background-color: #ec971f; z-index: auto">Guardar como borrador</button>
                                                @endif
                                                <button onclick="window.location ='/gestionar-trabajos'" class="tg-btn-danger">Cancelar</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        $('#formOfferJob').on('change keyup keydown paste cut', 'textarea', function () {
            $(this).height(0).height(this.scrollHeight);
        }).find('textarea').change();
    </script>
    <script src="{{ asset('js/offer-job/image.js') }}"></script>
    <script src="{{ asset('js/offer-job/benefit.js') }}"></script>
    <script src="{{ asset('js/offer-job/offer.js') }}"></script>
    <script>
        $(function() {
            $('#select_department').on('change', onSelectDepartmentChange);
            $('#select_province').on('change', onSelectProvinceChange);
            $('#select-category').on('change', onSelectCategoryChange);
            $('#image_option').on('change', showSectionImage);
        });

        function showSectionImage() {
            let contentImage = $('#content-images');
            if($(this).is(":checked")) {
                contentImage.show()
            } else {
                contentImage.hide()
            }
        }

        function onSelectDepartmentChange() {
            var department_id = $(this).val();
            if (! department_id) {
                $('#select_province').html('<option value="">Seleccione provincia</option>');
                return;
            }
            if(department_id == 7){
                $('#select_district').attr('required', false);
                $('#select_district').html('<option value="">No aplica</option>');
            }else{
                $('#select_district').attr('required', true);
                $('#select_district').html('<option value="">Seleccione distrito</option>');
            }
            // AJAX
            $.get('/api/department/'+department_id+'/provinces', function (data) {
                var html_select = '<option value="">Seleccione provincia</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_province').html(html_select);
            });
        }

        function onSelectProvinceChange() {
            var province_id = $(this).val();

            if (! province_id) {
                $('#select-district').html('<option value="">Seleccione distrito</option>');
                return;
            }
            if($('#select_department').val() == 7){
                $('#select_district').html('<option value="">No aplica</option>');
            }else{
                // AJAX
                $.get('/api/province/'+province_id+'/districts', function (data) {
                    var html_select = '<option value="">Seleccione distrito</option>';
                    for (var i=0; i<data.length; ++i)
                        html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                    $('#select_district').html(html_select);
                });
            }
        }

        function onSelectCategoryChange() {
            var category_id = $(this).val();

            if (! category_id) {
                $('#select-subcategory').html('<option value="">Profesional en...</option>');
                return;
            }

            // AJAX
            $.get('/api/category/'+category_id+'/subcategories', function (data) {
                var html_select = '<option value="">Profesional en...</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select-subcategory').html(html_select);
            });
        }
    </script>
@endsection
