@extends('layouts.app')

@section('styles')
    <link href="{{ asset('/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Categorías</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Categorías</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Categorías</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Inner Page Banner Start-->
    <div class="tg-mapinnerbanner">
        <div class="tg-searchbox">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-lg-push-1">
                        <form class="tg-themeform tg-formsearch">
                            <fieldset>
                                <div class="form-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="Keyword">
                                </div>
                                <div class="form-group tg-inputwithicon">
                                    <div class="locate-me-wrap">
                                        <div id="location-pickr-map" class="elm-display-none"></div>
                                        <input type="text"  autocomplete="on" id="location-address" value="" name="geo_location" placeholder="Geo location" class="form-control">
                                        <a href="javascript:;" class="geolocate"><i class="fa fa-crosshairs"></i></a>
                                        <a href="javascript:;" class="geodistance"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <div class="geodistance_range elm-display-none">
                                            <div class="distance-ml">Distance in&nbsp;( Miles )<span> 50</span></div>
                                            <input type="hidden" name="geo_distance" value="50" class="geo_distance" />
                                            <div class="geo_distance" id="geo_distance"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
										<span class="tg-select">
											<select>
												<option value="Not Selected">Category</option>
												<option value="Automotive">Automotive</option>
												<option value="Beauty &amp; Personal Care">Beauty &amp; Personal Care</option>
												<option value="Business">Business</option>
												<option value="Child Care">Child Care</option>
												<option value="Cleaning">Cleaning</option>
												<option value="Computer &amp; Mobile Service">Computer &amp; Mobile Service</option>
												<option value="Food &amp; Drinks">Food &amp; Drinks</option>
												<option value="General Labor">General Labor</option>
												<option value="Construction &amp; Renovation">Construction &amp; Renovation</option>
												<option value="Health &amp; Fitness">Health &amp; Fitness</option>
												<option value="Pet Care">Pet Care</option>
												<option value="Traning &amp; Education">Traning &amp; Education</option>
												<option value="Transportation &amp; Travel">Transportation &amp; Travel</option>
											</select>
										</span>
                                </div>
                                <button class="tg-btn" type="submit"><i class="lnr lnr-magnifier"></i></button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="tg-map" class="tg-map"></div>
        <div class="tg-mapcontrols">
            <span id="doc-mapplus"><i class="fa fa-plus"></i></span>
            <span id="doc-mapminus"><i class="fa fa-minus"></i></span>
            <span id="doc-lock"><i class="fa fa-lock"></i></span>
            <span id="tg-geolocation"><i class="fa fa-crosshairs"></i></span>
        </div>
        <a id="tg-btnmapview" class="tg-btn tg-btnmapview" href="#">
            <span>Map View</span>
            <i class="lnr lnr-chevron-down"></i>
        </a>
    </div>
    <!--Inner Page Banner End-->

    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout" style="padding-bottom: 80px">
        <div class="container">
            <div class="row">
                <div id="tg-content" class="tg-content">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-widget tg-widgetcontact" style="padding: 40px 0">
                            <div class="tg-widgettitle">
                                <h3>Buscar por:</h3>
                            </div>
                            <div class="tg-widgetcontent">
                                <form class="tg-themeform" method="GET" action="{{ url('providers') }}">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="id" placeholder="ID">
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="username" placeholder="Nombre de usuario">
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <span class="tg-select" style="text-transform: none">
                                                        <input type="hidden" name="subcategory_id" class="form-control" id="select-subcategory">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <span class="tg-select">
                                                        <select name="category_id" id="select-category">
                                                            <option value="">Categoría</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="tg-btn tg-btn-lg" type="submit" href="javascript:void(0)">Buscar</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tg-categories tg-categoriesgrid">
                        @foreach ($categories as $category)
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                            <div class="tg-category">
                                <figure>
                                    <img src="{{ $category->image_url }}" alt="{{ $category->name }}">
                                    <figcaption class="tg-automotive">
                                        <span class="{{$category->icon->code}} tg-categoryicon"></span>
                                        <span class="tg-categoryname">{{ $category->name }}</span>
                                        <a href="{{ url(('/categories/'.$category->slug)) }}" class="tg-themetag tg-tagjobcount">
                                            <i class="fa fa-clone"></i>
                                            <em>322</em>
                                        </a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @include('pagination.default', ['paginator' => $categories])
                </div>
            </div>
            <hr style="border: 1px solid #5dc560; margin: 30px 0">
            <div class="row">
                <div class="col-sm-4 text-center">
                    <img src="{{ asset('images/hombre-con-letrero.png') }}" alt="Propuesta de categorias">
                </div>
                <div class="col-sm-8">
                    <label>Si tu trabajo pertenece a una categoría que aún no la tenemos registrada. ¡tranquilo! aquí tienes un espacio
                    para solicitarlo y pronto lo tendrás disponible.</label>

                    <label for="new_category">Qué categoría propones:</label>
                    <input id="new_category" name="new_category" type="text">
                    <div id="formNewCategory">
                        {{ csrf_field() }}
                        <label for="description" style="margin-top: 10px">Explícanos el área de trabajo:</label>
                        <div class="form-group">
                            <textarea class="form-control" id="description" name="description" rows="2"></textarea>
                        </div>
                        <div class="form-group">
                            <button data-send="" class="tg-btn pull-right">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        $('#formNewCategory').on('change keyup keydown paste cut', 'textarea', function () {
            $(this).height(0).height(this.scrollHeight);
        }).find('textarea').change();
    </script>
    <script src="{{ asset('/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
    <script>
        $('#select-subcategory').select2({
            placeholder: 'Busca aquí: pintor, niñera, limpieza de casa…',
            ajax: {
                url: '/api/subcategories',
                dataType: 'json',
                delay: 250,

                data: function (query) {
                    return {
                        queryText: query
                    };
                },

                results: function (data) {
                    return {
                        results: data
                    };
                },

                cache: true
            }
        });
    </script>
    <script>
        $(function() {
            $('#select-subcategory').on('change', onSelectCategory);
        });

        function onSelectCategory() {
            var subcategory_id = $(this).val();
            // AJAX
            $.get('/api/subcategories/'+subcategory_id+'/category', function (data) {
                var html_select = '<option value="'+data.id+'" selected>'+data.name+'</option>';
                $('#select-category').html(html_select);
            });
        }
    </script>
    <script>
        jQuery(document).ready(function(e) {
            sp_init_map_script('tg-map');
        });
    </script>
@endsection