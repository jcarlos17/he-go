@extends('layouts.app')
@section('styles')
    <style>
        .tg-checkbox input[type=checkbox]:checked + label:before, .tg-checkbox input[type=checkbox]:checked + label del {
            color: #5dc560;
        }
    </style>
@endsection
@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Mi perfil público</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Mi perfil público</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Mi perfil público</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-haslayout tg-paddingzero">
        <div class="tg-serviceprovider tg-detailpage tg-serviceproviderdetail">
            <div class="tg-detailpagehead">
                <figure>
                    @if($user->verifyPrivacity(2, $user->id))
                        <img src="{{ $user->banner_selected }}" alt="image description">
                    @else
                        <div style="width: 1350px; height: 210px; background-color: #2b2b2b;"></div>
                    @endif
                    <figcaption style="padding: 40px 0;">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="tg-detailpageheadcontent">
                                        @if($user->verifyPrivacity(1, $user->id))
                                            <div class="tg-companylogo">
                                                <img src="{{ $user->photo_selected }}" alt="{{ $user->name }}">
                                            </div>
                                        @endif
                                        @if($user->is_provider)
                                            <div class="medal-lv-one">
                                                <div class="circle-transparent"></div>
                                                <img src="{{ asset('images/levels/level_1_color.png') }}" alt="Nivel 01: Principiante">
                                                <br>
                                                <small>Principiante</small>
                                            </div>
                                            <div class="medal-lv-two">
                                                <div class="circle-transparent"></div>
                                                {{--<img src="{{ asset('images/levels/level_2_color.png') }}" alt="Nivel 02: Bronce">--}}
                                                {{--<br>--}}
                                                {{--<small>Bronce</small>--}}
                                            </div>
                                            <div class="medal-lv-three">
                                                <div class="circle-transparent"></div>
                                                {{--<img src="{{ asset('images/levels/level_3_color.png') }}" alt="Nivel 03: Plata - Experto">--}}
                                                {{--<br>--}}
                                                {{--<small>Plata - Experto</small>--}}
                                            </div>
                                            <div class="medal-lv-four">
                                                <div class="circle-transparent"></div>
                                                {{--<img src="{{ asset('images/levels/level_4_color.png') }}" alt="Nivel 04: Master - Gold">--}}
                                                {{--<br>--}}
                                                {{--<small>Master - Gold</small>--}}
                                            </div>
                                            <div class="medal-lv-five">
                                                <div class="circle-transparent"></div>
                                                {{--<img src="{{ asset('images/levels/level_5_color.png') }}" alt="Nivel 05: Pro Master Top">--}}
                                                {{--<br>--}}
                                                {{--<small>Pro Master Top</small>--}}
                                            </div>
                                        @endif
                                        <div class="tg-companycontent">
                                            <ul class="tg-tags">
                                                <li><a class="tg-tag tg-featuredtag" href="#">Destacado</a></li>
                                                <li><a class="tg-tag tg-verifiedtag" href="#">Verificado</a></li>
                                            </ul>
                                            <div class="tg-title">
                                                <h1>{{ $user->name }}</h1>

                                                <span>{{ $user->only_company_name }}</span>

                                            </div>
                                            <ul class="tg-matadata">
                                                <li><span class="tg-stars"><span></span></span></li>
                                                <li>
                                                    <i class="fa fa-thumbs-o-up"></i>
                                                    <em>99% (1009 votes)</em>
                                                </li>
                                            </ul>
                                        </div>
                                        {{--<button class="tg-btn" type="buttton" data-toggle="modal" data-target=".tg-appointmentModal">Make Appointment</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </figcaption>
                </figure>
            </div>
            <div class="tg-companynameandviews">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h2>Mecánica y talleres</h2>
                            <span class="tg-totalsviews">
                                <i class="fa fa-eye"></i>
                                <i>126709</i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tg-twocolumns" class="tg-twocolumns">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            @if(auth()->check() && auth()->user() != $user && (auth()->user()->is_provider || $user->is_provider))
                                <form method="POST" action="{{ url('chat/new') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="to_id" value="{{$user->id}}">
                                    <button class="tg-chat"><i class="fa fa-comments"></i>Iniciar chat</button>
                                </form>
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if(auth()->check() && auth()->user() != $user)
                                @if($favoriteExists)
                                    <button class="tg-favorite" id="btnFavorite"><i class="fa fa-heart"></i>Agregado a favoritos</button>
                                @else
                                    <button class="tg-favorite" id="btnFavorite" data-favorite="{{ url('favorite/'.$user->id.'/add') }}"><i class="fa fa-heart-o"></i>Agregar a favoritos</button>
                                @endif
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if(auth()->check() && auth()->id() != $user->id && $user->verifyPrivacity(3, $user->id) && $user->is_provider)
                            <button class="tg-appointment" data-toggle="modal" data-target="#modalAppointmentOne"><i class="fa fa-calendar"></i>Reservar cita</button>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 pull-left">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-advertisement">
                                    @if(auth()->check() && auth()->user()->is_provider)
                                    @if($imgTopPublicProfile)
                                        <a href="javascript:void(0);"><img src="{{ $imgTopPublicProfile->image_url }}" alt="image description"></a>
                                    {{--@else--}}
                                        {{--<a href="javascript:void(0);"><img src="{{ asset('images/advertisement/img-01.png') }}" alt="image description"></a>--}}
                                    @endif
                                    @else
                                        @if($imgTopPublicProfileBS)
                                            <a href="javascript:void(0);"><img src="{{ $imgTopPublicProfileBS->image_url }}" alt="image description"></a>
                                        {{--@else--}}
                                            {{--<a href="javascript:void(0);"><img src="{{ asset('images/advertisement/img-01.png') }}" alt="image description"></a>--}}
                                        @endif
                                    @endif
                                </div>
                                <div class="tg-companyfeatures tg-listfeatures">
                                    @if($user->is_provider)
                                        {{--Carta de presentacion--}}
                                        <div class="tg-companyfeaturebox tg-introduction">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Carta de presentación</h3>
                                            </div>
                                            <div class="tg-description">
                                                <p>{!! nl2br($user->presentation)  !!}</p>
                                            </div>
                                        </div>
                                        {{--Categorías de trabajo--}}
                                        <div class="tg-companyfeaturebox tg-services">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Categorías de trabajo</h3>
                                            </div>
                                            <div id="tg-accordion" class="tg-accordion">
                                                @foreach($userCategories as $category)
                                                    <div class="tg-service tg-panel">
                                                        <div class="tg-accordionheading"><h4><span>{{ $category->name }}</span></h4></div>
                                                        <div class="tg-panelcontent">
                                                            <div class="tg-description">
                                                                @foreach($category->userSubcategories as $subcategory)
                                                                    <p style="margin-bottom: 5px; font-size: 12px">
                                                                        {{ $subcategory->name }}
                                                                    </p>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @if($user->verifyPrivacity(6, $user->id))
                                        {{--Mis servicios--}}
                                        <div class="tg-companyfeaturebox tg-services">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Mis servicios</h3>
                                            </div>
                                            <div id="tg-accordion" class="tg-accordion">
                                                @foreach($user->services as $service)
                                                    <div class="tg-service tg-panel">
                                                        <div class="tg-accordionheading"><h4><span>{{ $service->title }}</span><span>{{ $service->price_value }}</span></h4></div>
                                                        <div class="tg-panelcontent">
                                                            <div class="tg-description">
                                                                <p>{{ $service->description }}</p>
                                                            </div>
                                                            @if(auth()->check() && auth()->id() != $user->id && $user->verifyPrivacity(3, $user->id) && $user->is_provider)
                                                                <button class="tg-appointment pull-right" data-toggle="modal" data-target="#modalAppointmentOne" style="margin: 0; line-height: 30px">Contratar este servicio</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        {{--Mis servicios incluyen--}}
                                        <div class="tg-companyfeaturebox tg-amenities">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Mis servicios incluyen</h3>
                                            </div>
                                            <ul>
                                                @foreach($user->service_features as $key => $service_feature)
                                                    <li class="tg-activated">
                                                        <i class="lnr {{ $service_feature->icon }}"></i>
                                                        <span>{{ $service_feature->name }}</span>
                                                    </li>
                                                    @if(($key+1)%3 == 0)
                                                        <div class="clearfix"></div>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        {{--Preparación profesional--}}
                                        <div class="tg-companyfeaturebox tg-qaulifications">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Preparación profesional</h3>
                                            </div>
                                            <div id="tg-accordion" class="tg-accordion">
                                                @foreach($user->qualifications as $qualification)
                                                    <div class="tg-service tg-panel">
                                                        <div class="tg-accordionheading"><h4><span>{{ $qualification->title }}</span><span>{{ $qualification->date_interval }}</span><em>{{ $qualification->institution }}</em></h4></div>
                                                        <div class="tg-panelcontent">
                                                            <div class="tg-description">
                                                                <p>{{ $qualification->description }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        {{--Experiencia--}}
                                        <div class="tg-companyfeaturebox tg-experience">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Experiencia</h3>
                                            </div>
                                            <div id="tg-accordion" class="tg-accordion">
                                                @foreach($user->experiences as $experience)
                                                    <div class="tg-service tg-panel">
                                                        <div class="tg-accordionheading"><h4><span>{{ $experience->title }}</span><span>{{ $experience->date_interval }}</span><em>{{ $experience->company }}</em></h4></div>
                                                        <div class="tg-panelcontent">
                                                            <div class="tg-description">
                                                                <p>{{ $experience->description }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        {{--Certificados y premios--}}
                                        <div class="tg-companyfeaturebox tg-certicicatesawards">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Certificados y premios</h3>
                                            </div>
                                            <ul>
                                                @foreach($user->certificates as $certificate)
                                                    <li>
                                                        <figure><img src="{{ $certificate->image_url }}" alt="{{ $certificate->title }}"></figure>
                                                        <div class="tg-textbox">
                                                            <h4>{{ $certificate->title }}</h4>
                                                            <time datetime="2016-08-08">{{ $certificate->date }}</time>
                                                            <div class="tg-description">
                                                                <p>{{ $certificate->description }}</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        {{--Idiomas--}}
                                        <div class="tg-companyfeaturebox tg-languages">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Idiomas</h3>
                                            </div>
                                            <ul class="tg-themeliststyle tg-themeliststyledisc">
                                                @foreach($user->languages as $language)
                                                    <li>{{ $language->language }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        {{--Nuestro equipo--}}
                                        @if($user->verifyPrivacity(7, $user->id))
                                        <div class="tg-companyfeaturebox tg-ourteam">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Nuestro equipo</h3>
                                            </div>
                                            <ul class="tg-teammembers">
                                                <li>
                                                    <div class="tg-teammember">
                                                        <figure><a href="javascript:void(0);"><img src="{{ asset('images/teammembers/img-01.jpg') }}" alt="image description"></a></figure>
                                                        <div class="tg-memberinfo">
                                                            <h5><a href="#">Eldridge Hemenway</a></h5>
                                                            <a href="#">View Full Profile</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="tg-teammember">
                                                        <figure><a href="javascript:void(0);"><img src="{{ asset('images/teammembers/img-02.jpg') }}" alt="image description"></a></figure>
                                                        <div class="tg-memberinfo">
                                                            <h5><a href="#">Eldridge Hemenway</a></h5>
                                                            <a href="#">View Full Profile</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="tg-teammember">
                                                        <figure><a href="javascript:void(0);"><img src="{{ asset('images/teammembers/img-03.jpg') }}" alt="image description"></a></figure>
                                                        <div class="tg-memberinfo">
                                                            <h5><a href="#">Eldridge Hemenway</a></h5>
                                                            <a href="#">View Full Profile</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="tg-teammember">
                                                        <figure><a href="javascript:void(0);"><img src="{{ asset('images/teammembers/img-04.jpg') }}" alt="image description"></a></figure>
                                                        <div class="tg-memberinfo">
                                                            <h5><a href="#">Eldridge Hemenway</a></h5>
                                                            <a href="#">View Full Profile</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="tg-teammember">
                                                        <figure><a href="javascript:void(0);"><img src="{{ asset('images/teammembers/img-05.jpg') }}" alt="image description"></a></figure>
                                                        <div class="tg-memberinfo">
                                                            <h5><a href="#">Eldridge Hemenway</a></h5>
                                                            <a href="#">View Full Profile</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="tg-teammember">
                                                        <figure><a href="javascript:void(0);"><img src="{{ asset('images/teammembers/img-06.jpg') }}" alt="image description"></a></figure>
                                                        <div class="tg-memberinfo">
                                                            <h5><a href="#">Eldridge Hemenway</a></h5>
                                                            <a href="#">View Full Profile</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        @endif
                                        {{--Gallería de fotos--}}
                                        @if($user->verifyPrivacity(8, $user->id))
                                        <div class="tg-companyfeaturebox tg-gallery">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Galleria de fotos</h3>
                                            </div>
                                            <ul>
                                                @foreach($user->galleries as $gallery)
                                                <li>
                                                    <div class="tg-galleryimgbox">
                                                        <figure>
                                                            <img src="{{ $gallery->image_url }}" alt="image description">
                                                            {{--<a class="tg-btngallery" href="{{ $gallery->image_url }}" data-rel="prettyPhoto[gallery]"><i class="lnr lnr-magnifier"></i></a>--}}
                                                        </figure>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        {{--Videos--}}
                                        @if($user->verifyPrivacity(9, $user->id))
                                        <div class="tg-companyfeaturebox tg-videos">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Videos</h3>
                                            </div>
                                            <ul>
                                                @foreach($user->videos as $video)
                                                    <li>
                                                        <div class="tg-videobox">
                                                            <figure>
                                                                <a href="http://mydesignhoard.com/HTML/html/service_provider/images/placeholder/img-03.jpg" data-rel="prettyPhoto[videogallery]">
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/placeholder/img-03.jpg" alt="videos">
                                                                </a>
                                                            </figure>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        {{--Cartelería--}}
                                        <div class="tg-companyfeaturebox tg-gallery">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Cartelería</h3>
                                            </div>
                                            <ul>
                                                @foreach($user->brochures as $brochure)
                                                    <li>
                                                        <div class="tg-galleryimgbox">
                                                            <figure>
                                                                <img src="{{ $brochure->brochure_url }}" alt="image description">
                                                                <a class="tg-btngallery" target="_blank" href="{{ $brochure->view_url }}"><i class="lnr lnr-magnifier"></i></a>
                                                            </figure>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if($user->is_seeker)
                                        <div class="tg-companyfeaturebox tg-introduction">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Historial</h3>
                                            </div>
                                            <div class="tg-dashboardnotifications">
                                                <h4 style="padding-left: 15px; font-size: 15px">Total de ofertas de trabajo publicadas:</h4>
                                                <div class="tg-dashboardoffer">
                                                    <div class="tg-dashboardnotofication-published">
                                                        <div class="tg-dashboardinfo-published">
                                                            <h3>{{ $user->jobs_published }}</h3>
                                                            <span>Publicadas</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tg-dashboardoffer">
                                                    <div class="tg-dashboardnotofication-completed">
                                                        <div class="tg-dashboardinfo-completed">
                                                            <h3>--</h3>
                                                            <span>Completadas</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tg-dashboardoffer">
                                                    <div class="tg-dashboardnotofication-developing">
                                                        <div class="tg-dashboardinfo-developing">
                                                            <h3>{{ $user->countJobs('Cerrado') }}</h3>
                                                            <span>En desarrollo</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tg-dashboardoffer">
                                                    <div class="tg-dashboardnotofication-closed">
                                                        <div class="tg-dashboardinfo-closed">
                                                            <h3>{{ $user->jobs_closed }}</h3>
                                                            <span>Cerradas</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-companyfeaturebox tg-introduction">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Total: {{ $user->jobs_published }}</h3>
                                            </div>
                                            <table class="tg-tablejoblidting">
                                                <tbody>
                                                @foreach($user->two_last_jobs as $offerJob)
                                                    <tr style="display: table; width: 100%">
                                                        <td class="col-md-12">
                                                            <figure class="tg-companylogo">
                                                                <img src="{{ $offerJob->user->photo_selected }}" alt="image description">
                                                            </figure>
                                                            <div class="tg-contentbox">
                                                                <a class="tg-tag tg-featuredtag" href="#">{{ $offerJob->job_type_name }}</a>
                                                                <div class="tg-title">
                                                                    <h3><a href="#">{{ $offerJob->title }}</a></h3>
                                                                </div>
                                                                <span>{{ $offerJob->user->name }}</span>
                                                            </div>
                                                            <div class="tg-contentbox" style="width: 100%">
                                                                <div class="tg-btnactions pull-right" style="float: none; margin-top: 10px">
                                                                    @if($offerJob->is_published)
                                                                    <span style="color: #5c6bc0; font-size: 15px">Publicada</span>
                                                                    @endif
                                                                    @if($offerJob->status == 'Cerrado')
                                                                        <span style="color: #26a69a; font-size: 15px">En desarrollo</span>
                                                                    @endif
                                                                    @if($offerJob->is_closed)
                                                                        <span style="color: #ef5350; font-size: 15px">Cerrada</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    {{--Opiniones--}}
                                    <div class="tg-companyfeaturebox tg-reviews">
                                        <div class="tg-companyfeaturetitle">
                                            <h3>Opiniones</h3>
                                        </div>
                                        <div class="tg-feedbacks">
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-07.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-08.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-09.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-10.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-11.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-12.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-13.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-07.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-08.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-09.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-10.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-11.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-12.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-13.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-feedback">
                                                <figure><a href="javascript:void(0);"><img src="{{ asset('images/thumbnails/img-07.jpg') }}" alt="image description"></a></figure>
                                                <div class="tg-feedbackcontent">
                                                    <div class="tg-feedbackbox">
                                                        <div class="tg-contenthead">
                                                            <div class="tg-leftbox">
                                                                <div class="tg-name">
                                                                    <h4><a href="#">Sensation &amp; Pain in PMT</a></h4>
                                                                </div>
                                                                <ul class="tg-matadata">
                                                                    <li><a href="#">Edward Coldiron</a></li>
                                                                    <li><a href="#">10 Min Ago</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tg-overallratingbox">
                                                                <span class="tg-stars"><span></span></span>
                                                                <div class="tg-overallratingarea">
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                    <div class="tg-overallrating">
                                                                        <ul class="tg-servicesrating">
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Ease of Appointment</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Promptness</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Courteous Staff</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Accurate Diagnosis</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Professional Manner</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Spends Time with Me</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Follows Up After Visit</em>
                                                                            </li>
                                                                            <li>
                                                                                <span class="tg-stars"><span></span></span>
                                                                                <em>Work Knowledge</em>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tg-description">
                                                            <p>Cliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip modoia aiat irure dolor in reprehenderit in.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-btnbox">
                                            <a class="tg-btn tg-btnloadmore" href="javascript:void(0);">Mostrar más</a>
                                        </div>
                                    </div>
                                    @if(auth()->check() && $user->is_provider && auth()->id() != $user->id)
                                        <div class="tg-companyfeaturebox tg-reviewformrating">
                                            <p>La reputación de un Prestador de Servicios es la clave para que tú, como cliente pueda tener una imagen real de su desempeño y forma de trabajar. </p>
                                            <p><b>{{ $user->name }}</b> te indica que valores su actuación.</p>
                                            <div id="alert-opinions"></div>
                                            <form class="tg-themeform tg-formleavefeedback">
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                                                            <div class="tg-companyfeaturetitle">
                                                                <h3>Deja tu Comentario</h3>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" id="title" name="title" class="form-control" placeholder="Título del comentario">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-reviewtitle">
                                                                    <input type="text" name="reviewtitle" class="form-control" placeholder="¿Experiencia positiva?" disabled>
                                                                </div>
                                                                <div class="tg-recommendedradio">
                                                                        <span class="tg-radio">
                                                                            <input type="radio" id="yes" name="recommended" value="yes" checked>
                                                                            <label for="yes"><i class="fa fa-thumbs-o-up"></i> yes</label>
                                                                        </span>
                                                                    <span class="tg-radio">
                                                                            <input type="radio" id="no" name="recommended" value="no">
                                                                            <label for="no"><i class="fa fa-thumbs-o-down"></i>no</label>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea class="form-control" id="description" name="description" placeholder="Descripción"></textarea>
                                                            </div>
                                                            <button type="button" id="btnSubmit" data-opinion="{{ url('opinions/'.$user->id.'/'.auth()->id()) }}" class="tg-btn">Submit</button>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-5">
                                                            <div class="tg-companyfeaturetitle">
                                                                <h3>Deja tu Calificación</h3>
                                                            </div>
                                                            <ul class="tg-servicesrating">
                                                                <li>
                                                                    <em>Facilidad de contacto</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Cumple los horarios</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Identica el problema</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Demuestra profesionalismo</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Respetuoso y cortes</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Asesoramiento</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Trabaja con conocimiento</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                                <li>
                                                                    <em>Materiales y herramientas</em>
                                                                    <span class="tg-stars"><span></span></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pull-right">
                            <aside id="tg-sidebar" class="tg-sidebar">
                                <div class="tg-widget tg-widgetlocationandcontactinfo">
                                    <div class="tg-mapbox">
                                        <a class="tg-addtofavorite" href="javascript:void(0);"><i class="fa fa-heart"></i>Add To Favourite</a>
                                        <div id="tg-locationmap" class="tg-locationmap"></div>
                                    </div>
                                    @if($user->verifyPrivacity(4, $user->id))
                                    <ul class="tg-contactinfo">
                                        @foreach($user->addresses as $address)
                                            <li>
                                                <address>{{ $address->description_complete }}</address>
                                            </li>
                                        @endforeach
                                        <li>
                                            <i class="lnr lnr-phone-handset"></i>
                                            <span>{{ $user->mobile }}</span>
                                        </li>
                                        <li>
                                            <i class="lnr lnr-phone"></i>
                                            <span>{{ $user->phone }}</span>
                                        </li>
                                        <li>
                                            <i class="lnr lnr-apartment"></i>
                                            <span>{{ $user->company_name }}</span>
                                        </li>
                                        <li>
                                            <i class="lnr lnr-envelope"></i>
                                            <span><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></span>
                                        </li>
                                        @if($user->website)
                                            <li>
                                                <i class="lnr lnr-laptop"></i>
                                                <span><a href="{{ $user->website }}" target="_blank">{{ $user->website }}</a></span>
                                            </li>
                                        @endif
                                    </ul>
                                    @endif
                                </div>
                                <div class="tg-widget tg-widgetcontact">
                                    <div class="tg-widgettitle">
                                        <h3>Formulario contacto</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <div id="alertMessageContact"></div>
                                        @if(auth()->check())
                                        <div class="tg-themeform">
                                            <fieldset>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="topicContact" placeholder="Asunto">
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" id="messageContact" placeholder="Mensaje"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <img src="{{ asset('images/placeholder/img-02.png') }}" alt="image description">
                                                </div>
                                                <button class="tg-btn tg-btn-lg" data-message-contact="{{ url('message-contact/'.$user->id.'/send') }}">Enviar mensaje</button>
                                            </fieldset>
                                        </div>
                                        @else
                                            <div class="alert alert-info tg-alertmessage fade in">
                                                <i class="lnr lnr-flag"></i>
                                                <span><strong>Debe iniciar sesión para contactar a este usuario</strong></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @if($user->verifyPrivacity(5, $user->id) && $user->is_provider)
                                <div class="tg-widget tg-widgetbusi, $user->idnesshours">
                                    <div class="tg-widgettitle">
                                        <h3>Horas de trabajo</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul>
                                            <li id="sh-SUN">
                                                <span class="tg-dayname">Emergencia las 24 horas</span>
                                                @if($user->attendEmergency($user->id))
                                                    <div class="tg-timebox">
                                                        <i class="lnr lnr-clock"></i>
                                                        <time datetime="2016-10-10" style="color: #5dc560;">Sí, contáctame</time>
                                                    </div>
                                                @else
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @endif
                                            </li>
                                            <li id="sh-MON">
                                                <span class="tg-dayname">Lunes</span>
                                                @if($user->attend('Lunes', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Lunes', $user->id) as $hour)
                                                    <div class="tg-timebox">
                                                        <i class="lnr lnr-clock"></i>
                                                        <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                    </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                            <li id="sh-TUE">
                                                <span class="tg-dayname">Martes</span>
                                                @if($user->attend('Martes', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Martes', $user->id) as $hour)
                                                        <div class="tg-timebox">
                                                            <i class="lnr lnr-clock"></i>
                                                            <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                            <li id="sh-WED">
                                                <span class="tg-dayname">Miércoles</span>
                                                @if($user->attend('Miercoles', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Miercoles', $user->id) as $hour)
                                                        <div class="tg-timebox">
                                                            <i class="lnr lnr-clock"></i>
                                                            <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                            <li id="sh-THU">
                                                <span class="tg-dayname">Jueves</span>
                                                @if($user->attend('Jueves', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Jueves', $user->id) as $hour)
                                                        <div class="tg-timebox">
                                                            <i class="lnr lnr-clock"></i>
                                                            <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                            <li id="sh-FRI">
                                                <span class="tg-dayname">Viernes</span>
                                                @if($user->attend('Viernes', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Viernes', $user->id) as $hour)
                                                        <div class="tg-timebox">
                                                            <i class="lnr lnr-clock"></i>
                                                            <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                            <li id="sh-SAT">
                                                <span class="tg-dayname">Sábado</span>
                                                @if($user->attend('Sábado', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Sábado', $user->id) as $hour)
                                                        <div class="tg-timebox">
                                                            <i class="lnr lnr-clock"></i>
                                                            <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                            <li id="sh-SUN">
                                                <span class="tg-dayname">Domingo</span>
                                                @if($user->attend('Domingo', $user->id))
                                                    <div class="tg-timebox" style="line-height: 17px">
                                                        <i class="lnr lnr-lock"></i>
                                                        <div class="tg-checkbox" style="margin-top: 15px; width: 50%">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" checked disabled style="">
                                                            <label for="Emergencia" style="color: #5dc560;">Cerrado</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($user->hours('Domingo', $user->id) as $hour)
                                                        <div class="tg-timebox">
                                                            <i class="lnr lnr-clock"></i>
                                                            <time datetime="2016-10-10">{{ $hour->complete }}</time>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @endif
                                <div class="tg-widget tg-widgetshare">
                                    <div class="tg-widgettitle">
                                        <h3>Descargar cartelería</h3>
                                    </div>
                                    <div class="tg-widgetcontent">

                                    </div>
                                </div>
                                <div class="tg-widget tg-widgetshare">
                                    <div class="tg-widgettitle">
                                        <h3>Compartir</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul class="tg-socialicons">
                                            <li class="tg-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="tg-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="tg-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="tg-googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="tg-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                                            <li class="tg-vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                            <li class="tg-tumblr"><a href="#"><i class="fa fa-tumblr"></i></a></li>
                                            <li class="tg-yahoo"><a href="#"><i class="fa fa-yahoo"></i></a></li>
                                            <li class="tg-yelp"><a href="#"><i class="fa fa-yelp"></i></a></li>
                                            <li class="tg-pinterestp"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li class="tg-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                            <li class="tg-stumbleupon"><a href="#"><i class="fa fa-stumbleupon"></i></a></li>
                                            <li class="tg-reddit"><a href="#"><i class="fa fa-reddit"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tg-widget tg-widgetrelatedposts">
                                    <div class="tg-widgettitle">
                                        <h3>Proveedores relacionados</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul>
                                            <li>
                                                <div class="tg-serviceprovidercontent">
                                                    <div class="tg-companylogo"><a href="#"><img src="{{ asset('images/logos/img-04.png') }}" alt="image description"></a></div>
                                                    <div class="tg-companycontent">
                                                        <div class="tg-title">
                                                            <h3><a href="#">Veronika Brubaker</a></h3>
                                                        </div>
                                                        <ul class="tg-matadata">
                                                            <li><span class="tg-stars"><span></span></span></li>
                                                            <li>
                                                                <i class="fa fa-thumbs-o-up"></i>
                                                                <em>99% (1009 votes)</em>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="tg-serviceprovidercontent">
                                                    <div class="tg-companylogo"><a href="#"><img src="{{ asset('images/logos/img-06.png') }}" alt="image description"></a></div>
                                                    <div class="tg-companycontent">
                                                        <div class="tg-title">
                                                            <h3><a href="#">Royce Sandberg</a></h3>
                                                        </div>
                                                        <ul class="tg-matadata">
                                                            <li><span class="tg-stars"><span></span></span></li>
                                                            <li>
                                                                <i class="fa fa-thumbs-o-up"></i>
                                                                <em>99% (1009 votes)</em>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="tg-serviceprovidercontent">
                                                    <div class="tg-companylogo"><a href="#"><img src="{{ asset('images/logos/img-04.png') }}" alt="image description"></a></div>
                                                    <div class="tg-companycontent">
                                                        <div class="tg-title">
                                                            <h3><a href="#">Juliana Shiroma</a></h3>
                                                        </div>
                                                        <ul class="tg-matadata">
                                                            <li><span class="tg-stars"><span></span></span></li>
                                                            <li>
                                                                <i class="fa fa-thumbs-o-up"></i>
                                                                <em>99% (1009 votes)</em>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <a class="tg-views" href="javascript:void(0);">Mostrar todo</a>
                                    </div>
                                </div>
                                @if($user->is_provider)
                                <div class="tg-widget tg-widgetclaim">
                                    <div class="tg-widgettitle">
                                        <h3>Formulario reclamos/reportes</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <div id="alertMessageClaim"></div>
                                        @if(auth()->check())
                                        <div class="tg-themeform">
                                            <fieldset>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="topicClaim" placeholder="Tema">
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" id="messageClaim" placeholder="Detalle"></textarea>
                                                </div>
                                                <button class="tg-btn tg-btn-lg" data-message-claim="{{ url('message-claim/'.$user->id.'/send') }}">Reportar</button>
                                            </fieldset>
                                        </div>
                                        @else
                                            <div class="alert alert-info tg-alertmessage fade in">
                                                <i class="lnr lnr-flag"></i>
                                                <span><strong>Debe iniciar sesión para enviar su reclamo</strong></span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @endif
                                @if($user->is_provider)
                                    <div class="tg-widget tg-widgettags">
                                        <div class="tg-widgettitle">
                                            <h3>Etiquetas</h3>
                                        </div>
                                        <div class="tg-widgetcontent">
                                            <a class="tg-tag" href="javascript:void(0);">Construction</a>
                                            <a class="tg-tag" href="javascript:void(0);">Verified</a>
                                            <a class="tg-tag" href="javascript:void(0);">Furniture Making</a>
                                            <a class="tg-tag" href="javascript:void(0);">Nail</a>
                                            <a class="tg-tag" href="javascript:void(0);">Hammer</a>
                                            <a class="tg-tag" href="javascript:void(0);">Fun Time</a>
                                            <a class="tg-tag" href="javascript:void(0);">PSD Template</a>
                                            <a class="tg-tag" href="javascript:void(0);">Renovation</a>
                                            <a class="tg-tag" href="javascript:void(0);">Palace</a>
                                            <a class="tg-tag" href="javascript:void(0);">Dream Makers</a>
                                        </div>
                                    </div>
                                @endif
                                <div class="tg-advertisement">
                                    @if(auth()->check() && auth()->user()->is_provider)
                                        @if($imgBottomPublicProfile)
                                            <a href="javascript:void(0);"><img src="{{ $imgBottomPublicProfile->image_url }}" alt="image description"></a>
                                        {{--@else--}}
                                            {{--<a href="javascript:void(0);"><img src="{{ asset('images/advertisement/img-01.png') }}" alt="image description"></a>--}}
                                        @endif
                                    @else
                                        @if($imgBottomPublicProfileBS)
                                            <a href="javascript:void(0);"><img src="{{ $imgBottomPublicProfileBS->image_url }}" alt="image description"></a>
                                        {{--@else--}}
                                            {{--<a href="javascript:void(0);"><img src="{{ asset('images/advertisement/img-02.png') }}" alt="image description"></a>--}}
                                        @endif
                                    @endif
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal fade tg-appointmentModal" id="modalAppointmentOne" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <!--<a class="tg-closemodal" href="javascript:void(0);" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>-->
                    <h2>Seleccione Fecha de cita</h2>
                </div>
                <div class="tg-themeform tg-formappointment">
                    <div class="tg-modalbody" style="width: 50%">
                        <div id="tg-datepicker" class="tg-datepicker"></div>
                    </div>
                    <div class="tg-modalfoot" style="clear: none">
                        <h2>Seleccione hora de cita:</h2>
                        <div class="tg-availabletimeslotbox">
                            <div class="tg-dateandcount">
                                <time id="date-picker" datetime="2017-02-02"></time>
                                <span>11 Available</span>
                            </div>
                            <div class="tg-timeslotsradio">
                            </div>
                        </div>
                    </div>
                    <div class="tg-modalfoot">
                        <div class="tg-btnbox">
                            <button class="tg-btn" type="button" id="btnShowModalTwo" style="display: none;">continue</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade tg-appointmentModal" id="modalAppointmentTwo" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <form class="tg-themeform tg-formappointment">
                    <div class="tg-modalbody">
                        <div class="tg-appointmentsetting">
                            <form class="tg-formbookappointment">
                                <fieldset class="tg-formstepone tg-current">
                                    <div class="tg-appointmenthead">
                                        <a class="tg-btn tg-btnedite" href="javascript:void(0);"><i class="lnr lnr-pencil"></i></a>
                                        <div class="tg-appointmentheading">
                                            <h2>Reservar cita</h2>
                                        </div>
                                    </div>
                                    <div class="tg-progressbox">
                                        <ul class="tg-formprogressbar">
                                            <li class="tg-active"><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                        </ul>
                                        <h3>Que debes saber antes de empezar</h3>
                                        <div class="tg-description">
                                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laboraum laudantium totam aperiam ab illo inventore veritatis quasi architecto beatae vitae dicta sunt explicabo enim ipsam voluptatem quia voluptas sit aspernatur aut fugit sed equi nesciunt.</p>
                                            <p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
                                        </div>
                                        <div class="tg-btnarea">
                                            <a href="javascript:void(0);" class="tg-btn tg-btnnext">Entendido</a>
                                            <a href="javascript:void(0);" class="tg-btndontwant" data-dismiss="modal">No, no quiero</a>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="tg-formsteptwo">
                                    <div class="tg-appointmenthead">
                                        <a class="tg-btn tg-btnedite" href="javascript:void(0);"><i class="lnr lnr-pencil"></i></a>
                                        <div class="tg-appointmentheading">
                                            <h2 class="titulo"></h2>
                                        </div>
                                    </div>
                                    <div class="tg-progressbox">
                                        <ul class="tg-formprogressbar">
                                            <li class="tg-active"><span>Instrucciones</span></li>
                                            <li><span>Detalles</span></li>
                                            <li><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                        </ul>
                                        <div class="tg-appointmentinfo">
                                            <div class="form-group">
                                                <div class="tg-heading">
                                                    <h3>Información de la cita</h3>
                                                </div>
                                                <div id="alertAppointmentInformation"></div>
                                                <div class="tg-reminderemail">
                                                    <span>Confirmación de reserva vía:*</span>
                                                    <div class="tg-checkbox">
                                                        <input type="checkbox" id="email" name="confirmation_type">
                                                        <label for="email">Email</label>
                                                    </div>
                                                    <div class="tg-checkbox">
                                                        <input type="checkbox" id="phone" name="confirmation_type">
                                                        <label for="phone">Phone</label>
                                                    </div>
                                                    <div class="tg-checkbox">
                                                        <input type="checkbox" id="pdf" name="confirmation_type">
                                                        <label for="pdf">PDF</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="form-group">
                                                <span class="tg-select">
                                                    <select id="select-option">
                                                        <option value="">Elija una opción*</option>
                                                        <option value="Service">Contratar un servicio</option>
                                                        <option value="Free">Reservar cita</option>
                                                    </select>
                                                </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <span class="tg-select">
                                                            <select id="select-service">

                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-12 col-md-12 col-lg-12">
                                                    <textarea id="description" placeholder="Escriba una nota o información para el prestador de servicio" rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-userdetail">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="tg-btnarea">
                                                        <a class="tg-btn tg-btnprevious" href="javascript:void(0);">Regresar</a>
                                                        <a class="tg-btn tg-btnnext" href="javascript:void(0);" id="btnShowAppointment">Continuar</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="tg-formstepthree">
                                    <div class="tg-appointmenthead">
                                        <a class="tg-btn tg-btnedite" href="javascript:void(0);"><i class="lnr lnr-pencil"></i></a>
                                        <div class="tg-appointmentheading">
                                            <h2 class="titulo"></h2>
                                        </div>
                                    </div>
                                    <div class="tg-progressbox">
                                        <ul class="tg-formprogressbar">
                                            <li class="tg-active"><span>Instrucciones</span></li>
                                            <li><span>Detalles</span></li>
                                            <li><span>Solicitar código</span></li>
                                            <li><span>Instrucciones</span></li>
                                            <li><span>Instrucciones</span></li>
                                        </ul>
                                        <div class="form-group">
                                            <div class="tg-heading">
                                                <h3>Solicite su código vía:</h3>
                                            </div>
                                        </div>
                                        {{--<div class="tg-description">--}}
                                            {{--<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet consectetur adipisci velit sed quia non numquam eius modi <a href="#">yourmail@domain.com</a> tempora incidunt ut labore et dolore. If did not receive please send us email at <a href="#">support@domain</a>.com OR Call us at 0800 1234 567.</p>--}}
                                        {{--</div>--}}
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="text" name="cellphone" class="form-control" placeholder="Teléfono móvil">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="tg-btnarea">
                                                    <a class="tg-btn tg-btnprevious" href="javascript:void(0);">Regresar</a>
                                                    <a class="tg-btn tg-btnnext" href="javascript:void(0);">Solicitar código</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="tg-formstepfour">
                                    <div class="tg-appointmenthead">
                                        <a class="tg-btn tg-btnedite" href="javascript:void(0);"><i class="lnr lnr-pencil"></i></a>
                                        <div class="tg-appointmentheading">
                                            <h2 class="titulo">Thu, Jan 25 (Tomorrow) at 12:00</h2>
                                        </div>
                                    </div>
                                    <div class="tg-progressbox">
                                        <ul class="tg-formprogressbar">
                                            <li class="tg-active"><span>Instrucciones</span></li>
                                            <li><span>Detalles</span></li>
                                            <li><span>Solicitar código</span></li>
                                            <li><span>Payment Options</span></li>
                                            <li><span>Instrucciones</span></li>
                                        </ul>
                                        <div class="form-group">
                                            <div class="tg-heading">
                                                <h3>How Would You Like To Pay?</h3>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-radio">
                                                <input type="radio" id="bank" name="payment" checked>
                                                <label for="bank">Pay Through Bank Transfer</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-radio">
                                                <input type="radio" id="paypal" name="payment">
                                                <label for="paypal">Pay through Paypal <img src="{{ asset('images/paypal.jpg') }}" alt="image description"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-radio">
                                                <input type="radio" id="creditcard" name="payment">
                                                <label for="creditcard">Pay through Credit Cards <img src="{{ asset('images/cards.jpg') }}" alt="image description"></label>
                                            </div>
                                        </div>
                                        <div class="tg-btnarea">
                                            <a class="tg-btn tg-btnprevious" href="javascript:void(0);">Regresar</a>
                                            <a class="tg-btn tg-btnnext" href="javascript:void(0);">Continuar</a>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="tg-formstepfive">
                                    <div class="tg-appointmenthead">
                                        <a class="tg-btnedite" href="#"><i class="lnr lnr-printer"></i></a>
                                        <div class="tg-appointmentheading">
                                            <h2>Gracias por darnos la oportunidad</h2>
                                        </div>
                                    </div>
                                    <div class="tg-progressbox">
                                        <ul class="tg-formprogressbar">
                                            <li class="tg-active"><span>Instrucciones</span></li>
                                            <li><span>Detalles</span></li>
                                            <li><span>Solicitar código</span></li>
                                            <li><span>Payment Options</span></li>
                                            <li><span>All Done</span></li>
                                        </ul>
                                        <div class="form-group">
                                            <div class="tg-heading">
                                                <h3>Resumen de la cita</h3>
                                            </div>
                                            <div class="tg-reminderemail">
                                                <span>Cita ID: 1558AJYC22</span>
                                            </div>
                                        </div>
                                        <ul class="tg-appointmentsummry">
                                            <li>
                                                <strong>Fecha:</strong>
                                                <span id="date_span"></span>
                                            </li>
                                            <li>
                                                <strong>Hora:</strong>
                                                <span id="time_span">12:00pm</span>
                                            </li>
                                            <li>
                                                <strong>Nombre prestador:</strong>
                                                <span>{{ $user->name }}</span>
                                            </li>
                                            <li>
                                                <strong>Servicio:</strong>
                                                <span id="service_span"></span>
                                            </li>
                                            <li>
                                                <strong>Tipo cita:</strong>
                                                <span id="appointment_span"></span>
                                            </li>
                                            <li>
                                                <strong>Descripción:</strong>
                                                <span>
                                            <div class="tg-description">
                                                <p id="description_span"></p>
                                            </div>
                                        </span>
                                            </li>
                                        </ul>
                                        <div class="tg-btnarea">
                                            <a class="tg-btn tg-btnprevious" href="javascript:void(0);">Regresar</a>
                                            <a class="tg-btn" href="javascript:void(0);" data-appointment="{{ url('/reserve/'.$user->id.'/appointment') }}">Finalizar</a>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // information basic
        $(document).on('click', '[data-opinion]', function () {
            let urlOpinion = $(this).data('opinion');
            let title = $('#title');
            let description = $('#description');
            $('#btnSubmit').prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: urlOpinion,
                data:{
                    '_token':$('input[name=_token]').val(),
                    'title':title.val(),
                    'description':description.val(),
                },
                success:function (){
                    $('.alert-success').remove();
                    $('.alert-warning').remove();
                    let $alert = $('#alert-opinions');
                    $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-star"></i>' +
                        '<span><strong>Se envió correctamente su opinión</strong></span>' +
                        '</div>');
                    title.val("");
                    description.val("");

                    $('#btnSubmit').prop('disabled', false);
                },
                error:function (data) {
                    $('.alert-success').remove();
                    $('.alert-warning').remove();
                    let $data = data.responseJSON;

                    let $alert = $('#alert-opinions');

                    if ($data.description)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.description +'</strong></span>' +
                            '</div>');

                    $('#btnSubmit').prop('disabled', false);
                }
            });
        });
    </script>
    <script>
        $(document).on('click', '[data-favorite]', function () {
            // request al servidor
            var urlAddFavorite = $(this).data('favorite');
            $.ajax({
                type: 'GET',
                url: urlAddFavorite,

                success:function (data){
                    $('#btnFavorite').replaceWith('<button class="tg-favorite" id="btnFavorite"><i class="fa fa-heart"></i>Agregado a favoritos</button>')
                }
            });

        });

        $(document).on('click', '[data-message-contact]', function () {
            // request al servidor
            const urlSendMessage = $(this).data('message-contact');
            const topic = $('#topicContact');
            const message = $('#messageContact');
            const btnSendContact = $('#btnSendContact');
            const $alert = $('#alertMessageContact');

            btnSendContact.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: urlSendMessage,
                data: {
                    '_token':$('input[name=_token]').val(),
                    'topic':topic.val(),
                    'message':message.val(),
                },

                success:function (data){
                    btnSendContact.prop('disabled', false);
                    topic.val("");
                    message.val("");
                    $('.alert-warning').remove();
                    $('.alert-success').remove();

                    $alert.append('<div class="alert alert-success tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-star"></i>' +
                        '<span><strong>Tu mensaje se ha enviado correctamente. Serás contactado al correo o teléfono de tu cuenta ('+data.from_email+' o '+data.from_phone+').</strong></span>' +
                        '</div>');
                },
                error:function (data) {
                    btnSendContact.prop('disabled', false);
                    $('.alert-warning').remove();
                    $('.alert-success').remove();

                    const $data = data.responseJSON;
                    if ($data.topic)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.topic +'</strong></span>' +
                            '</div>');
                    if ($data.message)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.message +'</strong></span>' +
                            '</div>');
                }
            });

        });

        $(document).on('click', '[data-message-claim]', function () {
            // request al servidor
            const urlSendMessage = $(this).data('message-claim');
            const topic = $('#topicClaim');
            const message = $('#messageClaim');
            const btnSendClaim = $('#btnSendClaim');
            const $alert = $('#alertMessageClaim');

            btnSendClaim.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: urlSendMessage,
                data: {
                    '_token':$('input[name=_token]').val(),
                    'topic':topic.val(),
                    'message':message.val(),
                },

                success:function (data){
                    btnSendClaim.prop('disabled', false);
                    topic.val("");
                    message.val("");
                    $('.alert-warning').remove();
                    $('.alert-success').remove();

                    $alert.append('<div class="alert alert-success tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-star"></i>' +
                        '<span><strong>Tu reclamo se ha enviado correctamente. Serás contactado al correo o teléfono de tu cuenta ('+data.from_email+' o '+data.from_phone+').</strong></span>' +
                        '</div>');
                },
                error:function (data) {
                    btnSendClaim.prop('disabled', false);
                    $('.alert-warning').remove();
                    $('.alert-success').remove();

                    const $data = data.responseJSON;
                    if ($data.topic)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.topic +'</strong></span>' +
                            '</div>');
                    if ($data.message)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.message +'</strong></span>' +
                            '</div>');
                }
            });

        });
    </script>
    <script>
        $(function() {
            $('#select-option').on('change', onSelectOption);
            $('#btnShowAppointment').on('click', onShowAppointment);
        });

        function onSelectOption() {
            const option = $(this).val();
            const userId = '{{$user->id}}';
            // AJAX
            $.get('/api/user/'+userId+'/'+option+'/services', function (data) {
                var html_select = '<option value="">Seleccione tipo de servicio</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].title+'</option>';
                $('#select-service').html(html_select);
            });
        }

        function onShowAppointment() {
            const date = $('#tg-datepicker').datepicker({ dateFormat: "dd/mm/yyyy" }).val();
            const time = $('input[name="tg-timeslot"]:checked').attr("value");
            const option = $('#select-option option:selected').val();
            const service = $('#select-service option:selected').text();
            const description = $('#description').val();

            if(option.length === 0 || service.length === 0 || description.length === 0)
            {
                $('.alert-warning').remove();
                $('#alertAppointmentInformation').append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                    '<i class="lnr lnr-bug"></i>' +
                    '<span><strong>Es necesario llenar todos los campos antes de continuar</strong></span>' +
                    '</div>');
                e.preventDefault();
            }

            $('#date_span').replaceWith('<span id="date_span">'+date+'</span>');
            $('#time_span').replaceWith('<span id="time_span">'+time+'</span>');
            $('#service_span').replaceWith('<span id="service_span">'+service+'</span>');
            if(option === 'Service')
                $('#appointment_span').replaceWith('<span id="appointment_span">Contratar un servicio</span>');
            else
                $('#appointment_span').replaceWith('<span id="appointment_span">Reservar cita</span>');

            $('#description_span').replaceWith('<p id="description_span">'+description+'</p>');
        }
    </script>
    <script>
        $(function() {
            $('#btnShowModalTwo').click(function() {
                const date = $('#tg-datepicker').datepicker({ dateFormat: "dd/mm/yyyy" }).val();
                const time = $('input[name="tg-timeslot"]:checked').attr("value");

                $('#modalAppointmentOne').modal('hide');
                $('.titulo').replaceWith('<h2>'+date+' de '+time+'</h2>');

                $("#modalAppointmentOne").on("hidden.bs.modal", function () {
                    $('#modalAppointmentTwo').modal('show');
                });
            });
        });
    </script>
    <script>
        $(function() {
            $('#tg-datepicker').on('change', onDatePicker);
        });

        function onDatePicker() {
            $('#btnShowModalTwo').hide();
            const date = $(this).datepicker({ dateFormat: "dd/mm/yyyy" }).val();
            const userId = '{{$user->id}}';
            const url = '/api/provider/'+userId+'/hours?date='+date;

            $('#date-picker').replaceWith('<time id="date-picker" datetime="'+date+'">'+date+'</time>');

            $.get(url, function (data) {
                $('.tg-timeslotsradio').empty();

                var html_select = '';
                for (var i=0; i<data.length; ++i)
                    for(var j=1; j<data[i].length;j++)
                        html_select += '<span class="tg-radio"><input type="radio" id="'+data[i][j-1]+' - '+data[i][j]+'" onchange="showButton()" name="tg-timeslot" value="'+data[i][j-1]+' - '+data[i][j]+'"><label for="'+data[i][j-1]+' - '+data[i][j]+'">'+data[i][j-1]+' - '+data[i][j]+'</label></span>';

                $('.tg-timeslotsradio').html(html_select);
            });
        }
    </script>
    <script>
        function showButton() {
            $('#btnShowModalTwo').show();
        }
    </script>
    <script>
        $(document).on('click', '[data-appointment]', function () {
            const date = $('#tg-datepicker').datepicker({ dateFormat: "dd/mm/yyyy" }).val();
            const time = $('input[name="tg-timeslot"]:checked').attr("value");
            const option = $('#select-option option:selected').val();
            const service = $('#select-service option:selected').val();
            const description = $('#description').val();
            const urlAppointment = $(this).data('appointment');

            $.ajax({
                type: 'POST',
                url: urlAppointment,
                data: {
                    '_token':$('input[name=_token]').val(),
                    'date': date,
                    'time': time,
                    'option': option,
                    'service': service,
                    'information': description,
                },

                success:function (data){
                    $('#modalAppointmentTwo').modal('hide');
                }
            });

        });
    </script>
@endsection
