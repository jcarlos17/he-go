@extends('layouts.app')

{{--@section('section')--}}
    {{--width=device-width, initial-scale=1--}}
{{--@endsection--}}

@section('styles')
    <link rel="stylesheet" href="{{ asset('/css/chatbox.css') }}">
@endsection

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Mensajes de chat</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Mensajes de chat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Mensajes de chat</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="javascript:void(0);" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container" style="width: 100%">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns" style="border: 1px solid #f7f7f7;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="app">
                            <div class="row app-one">
                                <div class="col-sm-3 side" style="padding-right: 0; padding-left: 0">
                                    <div class="side-one">
                                        <div class="row heading">
                                            <div class="col-sm-3 col-xs-3 heading-avatar">
                                                <div class="heading-contact">
                                                    <a class="heading-name-contact">All conversations</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row searchBox">
                                            <div class="col-sm-12 searchBox-inner">
                                                <div class="form-group has-feedback">
                                                    <input id="searchText" type="text" class="form-control" name="searchText" placeholder="Buscar...">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row sideBar">
                                            @foreach($chats as $chat)
                                            <div id="sidebarBody{{$chat->id}}" class="row sideBar-body" data-id="{{$chat->id}}" onClick="showConversation($(this).data('id'))" data-show="{{ url('chat/'.$chat->id.'/conversation') }}">
                                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                                    <div class="avatar-icon">
                                                        <span class="contact-status online"></span>
                                                        <img src="{{ $chat->contact->photo_selected }}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                                    <div class="row">
                                                        <div class="col-sm-8 col-xs-8 sideBar-name content-message" style="padding-left: 0">
                                                            <span class="name-meta">{{ $chat->contact->name }}</span>
                                                            <br>
                                                            <span class="heading-online">{{ $chat->last_message }}</span>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                                            <span class="time-meta">{{ $chat->last_hour }}</span>
                                                            <a class="heading-icon pull-right" href="javascript:void(0);"><i class="fa fa-star-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-9 conversation" style="padding-right: 0; padding-left: 0">
                                    <div class="row heading">
                                        <div id="headingContact" class="col-sm-8 heading-name">

                                        </div>
                                        <div class="col-sm-4 pull-right" style="padding-left: 0; padding-right: 15px">
                                            {{--<div class="pull-right">--}}
                                                {{--<a class="heading-icon" href="#!">--}}
                                                    {{--<i class="fa fa-tag fa-iconx  pull-right" aria-hidden="true"></i>--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                            <div class="pull-right">
                                                <a class="heading-icon" href="#!">
                                                    <i class="fa fa-trash-o fa-iconx  pull-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="pull-right">
                                                <a class="heading-icon" href="#!">
                                                    <i class="fa fa-inbox fa-iconx  pull-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="pull-right">
                                                <a class="heading-icon" href="#!">
                                                    <i class="fa fa-envelope-open-o fa-iconx  pull-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="pull-right">
                                                <a class="heading-icon" href="#!">
                                                    <i class="fa fa-star-o fa-iconx  pull-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8" id="container-message" style="padding: 0 !important;">
                                        <div class="row message" id="scrollMessages">
                                            {{--<div class="row message-previous">--}}
                                            {{--<div class="col-sm-12 previous">--}}
                                            {{--<a onclick="previous(this)" id="ankitjain28" name="20">--}}
                                            {{--Show Previous Message!--}}
                                            {{--</a>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            <div id="contentMessages" class="row message-body">

                                            </div>
                                        </div>

                                        <div class="row reply">
                                            {{ csrf_field() }}
                                            <div class="col-sm-1 reply-emojis text-center">
                                                <i class="fa fa-smile-o fa-2x"></i>
                                            </div>
                                            <div class="col-sm-1 reply-recording text-center">
                                                <i class="fa fa-paperclip fa-2x" aria-hidden="true"></i>
                                            </div>
                                            <div class="col-sm-9 reply-main">
                                                <textarea id="textMessage" class="form-control" rows="2" placeholder="Escribe un mensaje aquí" disabled></textarea>
                                            </div>
                                            <div class="col-sm-1 reply-send text-center">
                                                <i id="btnSend" class="fa fa-send fa-2x" aria-hidden="true" data-send="" style="display: none;"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="container-info"></div>
                                    <template id="templateInfoContact">
                                        <div class="row info">
                                            <div class="row info-heading">
                                                <div class="col-sm-12 info-content text-center">
                                                    <div class="heading-avatar-info">
                                                        <img id="imgContact" src="">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 info-content text-center">
                                                    <a href="" class="heading-name-meta"></a>
                                                    <span id="cityContact" class="info-city"></span>
                                                </div>
                                            </div>
                                            <div class="row info-content">
                                                <div class="col-sm-6 info-title-description">
                                                    <span class="">Valoraciones</span>
                                                </div>
                                                <div class="col-sm-6 info-description">
                                                    <i class="fa fa-star valoration-star"></i> <b>5.0</b> <span class="info-city">(167)</span>
                                                </div>
                                                {{--</div>--}}
                                                {{--<div class="row info-content">--}}
                                                <div class="col-sm-6 info-title-description">
                                                    <span>Responde en</span>
                                                </div>
                                                <div class="col-sm-6 info-description">
                                                    <p>1h</p>
                                                </div>
                                                {{--</div>--}}
                                                {{--<div class="row info-content">--}}
                                                <div class="col-sm-6 info-title-description">
                                                    <span>Contacto</span>
                                                </div>
                                                <div class="col-sm-6 info-description">
                                                    <p id="phoneContact"></p>
                                                    <p id="emailContact"></p>
                                                </div>
                                                {{--</div>--}}
                                                {{--<div class="row info-content">--}}
                                                <div class="col-sm-6 info-title-description">
                                                    <span class="">Categorías de trabajo</span>
                                                </div>
                                                <div class="col-sm-6 info-description">
                                                    <p>Carpinteria</p>
                                                    <p>Electricidad</p>
                                                    <p>Albañil</p>
                                                </div>
                                                {{--</div>--}}
                                                {{--<div class="row info-content">--}}
                                                <div class="col-sm-6 info-title-description">
                                                    <span class="">Mis servicios incluyen</span>
                                                </div>
                                                <div class="col-sm-6 info-description">
                                                    <p>Factura</p>
                                                    <p>Emergencias</p>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                    <template id="templateMessageReceiver">
                                        <div class="row message-body">
                                            <div class="col-sm-12 message-main-receiver">
                                                <div class="receiver">
                                                    <div class="message-text"></div>
                                                    <span class="message-time pull-right"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                    <template id="templateMessageSender">
                                        <div class="row message-body">
                                            <div class="col-sm-12 message-main-sender">
                                                <div class="sender">
                                                    <div class="message-text"></div>
                                                    <span class="message-time pull-right"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        @if($lastId)
        showConversation({{$lastId}});
        @endif
        @if(session('chatId'))
            showConversation({{session('chatId')}});
        @endif
        function showConversation(id) {
            // request al servidor
            const urlShow = 'chat/'+id+'/conversation';
            // var id = $(this).data('id');
            $.ajax({
                type: 'GET',
                url: urlShow,

                success:function (data){
                    $('.sideBar-body').removeClass('active-green');
                    $('#sidebarBody'+id).addClass('active-green');
                    //conversation
                    $('#headingContact').empty();
                    $('#headingContact').append('<span class="contact-heading-status online"></span>' +
                        '<a href="/perfil-publico/'+data.contact.id+'" class="heading-name-meta">'+data.contact_name+'</a>' +
                        '<span class="heading-online">En linea | Hora local 12:00pm</span>');
                    $('#textMessage').prop('disabled', false);
                    $('#btnSend').replaceWith('<i id="btnSend" class="fa fa-send fa-2x" aria-hidden="true" data-send="/chat/'+data.id+'/message" style="display: none;"></i>');

                    var $containerMessage = $('#contentMessages');
                    $containerMessage.empty();
                    var conversations = data.conversations;
                    for (var i=0; i< conversations.length; ++i){
                        if(conversations[i].user_auth){
                            var templateMessageSender = $('#templateMessageSender').html();
                            templateMessageSender = templateMessageSender.replace('<div class="message-text"></div>', '<div class="message-text">'+conversations[i].message+'</div>');
                            templateMessageSender = templateMessageSender.replace('<span class="message-time pull-right"></span>', '<span class="message-time pull-right">'+conversations[i].date+'</span>');
                            $containerMessage.append(templateMessageSender);
                        }
                        else{
                            var templateMessageReceived = $('#templateMessageReceiver').html();
                            templateMessageReceived = templateMessageReceived.replace('<div class="message-text"></div>', '<div class="message-text">'+conversations[i].message+'</div>');
                            templateMessageReceived = templateMessageReceived.replace('<span class="message-time pull-right"></span>', '<span class="message-time pull-right">'+conversations[i].date+'</span>');
                            $containerMessage.append(templateMessageReceived);
                        }
                    }

                    //info
                    var $containerInfo = $('#container-info');
                    $containerInfo.empty();
                    var templateInfo = $('#templateInfoContact').html();

                    templateInfo = templateInfo.replace('src=""', 'src="'+data.contact_photo_selected+'"');
                    templateInfo = templateInfo.replace('<a href="" class="heading-name-meta"></a>', '<a href="/perfil-publico/'+data.contact.id+'" class="heading-name-meta" style="text-align: center">'+data.contact_name+'</a>');
                    templateInfo = templateInfo.replace('<span id="cityContact" class="info-city"></span>', '<span id="cityContact" class="info-city">'+data.contact_location+'</span>');
                    templateInfo = templateInfo.replace('<p id="phoneContact"></p>', '<p id="phoneContact">'+data.contact_phone+'</p>');
                    templateInfo = templateInfo.replace('<p id="emailContact"></p>', '<p id="emailContact">'+data.contact.email+'</p>');
                    $containerInfo.append(templateInfo);
                    $('#scrollMessages').scrollTop($('#scrollMessages')[0].scrollHeight);
                }
            });
        }

        $('#textMessage').bind('input propertychange', function() {
            var btnSend = $("#btnSend");
            btnSend.hide();

            if(this.value.length > 0){
                btnSend.show();
            }
        });

        $("#textMessage").keypress(function (e) {
            if(e.which === 13 && !e.shiftKey && this.value.length > 0) {
                $('#btnSend').click();
                e.preventDefault();
                return false;
            }
        });
    </script>
    <script>
        $(document).on('click', '[data-send]', function () {
            // request al servidor
            var urlSend = $(this).data('send');
            var $message = $('#textMessage');
            $.ajax({
                type: 'POST',
                url: urlSend,
                data:{
                    '_token':$('input[name=_token]').val(),
                    'message': $message.val()
                },

                success:function (data){
                    //conversation
                    var $containerMessage = $('#contentMessages');
                    // $containerMessage.empty();
                    var templateMessage = $('#templateMessageSender').html();

                    templateMessage = templateMessage.replace('<div class="message-text"></div>', '<div class="message-text">'+data.message+'</div>');
                    templateMessage = templateMessage.replace('<span class="message-time pull-right"></span>', '<span class="message-time pull-right">'+data.date+'</span>');
                    $containerMessage.append(templateMessage);

                    $('#scrollMessages').scrollTop($('#scrollMessages')[0].scrollHeight);
                    $('#btnSend').hide();
                    $('#textMessage').val('');
                }
            });

        });
    </script>
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        const userId = {{ auth()->id() }};
        var pusher = new Pusher('01bb61e5ad5bc975923f', {
            cluster: 'us2',
            forceTLS: true
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            if(data.user_id !== userId){
                //conversation
                var $containerMessage = $('#contentMessages');
                // $containerMessage.empty();
                var templateMessage = $('#templateMessageReceiver').html();

                templateMessage = templateMessage.replace('<div class="message-text"></div>', '<div class="message-text">'+data.message+'</div>');
                templateMessage = templateMessage.replace('<span class="message-time pull-right"></span>', '<span class="message-time pull-right">'+data.date+'</span>');
                $containerMessage.append(templateMessage);

                $('#scrollMessages').scrollTop($('#scrollMessages')[0].scrollHeight);
                $('#btnSend').hide();
                $('#textMessage').val('');
            }
        });
    </script>
@endsection
