@extends('layouts.app')

@section('styles')
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Mi membresia</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Mi membresia</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8  col-lg-9 pull-right">
                        <form class="tg-formtheme">
                            <fieldset>
                                <div class="alert alert-info tg-alertmessage fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>
                                    <i class="lnr lnr-flag"></i>
                                    <span><strong>silver Package is about To Expire:</strong> Adipisicing elit tempor incididunt ut labore et dolore magna aliqua enim minimati</span>
                                </div>
                                <div class="tg-pkgexpireyandcounter">
                                    <div class="tg-pkgexpirey">
                                        <span>Current Package:</span>
                                        <h3>Silver Package</h3>
                                        <a href="#">renew now</a>
                                    </div>
                                    <div class="tg-timecounter tg-expireytimecounter">
                                        <div id="tg-countdown" class="tg-countdown">
                                            <div id="tg-note" class="tg-note"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-dashboardbox">
                                    <div class="tg-dashboardtitle">
                                        <h2>Mi membresia</h2>
                                    </div>
                                    <div class="tg-packagesbox">
                                        <div class="tg-summary">
                                            <div class="tg-tablescroll">
                                                <table class="table table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-left" style="padding-left: 20px">
                                                                <h4>Características de cada plan</h4>
                                                            </th>
                                                            <th class="text-center">
                                                                <h4 style="color: #5dc560;">Plan joven*</h4>
                                                                <br>
                                                                <small>¿tienes entre 18 y 22 años?</small>
                                                            </th>
                                                            <th class="text-center">
                                                                <h4 style="color: #756fb4">Tarifa plana</h4>
                                                                <span style="color: #756fb4">Ilimitado</span>
                                                                <br>
                                                                <small>Emprendedores y Empresas</small>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">
                                                                <span>Acceso ilimitado a todas las ofertas de trabajo</span>
                                                            </td>
                                                            <td style="color: #f2646f" class="text-center">no</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Consulta de forma ilimitada los datos de los clientes</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Envía propuestas/proformas ilimitadamente</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Zonas de trabajo ilimitada</td>
                                                            <td style="color: #f2646f" class="text-center">no</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Categorías y sub categorías de trabajo ilimitadas</td>
                                                            <td style="color: #f2646f" class="text-center">no</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Gestiona tus citas con tus clientes</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Ofrecer servicios a precio cerrado</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Gestión de horarios de trabajo</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Crea tu agenda de disponibilidad de trabajo</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Perfil profesional muy completo</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Visibilidad en todos los buscadores</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Soporte técnico de atención al cliente personalizado</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Comparte tu perfil en todas las redes sociales</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Crea tu red de colegas de confianza</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Mensajes ilimitados en tiempo real</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Sugerir tu perfil en fichas de la competencia</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left" style="padding-left: 20px">Insignias por meritos a tu desempeño</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                            <td style="color: #5dc560;" class="text-center">si</td>
                                                        </tr>
                                                    </tbody>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group" style="margin-left: 10px">
                                                                    <div class="tg-registeras" style="float: left;">
                                                                        <div class="tg-radio">
                                                                            <input type="radio" id="mounth6" name="pay" value="6" onchange="showDescription(this.value)">
                                                                            <label for="mounth6">Pago único cada 6 meses</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="tg-pkgplanhead" style="padding: 10px 0">
                                                                    <h4><span>S/ 60.00</span></h4>
                                                                    <div class="form-group" style="margin: 10px 0;">
                                                                        <button style="font: 400 14px/30px 'Work Sans', Arial, Helvetica, sans-serif" data-feature="" class="tg-btn text-center">Elegir</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="tg-pkgplanhead" style="padding: 10px 0">
                                                                    <h4><span>S/ 150.00</span></h4>
                                                                    <div class="form-group" style="margin: 10px 0;">
                                                                        <button style="font: 400 14px/30px 'Work Sans', Arial, Helvetica, sans-serif" data-feature="" class="tg-btn text-center">Elegir</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group" style="margin-left: 10px">
                                                                    <div class="tg-registeras" style="float: left;">
                                                                        <div class="tg-radio">
                                                                            <input type="radio" id="mounth12" name="pay" value="12" onchange="showDescription(this.value)">
                                                                            <label for="mounth12">Pago único cada 12 meses</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="float: left;margin-left: 30px">
                                                                    <div class="tg-radio">
                                                                        <input type="radio" id="chance1" name="chance" value="1" onchange="showDescription(this.value)">
                                                                        <label style="font-size: 12px" for="chance1">Quiero (1) una oportunidad para participar en el sorteo “El Mundo y yo”</label>
                                                                    </div>
                                                                </div>
                                                                <div style="float: left;margin-left: 30px">
                                                                    <div class="tg-radio">
                                                                        <input type="radio" id="chance10" name="chance" value="10" onchange="showDescription(this.value)">
                                                                        <label style="font-size: 12px" for="chance10">Quiero (10) diez oportunidades para participar en el sorteo “El Mundo y yo”</label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="tg-pkgplanhead" style="padding: 10px 0">
                                                                    <h4><span>S/ 100.00</span></h4>
                                                                    <div class="form-group" style="margin: 10px 0;">
                                                                        <button style="font: 400 14px/30px 'Work Sans', Arial, Helvetica, sans-serif" data-feature="" class="tg-btn text-center">Elegir</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="tg-pkgplanhead" style="padding: 10px 0">
                                                                    <h4><span>S/ 210.00</span></h4>
                                                                    <div class="form-group" style="margin: 10px 0;">
                                                                        <button style="font: 400 14px/30px 'Work Sans', Arial, Helvetica, sans-serif" data-feature="" class="tg-btn text-center">Elegir</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tg-packagesbox">
                                        <div class="row" id="descriptionTen" style="display: none;">
                                            <div class="col-sm-9">
                                                <h4>Ganas 10 oportunidades más para participar
                                                    en el concurso promocional “El mundo y Yo”</h4>
                                                <p>Al contratar el plan anual Tarifa plana - ilimitada recibirás un código de invitación para
                                                    que traigas a un colega a unirse a He Go y juntos alcanzar el éxito profesional.
                                                    Las 10 oportunidades adicionales se harán validas una vez se valide el código
                                                    con un nuevo registro.Tu esfuerzo tiene premio</p>
                                            </div>
                                            <div class="col-sm-3">
                                                <img src="{{ asset('images/world_me.png') }}" alt="El mundo y yo">
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="#!">Ver la promoción</a>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="tg-checkbox pull-right">
                                                    <input type="checkbox" id="conditions" name="condition" value="ok" {{old('condition') == 'ok' ? 'checked' : '' }}>
                                                    <label for="conditions">Acepto los términos y condiciones de la promoción</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-summary" style="margin-top: 20px">
                                            <div class="tg-tablescroll">
                                                <table class="table table-striped tg-dashboardtable">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="2">Resumen</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Plan Joven ilimitado</td>
                                                        <td>S/ 210.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="margin-left: 20px">- 10 (diez) oportunidades concurso “El Mundo y yo”</td>
                                                        <td>S/ 0.00</td>
                                                    </tr>
                                                    <tr style="background: #e6e7e9">
                                                        <td>Total</td>
                                                        <td>S/ 210.00</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tg-paymentoption">
                                            <div class="tg-tablescroll">
                                                <table class="table table-striped tg-dashboardtable">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="2">Métodos de pago</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="tg-radio">
                                                                <input type="radio" id="option_tc" name="method" value="tc">
                                                                <label for="option_tc">
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/icons/icon-31.png" alt="image description"> Pago con tarjeta (10% de dscto)
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tg-radio">
                                                                <input type="radio" id="option_bn" name="method" value="bn">
                                                                <label for="option_bn">
                                                                    <img style="max-width: 20%" src="{{ asset('images/logo_bn.png') }}" alt="image description"> Pago con ingreso en cuenta en el Banco de la Nación
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tg-pkgfoot">
												<span style="padding: 0" class="tg-checkbox pull-right">
													<input type="checkbox" id="rermsconditions" name="currentjob" value="agreed">
													<label for="rermsconditions">Acepto <a href="javascript:void(0);">términos y condiciones</a></label>
												</span>
                                                <br>
                                                <span style="padding: 0" class="tg-checkbox pull-right">
													<input type="checkbox" id="rermsconditions2" name="currentjob2" value="agreed">
													<label for="rermsconditions2">Acepto <a href="javascript:void(0);">términos y condiciones</a> de la promoción</label>
												</span>
                                            <button type="button" class="tg-btn" data-toggle="modal" data-target="#pageModal">Pagar ahora</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <div class="modal fade tg-categoryModal" id="pageModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document" style="max-width: 1000px">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <img src="{{ asset('images/logo.png') }}" alt="">
                    <div class="pull-right">
                        <span>Pedido Nro: 2530/2018</span>
                        <br>
                        <span>Fecha: 04/11/2018</span>
                        <br>
                        <span>Hora: 03:30 pm</span>
                    </div>
                    <br>
                    <h2 style="margin-top: 20px">Orden informativa para pago en ventanilla de sucursal bancaria</h2>
                </div>
                <div class="tg-modalbody">
                    <div class="row">
                        <div class="col-sm-4">
                            <h3 style="font-weight: 600">DATOS PARA DEPÓSITO</h3>
                        </div>
                        <div class="col-sm-4">
                            <span>Entidad bancaria:</span><br>
                            <span>Número de cuenta:</span><br>
                            <span>Código de cuenta interbancario (CCI):</span><br>
                            <span>Concepto:</span><br>
                            <span>Referencia del servicio:</span><br>
                            <span>Importe:</span>
                        </div>
                        <div class="col-sm-4">
                            <span>Banco de la Nación - Perú</span><br>
                            <span>2587 258 4588 478</span><br>
                            <span>12345 12345 12345 12345</span><br>
                            <span>Pago de Servicios</span><br>
                            <span>Membresía - Tarifa Plana Ilimitada</span><br>
                            <span>210.00 soles</span>
                        </div>
                        <div class="col-sm-8 col-lg-offset-4"><hr></div>
                        <div class="col-sm-4">
                            <h3 style="font-weight: 600">DATOS DEL CLIENTE</h3>
                        </div>
                        <div class="col-sm-4">
                            <span>Nombre y Apellidos:</span><br>
                            <span>ID único:</span><br>
                            <span>Documento de identidad:</span><br>
                            <span>Cargo:</span><br>
                            <span>Dirección:</span><br>
                            <span>Teléfono móvil:</span>
                        </div>
                        <div class="col-sm-4">
                            <span>Juan Perez Conde / La Tuerca S.A</span><br>
                            <span>35</span><br>
                            <span>Pasaporte - 12345678</span><br>
                            <span>Empresa / Prestador de servicios independiente</span><br>
                            <span> Av. simpre viva, Manzana H, Lote 4,Trujillo, La Libertad</span><br>
                            <span>0051 - 954 954 954</span>
                        </div>
                        <div class="col-sm-8 col-lg-offset-4"><hr></div>
                        <div class="col-sm-4">
                            <h3 style="font-weight: 600">DATOS DEL BENEFICIARIO</h3>
                        </div>
                        <div class="col-sm-4">
                            <span>Razón Social:</span><br>
                        </div>
                        <div class="col-sm-4">
                            <span>Red Digital 3.0 S.L</span><br>
                        </div>
                    </div>
                    <p style="background-color: #ebebec; color: black; margin-top: 20px; padding: 5px">
                        Con este medio de pago seleccionado, se recomienda hacer el deposito
                        dentro de los 3 días siguientes para evitar que el sistemacancele tu pedido automatícamente
                    </p>
                    <p>
                        Te recordamos que tu orden no será procesada hasta haber registrado el pago correspondiente,
                        por lo cual terequerimos notificarlo a la brevedad posible con cualquiera de las siguientes vías:
                    </p>
                    <ol>
                        <li><strong>Envíanos un email:</strong> toma una foto a esta orden de pago y al voucher del banco >
                            adjuntalos y envía los dos archivos al email: pagos@redhego.com. en un lapso de
                              24 horas tu cuenta se activara.</li>
                        <li><strong>Adjunta estos dos archivos desde tu cuenta (recomendado):</strong> validar documentación de pago</li>
                    </ol>
                </div>
                <div class="tg-modalfoot">
                    <p>Ayuda al usuario: soportepagos@redhego.com  / 00 34-691 286 384</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function showDescription(val) {
            if(val == 6){
                $('#descriptionTen').hide();
                $('input[name="chance"]').prop("checked", false);
            }
            else if(val == 10){
                $('#descriptionTen').show();
                $('#mounth12').prop("checked", true);
            }
            else if(val == 1){
                $('#descriptionTen').hide();
                $('#mounth12').prop("checked", true);
            }
            else
            {
                $('#descriptionTen').hide();
            }
        }

        function expireyCounter(){
            var note = jQuery('#tg-note'),
                ts = new Date(2017, 12, 31),
                newYear = true;
            if((new Date()) > ts){
                ts = (new Date()).getTime() + 10*24*60*60*1000;
                newYear = false;
            }
            jQuery('#tg-countdown').countdown({
                timestamp	: ts,
                callback	: function(days, hours, minutes, seconds){
                    var message = "";
                    message += days + " day" + ( days==1 ? '':'s' ) + ", ";
                    message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
                    message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
                    message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";
                    /*if(newYear){
                        message += "left until the new year!";
                    }
                    else {
                        message += "left to 10 days from now!";
                    }*/
                    note.html(message);
                }
            });
        }
        expireyCounter();
    </script>
@endsection
