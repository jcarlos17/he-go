<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
    <aside id="tg-sidebar" class="tg-sidebar">
        <div class="tg-widgetprofile">
            <figure class="tg-profilebannerimg">
                <img id="menuBanner" src="{{ asset('/images/red_icons.svg') }}" alt="image description">
            </figure>
            <div class="tg-widgetcontent">
                <figure>
                    <img id="menuPhoto" src="{{ auth()->user()->photo_selected }}" alt="{{ auth()->user()->name }}">
                    {{--<a class="tg-btnedite" href="javascript:void(0);"><i class="lnr lnr-pencil"></i></a>--}}
                </figure>
                <div class="tg-admininfo">
                    <h3>{{ auth()->user()->name }}</h3>
                    <h4>Bright Future Group &amp; Company</h4>
                    <ul class="tg-matadata">
                        <li><span class="tg-stars"><span></span></span></li>
                        <li>
                            <i class="fa fa-thumbs-o-up"></i>
                            <em>99% (1009 votes)</em>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-sm-6" style="padding-right: 0">
                            <div class="progress" style="margin: 10px 0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:{{ auth()->user()->progress_percentage }}%; background: #5dc560">
                                    {{ auth()->user()->progress_percentage }}%
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6" style="margin-top: 10px; padding-left: 0">
                            <b><small style="color: #5dc560">Perfil completado</small></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tg-widgetdashboard">
            @if(auth()->user()->is_provider)
                <a href="{{ url('request-rating') }}" class="tg-btn tg-btn-lg" data-message-contact="" style="font: 400 14px/25px 'Work Sans', Arial, Helvetica, sans-serif;">
                    SOLICITAR CALIFICACIÓN
                </a>
            @endif
            <nav id="tg-dashboardnav" class="tg-dashboardnav">
                <ul>
                    <li @if(request()->is('insights')) class="tg-active" @endif>
                        <a href="{{ url('/insights') }}">
                            <i class="lnr lnr-database"></i>
                            <span>Vista Inicial</span>
                        </a>
                    </li>
                    @if(auth()->user()->is_admin)
                        <li @if(request()->is('admin/categories')) class="tg-active" @endif>
                            <a href="{{ url('admin/categories') }}">
                                <i class="lnr lnr-menu-circle"></i>
                                <span>Gestionar categorias</span>
                            </a>
                        </li>
                        <li @if(request()->is('admin/images')) class="tg-active" @endif>
                            <a href="{{ url('admin/images') }}">
                                <i class="lnr lnr-picture"></i>
                                <span>Gestionar imágenes</span>
                            </a>
                        </li>
                        <li @if(request()->is('admin/packages')) class="tg-active" @endif>
                            <a href="{{ url('admin/packages') }}">
                                <i class="lnr lnr-layers"></i>
                                <span>Gestionar membresias</span>
                            </a>
                        </li>
                    @endif
                    <li @if(request()->is('perfil-publico/'.auth()->id())) class="tg-active" @endif>
                        <a href="{{ url('/perfil-publico/'.auth()->id()) }}">
                            <i class="lnr lnr-user"></i>
                            <span>Mi perfil público</span>
                        </a>
                    </li>
                    <li @if(request()->is('profile-settings')) class="tg-active" @endif>
                        <a href="{{ url('/profile-settings') }}">
                            <i class="lnr lnr-cog"></i>
                            <span>Configuración de perfil</span>
                        </a>
                    </li>
                    <li @if(request()->is('gestionar-trabajos')) class="tg-active" @endif>
                        <a href="{{ url('/gestionar-trabajos') }}">
                            <i class="lnr lnr-paperclip"></i>
                            <span>Gestión de trabajos</span>
                        </a>
                    </li>
                    <li @if(request()->is('chat')) class="tg-active" @endif>
                        <a href="{{ url('/chat') }}">
                            <i class="lnr lnr-bubble"></i>
                            <span>Mensajes de chat</span>
                        </a>
                    </li>
                    {{--<li @if(request()->is('profile-observation')) class="tg-active" @endif>--}}
                        {{--<a href="{{ url('/profile-observation') }}">--}}
                            {{--<i class="lnr lnr-eye"></i>--}}
                            {{--<span>Perfiles en observación</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    @if(auth()->user()->is_provider)
                        <li @if(request()->is('business-hours')) class="tg-active" @endif>
                            <a href="{{ url('/business-hours') }}">
                                <i class="lnr lnr-clock"></i>
                                <span>Horario de trabajo</span>
                            </a>
                        </li>
                        <li @if(request()->is('manage-services')) class="tg-active" @endif>
                            <a href="{{ url('/manage-services') }}">
                                <i class="lnr lnr-briefcase"></i>
                                <span>Administrar mis servicios</span>
                            </a>
                        </li>
                        <li @if(request()->is('manage-team')) class="tg-active" @endif>
                            <a href="{{ url('/manage-team') }}">
                                <i class="lnr lnr-users"></i>
                                <span>Mi red de colaboradores</span>
                            </a>
                        </li>
                        <li @if(request()->is('proposal-sent')) class="tg-active" @endif>
                            <a href="{{ url('/proposal-sent') }}">
                                <i class="lnr lnr-map"></i>
                                <span>Propuestas enviadas</span>
                            </a>
                        </li>
                    @endif
                    <li @if(request()->is('proposal-received')) class="tg-active" @endif>
                        <a href="{{ url('/proposal-received') }}">
                            <i class="lnr lnr-map"></i>
                            <span>Propuestas recibidas</span>
                        </a>
                    </li>
                    @if(auth()->user()->is_provider)
                        <li @if(request()->is('admin/send-message')) class="tg-active" @endif>
                            <a href="{{ url('/admin/send-message') }}">
                                <i class="lnr lnr-location"></i>
                                <span>Enviar mensaje</span>
                            </a>
                        </li>
                    @endif

                    <li @if(request()->is('private-messages')) class="tg-active" @endif>
                        <a href="{{ url('/private-messages') }}">
                            <i class="lnr lnr-envelope"></i>
                            <span>Mensajes internos</span>
                            <em class="tg-totalmessages">526</em>
                            <em class="tg-newmessages">1</em>
                        </a>
                    </li>
                    <li @if(request()->is('appointments')) class="tg-active" @endif>
                        <a href="{{ url('/appointments') }}">
                            <i class="lnr lnr-calendar-full"></i>
                            <span>Agenda y citas</span>
                        </a>
                    </li>
                    @if(auth()->user()->is_provider)
                    <li @if(request()->is('opinions')) class="tg-active" @endif>
                        <a href="{{ url('/opinions') }}">
                            <i class="lnr lnr-list"></i>
                            <span>Opiniones</span>
                        </a>
                    </li>
                    <li @if(request()->is('appointment-settings')) class="tg-active" @endif>
                        <a href="{{ url('/appointment-settings') }}">
                            <i class="lnr lnr-pushpin"></i>
                            <span>Administrar agenda</span>
                        </a>
                    </li>
                    @endif
                    <li @if(request()->is('favourite-listings')) class="tg-active" @endif>
                        <a href="{{ url('/favourite-listings') }}">
                            <i class="lnr lnr-heart"></i>
                            <span>Mi lista de favoritos</span>
                        </a>
                    </li>

                    @if(auth()->user()->is_provider)
                        <li @if(request()->is('invoices')) class="tg-active" @endif>
                            <a href="{{ url('/invoices') }}">
                                <i class="lnr lnr-list"></i>
                                <span>Boletines de servicios</span>
                                <em class="tg-invoices">23</em>
                            </a>
                        </li>
                        <li class="tg-updatepackage @if(request()->is('update-package')) tg-active @endif">
                            <a href="{{ url('/update-package') }}">
                                <i class="lnr lnr-layers"></i>
                                <span>Mi membresía</span>
                                <em class="lnr lnr-flag tg-taginfo"></em>
                            </a>
                        </li>
                        <li class="tg-updatepackage @if(request()->is('finances')) tg-active @endif">
                            <a href="{{ url('/finances') }}">
                                <i class="lnr lnr-chart-bars"></i>
                                <span>Mis finanzas</span>
                            </a>
                        </li>
                        <li @if(request()->is('privacy-settings')) class="tg-active" @endif>
                            <a href="{{ url('/privacy-settings') }}">
                                <i class="lnr lnr-lock"></i>
                                <span>Configuración de privacidad</span>
                            </a>
                        </li>
                    @endif
                    <li @if(request()->is('security-settings')) class="tg-active" @endif>
                        <a href="{{ url('/security-settings') }}">
                            <i class="lnr lnr-construction"></i>
                            <span>Configuraciones de seguridad</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="lnr lnr-exit"></i>
                            <span>Cerrar sesión</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="tg-widgetdashboard hidden-xs">
            <div class="tg-banneradd">
                <figure>
                    @if(auth()->check() && auth()->user()->is_provider)
                        @if(imgBottomInsight())
                            <a href="javascript:void(0);"><img src="{{ imgBottomInsight()->image_url }}" alt="image description"></a>
                            {{--@else--}}
                            {{--<a href="javascript:void(0);"><img src="http://mydesignhoard.com/HTML/html/service_provider/images/add-01.png" alt="image description"></a>--}}
                        @endif
                    @else
                        @if(imgBottomInsightBS())
                            <a href="javascript:void(0);"><img src="{{ imgBottomInsightBS()->image_url }}" alt="image description"></a>
                            {{--@else--}}
                            {{--<a href="javascript:void(0);"><img src="http://mydesignhoard.com/HTML/html/service_provider/images/add-01.png" alt="image description"></a>--}}
                        @endif
                    @endif
                </figure>
            </div>
        </div>
    </aside>
</div>