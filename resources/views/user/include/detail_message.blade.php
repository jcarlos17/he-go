<div class="tg-emailsubjectactions">
    <div class="tg-emailsubject">
        <h2>{{ $message->topic }}</h2>
    </div>
    <div class="tg-btnactions">
        <a class="tg-btnreplyall" href="javascript:void(0);"><i class="fa fa-reply-all"></i></a>
        <a class="tg-btnreply" href="javascript:void(0);"><i class="fa fa-reply"></i></a>
        <a class="tg-btnfarword" href="javascript:void(0);"><i class="fa fa-mail-forward"></i></a>
        <a class="tg-btndelemail" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
    </div>
</div>
<div class="tg-emailauthor">
    <div class="tg-attachemtntime">
        <a class="tg-attachemtn" href="javascript:void(0);"><i class="fa fa-paperclip"></i></a>
        <time datetime="2016-12-12">{{ $message->hour }}</time>
    </div>
    <figure><img src="{{ $message->from->photo_selected }}" alt="image desription" style="width: 40px"></figure>
    <div class="tg-emailauthorinfo">
        <h3>{{ $message->from->name }}</h3>
        <span>Para mi:</span>
    </div>
</div>
<div class="tg-emailmessagedetail">
    <span>Hola {{ $message->to->name }},</span>
    <div class="tg-description">
        <p>{!! nl2br($message->message)  !!}</p>
    </div>
    @if($message->type == 'message_claim')
        <div class="alert alert-info tg-alertmessage fade in">
            <span style="margin: 0"><strong>Es importante contactar a este usuario directamente para atender su reclamo.</strong></span>
        </div>
    @endif
    <div class="tg-senderinfo">
        {{--<span>Gracias y saludos</span>--}}
        <span>{{ $message->from->name }}</span>
        <span>teléfono: {{ $message->from->phone }}</span>
        <em>Email: </em><a href="mailto:{{ $message->from->email }}"> {{ $message->from->email }}</a>
    </div>
</div>
<div class="tg-emailattachemnets">
    <div class="tg-attachementhead">
        <h3>Archivo adjunto</h3>
        <a href="#" class="tg-btndownloadattachment"><i class="lnr lnr-download"></i></a>
    </div>
    <ul class="tg-imgattachemnet">
        <li><a href="javascript:void(0);"><img src="http://mydesignhoard.com/HTML/html/service_provider/images/attachments/img-01.jpg" alt="image description"></a></li>
        <li><a href="javascript:void(0);"><img src="http://mydesignhoard.com/HTML/html/service_provider/images/attachments/img-02.jpg" alt="image description"></a></li>
        <li><a href="javascript:void(0);"><img src="http://mydesignhoard.com/HTML/html/service_provider/images/attachments/img-03.jpg" alt="image description"></a></li>
        <li><a href="javascript:void(0);"><img src="http://mydesignhoard.com/HTML/html/service_provider/images/attachments/img-04.jpg" alt="image description"></a></li>
    </ul>
</div>