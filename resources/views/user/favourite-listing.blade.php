@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Lista de favoritos</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Lista de favoritos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard">
                                <div class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-dashboardfavoritelisting">
                                            <div class="tg-dashboardtitle">
                                                <h2>Mi lista de favoritos</h2>
                                                {{--<button class="tg-btnaddservices">Delete All</button>--}}
                                            </div>
                                            <div class="tg-dashboardappointmentbox">
                                                @foreach($user->favorites as $favorite)
                                                    <div class="tg-dashboardappointment">
                                                        <div class="tg-servicetitle">
                                                            <figure>
                                                                <img src="{{ $favorite->userFavorite->photo_selected }}" alt="image description">
                                                            </figure>
                                                            <div class="tg-clientcontent">
                                                                <h2><a href="javascript:void(0);">{{ $favorite->userFavorite->name }}</a></h2>
                                                                <ul class="tg-matadata">
                                                                    <li><span class="tg-stars"><span></span></span></li>
                                                                    <li>
                                                                        <i class="fa fa-thumbs-o-up"></i>
                                                                        <em>99% (1009 votes)</em>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="tg-btntimeedit">
                                                            <button class="tg-btndel" data-delete="{{ url('favorite/'.$favorite->id.'/delete') }}"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-delete]', function () {
            // request al servidor
            const urldeleteFavorite = $(this).data('delete');
            const $div = $(this).closest('.tg-dashboardappointment');
            $.ajax({
                type: 'GET',
                url: urldeleteFavorite,

                success:function (){
                    $div.remove();
                }
            });

        });
    </script>
@endsection
