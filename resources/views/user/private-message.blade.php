@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Mensajes internos</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Mensajes internos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboarprivatemessages">
                                <form class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-dashboardmessages">
                                            <div class="tg-dashboardtitle">
                                                <h2>Avisos</h2>
                                            </div>
                                            <div class="tg-dashboardmessagesbox">
                                                <div class="tg-messageshead">
                                                    <ul class="tg-actionnav">
                                                        <li>
                                                            <a href="#">
                                                                <div class="tg-checkbox">
                                                                    <input type="checkbox" name="selectall" id="selectall" value="selectall">
                                                                    <label for="selectall">Select all</label>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li><a href="#">Mark as unread</a></li>
                                                        <li><a href="#">Add to spam / junk</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                    <ul class="tg-messagespagecount">
                                                        <li>1-12 of 526</li>
                                                        <li><a href="#" class="tg-btn"><i class="fa fa-angle-left"></i></a></li>
                                                        <li><a href="#" class="tg-btn"><i class="fa fa-angle-right"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="tg-emailnavbox tg-emailnavscroll">
                                                    <ul class="tg-emailnav">
                                                        <li>
                                                            <div class="form-group">
                                                                <button type="button"><i class="lnr lnr-magnifier"></i></button>
                                                                <input type="search" name="search" class="form-control" placeholder="Search Email">
                                                            </div>
                                                        </li>
                                                        @foreach($messages as $message)
                                                            <li data-show="{{ url('/private-message/'.$message->id.'/show') }}">
                                                                <div class="tg-checkbox">
                                                                    <input type="checkbox" id="email-{{$message->id}}" name="email-{{$message->id}}" value="email-{{$message->id}}" checked>
                                                                    <label for="email-{{$message->id}}">{{ $message->from->name }}
                                                                        <div class="tg-emailhead">
                                                                            <div class="tg-timeattachment">
                                                                                <i class="fa fa-paperclip"></i>
                                                                                <span>{{ $message->hour }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tg-messageinfo">
                                                                            <span>{{ $message->topic_presentation }}</span>
                                                                            <div class="tg-description">
                                                                                <p>{{ $message->message_presentation }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="tg-emailmessage">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-show]', function () {

            const urlHour = $(this).data('show');
            const content = $('.tg-emailmessage');
            $.ajax({
                url: urlHour,
                type: 'GET',

                success:function (data){
                    content.empty();
                    content.html(data);

                },
            });
        });
    </script>
@endsection
