@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Vista inicial</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Vista inicial</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard">
                                <div class="tg-banneradd">
                                    <figure>
                                        @if(auth()->check() && auth()->user()->is_provider)
                                            @if($imgTopInsight)
                                                <a href="javascript:void(0);" style="margin-bottom: 15px"><img src="{{ $imgTopInsight->image_url }}" alt="image description"></a>
                                            {{--@else--}}
                                                {{--<a href="javascript:void(0);"><img src="{{ asset('images/advertisement/img-01.png') }}" alt="image description"></a>--}}
                                            @endif
                                        @else
                                            @if($imgTopInsightBS)
                                                <a href="javascript:void(0);" style="margin-bottom: 15px"><img src="{{ $imgTopInsightBS->image_url }}" alt="image description"></a>
                                            {{--@else--}}
                                                {{--<a href="javascript:void(0);"><img src="{{ asset('images/advertisement/img-02.png') }}" alt="image description"></a>--}}
                                            @endif
                                        @endif
                                    </figure>
                                </div>
                                <div class="tg-alertmessages tg-ceomessage" style="margin: 0">
                                    <div class="alert alert-danger tg-alertmessage fade in">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>
                                        <h2>Greetings &amp; Welcome!</h2>
                                        <span>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore mana aliqua enim adiata mini veniata quis nostrud exercitation ullamco laboris nisi ut aliquip extea commodo.</span>
                                        <div class="tg-ceobottom">
                                            <div class="tg-ceocontent">
                                                <figure>
                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/ceo-img.jpg" alt="image description">
                                                </figure>
                                                <div class="tg-ceoinfo">
                                                    <span>Regards,</span>
                                                    <span>Arden Hockenberry (C.E.O)</span>
                                                </div>
                                            </div>
                                            <strong class="tg-logo"><img src="images/slogo.png" alt="image description"></strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-dashboardnotifications">
                                    <div class="tg-dashboardnotificationholder">
                                        <div class="tg-dashboardnotofication tg-totaljobs">
                                            <i class="lnr lnr-calendar-full"></i>
                                            <div class="tg-dashboardinfo">
                                                <h3>Total Jobs Posted</h3>
                                                <span>3547 Jobs Posted</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tg-dashboardnotificationholder">
                                        <div class="tg-dashboardnotofication tg-totalappointment">
                                            <i class="lnr lnr-layers"></i>
                                            <div class="tg-dashboardinfo">
                                                <h3>Total Appointments</h3>
                                                <span>57521 Appointments Served</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tg-dashboardnotificationholder">
                                        <div class="tg-dashboardnotofication tg-currentpackege">
                                            <i class="lnr lnr-eye"></i>
                                            <div class="tg-dashboardinfo">
                                                <h3>Current Package</h3>
                                                <span>Basic Package</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($user->is_seeker)
                                    <div class="tg-dashboardnotifications text-center">
                                        <img src="{{ asset('images/vista_inicial_BS.png') }}" alt="">
                                    </div>
                                    <div class="tg-competinggraph">
                                        <h2 style="margin-bottom: 10px">Encuentra de la forma más fácil al profesional, emprendedor, empresa o negocio local de tu zona o de todo el país</h2>
                                        <p>Desde un electricista a las 3 de la madrugada a un técnico de computadoras a domicilio.<br>
                                            Desde un cocinero de comida tailandesa para esa ocasión especial a un abogado especializado en extranjería que te ayudará
                                        a establecerte en el país de tus sueños.</p>
                                    </div>
                                @endif
                                @if($user->is_provider)
                                    <h5 style="margin-bottom: 10px; font-weight: 600">Documento oficial de identificación</h5>
                                    <p>
                                        Nuestro compromiso de transparencia nos obliga a verificar tu identidad como persona natural o persona jurídica mediante
                                        la documentación que nos proporcionas. Recuerda que esto es solo para gestión interna, estos datos no se mostraran al
                                        público. Tienes 7 días para identificarte, caso contrario se inhabilitara tu perfil hasta subsanar esta incidencia.
                                    </p>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-5 col-lg-offset-1">
                                                    <div style="padding: 5px 10px 10px 10px; border-radius: 10px; border: 2px dashed #ddd;">
                                                        <p style="margin: 5px; font-weight: 600; color: #5dc560">Verificado</p>
                                                        <img src="{{ asset('images/icon_dni_view.png') }}" style="max-height: 75px">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div style="padding: 5px 10px 10px 10px; border-radius: 10px; border: 2px dashed #ddd;">
                                                        <p style="margin: 5px; font-weight: 600; color: #f2646f">Pendiente</p>
                                                        <img src="{{ asset('images/icon_dni_view.png') }}" style="max-height: 75px">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group text-center" style="padding-top: 20px;">
                                                    <button class="tg-btn" data-toggle="modal" data-target="#imageDocument">Añadir</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <p style="color: #5dc560; font-weight: 600"><i class="fa fa-check" style="margin-right: 10px"></i>Documento de identidad</p>
                                            <p><i class="fa fa-close" style="margin-right: 10px; color: #f2646f"></i>Recibo de servicios públicos</p>
                                            <p><i class="fa fa-close" style="margin-right: 10px; color: #f2646f"></i>Teléfono verificado</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ asset('images/man_lupa.png') }}" alt="">
                                        </div>
                                    </div>
                                    <h5 style="margin: 10px 0; font-weight: 600">Tu Esfuerzo es tu escaparate y tu camino hacia el éxito</h5>
                                    <p>
                                        Los clientes te valorarán según tus logros, demuestra que eres de confianza cumpliendo todos los objetivos de cada nivel
                                        y luce con orgullo cada medalla en tu perfil y con ello atraerás potenciales clientes.
                                    </p>
                                    <div class="row">
                                        <div class="col-sm-2 col-sm-offset-1">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#level_1">
                                                <img style="max-height: 147px" src="{{ asset('images/levels/level_1_color.png') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#level_1">
                                                <img style="max-height: 147px" src="{{ asset('images/levels/level_2_gris.png') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#level_1">
                                                <img style="max-height: 147px" src="{{ asset('images/levels/level_3_gris.png') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#level_1">
                                                <img style="max-height: 147px" src="{{ asset('images/levels/level_4_gris.png') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#level_1">
                                                <img style="max-height: 147px" src="{{ asset('images/levels/level_5_gris.png') }}" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <h5 style="margin: 20px 0">Tu nivel: <span style="color: #5dc560">Principiante</span></h5>
                                    <p class="text-center">Tener un perfil completo y lucir con orgullo las medallas que fortalecen tu transparencia
                                        y esfuerzo al trabajo aumentarán tu reputación y demanda.
                                    </p>
                                    <h5 style="margin:30px 0; font-weight: 600">Promociones</h5>
                                    <div class="row" style="margin-bottom: 30px">
                                        <div class="col-sm-2 text-center">
                                            <img src="{{ asset('images/icon_watch.png') }}" style="width: 60%">
                                            <p style="margin-top: 10px; font-weight: 600">PROMOCIONES ACTIVAS</p>
                                        </div>
                                        <div class="col-sm-6 text-center">
                                            <h5><span style="font-weight: 600; height: auto;">- El mundo y yo -</span> <small>Ver consurso</small></h5>
                                        </div>
                                        <div class="col-sm-4" style="border-left: 2px solid #5dc560">
                                            <h5>Mis números</h5>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <ul>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                        <li style="line-height: 20px">12345678</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="tg-profilewidgetholder">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="tg-profilewidget tg-peaoplereviews">
                                                <div class="tg-profilewidgethead">
                                                    <a class="tg-btnrefresh" href="javascript:void(0);"><i class="lnr lnr-sync"></i></a>
                                                    <h2>People Reviews</h2>
                                                </div>
                                                <div class="tg-box">
                                                    <div class="tg-reviewarea">
                                                        <ul class="tg-reviews tg-themescrollbar">
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-08.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">Good Service But Not ...</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-09.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">Professional Indeed</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-10.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">They Are Awsome</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-11.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">Good Keep It Up!</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-08.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">Good Service But Not ...</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-09.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">Good Service But Not ...</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <figure>
                                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/thumbnails/img-10.jpg" alt="image description">
                                                                </figure>
                                                                <div class="tg-reviewcontent">
                                                                    <div class="tg-reviewhead">
                                                                        <div class="tg-reviewheading">
                                                                            <h3><a href="javascript:void(0);">Good Service But Not ...</a></h3>
                                                                        </div>
                                                                        <span class="tg-stars"><span></span></span>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>Labore et dolore magna aliqua enim inim veniam quis nostrud exercitation ullamco...</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="tg-profilewidget tg-recentactivities">
                                                <div class="tg-profilewidgethead">
                                                    <a class="tg-btnrefresh" href="javascript:void(0);"><i class="lnr lnr-sync"></i></a>
                                                    <h2>Recet Activities</h2>
                                                </div>
                                                <div class="tg-box">
                                                    <div class="tg-reviewarea">
                                                        <ul class="tg-reviews tg-themescrollbar">
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Logged In Via Mobile</h3>
                                                                    <span>02 Minutes Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Liked In <a href="#">David Jhonson</a></h3>
                                                                    <span>01 Day Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Updated Your Profile Photo</h3>
                                                                    <span>01 Day Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Posted A Job - <a href="#">Carpenter Required</a></h3>
                                                                    <span>03 Day Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Subscribed Basic Package</h3>
                                                                    <span>03 Days Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Liked In <a href="#">David Jhonson</a></h3>
                                                                    <span>02 Minutes Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Updated Your Profile Photo</h3>
                                                                    <span>02 Minutes Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Posted A Job - <a href="#">Carpenter Required</a></h3>
                                                                    <span>02 Minutes Ago</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="tg-recentcontent">
                                                                    <h3>You Logged In Via Mobile</h3>
                                                                    <span>02 Minutes Ago</span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="tg-profilewidget tg-todolists">
                                                <div class="tg-profilewidgethead">
                                                    {{--<a class="tg-btnrefresh" href="javascript:void(0);"><i class="fa fa-plus"></i></a>--}}
                                                    <h2>Lista de tareas</h2>
                                                </div>
                                                <div id="alert-task"></div>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="new_task" placeholder="Nueva tarea">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <button class="tg-btn" data-task="{{ url('/tasks/add') }}" style="padding: 0 10px;"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tg-box">
                                                    <div class="tg-reviewarea">
                                                        <ul id="uploadedTasks" class="tg-reviews tg-themescrollbar">
                                                            @foreach($user->tasks as $task)
                                                                <li id="item-task">
                                                                    <div class="tg-checkbox">
                                                                        <input type="checkbox" id="{{ $task->id }}" value="{{ $task->id }}" onchange="save(this.value,this.checked);" {{ $task->date_completed ? 'checked' : '' }}>
                                                                        <label for="{{ $task->id }}">
                                                                            @if($task->date_completed)
                                                                                <span id="checked{{$task->id}}"><del>{{ $task->description }}</del></span>
                                                                            @else
                                                                                <span id="checked{{$task->id}}">{{ $task->description }}</span>
                                                                            @endif
                                                                            <a data-deletetask="{{ url('/tasks/'.$task->id.'/delete') }}" href="javascript:void(0);" class="tg-btndel">Eliminar</a></label>
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                        <template id="templateTask">
                                                            <li id="item-task">
                                                                <div class="tg-checkbox">
                                                                    <input type="checkbox" id="" value="" onchange="save(this.value,this.checked);">
                                                                    <label for=""><span id="checked"></span><a data-deletetask="" href="javascript:void(0);" class="tg-btndel">Eliminar</a></label>
                                                                </div>
                                                            </li>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="tg-profilewidget tg-weathercast">
                                                <div class="tg-profilewidgethead">
                                                    <a class="tg-btnrefresh" href="javascript:void(0);"><i class="lnr lnr-sync"></i></a>
                                                    <h2>Weather</h2>
                                                </div>
                                                <div class="tg-box">
                                                    <div class="tg-reviewarea">
                                                        <div id="weather"></div>
                                                    </div>
                                                    <div class="tg-weatherinfo">
                                                        <h3>Manchester, England</h3>
                                                        <ul>
                                                            <li><span>-12°</span><span>Partly Cloudy</span></li>
                                                            <li><span>High: -2° Low: -14°</span><span>Wind: SSW 3.94km/h</span></li>
                                                            <li><span>Humidity: 63</span><span>Visibility: 11.21</span></li>
                                                            <li><span>Sunrise: 7:36 am</span><span>Sunset: 4:49 pm</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <!--Theme Modal Box Start-->
    <div class="modal fade tg-socialModal" id="imageDocument" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Imágenes del documento de identidad</h2>
                </div>
                <div id="alert-identity"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        <div class="tg-upload">
                            <div class="tg-uploadhead">
                                <span>
                                    <h3>Subir documentos de identidad</h3>
                                    <i class="fa fa-exclamation-circle"></i>
                                </span>
                                <i class="lnr lnr-upload"></i>
                            </div>
                            <div class="tg-box">
                                <label class="tg-fileuploadlabel" for="inputDocIdentity">
                                    <i class="lnr lnr-cloud-upload"></i>
                                    <span>O arrastre su archivo aquí para cargar</span>
                                    <form id="formDocIdentity" style="display: none">
                                        {{ csrf_field() }}
                                        <input id="inputDocIdentity" class="tg-fileinput" type="file" multiple>
                                    </form>
                                </label>
                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                    <div id="uploadedDocIdentity" class="tg-galleryimages">
                                        @foreach($user->identities as $doc_identity)
                                            <div id="itemDocIdentity" class="tg-galleryimg">
                                                <figure>
                                                    <img src="{{ $doc_identity->doc_identity_url }}" alt="{{ $doc_identity->file }}" style="height: 80px">
                                                    <figcaption>
                                                        <a href="{{ $doc_identity->view_doc_url }}" target="_blank"><i class="fa fa-eye"></i></a>
                                                        <a data-deleteidentity="{{ url('/doc-identity/'.$doc_identity->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        @endforeach
                                    </div>
                                    <template id="templateDocIdentity">
                                        <div id="itemDocIdentity" class="tg-galleryimg">
                                            <figure>
                                                <img src="" alt="" style="height: 80px">
                                                <figcaption>
                                                    <a href="" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a data-deleteidentity=""><i class="fa fa-close"></i></a>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tg-themeform tg-formaddsocial">
                        <div class="tg-upload">
                            <div class="tg-uploadhead">
                                <span>
                                    <h3>Subir recibos de servicios públicos</h3>
                                    <i class="fa fa-exclamation-circle"></i>
                                </span>
                                <i class="lnr lnr-upload"></i>
                            </div>
                            <div class="tg-box">
                                <label class="tg-fileuploadlabel" for="inputPublicService">
                                    <i class="lnr lnr-cloud-upload"></i>
                                    <span>O arrastre su archivo aquí para cargar</span>
                                    <form id="formPublicService" style="display: none">
                                        {{ csrf_field() }}
                                        <input id="inputPublicService" class="tg-fileinput" type="file" multiple>
                                    </form>
                                </label>
                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                    <div id="uploadedPublicService" class="tg-galleryimages">
                                        @foreach($user->public_services as $publicService)
                                            <div id="itemPublicService" class="tg-galleryimg">
                                                <figure>
                                                    <img src="{{ $publicService->doc_identity_url }}" alt="{{ $publicService->file }}" style="height: 80px">
                                                    <figcaption>
                                                        <a href="{{ $publicService->view_doc_url }}" target="_blank"><i class="fa fa-eye"></i></a>
                                                        <a data-deletepublic="{{ url('/doc-identity/'.$publicService->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        @endforeach
                                    </div>
                                    <template id="templatePublicService">
                                        <div id="itemPublicService" class="tg-galleryimg">
                                            <figure>
                                                <img src="" alt="" style="height: 80px">
                                                <figcaption>
                                                    <a href="" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a data-deletepublic=""><i class="fa fa-close"></i></a>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade tg-socialModal" id="level_1" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document" style="width: 1350px">
            <div class="modal-content tg-modalcontent">
                <div class="tg-themeform tg-formrefinesearch">
                    <div class="tg-modalbody text-center">
                        <div class="row">
                            <div class="col-sm-4" style="padding: 15px;">
                                <h5 style="margin-bottom: 20px">Medallas del profesional</h5>
                                <img style="max-height: 70px" src="{{ asset('images/levels/level_1_color.png') }}" alt="">
                                <h5 style="margin: 20px 0; color: #5dc560">Principiante</h5>
                                <small style="font-size: 80%">Tu carrera hacia el éxito ha empezado y como muestra de <strong>compromiso</strong> para
                                    <strong>mejorar tu ranking
                                        y tu reputación</strong> deberás tener un plan activo y un perfil público
                                </small>
                                <br>
                                <small>
                                    Completa los objetivos requeridos en cada nivel, aumenta tu ranking,
                                    estrena nueva medalla y gana más clientes.
                                </small>
                            </div>
                            <div class="col-sm-4" style="padding: 15px;">
                                <h5 style="margin-bottom: 20px">¡Manos a la obra!</h5>
                                <img style="max-height: 70px" src="{{ asset('images/levels/level_2_color.png') }}" alt="">
                                <h5 style="margin: 20px 0; color: #5dc560">Bronce</h5>
                                <small>Haz empezado con buen pie, pero para ser visto necesitas tener un buen perfil que
                                    te describa y muestre lo mejor de tu trabajo
                                </small>
                                <br>
                                <div class="text-left">
                                    <small>Completa estos objetivos y sube de nivel:</small>
                                    <br>
                                    <small><i class="fa fa-close" style="color: #f2646f"></i> Consigue un 100% de perfil completado.</small>
                                    <br>
                                    <small><i class="fa fa-close" style="color: #f2646f"></i> Identifícate con tu documento oficial de identidad.</small>
                                    <br>
                                    <small><i class="fa fa-close" style="color: #f2646f"></i> Presentanos un recibo de servicios público (luz, agua, teléfono).</small>
                                </div>
                            </div>
                            <div class="col-sm-4" style="padding: 15px;">
                                <h5 style="margin-bottom: 20px">¡Vamos a toda máquina!</h5>
                                <img style="max-height: 70px" src="{{ asset('images/levels/level_3_color.png') }}" alt="">
                                <h6 style="margin: 20px 0; color: #5dc560">Plata - Experto</h6>
                                <small>Ya nos conoces y sabes que buscamos que rindas al máximo y muestres lo mejor de ti
                                    generando confianza y conpromiso.
                                </small>
                                <br>
                                <div class="text-left">
                                    <small class="text-left">Completa estos objetivos y sube de nivel:</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 2 ofertas de trabajo y completalas.</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Lista 5 servicios con precio cerrado.</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 2 opiniones.</small>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-4 col-sm-offset-2" style="padding: 15px;">
                                <h5 style="margin-bottom: 20px">Ser un referente</h5>
                                <img style="max-height: 70px" src="{{ asset('images/levels/level_4_color.png') }}" alt="">
                                <h5 style="margin: 20px 0; color: #5dc560">Master - Gold</h5>
                                <small>Tu esfuerzo y perseverancia va dando sus frutos. Cada día son más las personas
                                    que buscan a profesionales con experiencia y confianza en la plataforma.</small>
                                <br>
                                <div class="text-left">
                                    <small class="text-left">Completa estos objetivos y sube de nivel:</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 5 ofertas de trabajo y completalas.</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 4 valoraciones de 5 estrellas.</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 1 pago por tus servicios usando Paypal.</small>
                                </div>
                            </div>
                            <div class="col-sm-4" style="padding: 15px;">
                                <h5 style="margin-bottom: 20px">Maestro de maestros</h5>
                                <img style="max-height: 70px" src="{{ asset('images/levels/level_5_color.png') }}" alt="">
                                <h5 style="margin: 20px 0; color: #5dc560">Pro master top</h5>
                                <small>Bienvenido al último nivel. Cumpliendo estos objetivos aumentarás considerablemente las
                                    visitas a tu perfil y el número de solicitudes de trabajo.</small>
                                <br>
                                <div class="text-left">
                                    <small class="text-left">Completa estos objetivos y alcanza la cima:</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 8 ofertas de trabajo completadas.</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 10 opiniones.</small>
                                    <br>
                                    <small class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Publica un artículo de interés público sobre tu trabajo en el blog de Red He Go.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="modal fade tg-socialModal" id="level_2" tabindex="-1">--}}
        {{--<div class="modal-dialog tg-modaldialog" role="document">--}}
            {{--<div class="modal-content tg-modalcontent">--}}
                {{--<div class="tg-themeform tg-formrefinesearch">--}}
                    {{--<div class="tg-modalbody text-center">--}}
                        {{--<h2 style="font-size: 28px; margin-bottom: 20px">¡Manos a la obra!</h2>--}}
                        {{--<img style="max-height: 167px" src="{{ asset('images/levels/level_2_color.png') }}" alt="">--}}
                        {{--<h4 style="margin: 20px 0; color: #5dc560">Bronce</h4>--}}
                        {{--<p>Haz empezado con buen pie, pero para ser visto necesitas tener un buen perfil que--}}
                            {{--te describa y muestre lo mejor de tu trabajo</p>--}}
                        {{--<p class="text-left">Completa estos objetivos y sube de nivel:</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue un 100% de perfil completado.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Identifícate con tu documento oficial de identidad.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Presentanos un recibo de servicios público (luz, agua, teléfono).</p>--}}
                    {{--</div>--}}
                    {{--<div class="tg-modalfoot">--}}
                        {{--<a href="javascript:void(0)" class="pull-left" type="button" data-level-back="1">Atrás</a>--}}
                        {{--<a href="javascript:void(0)" class="pull-right" type="button" data-level-next="3">Siguiente</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal fade tg-socialModal" id="level_3" tabindex="-1">--}}
        {{--<div class="modal-dialog tg-modaldialog" role="document">--}}
            {{--<div class="modal-content tg-modalcontent">--}}
                {{--<div class="tg-themeform tg-formrefinesearch">--}}
                    {{--<div class="tg-modalbody text-center">--}}
                        {{--<h2 style="font-size: 28px; margin-bottom: 20px">¡Vamos a toda máquina!</h2>--}}
                        {{--<img style="max-height: 167px" src="{{ asset('images/levels/level_3_color.png') }}" alt="">--}}
                        {{--<h4 style="margin: 20px 0; color: #5dc560">Plata - Experto</h4>--}}
                        {{--<p>Ya nos conoces y sabes que buscamos que rindas al máximo y muestres lo mejor de ti--}}
                            {{--generando confianza y conpromiso.--}}
                        {{--</p>--}}
                        {{--<p class="text-left">Completa estos objetivos y sube de nivel:</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 2 ofertas de trabajo y completalas.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Lista 5 servicios con precio cerrado.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 2 opiniones.</p>--}}
                    {{--</div>--}}
                    {{--<div class="tg-modalfoot">--}}
                        {{--<a href="javascript:void(0)" class="pull-left" type="button" data-level-back="2">Atrás</a>--}}
                        {{--<a href="javascript:void(0)" class="pull-right" type="button" data-level-next="4">Siguiente</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal fade tg-socialModal" id="level_4" tabindex="-1">--}}
        {{--<div class="modal-dialog tg-modaldialog" role="document">--}}
            {{--<div class="modal-content tg-modalcontent">--}}
                {{--<div class="tg-themeform tg-formrefinesearch">--}}
                    {{--<div class="tg-modalbody text-center">--}}
                        {{--<h2 style="font-size: 28px; margin-bottom: 20px">Ser un referente</h2>--}}
                        {{--<img style="max-height: 167px" src="{{ asset('images/levels/level_4_color.png') }}" alt="">--}}
                        {{--<h4 style="margin: 20px 0; color: #5dc560">Master - Gold</h4>--}}
                        {{--<p>Tu esfuerzo y perseverancia va dando sus frutos. Cada día son más las personas--}}
                            {{--que buscan a profesionales con experiencia y confianza en la plataforma.</p>--}}
                        {{--<p class="text-left">Completa estos objetivos y sube de nivel:</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 5 ofertas de trabajo y completalas.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 4 valoraciones de 5 estrellas.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 1 pago por tus servicios usando Paypal.</p>--}}
                    {{--</div>--}}
                    {{--<div class="tg-modalfoot">--}}
                        {{--<a href="javascript:void(0)" class="pull-left" type="button" data-level-back="3">Atrás</a>--}}
                        {{--<a href="javascript:void(0)" class="pull-right" type="button" data-level-next="5">Siguiente</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal fade tg-socialModal" id="level_5" tabindex="-1">--}}
        {{--<div class="modal-dialog tg-modaldialog" role="document">--}}
            {{--<div class="modal-content tg-modalcontent">--}}
                {{--<div class="tg-themeform tg-formrefinesearch">--}}
                    {{--<div class="tg-modalbody text-center">--}}
                        {{--<h2 style="font-size: 28px; margin-bottom: 20px">Maestro de maestros</h2>--}}
                        {{--<img style="max-height: 167px" src="{{ asset('images/levels/level_5_color.png') }}" alt="">--}}
                        {{--<h4 style="margin: 20px 0; color: #5dc560">Pro master top</h4>--}}
                        {{--<p>Bienvenido al último nivel. Cumpliendo estos objetivos aumentarás considerablemente las--}}
                            {{--visitas a tu perfil y el número de solicitudes de trabajo.</p>--}}
                        {{--<p class="text-left">Completa estos objetivos y alcanza la cima:</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 8 ofertas de trabajo completadas.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Consigue 10 opiniones.</p>--}}
                        {{--<p class="text-left"><i class="fa fa-close" style="color: #f2646f"></i> Publica un artículo de interés público sobre tu trabajo en el blog de Red He Go.</p>--}}
                    {{--</div>--}}
                    {{--<div class="tg-modalfoot">--}}
                        {{--<a href="javascript:void(0)" class="pull-left" type="button" data-level-back="4">Atrás</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('scripts')
    <script src="{{ asset('js/insight/photo-doc-identity.js') }}"></script>
    <script src="{{ asset('js/insight/photo-public-service.js') }}"></script>
    <script>
        function updateProgressBarStatus(){
            $.ajax({
                type: 'GET',
                url: '/profile/percentage',

                success:function (data){
                    var $progressBar = $('.progress-bar');
                    $progressBar.css('width', data+'%');
                    $progressBar.text(data+'%');
                }
            });
        }
    </script>
    <script src="{{ asset('js/insight/task.js') }}"></script>
    <script>
        // $(document).on('click', '[data-level-next]', function () {
        //     const level_next = $(this).data('level-next');
        //     const level = level_next-1;
        //     var $modalShow = 1;
        //     $('#level_'+level).modal('hide');
        //     $('#level_'+level).on("hidden.bs.modal", function () {
        //         if($modalShow)
        //             $('#level_'+level_next).modal('show');
        //             $modalShow = 0;
        //
        //     });
        // });
        // $(document).on('click', '[data-level-back]', function () {
        //     const level_back = $(this).data('level-back');
        //     const level = level_back+1;
        //     var $modalShow = 1;
        //     $('#level_'+level).modal('hide');
        //     $('#level_'+level).on("hidden.bs.modal", function () {
        //         if($modalShow)
        //             $('#level_'+level_back).modal('show');
        //             $modalShow = 0;
        //     });
        // });
    </script>
@endsection
