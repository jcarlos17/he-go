@extends('layouts.app')

@section('styles')
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Configuración de perfil</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Configuración de perfil</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprofilesetting">
                                <div class="tg-themeform">
                                    <fieldset>
                                        @if(! $user->verify_identity)
                                        <div class="tg-dashboardbox tg-uploadphotos">
                                            <div class="tg-dashboardtitle">
                                                <h2>Subir imágenes</h2>
                                            </div>
                                            <div class="tg-uploadbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alert-photo"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
																	<span>
																		<h3>Subir foto de perfil / logo</h3>
																		<i class="fa fa-exclamation-circle"></i>
																	</span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputPhoto">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formPhoto" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputPhoto" class="tg-fileinput" type="file" name="image" accept="image/*" multiple>
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedPhotos" class="tg-galleryimages">
                                                                        @foreach ($user->photos as $photo)
                                                                            <div id="itemPhoto" class="tg-galleryimg">
                                                                                <figure>
                                                                                    <img src="{{ $photo->photo_url }}" alt="{{ $user->name }}" style="width: 80px; height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-selectphoto="{{ url('/photo/'.$photo->id.'/select') }}"><i class="fa fa-check"></i></a>
                                                                                        <a data-deletephoto="{{ url('/photo/'.$photo->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                    <template id="templatePhoto">
                                                                        <div id="itemPhoto" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="width: 80px; height: 80px">
                                                                                <figcaption>
                                                                                    <a data-selectphoto=""><i class="fa fa-check"></i></a>
                                                                                    <a data-deletephoto=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templatePhotoLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alert-banner"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
																	<span>
																		<h3>Subir imagen banner</h3>
																		<i class="fa fa-exclamation-circle"></i>
																	</span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputBanner">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formBanner" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputBanner" class="tg-fileinput" type="file" name="file" accept="image/*" multiple>
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedBanners" class="tg-galleryimages" style="width:1800px;">
                                                                        @foreach ($user->banners as $banner)
                                                                            <div id="itemBanner" class="tg-galleryimg">
                                                                                <figure>
                                                                                    <img src="{{ $banner->banner_url }}" alt="{{ $user->name }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-selectbanner="{{ url('/banner/'.$banner->id.'/select') }}"><i class="fa fa-check"></i></a>
                                                                                        <a data-deletebanner="{{ url('/banner/'.$banner->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                </figure>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                    <template id="templateBanner">
                                                                        <div id="itemBanner" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-selectbanner=""><i class="fa fa-check"></i></a>
                                                                                    <a data-deletebanner=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateBannerLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="tg-dashboardbox tg-basicinformation">
                                            <div class="tg-dashboardtitle">
                                                <h2>Información básica</h2>
                                                {{--<a class="tg-btnaddnew" href="#" data-toggle="modal" data-target="#imageDocument">+ Agregue imágenes del documento de identificación</a>--}}
                                            </div>
                                            <div id="alert-basicinformation"></div>
                                            @if(session('notification'))
                                            <div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>
                                                <i class="lnr lnr-bug"></i>
                                                <span><strong>{{ session('notification') }}</strong></span>
                                            </div>
                                            @endif
                                            <div class="tg-basicinformationbox">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="name" placeholder="Nombres" value="{{ $user->name }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="last_name" placeholder="Apellidos" value="{{ $user->last_name }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="phone" placeholder="Teléfono" value="{{ $user->phone }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="mobile" placeholder="Celular" value="{{ $user->mobile }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="company_name" placeholder="Nombre de la empresa (opcional)" value="{{ $user->company_name }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" name="email" placeholder="Correo" value="{{ $user->email }}" {{ $user->is_admin ? '' : 'disabled' }}>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="website" placeholder="Sitio web" value="{{ $user->website }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="identity_type">
                                                                <option value="">Seleccione tipo de documento</option>
                                                                <option value="dni" @if($user->identity_type == 'dni') selected @endif>DNI</option>
                                                                <option value="passport" @if($user->identity_type == 'passport') selected @endif>PASAPORTE</option>
                                                                <option value="immigration_card" @if($user->identity_type == 'immigration_card') selected @endif>CARNET DE EXTRANJERÍA</option>
                                                                <option value="file_ruc" @if($user->identity_type == 'file_ruc') selected @endif>FICHA RUC/SUNAT</option>
                                                                <option value="other" @if($user->identity_type == 'other') selected @endif>OTRO...</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="identity_document" value="{{ $user->identity_document }}" placeholder="Documento de identificación">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="form-group">
                                                            <button data-updateinformation="{{ url('/basic-information/'.$user->id.'/edit') }}" class="tg-btn">Guardar cambios</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(! $user->verify_identity)
                                            @if($user->is_provider)
                                            <div class="tg-dashboardbox tg-amenitiesfeatures">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Categorías de trabajo</h2>
                                                </div>
                                                <div id="alert-category"></div>
                                                <div class="tg-amenitiesfeaturesbox">
                                                    <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="category_id" id="select-category">
                                                                <option value="">Seleccione una categoría</option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                        <button class="tg-btn" data-category="{{ url('/categories/'.$user->id.'/add') }}">Agregar</button>
                                                    </div>
                                                    <div id="container-categories">
                                                        @foreach($userCategories as $category)
                                                            <div class="form-group" id="category{{$category->id}}">
                                                                <h5 style="padding-top: 10px">
                                                                    <span>{{ $category->name }}</span>
                                                                    <a class="btn-delete" data-deletecategory="{{ url('/categories/'.$category->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                </h5>
                                                                <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="subcategory_id" id="select-subcategory{{$category->id}}">
                                                                        <option value="">Seleccione una subcategoría</option>
                                                                        @foreach($category->subcategories as $subcategory)
                                                                            <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </span>
                                                                    <button class="tg-btn" data-id="{{$category->id}}" data-subcategory="{{ url('/categories/subcat/'.$user->id.'/add') }}">Agregar</button>
                                                                </div>
                                                                <ul id="list-subcategory{{$category->id}}" class="tg-tagdashboardlist">
                                                                    @foreach($category->userSubcategories as $subcategory)
                                                                        <li>
                                                                        <span class="tg-tagdashboard">
                                                                            <a data-deletesubcategory="{{ url('/categories/subcat/'.$subcategory->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                            <em>{{ $subcategory->name }}</em>
                                                                        </span>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <template id="templateNewCategory">
                                                        <div class="form-group" id="category">
                                                            <h5 style="padding-top: 10px">
                                                                <span></span>
                                                                <a class="btn-delete" data-deletecategory=""><i class="fa fa-close"></i></a>
                                                            </h5>
                                                            <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="subcategory_id" id="select-subcategory">
                                                                        <option value="">Seleccione subcategoría</option>
                                                                    </select>
                                                                </span>
                                                                <button class="tg-btn" data-id="" data-subcategory="">Agregar</button>
                                                            </div>
                                                            <ul id="list-subcategory" class="tg-tagdashboardlist">
                                                            </ul>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-socialinformation">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Enlaces redes sociales</h2>
                                                    {{--<a class="tg-btnaddnew" href="#" data-toggle="modal" data-target=".tg-socialModal">+ add new</a>--}}
                                                </div>
                                                <div id="alert-socialinformation"></div>
                                                <div class="tg-socialinformationbox">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group tg-inputwithicon tg-facebook">
                                                                <i class="tg-icon fa fa-facebook"></i>
                                                                <input type="text" class="form-control" name="facebooklink" placeholder="Facebook Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->facebooklink }}" @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group tg-inputwithicon tg-twitter">
                                                                <i class="tg-icon fa fa-twitter"></i>
                                                                <input type="text" class="form-control" name="twitterlink" placeholder="Twitter Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->twitterlink }}" @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group tg-inputwithicon tg-linkedin">
                                                                <i class="tg-icon fa fa-linkedin"></i>
                                                                <input type="text" class="form-control" name="linkedinlink" placeholder="Linkedin Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->linkedinlink }}" @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group tg-inputwithicon tg-skype">
                                                                <i class="tg-icon fa fa-skype"></i>
                                                                <input type="text" class="form-control" name="skypelink" placeholder="Skype Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->skypelink }}" @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group tg-inputwithicon tg-googleplus">
                                                                <i class="tg-icon fa fa-google-plus"></i>
                                                                <input type="text" class="form-control" name="googlepluslink" placeholder="Google Plus Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->googlepluslink }}" @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <div class="form-group tg-inputwithicon tg-pinterestp">
                                                                <i class="tg-icon fa fa-pinterest-p"></i>
                                                                <input type="text" class="form-control" name="pinterestlink" placeholder="Pinterest Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->pinterestlink }}" @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <button data-updatesocial="{{ url('/social-information/'.$user->id.'/edit') }}" class="tg-btn">Guardar cambios</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-introduction">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Carta de presentación</h2>
                                                </div>
                                                <div id="alert-presentation"></div>
                                                <div class="tg-introductionbox">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="presentation" placeholder="La primera impresión es lo que cuenta y por ello aquí debes contarnos un poco sobre ti mismo, sobre tu formación, experiencia profesional, y logros obtenidos. Recuerda escribir de forma legible y sin fallas ortográficas.">{{ $user->presentation }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <button data-updatepresentation="{{ url('/presentation/edit') }}" class="tg-btn">Guardar cambios</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-location">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Ubicación / Dirección</h2>
                                                </div>
                                                <div id="alert-address"></div>
                                                <div class="tg-locationbox">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="department_id" id="select_department" required>
                                                                        <option value="">Seleccione departamento</option>
                                                                        @foreach ($departments as $department)
                                                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="province_id" id="select_province" required>
                                                                        <option value="">Seleccione provincia</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="district_id" id="select_district" required>
                                                                        <option value="">Seleccione distrito</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="description" id="description" placeholder="Ingrese dirección">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <button class="tg-btn" data-address="{{ url('/address/'.$user->id.'/add') }}">Agregar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ul id="add-address" class="tg-tagdashboardlist">
                                                        @foreach($user->addresses as $address)
                                                            <li>
                                                                <span class="tg-tagdashboard">
                                                                    <a data-deleteaddress="{{ url('/address/'.$address->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                    <em>{{ $address->description_complete }}</em>
                                                                </span>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-languages">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Idiomas disponibles</h2>
                                                </div>
                                                <div id="alert-language"></div>
                                                <div class="tg-languagesbox">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <span class="tg-select">
                                                                <select name="language" id="select_language" required>
                                                                    <option value="">Seleccione idioma</option>
                                                                    <option value="Inglés" {{ (old("language") == 'Inglés' ? "selected":"") }}>Inglés</option>
                                                                    <option value="Árabe" {{ (old("language") == 'Árabe' ? "selected":"") }}>Árabe</option>
                                                                    <option value="Portugués" {{ (old("language") == 'Portugués' ? "selected":"") }}>Portugués</option>
                                                                    <option value="Ruso" {{ (old("language") == 'Ruso' ? "selected":"") }}>Ruso</option>
                                                                    <option value="Japonés" {{ (old("language") == 'Japonés' ? "selected":"") }}>Japonés</option>
                                                                    <option value="Alemán" {{ (old("language") == 'Alemán' ? "selected":"") }}>Alemán</option>
                                                                    <option value="Coreano" {{ (old("language") == 'Coreano' ? "selected":"") }}>Coreano</option>
                                                                    <option value="Francés" {{ (old("language") == 'Francés' ? "selected":"") }}>Francés</option>
                                                                    <option value="Turco" {{ (old("language") == 'Turco' ? "selected":"") }}>Turco</option>
                                                                    <option value="Italiano" {{ (old("language") == 'Italiano' ? "selected":"") }}>Italiano</option>
                                                                    <option value="Persa" {{ (old("language") == 'Persa' ? "selected":"") }}>Persa</option>
                                                                    <option value="Rumano" {{ (old("language") == 'Rumano' ? "selected":"") }}>Rumano</option>
                                                                    <option value="Holandés" {{ (old("language") == 'Holandés' ? "selected":"") }}>Holandés</option>
                                                                    <option value="Griego" {{ (old("language") == 'Griego' ? "selected":"") }}>Griego</option>
                                                                </select>
                                                            </span>
                                                            <button class="tg-btn" data-language="{{ url('/languages/'.$user->id.'/add') }}">Agregar</button>
                                                        </div>
                                                    <ul id="add-language" class="tg-tagdashboardlist">
                                                        @foreach($user->languages as $language)
                                                            <li>
                                                                <span class="tg-tagdashboard">
                                                                    <a data-delete="{{ url('/languages/'.$language->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                    <em>{{ $language->language }}</em>
                                                                </span>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            {{--Experience--}}
                                            <div class="tg-dashboardbox tg-certificatesawards">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Mi experiencia</h2>
                                                    <a class="tg-btnaddnew" href="#" data-toggle="modal" data-target="#experienceModal">Agregar nuevo</a>
                                                </div>
                                                <div id="uploadedExperience" class="tg-certificatesawardsbox">
                                                    @foreach($user->experiences as $experience)
                                                        <div id="new-experience" class="tg-certificatesaward">
                                                            <div class="tg-imgandtitle">
                                                                <h3 id="titleExperienceText{{$experience->id}}"><a href="javascript:void(0);">{{ $experience->title }}</a></h3>
                                                                <br>
                                                                <h3 id="companyExperienceText{{$experience->id}}" class="text-muted">{{ $experience->company }}</h3>
                                                            </div>
                                                            <div class="tg-btntimeedit">
                                                                <span id="dateIntervalText{{$experience->id}}">{{ $experience->date_interval }}</span>
                                                                <button class="tg-btnedite" data-showexperience="{{ url('/experience/'.$experience->id.'/show') }}"><i class="lnr lnr-pencil"></i></button>
                                                                <button class="tg-btndel" data-deleteexperience="{{ url('/experiences/'.$experience->id.'/delete') }}"><i class="lnr lnr-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <template id="templateExperience">
                                                        <div id="new-experience" class="tg-certificatesaward">
                                                            <div class="tg-imgandtitle">
                                                                <h3 id=""><a href="javascript:void(0);"></a></h3><br>
                                                                <h3 id="" class="text-muted"></h3>
                                                            </div>
                                                            <div class="tg-btntimeedit">
                                                                <span id=""></span>
                                                                <button class="tg-btnedite" data-showexperience=""><i class="lnr lnr-pencil"></i></button>
                                                                <button class="tg-btndel" data-deleteexperience=""><i class="lnr lnr-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>
                                            {{--Certificates--}}
                                            <div class="tg-dashboardbox tg-certificatesawards">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Certificados y premios</h2>
                                                    <a class="tg-btnaddnew" href="#" data-toggle="modal" data-target="#certificateModal">Agregar nuevo</a>
                                                </div>
                                                <div id="uploadedCertificates" class="tg-certificatesawardsbox">
                                                    @foreach($user->certificates as $certificate)
                                                        <div class="tg-certificatesaward">
                                                            <div class="tg-imgandtitle">
                                                                <figure id="ImageUrlText{{$certificate->id}}"><a href="#"><img src="{{ $certificate->image_url }}" align="image description"></a></figure>
                                                                <h3 id="titleCertificateText{{$certificate->id}}"><a href="javascript:void(0);">{{ $certificate->title }}</a></h3>
                                                            </div>
                                                            <div class="tg-btntimeedit">
                                                                <span id="dateCertificateText{{$certificate->id}}">{{ $certificate->date }}</span>
                                                                <button class="tg-btnedite" data-showcertificate="{{ url('/certificate/'.$certificate->id.'/show') }}"><i class="lnr lnr-pencil"></i></button>
                                                                <button class="tg-btndel" data-deletecertificate="{{ url('/certificates/'.$certificate->id.'/delete') }}"><i class="lnr lnr-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <template id="templateCertificate">
                                                        <div class="tg-certificatesaward">
                                                            <div class="tg-imgandtitle">
                                                                <figure id=""><a href="#"><img src="" align=""></a></figure>
                                                                <h3 id=""><a href="javascript:void(0);"></a></h3>
                                                            </div>
                                                            <div class="tg-btntimeedit">
                                                                <span id=""></span>
                                                                <button class="tg-btnedite" data-showcertificate=""><i class="lnr lnr-pencil"></i></button>
                                                                <button class="tg-btndel" data-deletecertificate=""><i class="lnr lnr-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>
                                            {{--Qualification--}}
                                            <div class="tg-dashboardbox tg-certificatesawards">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Mi preparación profesional</h2>
                                                    <a class="tg-btnaddnew" href="#" data-toggle="modal" data-target="#qualificationModal">Agregar nuevo</a>
                                                </div>
                                                <div id="uploadedQualification" class="tg-certificatesawardsbox">
                                                    @foreach($user->qualifications as $qualification)
                                                        <div id="new-qualification" class="tg-certificatesaward">
                                                            <div class="tg-imgandtitle">
                                                                <h3 id="titleQualificationText{{$qualification->id}}"><a href="javascript:void(0);">{{ $qualification->title }}</a></h3>
                                                                <br>
                                                                <h3 id="institutionQualificationText{{$qualification->id}}" class="text-muted">{{ $qualification->institution }}</h3>
                                                            </div>
                                                            <div class="tg-btntimeedit">
                                                                <span id="dateIntervalQualificationText{{$qualification->id}}">{{ $qualification->date_interval }}</span>
                                                                <button class="tg-btnedite" data-showqualification="{{ url('/qualification/'.$qualification->id.'/show') }}"><i class="lnr lnr-pencil"></i></button>
                                                                <button class="tg-btndel" data-deletequalification="{{ url('/qualifications/'.$qualification->id.'/delete') }}"><i class="lnr lnr-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <template id="templateQualification">
                                                        <div id="new-qualification" class="tg-certificatesaward">
                                                            <div class="tg-imgandtitle">
                                                                <h3 id=""><a href="javascript:void(0);"></a></h3><br>
                                                                <h3 id="" class="text-muted"></h3>
                                                            </div>
                                                            <div class="tg-btntimeedit">
                                                                <span id=""></span>
                                                                <button class="tg-btnedite" data-showqualification=""><i class="lnr lnr-pencil"></i></button>
                                                                <button class="tg-btndel" data-deletequalification=""><i class="lnr lnr-trash"></i></button>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>

                                            <div class="tg-companyfeaturebox tg-amenities" style="padding: 40px 0">
                                                <div class="tg-companyfeaturetitle">
                                                    <h3>Mis servicios incluyen</h3>
                                                </div>
                                                <div id="alert-feature"></div>
                                                <ul>
                                                    @foreach($features as $key => $feature)
                                                        <li>
                                                            <div class="tg-checkbox">
                                                                <input class="services" type="checkbox" id="{{ $feature->id }}" name="service[]" value="{{ $feature->id }}" {{ $feature->user_has_service ? 'checked' : '' }}>
                                                                <label for="{{ $feature->id }}"><span><i class="lnr {{ $feature->icon }}"></i> {{ $feature->name }}</span></label>
                                                            </div>
                                                        </li>
                                                        @if(($key+1)%3==0)
                                                            <div class="clearfix"></div>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                                <div class="form-group" style="padding-top: 20px">
                                                    <button data-feature="{{ url('/features/add') }}" class="tg-btn">Guardar cambios</button>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-imggallery">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Galería de fotos</h2>
                                                </div>
                                                <div id="alert-gallery"></div>
                                                <div class="tg-imggallerybox">
                                                    <div class="tg-upload">
                                                        <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir fotos</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                            <i class="lnr lnr-upload"></i>
                                                        </div>
                                                        <div class="tg-box">
                                                            <label class="tg-fileuploadlabel" for="inputImage">
                                                                <i class="lnr lnr-cloud-upload"></i>
                                                                <span>O arrastre sus archivos aquí para subir</span>
                                                                <form id="formImage" style="display: none">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" id="inputId" value="{{$user->id}}">
                                                                    <input name="file" class="tg-fileinput" type="file" accept="images/*" id="inputImage" multiple>
                                                                </form>
                                                            </label>
                                                            <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                <div id="uploadedImages" class="tg-galleryimages">
                                                                    @foreach($user->galleries as $gallery)
                                                                        <div class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="{{ $gallery->image_url }}" alt="image description">
                                                                                <figcaption><a data-deletegallery="{{ url('/galleries/'.$gallery->id.'/delete') }}"><i class="fa fa-close"></i></a></figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            <template id="templateImage">
                                                                <div class="tg-galleryimg">
                                                                    <figure>
                                                                        <img src="" alt="">
                                                                        <figcaption><a data-deletegallery=""><i class="fa fa-close"></i></a></figcaption>
                                                                    </figure>
                                                                </div>
                                                            </template>
                                                            <template id="templateImageLoading">
                                                                <div id="" class="tg-galleryimg tg-uploading">
                                                                    <figure>
                                                                        <img src="" alt="" style="height: 80px">
                                                                        <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                    </figure>
                                                                </div>
                                                            </template>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-videogallery">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Videos</h2>
                                                </div>
                                                <div id="alert-video"></div>
                                                <div class="tg-videogallerybox">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <input type="text" name="link" id="link" class="form-control" placeholder="Ingrese link del vídeo" value="{{ old('link') }}">
                                                        <button class="tg-btn" data-video="{{ url('/videos/'.$user->id.'/add') }}">Agregar</button>
                                                    </div>
                                                    <ul id="add-video">
                                                        @foreach($user->videos as $video)
                                                        <li>
                                                            <div class="tg-videobox">
                                                                <span class="tg-tagdashboard">
                                                                    <a data-deletevideo="{{ url('/videos/'.$video->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                    <figure>
                                                                        <a href="{{ $video->link }}" target="_blank">
                                                                            <img src="http://mydesignhoard.com/HTML/html/service_provider/images/placeholder/img-03.jpg">
                                                                        </a>
                                                                    </figure>
                                                                </span>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardbox tg-brochureupload">
                                                <div class="tg-dashboardtitle">
                                                    <h2>Mi stand y cartelería</h2>
                                                </div>
                                                <div id="alert-brochure"></div>
                                                <div class="tg-brochureuploadbox">
                                                    <div class="tg-upload">
                                                        <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir mi stand y cartelería</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                            <i class="lnr lnr-upload"></i>
                                                        </div>
                                                        <div class="tg-box">
                                                            <label class="tg-fileuploadlabel" for="inputBrochure">
                                                                <i class="lnr lnr-cloud-upload"></i>
                                                                <span>O arrastre sus archivos aquí para subir</span>
                                                                <form id="formBrochure" style="display: none">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" id="inputUserId" value="{{$user->id}}">
                                                                    <input id="inputBrochure" class="tg-fileinput" type="file" name="file" multiple>
                                                                </form>
                                                            </label>
                                                            <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                <div id="uploadedBrochures" class="tg-galleryimages">
                                                                    @foreach($user->brochures as $brochure)
                                                                        <div id="itemBrochure" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="{{ $brochure->brochure_url }}" alt="{{ $brochure->file }}" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a href="{{ $brochure->view_url }}" target="_blank"><i class="fa fa-eye"></i></a>
                                                                                    <a data-deletebrochure="{{ url('/brochures/'.$brochure->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                                <template id="templateBrochure">
                                                                    <div id="itemBrochure" class="tg-galleryimg">
                                                                        <figure>
                                                                            <img src="" alt="" style="height: 80px">
                                                                            <figcaption>
                                                                                <a href="" target="_blank"><i class="fa fa-eye"></i></a>
                                                                                <a data-deletebrochure=""><i class="fa fa-close"></i></a>
                                                                            </figcaption>
                                                                        </figure>
                                                                    </div>
                                                                </template>
                                                                <template id="templateBrochureLoading">
                                                                    <div id="" class="tg-galleryimg tg-uploading">
                                                                        <figure>
                                                                            <img src="" alt="" style="height: 80px">
                                                                            <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                        </figure>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                                <div class="tg-dashboardbox tg-location">
                                                    <div class="tg-dashboardtitle">
                                                        <h2>Ubicación / Dirección</h2>
                                                    </div>
                                                    <div id="alert-address"></div>
                                                    <div class="tg-locationbox">
                                                        {{ csrf_field() }}
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="department_id" id="select_department" required>
                                                                        <option value="">Seleccione departamento</option>
                                                                        @foreach ($departments as $department)
                                                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="province_id" id="select_province" required>
                                                                        <option value="">Seleccione provincia</option>
                                                                    </select>
                                                                </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                <div class="form-group">
                                                                <span class="tg-select">
                                                                    <select name="district_id" id="select_district" required>
                                                                        <option value="">Seleccione distrito</option>
                                                                    </select>
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-9">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="description" id="description" placeholder="Ingrese dirección">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <button class="tg-btn" data-address="{{ url('/address/'.$user->id.'/add') }}">Agregar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <ul id="add-address" class="tg-tagdashboardlist">
                                                            @foreach($user->addresses as $address)
                                                                <li>
                                                                <span class="tg-tagdashboard">
                                                                    <a data-deleteaddress="{{ url('/address/'.$address->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                    <em>{{ $address->description_complete }}</em>
                                                                </span>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tg-dashboardbox tg-socialinformation" style="padding-bottom: 50px">
                                                    <div class="tg-dashboardtitle">
                                                        <h2>Enlaces redes sociales</h2>
                                                        {{--<a class="tg-btnaddnew" href="#" data-toggle="modal" data-target=".tg-socialModal">+ add new</a>--}}
                                                    </div>
                                                    <div id="alert-socialinformation"></div>
                                                    <div class="tg-socialinformationbox">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="form-group tg-inputwithicon tg-facebook">
                                                                    <i class="tg-icon fa fa-facebook"></i>
                                                                    <input type="text" class="form-control" name="facebooklink" placeholder="Facebook Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->facebooklink }}" @endif>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="form-group tg-inputwithicon tg-twitter">
                                                                    <i class="tg-icon fa fa-twitter"></i>
                                                                    <input type="text" class="form-control" name="twitterlink" placeholder="Twitter Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->twitterlink }}" @endif>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="form-group tg-inputwithicon tg-linkedin">
                                                                    <i class="tg-icon fa fa-linkedin"></i>
                                                                    <input type="text" class="form-control" name="linkedinlink" placeholder="Linkedin Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->linkedinlink }}" @endif>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="form-group tg-inputwithicon tg-skype">
                                                                    <i class="tg-icon fa fa-skype"></i>
                                                                    <input type="text" class="form-control" name="skypelink" placeholder="Skype Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->skypelink }}" @endif>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="form-group tg-inputwithicon tg-googleplus">
                                                                    <i class="tg-icon fa fa-google-plus"></i>
                                                                    <input type="text" class="form-control" name="googlepluslink" placeholder="Google Plus Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->googlepluslink }}" @endif>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="form-group tg-inputwithicon tg-pinterestp">
                                                                    <i class="tg-icon fa fa-pinterest-p"></i>
                                                                    <input type="text" class="form-control" name="pinterestlink" placeholder="Pinterest Link" @if($user->socialnetwork) value="{{ $user->socialnetwork->pinterestlink }}" @endif>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="form-group">
                                                                    <button data-updatesocial="{{ url('/social-information/'.$user->id.'/edit') }}" class="tg-btn">Guardar cambios</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->

    <div class="modal fade tg-socialModal" id="certificateModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Agregar certificado o premio</h2>
                </div>
                <div id="alert-certificate"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="new-title" type="text" class="form-control" name="title" placeholder="Título del certificado o premio" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <textarea id="new-description" rows="3" class="form-control" name="description" placeholder="Descripción"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tg-upload">
                            <div class="tg-uploadhead">
                                <span>
                                    <h3>Subir imagen del certificado o premio</h3>
                                    <i class="fa fa-exclamation-circle"></i>
                                </span>
                                <i class="lnr lnr-upload"></i>
                            </div>
                            <div class="tg-box">
                                <label class="tg-fileuploadlabel" for="inputCertificate">
                                    <i class="lnr lnr-cloud-upload"></i>
                                    <span>O arrastre sus archivos aquí para subir</span>
                                    <input id="inputCertificate" class="tg-fileinput" type="file" name="file" accept="image/*">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button data-certificate="{{ url('/certificates/'.$user->id.'/add') }}" class="tg-btn">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="certificateEditModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Editar certificado o premio</h2>
                </div>
                <div id="alert-certificate-edit"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="titleCertificateEdit" type="text" class="form-control" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <textarea id="descriptionCertificateEdit" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tg-upload">
                            <div class="tg-uploadhead">
                                <span>
                                    <h3>Subir imagen del certificado o premio</h3>
                                    <i class="fa fa-exclamation-circle"></i>
                                </span>
                                <i class="lnr lnr-upload"></i>
                            </div>
                            <div class="tg-box">
                                <label class="tg-fileuploadlabel" for="inputCertificateImgEdit">
                                    <i class="lnr lnr-cloud-upload"></i>
                                    <span>O arrastre sus archivos aquí para subir</span>
                                    <em>(Subir sólo si quiere reemplazar la actual)</em>
                                    <input id="inputCertificateImgEdit" class="tg-fileinput" type="file">
                                </label>
                                <div class="tg-gallery">
                                    <div id="uploadedDocIdentity" class="tg-galleryimages">
                                        <div id="itemDocIdentity" class="tg-galleryimg">
                                            <figure>
                                                <img id="imageViewCertificate" src="" alt="" style="height: 80px">
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button id="btnCertificateEdit" data-editcertificate="" class="tg-btn">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="experienceModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Agregar mi experiencia</h2>
                </div>
                <div id="alert-experience"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="title-experience" type="text" class="form-control" name="title" placeholder="Título" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="company-experience" type="text" class="form-control" name="company" placeholder="Empresa" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <textarea id="description-experience" rows="3" class="form-control" name="description" placeholder="Descripción breve"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="startDate">Fecha Inicio</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input type="date" id="startDate" name="start_date" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="endDate">Fecha Fin <em>(Vacío: Hasta la actualidad)</em></label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input id="endDate" type="date" name="end_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button data-experience="{{ url('/experiences/'.$user->id.'/add') }}" class="tg-btn">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="experienceEditModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Editar mi experiencia</h2>
                </div>
                <div id="alert-experience-edit"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="titleExperienceEdit" type="text" class="form-control" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="companyExperienceEdit" type="text" class="form-control" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <textarea id="descriptionExperienceEdit" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="startDate">Fecha Inicio</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input type="date" id="startDateEdit" class="form-control" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="endDate">Fecha Fin <em>(Vacío: Hasta la actualidad)</em></label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input id="endDateEdit" type="date" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button id="btnExperienceEdit" data-editexperience="" class="tg-btn">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="qualificationModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Agregar mi preparación profesional</h2>
                </div>
                <div id="alert-qualification"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="title-qualification" type="text" class="form-control" name="title" placeholder="Título" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="institution-qualification" type="text" class="form-control" name="institution" placeholder="Institución" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="start_date">Fecha Inicio</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input type="date" id="start_date" name="start_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="end_date">Fecha Fin</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input id="end_date" type="date" name="end_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button data-qualification="{{ url('/qualifications/'.$user->id.'/add') }}" class="tg-btn">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="qualificationEditModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Editar mi preparación profesional</h2>
                </div>
                <div id="alert-qualification-edit"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="titleQualificationEdit" type="text" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="institutionQualificationEdit" type="text" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="start_date">Fecha Inicio</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input type="date" id="startDateQualificationEdit" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="end_date">Fecha Fin</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-calendar-full"></i>
                                        <input id="endDateQualificationEdir" type="date" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button id="btnQualificationEdit" data-editqualification="" class="tg-btn">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/profile-setting/basic-information.js') }}"></script>

    <script src="{{ asset('js/profile-setting/presentation.js') }}"></script>
    <script src="{{ asset('js/profile-setting/social-information.js') }}"></script>
    <script src="{{ asset('js/profile-setting/language.js') }}"></script>
    <script src="{{ asset('js/profile-setting/address.js') }}"></script>
    <script src="{{ asset('js/profile-setting/video.js') }}"></script>
    <script src="{{ asset('js/profile-setting/category.js') }}"></script>
    <script src="{{ asset('js/profile-setting/gallery.js') }}"></script>
    <script src="{{ asset('js/profile-setting/brochure.js') }}"></script>
    <script src="{{ asset('js/profile-setting/certificate.js') }}"></script>
    <script src="{{ asset('js/profile-setting/experience.js') }}"></script>
    <script src="{{ asset('js/profile-setting/photo-profile.js') }}"></script>
    <script src="{{ asset('js/profile-setting/photo-banner.js') }}"></script>
    <script src="{{ asset('js/profile-setting/qualification.js') }}"></script>
    <script src="{{ asset('js/profile-setting/feature.js') }}"></script>
    <script>
        function updateProgressBarStatus(){
            $.ajax({
                type: 'GET',
                url: '/profile/percentage',

                success:function (data){
                    var $progressBar = $('.progress-bar');
                    $progressBar.css('width', data+'%');
                    $progressBar.text(data+'%');
                }
            });
        }
    </script>
    <script>
        $(function() {

            $('#select_department').on('change', onSelectDepartmentChange);
            $('#select_province').on('change', onSelectProvinceChange);
            // $('#select-category').on('change', onSelectCategoryChange);
        });

        function onSelectDepartmentChange() {
            var department_id = $(this).val();
            if (! department_id) {
                $('#select_province').html('<option value="">Seleccione provincia</option>');
                return;
            }

            // AJAX
            $.get('/api/department/'+department_id+'/provinces', function (data) {
                var html_select = '<option value="">Seleccione provincia</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_province').html(html_select);
            });
        }

        function onSelectProvinceChange() {
            var province_id = $(this).val();

            if (! province_id) {
                $('#select-district').html('<option value="">Seleccione distrito</option>');
                return;
            }

            // AJAX
            $.get('/api/province/'+province_id+'/districts', function (data) {
                var html_select = '<option value="">Seleccione distrito</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select_district').html(html_select);
            });
        }

        function onSelectCategoryChange() {
            var category_id = $(this).val();

            if (! category_id) {
                $('#select-subcategory').html('<option value="">Seleccione subcategoría</option>');
                return;
            }

            // AJAX
            $.get('/api/category/'+category_id+'/subcategories', function (data) {
                var html_select = '<option value="">Seleccione subcategoría</option>';
                for (var i=0; i<data.length; ++i)
                    html_select += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                $('#select-subcategory').html(html_select);
            });
        }
    </script>
@endsection
