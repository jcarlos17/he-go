@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Administrar mis servicios</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Administrar mis servicios</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardmanageservices">
                                <div class="tg-dashboardhead">
                                    <div class="tg-dashboardtitle">
                                        <h2>Administrar servicios</h2>
                                    </div>
                                    <button class="tg-btnaddservices" data-toggle="modal" data-target="#serviceModal">+ Agregar nuevo servicio</button>
                                </div>
                                <div id="uploadedService" class="tg-dashboardservices">
                                    @foreach ($user->services as $service)
                                    <div id="item-service" class="tg-dashboardservice">
                                        <div class="tg-servicetitle">
                                            <h2 id="titleText{{$service->id}}"><a href="javascript:void(0);">{{ $service->title }}</a> <span>{{ $service->by_appointment }}</span></h2>
                                        </div>
                                        <div class="tg-btntimeedit">
                                            <span id="priceText{{$service->id}}">{{ $service->price_value }}</span>
                                            <button class="tg-btnedite" data-showservice="{{ url('/service/'.$service->id.'/show') }}"><i class="lnr lnr-pencil"></i></button>
                                            <button class="tg-btndel" data-delete="{{ url('/services/'.$service->id.'/delete') }}"><i class="lnr lnr-trash"></i></button>
                                        </div>
                                    </div>
                                    @endforeach
                                    <template id="templateService">
                                        <div id="item-service" class="tg-dashboardservice">
                                            <div class="tg-servicetitle">
                                                <h2 id=""><a href="javascript:void(0);"></a> <span></span></h2>
                                            </div>
                                            <div class="tg-btntimeedit">
                                                <span id=""></span>
                                                <button class="tg-btnedite" data-showservice=""><i class="lnr lnr-pencil"></i></button>
                                                <button class="tg-btndel" data-delete=""><i class="lnr lnr-trash"></i></button>
                                            </div>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <div class="modal fade tg-socialModal" id="serviceModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Agregar servicio</h2>
                </div>
                <div id="alert-service"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddservices">
                        <fieldset>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="text" id="title" class="form-control" placeholder="Título">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
										<span class="tg-select">
											<select id="type_value">
												<option value="">Seleccione tipo valor</option>
												<option value="PH">Por hora</option>
												<option value="PV">Por visita</option>
                                                <option value="HFO">Hasta fin de obra</option>
                                                <option value="G">Gratuito</option>
											</select>
										</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="N">
                                    <div class="form-group">
                                        <input type="number" id="price" class="form-control" placeholder="Precio del servicio (S/)">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea id="description" placeholder="Descripción"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="tg-checkbox">
                                        <input type="checkbox" name="appointment" id="appointment" value="1">
                                        <label for="appointment">Este servicio requiere cita previa</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn" data-service="{{ url('/services/add') }}">Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="serviceEditModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Editar servicio</h2>
                </div>
                <div id="alert-service"></div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formaddservices">
                        <fieldset>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <input type="text" id="titleEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
										<span class="tg-select">
											<select id="typeValueEdit">
												<option value="">Seleccione tipo valor</option>
												<option value="PH">Por hora</option>
												<option value="PV">Por visita</option>
                                                <option value="HFO">Hasta fin de obra</option>
                                                <option value="G">Gratuito</option>
											</select>
										</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="NEdit">
                                    <div class="form-group">
                                        <input type="number" id="priceEdit" class="form-control">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea id="descriptionEdit"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="tg-checkbox">
                                        <input type="checkbox" id="appointmentEdit" value="1" checked>
                                        <label for="appointmentEdit">Este servicio requiere cita previa</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn" id="btnEdit" data-edit="">Agregar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/manage-service/service.js') }}"></script>
    <script>
        $(function() {
            $('#type_value').change(function(){
                if ($('#type_value').val() === "G") {
                    $("#N").hide();
                }else{
                    $("#N").show();
                }
            });
        });
        $(document).on('change', function () {
            if ($('#typeValueEdit').val() === "G") {
                $("#NEdit").hide();
            }else{
                $("#NEdit").show();
            }
        })
    </script>
@endsection
