@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Citas reservadas</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Citas reservadas</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Citas reservadas</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardappointmentsetting">
                                <form class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-dashboardappointments">
                                            <div class="tg-dashboardtitle">
                                                <h2>Mis citas reservadas</h2>
                                            </div>
                                            <div id="tg-datepicker" class="tg-datepicker"></div>
                                            <div class="tg-sortfilters">
                                                <div class="tg-sortfilter tg-sortby">
                                                    <span>Sort By:</span>
                                                    <div class="tg-select">
                                                        <select>
                                                            <option>days</option>
                                                            <option>Type</option>
                                                            <option>date</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="tg-sortfilter tg-arrange">
                                                    <span>Arrange:</span>
                                                    <div class="tg-select">
                                                        <select>
                                                            <option>des</option>
                                                            <option>Asc</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="tg-sortfilter tg-show">
                                                    <span>Show:</span>
                                                    <div class="tg-select">
                                                        <select>
                                                            <option>All</option>
                                                            <option>24</option>
                                                            <option>all</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardappointmentbox">
                                                @foreach($appointments as $appointment)
                                                    <div class="tg-dashboardappointment">
                                                        <div class="tg-servicetitle">
                                                            <figure>
                                                                <img src="{{ $appointment->provider->photo_selected }}" alt="image description">
                                                            </figure>
                                                            <div class="tg-clientcontent">
                                                                <h2><a href="javascript:void(0);">{{ $appointment->provider->name }}</a></h2>
                                                                <span>(En cita)</span>
                                                            </div>
                                                        </div>
                                                        <div class="tg-serviceandservicetype">
                                                            <h3>Servicio</h3>
                                                            <span style="width: 160px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;">{{ $appointment->service->title }}</span>
                                                        </div>
                                                        <div class="tg-btntimeedit">
                                                            <div class="tg-appointmenttype">
                                                                <h3>Tipo cita</h3>
                                                                <span>{{ $appointment->option }}</span>
                                                            </div>
                                                            <a href="javascript:void(0);" class="tg-btnedite" data-show="{{ url('/appointment/'.$appointment->id.'/show') }}"><i class="lnr lnr-checkmark-circle"></i></a>
                                                            <a href="javascript:void(0);" class="tg-btndel" data-toggle="modal" data-target=".tg-appointmentrejectmodal"><i class="lnr lnr-cross-circle"></i></a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->

    <!--Theme Modal Box Start*-->
    <div class="modal fade tg-appointmentapprovemodal" id="modalDetailAppointment" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Appointment Detail</h2>
                </div>
                <div class="tg-modalbody">
                    <ul class="tg-invoicedetail">
                        <li><span>Nombre del cliente:</span><span id="nameClient"></span></li>
                        <li><span>Fecha:</span><span id="dateAppointment"></span></li>
                        <li><span>Hora:</span><span id="timeAppointment"></span></li>
                        <li><span>Servicio:</span><span id="serviceClient"></span></li>
                        <li><span>Tipo cita:</span><span id="typeAppointment"></span></li>
                        <li><span>Información:</span><span id="informationClient"></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Theme Modal Box End-->
    {{--<!--Theme Modal Box Start-->--}}
    {{--<div class="modal fade tg-appointmentrejectmodal" tabindex="-1">--}}
        {{--<div class="modal-dialog tg-modaldialog" role="document">--}}
            {{--<div class="modal-content tg-modalcontent">--}}
                {{--<div class="tg-modalhead">--}}
                    {{--<h2>Rejection Reason</h2>--}}
                {{--</div>--}}
                {{--<div class="tg-modalbody">--}}
                    {{--<form class="tg-themeform tg-formreject">--}}
                        {{--<fieldset>--}}
                            {{--<div class="form-group">--}}
								{{--<span class="tg-select">--}}
									{{--<select>--}}
										{{--<option>Select Reason</option>--}}
										{{--<option>Select Reason</option>--}}
										{{--<option>Select Reason</option>--}}
										{{--<option>Select Reason</option>--}}
										{{--<option>Select Reason</option>--}}
									{{--</select>--}}
								{{--</span>--}}
                            {{--</div>--}}
                            {{--<textarea placeholder="">Description</textarea>--}}
                        {{--</fieldset>--}}
                    {{--</form>--}}
                {{--</div>--}}
                {{--<div class="tg-modalfoot">--}}
                    {{--<button class="tg-btn tg-btnreject" type="submit">Reject now</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!--Theme Modal Box End-->--}}
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-show]', function () {
            // request al servidor
            const urlShowAppointment = $(this).data('show');
            const nameClient = $('#nameClient');
            const dateAppointment = $('#dateAppointment');
            const timeAppointment = $('#timeAppointment');
            const serviceClient = $('#serviceClient');
            const typeAppointment = $('#typeAppointment');
            const informationClient = $('#informationClient');

            $.ajax({
                type: 'GET',
                url: urlShowAppointment,

                success:function (data){
                    nameClient.replaceWith('<span id="nameClient">'+data.interested_name+'</span>');
                    dateAppointment.replaceWith('<span id="dateAppointment">'+data.date+'</span>');
                    timeAppointment.replaceWith('<span id="timeAppointment">'+data.time+'</span>');
                    serviceClient.replaceWith('<span id="serviceClient">'+data.service_title+'</span>');
                    typeAppointment.replaceWith('<span id="typeAppointment">'+data.option+'</span>');
                    informationClient.replaceWith('<span id="informationClient">'+data.information+'</span>');

                    $('#modalDetailAppointment').modal('show');
                }
            });
        });
    </script>
    <script>
        $(document).on('click', '[data-delete]', function () {
            // request al servidor
            const urldeleteHour = $(this).data('delete');
            const $div = $(this).closest('.tg-startendtime');
            $.ajax({
                type: 'GET',
                url: urldeleteHour,

                success:function (){
                    $div.remove();
                }
            });
        });
    </script>
@endsection
