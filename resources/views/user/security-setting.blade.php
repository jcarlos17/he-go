@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Configuraciones de seguridad</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Configuraciones de seguridad</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboardsecuritysetting">
                                <div class="tg-themeform tg-formsecuritysetting">
                                    <div id="alert-update"></div>
                                    <fieldset>
                                        <h2>Cambiar contraseña</h2>
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="password" id="current_password" class="form-control" placeholder="Contraseña actual" required>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="password" id="password" class="form-control" placeholder="Nueva contraseña" required>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="password" id="password_confirmation" class="form-control" placeholder="Confirmar nueva contraseña" required>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <button data-update="{{ url('/password/change') }}" class="tg-btn">Cambiar contraseña</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <h2>Desactivar cuenta</h2>
                                        <div class="tg-description">
                                            <p></p>
                                        </div>
                                        <div id="alert-deactivated"></div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <span class="tg-select">
                                                        <select id="selectReason">
                                                            <option value="">¿Por qué te vas?</option>
                                                            <option value="Excesivos correos y mensajes">Excesivos correos y mensajes</option>
                                                            <option value="Estoy perdido, necesito ayuda">Estoy perdido, necesito ayuda</option>
                                                            <option value="Poca información">Poca información</option>
                                                            <option value="Motivos personales">Motivos personales</option>
                                                            <option value="Otros">Otros</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="password" id="psw_deactivated" class="form-control" placeholder="Contraseña actual">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input type="password" id="psw_confirmation_deactivated" class="form-control" placeholder="Confirmar contraseña">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <textarea id="justificationDeactivated" placeholder="Nos apena que hayas llegado hasta este punto, pero tranquilo, no todo son malas noticias. Explícanos un poco el motivo por el cual nos abandonas y cómo podríamos solucionarlo que tenemos grandes noticias para ti."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <button id="sendDeactivated" type="submit" style="display: none"></button>
                                                <a href="#" class="tg-btn tg-btndeactivate" data-toggle="modal" data-target="#deactivatedModal">Desactivar cuenta</a>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->

    <!--Theme Modal Box Start-->
    <div class="modal fade tg-deactivatemodal" id="deactivatedModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="lnr lnr-cross"></span></button>
                <div class="tg-modalbody">
                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/deactivate-icon.png" alt="image description">
                    <h3>¿Está seguro que quiere dar de baja su cuenta?</h3>
                    <div class="tg-description">
                        <p>Una vez que la cuenta sea desactivada, se cerrará sesión automáticamente.</p>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn tg-btndeactivate" data-deactivated="{{ url('/deactivated/'.auth()->id()) }}">Si! Dar de baja</button>
                </div>
            </div>
        </div>
    </div>
    <!--Theme Modal Box End-->
@endsection

@section('scripts')
    <script src="{{ asset('js/security-setting/change-password.js') }}"></script>
    <script src="{{ asset('js/security-setting/user-deactivated.js') }}"></script>
@endsection
