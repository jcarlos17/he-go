@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Login / Registro</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="#">Inicio</a></li>
                        <li class="tg-active">Login - Registro</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <div class="tg-main-section tg-haslayout">
            <div class="container">
                <div class="row">
                    <div id="tg-content" class="tg-content">
                        <div class="tg-themeform tg-formlogin-register">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <div class="tg-heading">
                                    <h2>Registrarse como</h2>
                                </div>
                                <fieldset>
                                    <ul class="tg-tabnav" role="tablist">
                                        <li role="presentation" id="regularUser" class="{{ old('role') == 2 ? 'active' : '' }}">
                                            <a href="#regularuser" data-toggle="tab">
                                                <span class="lnr lnr-user"></span>
                                                <div class="tg-navcontent">
                                                    <h3>Usuario Único Regular</h3>
                                                    <span>Registrarse como buscador de servicios</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li role="presentation" class="{{ old('role') == 1 ? 'active' : '' }}">
                                            <a href="#company" data-toggle="tab">
                                                <span class="lnr lnr-briefcase"></span>
                                                <div class="tg-navcontent">
                                                    <h3>Empresa / Profesional</h3>
                                                    <span>Registrarse como prestador de servicios</span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tg-themetabcontent tab-content">
                                        @if ($errors->any() && old('type_form') == 'register')
                                            @foreach ($errors->all() as $error)
                                                <div class="alert alert-warning tg-alertmessage fade in" style="margin:1px 0;">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>
                                                    <i class="lnr lnr-bug"></i>
                                                    <span><strong>{{ $error }}</strong></span>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div class="tab-pane {{ old('role') == 2 ? 'active' : '' }} fade in" id="regularuser" style="margin-top: 20px">
                                            <form method="POST" action="{{ route('register') }}" role="form" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="type_form" value="register">
                                                <input type="hidden" name="role" value="2">
                                                <div class="form-group">
                                                    <input type="text" name="name" class="form-control" placeholder="Nombres" value="{{ old('name') }}">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="last_name" class="form-control" placeholder="Apellidos" value="{{ old('last_name') }}">
                                                </div>
                                                {{--<div class="form-group">--}}
                                                    {{--<span class="tg-select">--}}
                                                        {{--<select>--}}
                                                            {{--<option>Género</option>--}}
                                                            {{--<option>Masculino</option>--}}
                                                            {{--<option>Femenino</option>--}}
                                                        {{--</select>--}}
                                                    {{--</span>--}}
                                                {{--</div>--}}
                                                <div class="form-group">
                                                    <input type="text" name="phone" class="form-control" placeholder="Teléfono Móvil" value="{{ old('phone') }}">
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" name="email" class="form-control" placeholder="Correo" value="{{ old('email') }}">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="password" class="form-control" placeholder="Contraseña">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar contraseña">
                                                </div>
                                                <div class="form-group">
                                                    <img src="http://mydesignhoard.com/HTML/html/service_provider/images/placeholder/img-02.png" alt="image description">
                                                </div>
                                                <div class="form-group">
                                                    <div class="tg-checkbox">
                                                        <input type="checkbox" id="terms" name="condition" value="ok_seeker" {{old('condition') == 'ok_seeker' ? 'checked' : '' }}>
                                                        <label for="terms">He leído los <a target="_blank" href="{{ url('terms-conditions') }}">Términos &amp; Condiciones</a> y los Acepto</label>
                                                    </div>
                                                </div>
                                                <button class="tg-btn" type="submit">Registrar</button>
                                            </form>
                                            <ul class="tg-socialicons tg-socialsharewithtext">
                                                <li class="tg-facebook">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-facebook-f"></i>
                                                            <span>facebook</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-twitter">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-twitter"></i>
                                                            <span>twitter</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-linkedin">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-linkedin"></i>
                                                            <span>linkdin</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-googleplus">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-google-plus"></i>
                                                            <span>googl+</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-tumblr">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-tumblr"></i>
                                                            <span>tumblr</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-vimeo">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-vimeo"></i>
                                                            <span>vimeo</span>
                                                        </em>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane {{ old('role') == 1 ? 'active' : '' }} fade in" id="company">
                                            <form method="POST" action="{{ route('register') }}" role="form" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="type_form" value="register">
                                                <input type="hidden" name="role" value="1">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="tg-registeras">
                                                                <span>Registrarse como:</span>
                                                                <div class="tg-radio">
                                                                    <input type="radio" id="business" name="type_provider" value="Empresa" {{ old('type') == 'Empresa' ? 'checked' : '' }}>
                                                                    <label for="business">Empresa</label>
                                                                </div>
                                                                <div class="tg-radio">
                                                                    <input type="radio" id="professional" name="type_provider" value="Profesional" {{ old('type') == 'Profesional' ? 'checked' : '' }}>
                                                                    <label for="professional">Profesional</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="name" class="form-control" placeholder="Nombres" value="{{ old('name') }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="last_name" class="form-control" placeholder="Apellidos" value="{{ old('last_name') }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="company_name" class="form-control" placeholder="Nombre de la empresa" value="{{ old('company_name') }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="email" name="email" class="form-control" placeholder="Correo" value="{{ old('email') }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" name="password" class="form-control" placeholder="Contraseña">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar contraseña">
                                                        </div>
                                                        <div class="form-group">
                                                            <img src="http://mydesignhoard.com/HTML/html/service_provider/images/placeholder/img-02.png" alt="image description">
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="tg-checkbox">
                                                                <input type="checkbox" id="conditions" name="condition" value="ok" {{old('condition') == 'ok' ? 'checked' : '' }}>
                                                                <label for="conditions">He leído los <a href="#">Términos &amp; Condiciones</a> y los Acepto</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button class="tg-btn" type="submit">Registrar</button>
                                            </form>
                                            <ul class="tg-socialicons tg-socialsharewithtext">
                                                <li class="tg-facebook">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-facebook-f"></i>
                                                            <span>facebook</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-twitter">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-twitter"></i>
                                                            <span>twitter</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-linkedin">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-linkedin"></i>
                                                            <span>linkdin</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-googleplus">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-google-plus"></i>
                                                            <span>googl+</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-tumblr">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-tumblr"></i>
                                                            <span>tumblr</span>
                                                        </em>
                                                    </a>
                                                </li>
                                                <li class="tg-vimeo">
                                                    <a class="tg-roundicontext" href="javascript:void(0);">
                                                        <em class="tg-usericonholder">
                                                            <i class="fa fa-vimeo"></i>
                                                            <span>vimeo</span>
                                                        </em>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="tg-loginarea">
                                    <div class="tg-bordertitle">
                                        <h3>Inicia sesion</h3>
                                    </div>
                                    <fieldset>
                                        @if ($errors->any() && old('type_form') == 'login')
                                            @foreach ($errors->all() as $error)
                                                <div class="alert alert-warning tg-alertmessage fade in" style="margin:1px 0;">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>
                                                    <i class="lnr lnr-bug"></i>
                                                    <span><strong>{{ $error }}</strong></span>
                                                </div>
                                            @endforeach
                                        @endif
                                        <form class="form-group" method="POST" action="{{ route('login') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type_form" value="login">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" placeholder="Correo">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control" placeholder="Contraseña">
                                            </div>
                                            <div class="form-group">
                                                <div class="tg-checkbox">
                                                    <input type="checkbox" id="checkbox-signup"  name="remember" {{ old('remember') ? 'checked' : ''}}>
                                                    <label for="checkbox-signup">Recordar sesión</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="tg-btn tg-btn-lg" type="submit">Iniciar sesión</button>
                                            </div>
                                            {{--<a class="tg-btnforgotpass" href="#">Forgot your password?</a>--}}
                                        </form>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Login Register End
        *************************************-->
    </main>
    <!--Main End-->
@endsection
@section('scripts')
    <script>
        @if (! old('role'))
            $('#regularUser').addClass("active");
            $('#regularuser').addClass("active");
        @endif
    </script>
@endsection
