@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Términos y condiciones</h1>
                    </div>
                    {{--<ol class="tg-breadcrumb">--}}
                        {{--<li><a href="/">Inicio</a></li>--}}
                        {{--<li class="tg-active">Soy un buscador de servicios</li>--}}
                    {{--</ol>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode" style="padding: 0;">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">Términos y Condiciones de uso del sitio web
                                        redhego.com</h3>
                                </div>
                                <div class="tg-description">
                                    <h4>1. TITULARIDAD DEL SITIO WEB Y ACEPTACIÓN DE LOS TÉRMINOS DE USO</h4>
                                    <p class="text-justify">
                                        La lectura y aceptación de los presentes TÉRMINOS Y CONDICIONES DE USO
                                        (en adelante, los “Términos y Condiciones”) es condición necesaria para la
                                        utilización de los servicios de la web (en lo sucesivo, el “Sitio Web”).
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Los Términos de Uso constituyen Condiciones Generales de Contratación, tienen
                                        naturaleza contractual y obligacional y rigen la relación entre todo usuario
                                        del Sitio Web (en lo sucesivo, el “Usuario”) con Red He Go (en adelante, “He Go”),
                                        una empresa de responsabilidad individual de nacionalidad española propiedad de
                                        Melania Troya Lobo con CIF: 48821312V, con domicilio en calle virgen de los reyes 10,
                                        albaida del aljarafe, 41809, Sevilla. España y con e-mail: contacto@redhego.com.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                    La utilización de este Sitio Web y sus servicios implica la plena aceptación de las
                                    disposiciones incluidas en estos Términos de Uso conforme a la versión publicada por
                                    redhego.com en el momento en que el Usuario acceda al Sitio Web.La utilización de ciertos
                                    servicios ofrecidos a los Usuarios a través del Sitio Web puede encontrarse sometida a
                                    condiciones particulares propias que, según los casos, sustituyen, completan y/o modifican
                                    los presentes Términos de Uso. Por lo tanto, con anterioridad a la utilización de dichos servicios,
                                    el Usuario también ha de leer atentamente y aceptar asimismo las correspondientes
                                    condiciones particulares, en su caso.
                                    </p>
                                    <br>
                                    <h4>2. DESCRIPCIÓN DEL SERVICIO</h4>
                                    <p class="text-justify">
                                        La finalidad del Sitio Web es ofrecer un servicio de publicidad que permite a profesionales,
                                        empresas, tiendas, marcas relativos al sector de las obras y reformas en general y servicios
                                        para el hogar (en adelante, “los Prestadores de servicios” o individualmente
                                        considerados, “el Prestador”) anunciar y publicitar sus servicios con clientes que
                                        precisen dichos servicios (en adelante, “los Buscadores de Servicios”, o individualmente
                                        considerados, “buscador de servicio”). Asimismo, los Buscadores de servicios pueden anunciar
                                        sus requerimientos de servicios, para que los profesionales interesados o viceversa puedan
                                        ponerse en contacto con ellos para facilitarles información sobre esos servicios.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El sitio web redhego.com ofrece a sus clientes información sobre profesionales,
                                        empresas, tiendas y marcas relativos al sector de las obras y reformas en general
                                        y servicios para el hogar, así como la posibilidad de solicitar presupuestos a
                                        profesionales y ponerse en contacto con los mismos. Además, el servicio incluye
                                        información sobre los Profesionales, ya que los clientes pueden opinar públicamente
                                        en el Sitio Web sobre el servicio ofrecido por los referidos Profesionales.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El sitio web redhego.com no interviene en ningún caso en las negociaciones llevadas
                                        a cabo entre Buscador de servicios y Prestador de servicios, permaneciendo al
                                        margen de las mismas en todo momento, ni en la ejecución de los trabajos acordados
                                        entre Cliente y Proveedor, por lo que no es responsable, en ningún caso, del trabajo
                                        realizado por el Prestadores de servicios al buscador de servicios, ni del cumplimiento
                                        de las obligaciones entre sí, eximiendo expresamente el Buscador de servicios y el
                                        Prestador de servicios a redhego.com de cualquier desavenencia que pudiera surgir entre ambos.
                                    </p>
                                    <br>
                                    <h4>3. EDAD Y CAPACIDAD LEGAL</h4>
                                    <p class="text-justify">
                                        El Sitio Web se dirige a personas mayores de edad que cuenten con plena capacidad
                                        jurídica y de obrar necesaria para su utilización. Los menores de edad no están
                                        autorizados a utilizar el Sitio Web y no deberán, por tanto, utilizar los servicios
                                        del mismo, por lo que redhego.com se reserva el derecho a dar de baja y cancelar
                                        los datos de aquellos Usuarios que, habiendo sido requeridos para ello, no acrediten
                                        a satisfacción de redhego.com con las condiciones de contratación y ser mayores de edad.
                                    </p>
                                    <br>
                                    <h4>4. REGISTRO DE USUARIOS</h4>
                                    <p class="text-justify">
                                        El sitio web redhego.com condiciona la utilización de algunos de los servicios a
                                        la previa cumplimentación del correspondiente registro de Usuario, bien como Prestador
                                        de servicios, bien como buscador de servicios, debiendo indicar como mínimo la
                                        información marcada con un asterisco y elegir un nombre de usuario y una contraseña
                                        identificativa. El Usuario puede darse de baja del registro de He Go en cualquier
                                        momento siguiendo las pautas indicadas en el mismo sitio web o escribiéndonos a soporte@redhego.com.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Usuario se compromete a conservar su nombre de usuario y contraseña y a
                                        mantenerlos confidenciales, así como a usarlos con la diligencia debida.
                                        El Usuario se compromete a notificar a redhego.com a la mayor brevedad el uso no
                                        autorizado de su nombre de usuario y contraseña o cualquier otro fallo en la seguridad.
                                        El sitio web redhego.com no será responsable por los daños o pérdidas que se pudieran
                                        originar debido al no cumplimiento de esta obligación por su parte.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Usuario es el único responsable de la información suministrada en su registro,
                                        garantiza que los datos facilitados a redhego.com son veraces y actualizados y
                                        se hace responsable de comunicar a redhego.com cualquier modificación de los mismos
                                        y de mantener toda la información que haya facilitado actualizada de forma que
                                        responda en cada momento a su situación real.
                                    </p>
                                    <br>
                                    <h4>5. SOLICITUD DE PRESTACION DE SERVICIOS</h4>
                                    <p class="text-justify">
                                        El sitio web redhego.com ofrece a los clientes la posibilidad de solicitar un estudio,
                                        valoración y presupuesto para una oferta que requiera los servicios de los prestadores
                                        de servicios que se anuncien. Para ello, el Buscador de servicios registrado proporciona
                                        la información y características del trabajo sobre el que desea solicitar una valoración
                                        y se compromete a que sea veraz y ajustada a la realidad. En todo caso, el Buscador de
                                        servicios o el Prestador de servicios serán los únicos responsables de las informaciones
                                        falsas o inexactas que faciliten y de los perjuicios que pudiera causar a redhego.com o
                                        a terceros por dicha información.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Al solicitar una valoración, el Buscador de servicios consiente que sus datos de contacto
                                        y el contenido de su solicitud sean cedidos a terceros. Así, redhego.com podrá hacer público
                                        esos datos, por lo menos, para los prestadores de servicios, con la finalidad de que se
                                        pongan en contacto con el buscador de servicios para proporcionarle el presupuesto solicitado
                                        o información sobre el servicio solicitado. Tal comunicación de datos personales es
                                        necesaria para llevar a cabo el servicio solicitado por el Buscador de servicios y
                                        se encuentra regulada en la Política de Privacidad, que se puede consultar pinchando aquí.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Es posible que en alguna zona geográfica determinada redhego.com no disponga de contactos
                                        de Prestadores de servicios que puedan prestarle el servicio solicitado. En tales casos,
                                        redhego.com podrá disponer de acuerdos con otras empresas similares a nosotros que
                                        tal vez pudieran prestar este servicio. En caso de que el Buscador de servicios
                                        desee que sus datos NO sean proporcionados a alguna de estas empresas con la finalidad
                                        señalada, deberá indicarlo usando cualquiera de los medios de contacto.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Sobre la base de la información facilitada por el Buscador de servicios y, en
                                        su caso, de la visita realizada por el Prestador de servicios, éste realiza un
                                        presupuesto que será vinculante para el Prestador en caso de ser aceptado, salvo
                                        que la descripción del trabajo no se corresponda con la realidad o no haya facilitado
                                        la información completa. En este caso, el Prestador podrá modificar el presupuesto
                                        atendiendo a la realidad del trabajo a realizar.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Buscador de servicios no se encuentra obligado a aceptar ningún servicio presupuesto
                                        que le haya sido proporcionado por Prestadores contactados a través del sitio web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Buscador de servicios podrá informar a redhego.com de cualquier irregularidad
                                        detectada respecto de los Prestadores de servicios que se anuncian a través del
                                        Sitio Web. redhego.com no se responsabiliza de los trabajos ni de la profesionalidad
                                        de los Profesionales anunciados en el Sitio Web basándose en el principio de buena
                                        fe y que deben cumplir y aceptar con las condiciones de uso y políticas de privacidad.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Si redhego.com tuviera conocimiento de que algún Usuario está infringiendo la
                                        legalidad vigente, procederá de inmediato a cancelar su cuenta.
                                    </p>
                                    <br>
                                    <h4>6. MODIFICACIÓN O ELIMINACIÓN DE LA INFORMACIÓN PROFESIONAL</h4>
                                    <p class="text-justify">
                                        El Usuario podrá ejercitar sus derechos de acceso, rectificación, cancelación y
                                        oposición mediante un correo electrónico a soporte@redhego.com.com. Para más
                                        información sobre el uso de redhego.com sobre los datos que recibe, consulta
                                        nuestra Política de Privacidad.
                                    </p>
                                    <br>
                                    <h4>7. USO DE LOS SERVICIOS Y DEL SITIO WEB</h4>
                                    <p class="text-justify">
                                        Los Usuarios se comprometen a utilizar los servicios ofrecidos a través del Sitio
                                        Web de conformidad con la legislación vigente, los presentes Términos de Uso, las
                                        condiciones particulares que se concreten para ciertos servicios y demás avisos e
                                        instrucciones puestos en su conocimiento, así como con la moral y las buenas
                                        costumbres generalmente aceptadas y el orden público.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Los Usuarios renunciarán a utilizar cualquiera de los materiales e informaciones
                                        contenidos en este Sitio Web con fines ilícitos o expresamente prohibidos en los
                                        presentes Términos de Uso así como a las condiciones particulares que, en su caso,
                                        se habiliten, o en contra de los derechos e intereses de Red He Go, sus miembros
                                        o terceros y deberán responder frente a éstos en caso de contravenir o incumplir
                                        dichas obligaciones o, de cualquier modo (incluida la introducción o difusión de
                                        “virus informáticos”) dañar, inutilizar, sobrecargar, deteriorar o impedir la
                                        normal utilización de los materiales e información contenidos en el Sitio Web,
                                        los sistemas de información o los documentos, archivos y toda clase de contenidos
                                        almacenados en cualquier equipo informático (hacking) de redhego.com, de sus
                                        miembros o de cualquier Usuario del Sitio Web.
                                    </p>
                                    <br>
                                    <p><b>Política anti-spamming del Sitio Web</b></p>
                                    <br>
                                    <p>Los Usuarios se obligan a abstenerse de:</p>
                                    <br>
                                    <ul>
                                        <li class="text-justify">Recabar datos con finalidad publicitaria y de remitir publicidad de cualquier
                                            clase y comunicaciones con fines de venta u otras de naturaleza comercial.</li>
                                        <li class="text-justify">Poner a disposición de terceros, con cualquier finalidad, datos recabados en el Sitio Web.</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        Los Usuarios o terceros perjudicados por la recepción de mensajes no solicitados
                                        podrán informar de tal circunstancia remitiendo un mensaje a la siguiente dirección
                                        de correo electrónico a soporte@redhego.com.
                                    </p>
                                    <br>
                                    <p><b>Suspensión o revocación del Usuario</b></p>
                                    <br>
                                    <p class="text-justify">
                                        Si redhego.com considera que el Usuario incumple con las presentes Condiciones,
                                        se reserva el derecho a suspender o revocar su registro y derecho a acceder o
                                        usar el Sitio Web en cualquier momento y sin responsabilidad o necesidad de
                                        informar al Usuario.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El sitio web redhego.com podrá utilizar para ello cualquier método operativo,
                                        tecnológico, legal u otro disponible para que se respeten estas Condiciones
                                        (incluyendo, sin limitación, el bloqueo de direcciones IP).
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El sitio web redhego.com podrá notificar al Usuario acerca del bloqueo o
                                        cancelación de su acceso al Sitio Web, si bien no está obligada a ello.
                                    </p>
                                    <br>
                                    <h4>8. OPINIONES</h4>
                                    <p class="text-justify">
                                        Los usuarios podrán insertar opiniones y comentarios relativos a los Prestadores
                                        dados de alta en el Directorio de Profesionales. Dichas opiniones podrán estar
                                        basadas en aquellos servicios prestados como consecuencia del uso del Sitio Web
                                        o en aquellos otros realizados al margen del Sitio Web. Redhego.com se reserva
                                        el derecho de publicar, extractar, resumir o abreviar el contenido de las
                                        opiniones realizadas por los usuarios.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Los usuarios se comprometen a no transmitir, difundir o poner a disposición de
                                        terceros contenidos o comentarios u opiniones que, a título meramente indicativo y no exhaustivo:
                                    </p>
                                    <br>
                                    <ul>
                                        <li class="text-justify">De cualquier forma sean contrarios, menosprecien o atenten contra los derechos fundamentales y las libertades públicas reconocidas constitucionalmente, en los tratados internacionales y en el resto de la legislación vigente.</li>
                                        <li class="text-justify">Induzcan, inciten o promuevan actuaciones delictivas, denigratorias, difamatorias, infamantes, violentas o, en general, contrarias a la ley, a la moral y las buenas costumbres generalmente aceptadas o al orden público.</li>
                                        <li class="text-justify">Induzcan, inciten o promuevan actuaciones, actitudes o pensamientos discriminatorios por razón de sexo, raza, religión, creencias, edad o condición.</li>
                                        <li class="text-justify">No tengan en cuenta los bienes o servicios desarrollados por el Prestador o comentarios que no tengan ningún valor cualitativo (por ejemplo, “el trabajo no se ha terminado todavía”).</li>
                                        <li class="text-justify">Incorporen, pongan a disposición o permitan acceder a productos, elementos, mensajes o servicios delictivos, violentos, ofensivo, nocivos, degradantes o, en general, contrarios a la ley, a la moral y a las buenas costumbres generalmente aceptadas o al orden público.</li>
                                        <li class="text-justify">Induzcan o puedan inducir a terceros a un estado inaceptable de ansiedad o temor.</li>
                                        <li class="text-justify">Sean injuriosos, ofensivos o degradantes.</li>
                                        <li class="text-justify">No se correspondan con el trabajo desarrollado por el Profesional.</li>
                                        <li class="text-justify">Sean contrarios al derecho al honor, a la intimidad personal y familiar o a la propia imagen de las personas.</li>
                                        <li class="text-justify">Infrinjan la normativa dispuesta relativa al secreto de las comunicaciones.</li>
                                        <li class="text-justify">Sean relativos a la publicidad de sitios que sean competencia del Sitio Web o sus anunciantes.</li>
                                        <li class="text-justify">Sean falsos o no se correspondan a la realidad.</li>
                                        <li class="text-justify">Se trate de publicidad ilícita, engañosa o desleal.</li>
                                        <li class="text-justify">Provoquen por sus características (tales como formato, extensión, etc.) dificultades en el normal funcionamiento de los servicios.</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        Redhego.com se reserva el derecho de retirar del Sitio Web todos aquellos comentarios
                                        y opiniones que no sean acordes con los presentes Términos de Uso. Redhego.com en
                                        ningún caso se hace responsable de los contenidos u opiniones manifestadas por un Usuario en el Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Asimismo, redhego.com se reserva el derecho a modificar el contenido que publiquen
                                        los usuarios en el Sitio Web, a los únicos efectos publicitarios de atribuirle
                                        una mejor presentación y distribución en la Web, y de resaltar las características
                                        de los servicios ofrecidos, así como para adecuar el contenido al estilo y a
                                        los mínimos de calidad de redhego.com. A tal efecto, los usuarios autorizan a
                                        redhego.com a, de forma gratuita, realizar dichas modificaciones o adaptaciones,
                                        a libremente reproducir, distribuir, publicitar o explotar por cualquier forma o
                                        medio dicho contenido publicado por el Prestador, renunciando a cualesquiera derechos
                                        de contenido económico que le pudieran corresponder, en concepto de compensación o cualquier otro título.
                                    </p>
                                    <br>
                                    <h4>9. MENSAJERÍA PRIVADA</h4>
                                    <p class="text-justify">
                                        HE GO pone a disposición de los Usuarios un servicio de mensajería privada que les
                                        permite comunicarse entre sí. Los Usuarios se comprometen a no incluir en estos
                                        mensajes contenidos contrarios a la ley y a las presentes Condiciones.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO puede no controlar ni revisar los contenidos que se publiquen en dichos
                                        mensajes privados por parte de los Usuarios, por lo que serán los Usuario los
                                        únicos responsables de los textos, archivos y documentos que envíen por este medio,
                                        frente a redhego.com y frente a terceros.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        No obstante, RED HE GO como titular y dueño del Sitio Web, sí puede, a su exclusivo
                                        criterio y necesidad, tener acceso a dichos mensajes privados, incluyendo la lectura,
                                        control y registro, de la actividad de los Usuarios, extremo que los Usuarios conocen
                                        y aceptan. En especial, y al utilizar el Sitio Web y la mensajería privada, los Usuarios
                                        prestan su expreso consentimiento al monitoreo o grabación de su actividad, y reconocen
                                        que no tienen ninguna expectativa de privacidad respecto de la transmisión de
                                        comunicaciones a través de los distintos medios facilitados por HE GO a través del Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO no se responsabiliza en ningún caso de la información proporcionada por los
                                        Usuarios ni de su veracidad o pertinencia, siendo los contenidos e información
                                        proporcionada de exclusiva responsabilidad de quien la emite.
                                    </p>
                                    <br>
                                    <h4>12. PROPIEDAD INTELECTUAL E INDUSTRIAL</h4>
                                    <p class="text-justify">
                                        RED HE GO E.I.R.L es titular de los nombres de dominio:
                                    </p>
                                    <br>
                                    <ul>
                                        <li style="list-style: none">www.redhego.com</li>
                                        <li style="list-style: none">www.redhego.es</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        El Sitio Web en su totalidad, incluyendo sin exclusión alguna su diseño,
                                        estructura y distribución, textos y contenidos, logotipos, botones, imágenes,
                                        dibujos, marcas, nombres comerciales, código fuente, creatividades, así como
                                        todos los derechos de propiedad intelectual e industrial y cualquier otro signo
                                        distintivo, pertenecen o tienen como cesionaria a RED HE GO o, en su caso, a las
                                        personas físicas o jurídicas que figuran como autores o titulares de los derechos
                                        o de sus licenciantes, sin que pueda entenderse que el uso y acceso al Sitio Web
                                        y la utilización de sus servicios atribuya a los Usuarios derecho alguno sobre
                                        los citados elementos. Los Usuarios se comprometen a respetar estos derechos.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Material publicado por los Usuarios
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Usuario es el titular de todo el material que publique en el Sitio Web, siempre
                                        y cuando sea realmente el legítimo dueño de lo que publica. Al publicarlo, el
                                        Usuario garantiza que tiene el permiso para hacerlo y concede automáticamente
                                        una licencia no exclusiva, continua y universal a RED HE GO para que pueda
                                        utilizarlo. HE GO puede ceder o traspasar la licencia arriba mencionada a sus
                                        afiliados y sucesores sin que sea necesario el consentimiento del Usuario. HE GO
                                        se reserva el derecho a revelar la identidad del Usuario a terceros que afirmen
                                        que el material publicado en el Sitio Web viola sus derechos de propiedad
                                        intelectual, privacidad u otros.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO no asumirá responsabilidad alguna respecto de los derechos de propiedad
                                        intelectual o industrial titularidad de terceros que se vean infringidos por un Usuario.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Está prohibida la reproducción, explotación, alteración, distribución o comunicación
                                        pública del Sitio Web para usos diferentes de la legítima información o contratación
                                        por los Usuarios de los servicios ofertados.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Utilizando el Sitio Web, el Usuario reconoce que no dispone de ningún derecho de
                                        propiedad o licencia sobre los mismos.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO informa de la existencia de un sistema de reclamación de derechos sobre el
                                        contenido publicado en el Sitio Web, mediante la remisión de un correo electrónico
                                        a la siguiente dirección:
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        soporte@redhego.com.
                                    </p>
                                    <br>
                                    <h4>13. EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD</h4>
                                    <p class="text-justify">
                                        Salvo en aquellos casos expresamente descritos en los presentes Términos de Uso
                                        y el resto del marco normativo de este Sitio Web, HE GO no se responsabiliza por
                                        los daños y perjuicios de cualquier naturaleza que puedan deberse a la falta de
                                        exactitud, exhaustividad, actualidad, así como a errores u omisiones de los que
                                        pudieran adolecer las informaciones y servicios contenidos en este Sitio Web u
                                        otros contenidos a los que se pueda acceder a través del mismo ni asume ningún
                                        deber o compromiso de verificar ni de vigilar sus contenido e informaciones.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Exclusión de garantías y de responsabilidad por el funcionamiento del Sitio Web y sus servicios.</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        RED HE GO no garantiza la disponibilidad y continuidad del funcionamiento del
                                        Sitio Web y de sus servicios. HE GO procurará advertir con suficiente antelación
                                        de las interrupciones que pudieran suceder en el funcionamiento del Sitio Web y
                                        de sus servicios siempre que ello sea posible. HE GO, o su servidor, pueden no
                                        estar libres de virus, errores, programas espía (spyware), Troyanos o similar
                                        software malintencionado. HE GO no se hace responsable de cualquier daño al
                                        hardware o software de su ordenador u otro tipo de tecnología. Tampoco se hace
                                        responsable de ninguna pérdida de información a raíz de la transmisión, uso de
                                        datos o contenido erróneo publicado por los Usuarios.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO excluye, con toda la extensión permitida por el ordenamiento jurídico,
                                        cualquier responsabilidad por los daños y perjuicios de toda naturaleza que puedan
                                        deberse a la falta de disponibilidad o de continuidad del funcionamiento del Sitio
                                        Web y de sus servicios o la defraudación de la utilidad que los Usuarios hubiesen
                                        podido atribuir al Sitio Web y sus servicios y, en los usuarios, aunque no de modo
                                        exclusivo, a los fallos en el acceso a las distintas páginas web que configuren el Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Exclusión de garantías y de responsabilidad por la utilización del Sitio Web,
                                            de los servicios y de los contenidos por los Usuarios.</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO no garantiza que los Usuarios utilicen los servicios y contenidos del Sitio
                                        Web de conformidad con los presentes Términos de Uso y, en su caso, con las
                                        condiciones particulares que resulten de aplicación, ni que lo hagan de forma
                                        diligente y prudente. HE GO tampoco garantiza la veracidad de los datos que
                                        los Usuarios proporcionan sobre sí mismos bien en su área de registro, bien
                                        en sus participaciones en el Sitio Web. Asimismo, HE GO no garantiza que los
                                        contenidos colgados en el Sitio Web por los Usuarios cumplan con lo dispuesto
                                        en los presentes Términos de Uso, con las condiciones particulares y con la
                                        legalidad vigente. HE GO excluye cualquier responsabilidad por los daños y perjuicios
                                        de toda naturaleza que pudieran deberse a la utilización de los servicios y de
                                        los contenidos por parte de los Usuarios, o que puedan deberse a la falta de veracidad,
                                        vigencia o autenticidad de la información que los Usuarios proporcionan a otros
                                        Usuarios acerca de sí mismos y, en particular, aunque no de forma exclusiva, por
                                        los daños y perjuicios de toda naturaleza que puedan deberse a la suplantación
                                        de la personalidad de un tercero efectuada por un Usuario en cualquier clase de
                                        comunicación realizada a través del Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Exclusión de garantías sobre enlaces a otros sitios web</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Este Sitio Web contiene enlaces o hipervínculos a otras páginas web. El Usuario
                                        debe ser consciente de que HE GO no es responsable de las prácticas de privacidad
                                        ni de los contenidos de esas otras webs. Recomendamos a los Usuarios que sean
                                        conscientes de que al utilizar uno de estos hipervínculos están abandonando
                                        nuestro Sitio Web y deben leer y revisar las condiciones generales y las políticas
                                        de privacidad de esas otras webs.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Exclusión de garantías por la cualificación de los Prestadores y la ejecución
                                            de los trabajos acordados.</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO no intervendrá en ningún caso en las negociaciones llevadas a cabo entre
                                        Cliente y Proveedor, permaneciendo al margen de las mismas en todo momento, por
                                        lo que no será responsable, en ningún caso, del cumplimiento de las obligaciones
                                        del buscador de servicios con el Prestador de servicios y viceversa, eximiendo
                                        expresamente ambos a HE GO de cualquier desavenencia que pudiera surgir entre ambos.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Es responsabilidad exclusiva del buscador de servicios la comprobación de la
                                        idoneidad de los servicios ofrecidos por el Prestador de servicios, y que los
                                        mismos se adaptan a las necesidades del buscador de servicios. El buscador de
                                        servicios asume asimismo la responsabilidad de examinar la identidad, titulación,
                                        capacitación y competencia profesional del Prestador, así como de realizar el
                                        oportuno seguimiento sobre el trabajo encomendado, los materiales y la adecuación,
                                        estética o no, a lo requerido por el mismo y acordado en el oportuno presupuesto.
                                        En ningún caso HE GO controlará ni intervendrá en la ejecución de los trabajos
                                        realizados por el Prestador, ni en la adecuación, calidad o resultado de los mismos,
                                        por lo que HE GO se excluye, con toda la extensión permitida por el ordenamiento
                                        jurídico, cualquier responsabilidad por los daños y perjuicios de toda naturaleza
                                        que puedan deberse a la ejecución o resultado del trabajo encomendado al Prestador
                                        de servicios, y el buscador de servicios expresamente exime a HE GO de cualquier
                                        discrepancia, indemnización, contraprestación, responsabilidad, daño, perjuicio
                                        o menoscabo como resultado de la contratación o del trabajo realizado por el
                                        Prestador de servicios.
                                    </p>
                                    <br>
                                    <h4>15. DENUNCIA</h4>
                                    <p class="text-justify">
                                        En el caso de que cualquier Usuario o tercero considere que existen hechos o
                                        circunstancias que revelen el carácter ilícito de cualquier contenido en el
                                        Sitio Web o de su utilización, deberá ponerse en contacto con HE GO a través de
                                        soporte@redhego.com o bien a través de los datos de contacto indicados en el
                                        Sitio Web y en los presentes Términos de Uso, indicando siempre (i) los datos
                                        personales del reclamante (nombre, dirección, número de DNI o pasaporte, número
                                        de teléfono y dirección de correo electrónico) y (ii) la supuesta actividad
                                        ilícita llevada a cabo en el Sitio Web y su motivación que revelen dicho carácter
                                        ilícito.
                                    </p>
                                    <br>
                                    <h4>17. DURACIÓN Y TERMINACIÓN</h4>
                                    <p class="text-justify">
                                        La presentación de los servicios que HE GO proporciona a través del Sitio Web
                                        tiene en principio, una duración indefinida. No obstante, HE GO está autorizada
                                        para dar por terminada o suspender la prestación de sus servicios en cualquier
                                        momento, sin perjuicio de lo que se hubiere dispuesto al respecto en las
                                        correspondientes condiciones particulares. En cualquier caso, si fuera posible, HE GO procurará advertir previamente la terminación o la suspensión de cualquier servicio o de la totalidad de los mismos.
                                    </p>
                                    <br>
                                    <h4>18. LEGISLACIÓN APLICABLE Y JURISDICCIÓN COMPETENTE</h4>
                                    <p class="text-justify">
                                        Los presentes Términos de Uso se regirán por la legislación de cada usuario.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Las partes se someten, a su elección, para la resolución de los conflictos y
                                        con renuncia a cualquier otro fuero, a los juzgados y tribunales del domicilio del usuario.
                                    </p>
                                    <br>
                                    <h4>Y MODIFICACIONES DE LOS TÉRMINOS DE USO</h4>
                                    <p class="text-justify">
                                        Estos Términos de uso fueron actualizados por última vez el 02 de julio del 2018.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO se reserva el derecho a modificar, corregir o cambiar de forma unilateral
                                        estos Términos de Uso en cualquier momento. Cuando esto ocurra, se publicarán
                                        los nuevos Términos de Uso en el Sitio Web así como la fecha en la que entrarán
                                        en vigor en la parte final de los Términos de Uso. Recomendamos que se visite
                                        esta página con regularidad para estar al día de las modificaciones que se puedan
                                        producir. Si no acepta las modificaciones de estos Términos de uso, no utilice
                                        el Sitio Web. El uso del Sitio Web continuadamente, implica la aceptación de estos
                                        Términos de Uso y sus posibles modificaciones.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
