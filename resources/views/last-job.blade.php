@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Últimos trabajos</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Últimos trabajos</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Últimos trabajos</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout" style="padding: 100px 0">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 pull-right">
                    <div class="tg-sectionhead">
                        <div class="tg-sectiontitle">
                            <h2>Estrenamos futuro, seguimos en casa</h2>
                        </div>
                        <div class="tg-description">
                            <p>Ponemos a tu disposición la herramienta más completa para encontrar las ofertas de trabajos, servicios más demandados, prestadores de servicios y comercios locales de tu zona</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 pull-left text-center">
                    <img src="{{ asset('/images/paper_hand.svg') }}" alt="" style="width: 50%; margin-bottom: 20px">
                </div>
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 pull-right">
                        <div class="row">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-joblisting">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="tg-sortfilters">
                                            <div class="tg-sortfilter tg-sortby">
                                                <span>Ordenar por:</span>
                                                <div class="tg-select">
                                                    <select>
                                                        <option>Nombre</option>
                                                        <option>Tipo</option>
                                                        <option>Fecha</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="tg-sortfilter tg-arrange">
                                                <span>Organizar:</span>
                                                <div class="tg-select">
                                                    <select>
                                                        <option>Des</option>
                                                        <option>Asc</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="tg-sortfilter tg-show">
                                                <span>Mostrar:</span>
                                                <div class="tg-select">
                                                    <select>
                                                        <option>12</option>
                                                        <option>24</option>
                                                        <option>Todo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <table class="tg-tablejoblidting">
                                            <tbody>
                                            @foreach($lastJobs as $lastJob)
                                                <tr>
                                                    <td class="col-md-5">
                                                        <figure class="tg-companylogo">
                                                            <a href="{{ url('perfil-publico/'.$lastJob->user_id) }}"><img src="{{ $lastJob->user->hidden_photo_selected }}" alt="image description"></a>
                                                        </figure>
                                                        <div class="tg-contentbox">
                                                            <a class="tg-tag tg-featuredtag" href="#">{{ $lastJob->job_type }}</a>
                                                            <div class="tg-title">
                                                                <h3><a href="{{ url('/oferta-trabajo/'.$lastJob->id) }}">{{ $lastJob->title }}</a></h3>
                                                            </div>
                                                            <span>{{ $lastJob->user->hidden_name }}</span>
                                                        </div>
                                                    </td>
                                                    <td class="col-md-2">
                                                        <span>
                                                            {{ $lastJob->subcategory ? $lastJob->subcategory->name : '' }}
                                                        </span>
                                                    </td>
                                                    <td class="col-md-2">
                                                        <span>Caduca:</span><br>
                                                        <span>
                                                            {{ $lastJob->expire_date_format }}
                                                        </span>
                                                    </td>
                                                    <td class="col-md-3">
                                                        <span>
                                                            {{ $lastJob->location }}
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $lastJobs->links() }}
                                    </div>
                                    {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">--}}
                                        {{--<nav class="tg-pagination">--}}
                                            {{--<ul>--}}
                                                {{--<li class="tg-prevpage"><a href="#"><i class="fa fa-angle-left"></i></a></li>--}}
                                                {{--<li><a href="#">1</a></li>--}}
                                                {{--<li><a href="#">2</a></li>--}}
                                                {{--<li><a href="#">3</a></li>--}}
                                                {{--<li><a href="#">4</a></li>--}}
                                                {{--<li class="tg-active"><a href="#">5</a></li>--}}
                                                {{--<li>...</li>--}}
                                                {{--<li><a href="#">10</a></li>--}}
                                                {{--<li class="tg-nextpage"><a href="#"><i class="fa fa-angle-right"></i></a></li>--}}
                                            {{--</ul>--}}
                                        {{--</nav>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 pull-left">
                        <aside id="tg-sidebar" class="tg-sidebar">
                            <form class="tg-themeform tg-formrefinesearch">
                                <fieldset>
                                    <h4>Filtrar por categorías</h4>
                                    <div class="tg-checkboxgroup">
                                        <div class="form-group tg-inpuicon">
                                            <i class="lnr lnr-magnifier"></i>
                                            <input type="text" name="keywords" class="form-control" placeholder="Palabra clave">
                                        </div>
                                        <div class="form-group">
												<span class="tg-select">
													<select>
														<option>Todas las categorías</option>
														<option>Cab Service</option>
														<option>Car Care &amp; Servicing</option>
														<option>Car Dealer</option>
														<option>Rent A Car</option>
														<option>Beauty Parlor</option>
													</select>
												</span>
                                        </div>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-cab" name="filterbylocation" value="Cab">
												<label for="tg-cab">Cab Service</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-care" name="filterbylocation" checked value="Care">
												<label for="tg-care">Car Care &amp; Servicing</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-realer" name="filterbylocation" value="Dealer">
												<label for="tg-realer">Car Dealer</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-rent" name="filterbylocation" value="Aldborough Hatch">
												<label for="tg-rent">Rent A Car</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-beauty" name="filterbylocation" value="Beauty">
												<label for="tg-beauty">Beauty Parlor</label>
											</span>
                                        <a href="#" data-toggle="modal" data-target=".tg-categoryModal">Ver todo</a>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por ubicación</h4>
                                    <div class="tg-checkboxgroup">
                                        <div class="form-group">
                                            <span class="tg-select">
                                                <select>
                                                    <option>Seleccione departamento</option>
                                                    <option>Pakistan</option>
                                                    <option>United States</option>
                                                    <option>United Kingdom</option>
                                                    <option>United Arab Emirates</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="tg-select">
                                                <select>
                                                    <option>Seleccione provincia</option>
                                                    <option>Texas</option>
                                                    <option>Virginia</option>
                                                    <option>New York</option>
                                                    <option>Washington</option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="tg-select">
                                                <select>
                                                    <option>Seleccione distrito</option>
                                                    <option>Texas</option>
                                                    <option>Virginia</option>
                                                    <option>New York</option>
                                                    <option>Washington</option>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por trabajo/proyecto</h4>
                                    <div class="tg-checkboxgroup">
											<span class="tg-checkbox">
												<input type="checkbox" id="tg-cabservice" name="subcategories" value="Any" checked>
												<label for="tg-cabservice">freelance</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-carcareservicing" name="subcategories" value="Car Care &amp; Servicing">
												<label for="tg-carcareservicing">Eventual</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-cardealer" name="subcategories" value="Car Dealer">
												<label for="tg-cardealer">Por horas</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-rebtac" name="subcategories" value="Rent A Car">
												<label for="tg-rebtacar">Bajo modalidad</label>
											</span>
                                        <span class="tg-checkbox">
                                                <input type="checkbox" id="tg-rebtaca" name="subcategories" value="Rent A">
                                                <label for="tg-rebtacar">Por proyecto</label>
                                            </span>
                                        <span class="tg-checkbox">
                                                <input type="checkbox" id="tg-rebtacar" name="subcategories" value="Rent A Ca">
                                                <label for="tg-rebtacar">A convenir</label>
                                            </span>
                                        {{--<a href="#" data-toggle="modal" data-target=".tg-categoryModal">Ver todo</a>--}}
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por salario</h4>
                                    <div class="tg-checkbox">
                                        <div class="tg-checkboxgroup">
												<span class="tg-checkbox">
													<input type="checkbox" id="jobs" name="fresh" value="jobs">
													<label for="jobs">Todos los trabajos</label>
												</span>
                                            <span class="tg-checkbox">
													<input type="checkbox" id="other" name="fresh" checked value="Other">
													<label for="other">Otro</label>
												</span>
                                        </div>
                                    </div>
                                    <div id="tg-filterbysalary" class="tg-filterbysalary tg-themerangeslider"></div>
                                    <div class="tg-amountbox">
                                        <span>rango: </span>
                                        <input id="tg-salaryamount" type="text" readonly>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por experiencia</h4>
                                    <div class="tg-checkboxgroup">
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="all" name="fresh" value="all" checked>
                                            <label for="all">Todas</label>
                                        </span>
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="etudiant" name="fresh" value="fresh">
                                            <label for="fresh">Estudiante</label>
                                        </span>
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="none" name="fresh" value="1years">
                                            <label for="1years">No se requiere experiencia</label>
                                        </span>
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="1year" name="fresh" value="2years">
                                            <label for="2years">Menos de 1 año</label>
                                        </span>
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="2years" name="fresh" value="German">
                                            <label for="3years">Menos de 2 años</label>
                                        </span>
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="4years" name="fresh" value="German">
                                            <label for="3years">Menos de 4 años</label>
                                        </span>
                                        <span class="tg-checkbox">
                                            <input type="checkbox" id="5years" name="fresh" value="German">
                                            <label for="3years">5 años a más</label>
                                        </span>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Tipo de prioridad</h4>
                                    <div class="tg-checkboxgroup">
											<span class="tg-checkbox">
												<input type="checkbox" id="tg-none" name="subcategories" value="none" checked>
												<label for="tg-none">Busco información de precios</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-school" name="subcategories" value="Hihg School">
												<label for="tg-school">Está en proyecto</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-degree" name="subcategories" value="02 Year Degree">
												<label for="tg-degree">Proyecto iniciado</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-04degree" name="subcategories" value="04 Year Degree">
												<label for="tg-04degree">Para una fecha concreta</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="tg-gradutedegree" name="subcategories" value="Graduate Degree">
												<label for="tg-gradutedegree">Muy urgente</label>
											</span>
                                        {{--<a href="#" data-toggle="modal" data-target=".tg-categoryModal">Ver todo</a>--}}
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <h4>Filtrar por idioma</h4>
                                    <div class="tg-checkboxgroup">
											<span class="tg-checkbox">
												<input type="checkbox" id="alllanguages" name="languages" value="all languages" checked>
												<label for="alllanguages"> Todos los idiomas</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="chinese" name="languages" value="Chinese">
												<label for="chinese">Chino</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="english" name="languages" value="English">
												<label for="english">Inglés</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="french" name="languages" value="French">
												<label for="french">Francés</label>
											</span>
                                        <span class="tg-checkbox">
												<input type="checkbox" id="german" name="languages" value="German">
												<label for="german">Alemán</label>
											</span>
                                        <a href="#" data-toggle="modal" data-target=".tg-categoryModal">Ver todo</a>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <button class="tg-btn" type="submit">Aplicar</button>
                                    <button class="tg-btn" type="reset">Reiniciar</button>
                                </fieldset>
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection
