<!doctype html>
{{--<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->--}}
{{--<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->--}}
{{--<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->--}}
{{--<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->--}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>He Go</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=1024">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('css/morris.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.countdown.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/color.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    @yield('styles')
    <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>
<body class="tg-login">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!--************************************
        Preloader Start
*************************************-->
<div class="preloader-outer">
    <div class="pin"></div>
    <div class="pulse"></div>
</div>
<!--************************************
        Preloader End
*************************************-->
<!--************************************
        Wrapper Start
*************************************-->
<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
    <!--Header Start-->
    @include('includes.header')
    <!--Header End-->

    <!--Inner Page Banner Start-->
    @yield('banner')
    <!--Inner Page Banner End-->

    <!--Main Start-->
    @yield('main')
    <!--Main End-->

    <!--Footer Start-->
    @include('includes.footer')
    <!--Footer End-->
</div>
<!--Wrapper End-->

<!--************************************
        Theme Modal Box Start
*************************************-->
<div class="modal fade tg-categoryModal" tabindex="-1">
    <div class="modal-dialog tg-modaldialog" role="document">
        <div class="modal-content tg-modalcontent">
            <div class="tg-modalhead">
                <h2>Sub Categories</h2>
                <span class="tg-selecteditems">05 Items Selected</span>
            </div>
            <div class="tg-modalbody">
                <form class="tg-themeform tg-formrefinesearch">
                    <div class="tg-column">
                        <h3>Automotive</h3>
                        <div class="tg-checkboxgroup">
								<span class="tg-checkbox">
									<input type="checkbox" id="tg-cabserviceone" name="automotive" value="Cab Service">
									<label for="tg-cabserviceone">Cab Service</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-carcareservicingone" name="automotive" value="Car Care &amp; Servicing">
									<label for="tg-carcareservicingone">Car Care &amp; Servicing</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-cardealerone" name="automotive" value="Car Dealer">
									<label for="tg-cardealerone">Car Dealer</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-rentacarone" name="automotive" value="Rent A Car">
									<label for="tg-rentacarone">Rent A Car</label>
								</span>
                        </div>
                    </div>
                    <div class="tg-column">
                        <h3>Beauty &amp; Personal Care</h3>
                        <div class="tg-checkboxgroup">
								<span class="tg-checkbox">
									<input type="checkbox" id="tg-beautyparlor" name="beautypersonalcare" value="Beauty Parlor">
									<label for="tg-beautyparlor">Beauty Parlor</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-spacenter" name="beautypersonalcare" value="Spa Center">
									<label for="tg-spacenter">Spa Center</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-massagecenter" name="beautypersonalcare" value="Massage Center">
									<label for="tg-massagecenter">Massage Center</label>
								</span>
                        </div>
                    </div>
                    <div class="tg-column">
                        <h3>Business</h3>
                        <div class="tg-checkboxgroup">
								<span class="tg-checkbox">
									<input type="checkbox" id="tg-eventorganizer" name="business" value="Event Organizer">
									<label for="tg-eventorganizer">Event Organizer</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-financeandlegal" name="business" value="Finance &amp; Legal">
									<label for="tg-financeandlegal">Finance &amp; Legal</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-marketing" name="business" value="Marketing">
									<label for="tg-marketing">Marketing</label>
								</span>
                            <span class="tg-checkbox">
									<input type="checkbox" id="tg-webdevelopment" name="business" value="IT/Web Development">
									<label for="tg-webdevelopment">IT/Web Development</label>
								</span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tg-modalfoot">
                <button class="tg-btn" type="submit">apply</button>
                <button class="tg-btn" type="submit">Reset</button>
            </div>
        </div>
    </div>
</div>

<!--************************************
        Theme Modal Box End
*************************************-->
<script src="{{ asset('js/vendor/jquery-library.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/mapclustering/data.json') }}"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
<script src="{{ asset('js/mapclustering/markerclusterer.min.js') }}"></script>
<script src="{{ asset('js/jquery.simpleWeather.min.js') }}"></script>
<script src="{{ asset('js/mapclustering/infobox.js') }}"></script>
<script src="{{ asset('js/mapclustering/map.js') }}"></script>
<script src="{{ asset('js/jquery.countdown.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/scrollbar.min.js') }}"></script>
<script src="{{ asset('js/prettyPhoto.js') }}"></script>
<script src="{{ asset('js/raphael-min.js') }}"></script>
{{--<script src="{{ asset('js/morris.min.js') }}"></script>--}}
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/readmore.js') }}"></script>
<script src="{{ asset('js/gmap3.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
@yield('scripts')
<script>
    /* -------------------------------------
            DASHBOARD LEFT NAV TOGGLE
    -------------------------------------- */
    // Morris.Area({
    //     element: 'tg-competingchart',
    //     data: [
    //         {y: '2007', a: 100, b: 90, c: 95},
    //         {y: '2008', a: 75,  b: 65, c: 70},
    //         {y: '2009', a: 50,  b: 40, c: 45},
    //         {y: '2010', a: 75,  b: 65, c: 70},
    //         {y: '2011', a: 50,  b: 40, c: 50},
    //         {y: '2012', a: 75,  b: 65, c: 60},
    //         {y: '2013', a: 100, b: 90, c: 100},
    //         {y: '2014', a: 100, b: 90, c: 100},
    //         {y: '2015', a: 100, b: 90, c: 100},
    //         {y: '2016', a: 100, b: 90, c: 100},
    //         {y: '2017', a: 100, b: 90, c: 100},
    //     ],
    //     xkey: 'y',
    //     resize: true,
    //     redraw: true,
    //     hideHover: 'ture',
    //     ykeys: ['a', 'b', 'c'],
    //     gridTextColor: '#919191',
    //     lineColors :['#26a69a','#66bb6a','#9ccc65'],
    //     labels: ['Series A', 'Series B', 'Series C'],
    // });
</script>
<script>
    // $(document).ready(function() {
    //     $.simpleWeather({
    //         location: 'Austin, TX',
    //         woeid: '',
    //         unit: 'f',
    //         success: function(weather) {
    //             console.log(weather);
    //             html = '<div class="tg-weatherarea"><i class="lnr lnr-cloud"></i><div class="tg-weathercontent"> <span>'+weather.currently+'</span><h2>'+weather.temp+'&deg;'+weather.units.temp+'</h2><span>'+weather.city+', '+weather.region+'</span></div></div>';
    //
    //             $("#weather").html(html);
    //         },
    //         error: function(error) {
    //             $("#weather").html('<p>'+error+'</p>');
    //         }
    //     });
    // });
</script>
<script>
    $(document).on('click', '[data-offer]', function () {

        var urlOfferJob = $(this).data('offer');
        var $formOfferJob = $('#formOffer');
        var formData = new FormData();
        formData.append('_token', $formOfferJob.find('[name=_token]').val());
        $.ajax({
            url: urlOfferJob,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            type: 'POST',
            data: formData,
            success:function (data){
                window.location = "/publicar-oferta-trabajo/"+data.id
            },
            error:function () {
                alert('Error de conexión')
            }
        });
    });
</script>
</body>
</html>