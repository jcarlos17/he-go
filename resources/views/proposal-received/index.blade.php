@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Propuestas recibidas</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Propuestas recibidas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-joblisting tg-dashboardmanagejobs">
                                <div class="tg-dashboardhead">
                                    <div class="tg-dashboardtitle">
                                        <h2>Propuestas proformas recibidas</h2>
                                    </div>
                                    {{--<button class="tg-btnaddservices" data-toggle="modal" data-target=".tg-categoryModal">Post A New Job</button>--}}
                                </div>
                                <div class="tg-sortfilters">
                                    <div class="tg-sortfilter tg-sortby">
                                        <span>Sort By:</span>
                                        <div class="tg-select">
                                            <select>
                                                <option>Name</option>
                                                <option>Type</option>
                                                <option>date</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tg-sortfilter tg-arrange">
                                        <span>Arrange:</span>
                                        <div class="tg-select">
                                            <select>
                                                <option>des</option>
                                                <option>Asc</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tg-sortfilter tg-show">
                                        <span>Show:</span>
                                        <div class="tg-select">
                                            <select>
                                                <option>12</option>
                                                <option>24</option>
                                                <option>all</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <table class="tg-tablejoblidting">
                                        <tbody>
                                        @foreach($proposals as $proposal)
                                            <tr>
                                                <td class="col-md-8">
                                                    <figure class="tg-companylogo">
                                                        <a href="{{ url('perfil-publico/'.$proposal->from_id) }}"><img src="{{ $proposal->from->photo_selected }}" alt="image description"></a>
                                                        {{--<a class="tg-btndel" href="#"><i class="fa fa-close"></i></a>--}}
                                                    </figure>
                                                    <div class="tg-contentbox">
                                                        <a class="tg-tag tg-featuredtag" href="#">Freelance</a>
                                                        <div class="tg-title">
                                                            <h3><a href="javascript:void(0);" data-show="{{ url('/proposal-received/'.$proposal->id.'/show') }}">{{ $proposal->from->name }}</a></h3>
                                                        </div>
                                                        <span>By: Bright Future Group & Company</span>
                                                    </div>
                                                </td>
                                                @if($proposal->status == 'Enviada')
                                                    <td class="col-md-4">
                                                    <span style="background: #42a5f5; border-radius:3px; color: white; font-weight: 700; padding: 2px 5px">Propuesta en revisión</span><br>
                                                    <span>{{ $proposal->time_elapsed }}</span>
                                                    </td>
                                                @endif
                                                @if($proposal->status == 'Rechazada')
                                                <td class="col-md-4">
                                                    <span style="background: #ef5350; border-radius:3px; color: white; font-weight: 700; padding: 2px 5px">Propuesta rechazada</span><br>
                                                    <span>No califica</span>
                                                </td>
                                                @endif
                                                @if($proposal->status == 'Aceptada')
                                                    <td class="col-md-4">
                                                    <span style="background: #5dc560; border-radius:3px; color: white; font-weight: 700; padding: 2px 5px">Propuesta aceptada</span><br>
                                                    <span>En desarrollo</span>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <!--Main End-->
    <div class="modal fade" id="proposalReceivedModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-detailpage tg-jobdetail">
                    <div class="tg-detailpagehead">
                        <div class="tg-bordertitle">
                            <a class="tg-btnprint" href="javascript:void(0);"><i class="lnr lnr-printer"></i></a>
                            <a class="tg-btn tg-btnedite" href="javascript:void(0);" style="margin-right: 10px"><i class="lnr lnr-envelope"></i></a>
                            <h2 style="font-style: bold">Has enviado la siguiente propuesta a:</h2>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="tg-detailpageheadcontent tg-border-bottom" style="margin: 40px 0; border-top: 1px solid #5dc560">
                                    <div class="tg-companylogo"><img id="imagePR" src="" alt="image description"></div>
                                    <div class="tg-companycontent">
                                        <div class="tg-title" style="margin-bottom: 10px">
                                            <h3 id="nameToPR"></h3>
                                        </div>
                                        <ul class="tg-matadata">
                                            <li><span class="tg-stars"><span></span></span></li>
                                            <li style="color: #999">
                                                <i class="fa fa-thumbs-o-up"></i>
                                                <em>99% (1009 votes)</em>
                                            </li>
                                        </ul>
                                        <span id="catSubCatPR" class="tg-jobpostedby" style="margin: 5px 0;color: black"></span>
                                        <span id="locationPR" class="tg-jobpostedby" style="margin: 5px 0;color: black"></span>
                                        <span id="sendDatePR" class="tg-jobpostedby" style="margin: 5px 0;color: black">Enviado: </span>
                                    </div>
                                    <form method="POST" action="{{ url('chat/new') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="toId" name="to_id" value="">
                                        <button type="submit" class="tg-btn">Iniciar chat</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tg-twocolumns" class="tg-twocolumns">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-companyfeatures">
                                <div class="tg-companyfeaturebox tg-introduction">
                                    <div class="tg-companyfeaturetitle">
                                        <h3 class="text-center" style="font-size: 23px; margin-bottom: 20px">Propuesta proforma</h3>
                                        <h3 style="font-style: bold; margin-bottom: 10px">Título del trabajo a presupuestar:</h3>
                                        <h4 id="offerTitlePR" style="color: gray"></h4>
                                    </div>
                                    <div class="tg-dashboardbox tg-introduction" style="padding-bottom: 20px">
                                        <div class="tg-dashboardtitle">
                                            <h2>Descripción</h2>
                                        </div>
                                        <div class="tg-introductionbox">
                                            <div class="form-group">
                                                <p id="offerDescriptionPR"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                            <span class="col-sm-6">Movilidad geográfica</span>
                                            <div class="form-horizontal col-sm-4">
                                                <span id="geographicalMobilityPR"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                            <span class="col-sm-6">Materiales necesarios incluidos</span>
                                            <div class="form-horizontal col-sm-4">
                                                <span id="necessaryMaterialPR"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                            <span class="col-sm-6">Garantía mínima de 30 dias</span>
                                            <div class="form-horizontal col-sm-4">
                                                <span id="minimumGuaranteePR"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                            <span class="col-sm-6">Requiere adelanto (%)</span>
                                            <div class="form-horizontal col-sm-4">
                                                <span id="advancementPR"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                            <span class="col-sm-6">Cuento con seguro EPS/SCTR</span>
                                            <div class="form-horizontal col-sm-4">
                                                <span id="insurancePR"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                            <span class="col-sm-6">Precio final estimado S/.</span>
                                            <div class="form-horizontal col-sm-4">
                                                <span id="finalPricePR"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-dashboardbox tg-introduction" style="padding-top: 20px">
                                    <div class="tg-dashboardtitle">
                                        <h2>Detalles y desarrollo del servicio</h2>
                                    </div>
                                    <div class="tg-introductionbox">
                                        <div class="form-group">
                                            <p id="serviceDetailPR"></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-checkbox">
                                                <input type="checkbox" id="termsPR" disabled>
                                                <label for="termsPR">Propongo una entrevista personal para una correcta valoración del trabajo demandado
                                                    y la elaboración de un presupuesto final.</label>
                                            </div>
                                        </div>
                                        <div id="statusPR" class="form-group text-center">
                                            <button class="tg-btn-danger" id="btnReject">Rechazar</button>
                                            <a class="tg-btn" href="">Aceptar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="showReject" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document" style="max-width: 740px">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Rechazar propuesta</h2>
                </div>
                <form id="formReject" action="" method="POST">
                    {{ csrf_field() }}
                    <div class="tg-modalbody">
                        <div class="tg-detailpage tg-jobdetail">
                            <div class="tg-detailpagehead">
                                <p style="margin-bottom: 20px;color: black">
                                </p>
                            </div>
                            <div id="tg-twocolumns" class="tg-twocolumns">
                                <div id="tg-content" class="tg-content">
                                    <div class="tg-companyfeatures">
                                        <div class="tg-dashboardbox tg-introduction">
                                            <div class="tg-dashboardtitle">
                                                <h2>Justificación opcional del rechazo de la propuesta</h2>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" rows="4" name="reject_reason" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tg-modalfoot">
                        <button class="tg-btn-danger" style="float: right">Rechazar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-show]', function () {
            // request al servidor
            var urlShowProposal = $(this).data('show');
            $.ajax({
                type: 'GET',
                url: urlShowProposal,

                success:function (data){
                    $('#toId').replaceWith('<input type="hidden" id="toId" name="to_id" value="'+data.to_id+'">');
                    $('#imagePR').replaceWith('<img id="imagePR" src="'+data.from_photo_selected+'" alt="image description">');
                    $('#nameToPR').replaceWith('<h3 id="nameToPR">'+data.from_name+'</h3>');
                    $('#catSubCatPR').replaceWith('<span id="catSubCatPR" class="tg-jobpostedby" style="margin: 5px 0;color: black">'+data.cat_subcat+'</span>');
                    $('#locationPR').replaceWith('<span id="locationPR" class="tg-jobpostedby" style="margin: 5px 0;color: black">'+data.location+'</span>');
                    $('#sendDatePR').replaceWith('<span id="sendDatePR" class="tg-jobpostedby" style="margin: 5px 0;color: black">Enviado: '+data.send_date+'</span>');
                    $('#offerTitlePR').replaceWith('<h4 id="offerTitlePR" style="color: gray">'+data.offer_title+'</h4>');
                    $('#offerDescriptionPR').replaceWith('<span id="offerDescriptionPR"p>'+data.offer_description+'</span>');
                    $('#geographicalMobilityPR').replaceWith('<span id="geographicalMobilityPR">'+data.geographical_mobility+'</span>');
                    $('#necessaryMaterialPR').replaceWith('<span id="necessaryMaterialPR">'+data.necessary_material+'</span>');
                    $('#minimumGuaranteePR').replaceWith('<span id="minimumGuaranteePR">'+data.minimum_guarantee+'</span>');
                    $('#advancementPR').replaceWith('<span id="advancementPR">'+data.advancement+'</span>');
                    $('#insurancePR').replaceWith('<span id="insurancePR">'+data.insurance+'</span>');
                    $('#finalPricePR').replaceWith('<span id="finalPricePR">'+data.final_price+'</span>');
                    $('#serviceDetailPR').replaceWith('<span id="serviceDetailPR">'+data.service_detail+'</span>');
                    $('#termsPR').replaceWith('<input type="checkbox" id="termsPR" '+data.personal_interview+' disabled>');
                    if(data.status === 'Aceptada' || data.status === 'Rechazada'){
                        $('#statusPR').replaceWith('<div id="statusPR" class="form-group text-center"></div>');
                    }else{
                        $('#statusPR').replaceWith('<div id="statusPR" class="form-group text-center"><button onclick="showModalReject()" class="btn tg-btn-danger" id="btnReject" style="margin-right: 10px">Rechazar</button><a class="tg-btn" href="/proposal-received/'+data.id+'/accept">Aceptar</a></div>');
                    }
                    $('#proposalReceivedModal').modal('show');
                    //
                    $('#formReject').attr('action', '/proposal-received/'+data.id+'/reject');
                }
            });

        });
        function showModalReject() {
            $('#proposalReceivedModal').modal('hide');
            $("#proposalReceivedModal").on("hidden.bs.modal", function () {
                $('#showReject').modal('show');
            });
        }
    </script>
@endsection
