<header id="tg-header" class="tg-header tg-haslayout">
    <div class="tg-topbar tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <ul class="tg-addressinfo">
                        <li>
                            <i class="lnr lnr-envelope"></i>
                            <a href="mailto:info@domain.com">info@domain.com</a>
                        </li>
                        <li>
                            <i class="lnr lnr-phone-handset"></i>
                            <span>+4 1234 567890</span>
                        </li>
                    </ul>
                    <div class="tg-themedropdown tg-languagesdropdown">
                        <a href="javascript:void(0);" id="tg-languages" class="tg-btndropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <em><img src="{{ asset('images/flags/img-01.jpg') }}" alt="image description"></em>
                            <span>eng</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="tg-dropdownmenu" aria-labelledby="tg-languages">
                            <li>
                                <a href="#">
                                    <em><img src="{{ asset('images/flags/img-02.jpg') }}" alt="image description"></em>
                                    <span>chi</span>
                                </a>
                            </li>
                            <li class="tg-active">
                                <a href="#">
                                    <em><img src="{{ asset('images/flags/img-03.jpg') }}" alt="image description"></em>
                                    <span>eng</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <em><img src="{{ asset('images/flags/img-04.jpg') }}" alt="image description"></em>
                                    <span>rus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tg-adminbox">
                        @guest
                        <ul class="tg-addressinfo">
                            <li>
                                <i class="lnr lnr-users"></i>
                                <a href="{{ route('login') }}">Login / Registro</a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="{{ route('register') }}">Registro</a>--}}
                            {{--</li>--}}
                        </ul>
                        {{--<div class="tg-loginregister">--}}
                            {{--<a class="tg-btnlogin" href="javascript:void(0);">Login</a>--}}
                            {{--<a class="tg-btnregister" href="javascript:void(0);">Register</a>--}}
                        {{--</div>--}}

                        @else
                            <div class="tg-useradminbox">
                                <div class="tg-themedropdown tg-userdropdown">
                                    <a href="javascript:void(0);" id="tg-usermenu" class="tg-btndropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <em><img id="headerPhoto" src="{{ auth()->user()->photo_selected }}" alt="{{ auth()->user()->name }}" style="width: 30px; height: 30px"></em>
                                        <span>{{ auth()->user()->name }}</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="tg-dropdownmenu tg-usermenu" aria-labelledby="tg-usermenu">
                                        <nav id="tg-dashboardnav" class="tg-dashboardnav">
                                            <ul>
                                                <li @if(request()->is('insights')) class="tg-active" @endif>
                                                    <a href="{{ url('/insights') }}">
                                                        <i class="lnr lnr-database"></i>
                                                        <span>Vista Inicial</span>
                                                    </a>
                                                </li>
                                                <li @if(request()->is('perfil-publico/'.auth()->id())) class="tg-active" @endif>
                                                    <a href="{{ url('/perfil-publico/'.auth()->id()) }}">
                                                        <i class="lnr lnr-user"></i>
                                                        <span>Mi perfil público</span>
                                                    </a>
                                                </li>
                                                <li @if(request()->is('profile-settings')) class="tg-active" @endif>
                                                    <a href="{{ url('/profile-settings') }}">
                                                        <i class="lnr lnr-cog"></i>
                                                        <span>Configuración de perfil</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                        <i class="lnr lnr-exit"></i>
                                                        <span>Cerrar sesión</span>
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tg-navigationarea">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <strong class="tg-logo"><a href="#"><img src="{{ asset('images/logo.png') }}" alt="image description"></a></strong>
                    <div class="tg-rightarea">
                        <nav id="tg-nav" class="tg-nav">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
                                <ul>
                                    <li @if(request()->is('/')) class="current-menu-item" @endif><a href="/">Inicio</a></li>
                                    <li @if(request()->is('about')) class="current-menu-item" @endif><a href="{{ url('/about') }}">Nosotros</a></li>
                                    <li class="menu-item-has-children @if(request()->is('service-provider') || request()->is('search-service')) current-menu-item @endif">
                                        <a href="#">Cómo funciona</a>
                                        <ul class="sub-menu">
                                            <li @if(request()->is('service-provider')) class="current-menu-item" @endif>
                                                <a href="{{ url('/service-provider') }}">Soy un prestador de servicios</a>
                                            </li>
                                            <li @if(request()->is('search-service')) class="current-menu-item" @endif>
                                                <a href="{{ url('/search-service') }}">Soy un buscador de servicios</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li @if(request()->is('categories')) class="current-menu-item" @endif><a href="{{ url('/categories') }}">Categorías</a></li>
                                    <li @if(request()->is('ultimos-trabajos')) class="current-menu-item" @endif><a href="{{ asset('/ultimos-trabajos') }}">Últimos trabajos</a></li>
                                    {{--<li @if(request()->is('contact')) class="current-menu-item" @endif><a href="{{ asset('/contact') }}">Contacto</a></li>--}}
                                </ul>
                            </div>
                        </nav>
                        @guest
                            <a class="tg-btn tg-btnpostanewjob" href="{{ url('/login?redirect_to='.url()->current()) }}">Publicar trabajo</a>
                        @else
                            <div id="formOffer" class="hidden">{{ csrf_field() }}</div>
                            <button class="tg-btn tg-btnpostanewjob" data-offer="{{ url('publicar-oferta-trabajo') }}">Publicar trabajo</button>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>