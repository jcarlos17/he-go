@extends('layouts.app')

@section('styles')

@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Política de privacidad</h1>
                    </div>
                    {{--<ol class="tg-breadcrumb">--}}
                        {{--<li><a href="/">Inicio</a></li>--}}
                        {{--<li class="tg-active">Soy un buscador de servicios</li>--}}
                    {{--</ol>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-paddingzero tg-haslayout">
        <section class="tg-main-section tg-haslayout tg-bglight">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-secureandreliable">
                            <div class="tg-textshortcode" style="padding: 0;">
                                <div class="tg-bordertitle">
                                    <h3 style="margin: 0;color: #5dc560">POLÍTICA DE PRIVACIDAD</h3>
                                </div>
                                <div class="tg-description">
                                    <p class="text-justify">
                                        En virtud de lo establecido en la normativa sobre la Ley de protección de datos de carácter personal (LPDP), le informamos, que los datos de carácter personal, que puedan obtenerse a través de este sitio web con motivo de la solicitud, utilización y/o contratación de cualquiera de los servicios ofertados por los prestadores publicitados en el mismo, serán incorporados a ficheros responsabilidad de:
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        - RED HE GO ejerce la actividad de mediación como directorio digital de profesionales y posibles usuarios que busquen estos profesionales, en el caso de contratación de alguno de los servicios propuestos a través del presente sitio web y en base a las condiciones de contratación que tengan establecidas.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        - RED HE GO presta servicios de asistencia y mediación, donde los datos serán tratados para la gestión e información de presupuestos, así como para la realización y ejecución de las asistencias o servicios solicitados a través de este sitio web y en base a las condiciones de contratación establecidas.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        En adelante denominadas HE GO.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El titular acepta y consiente, que los datos personales facilitados sean utilizados con la finalidad de gestionar los servicios contratados, así como adecuar las ofertas comerciales a su perfil particular para el envío de información, publicidad y ofertas comerciales de las entidades afiliadas y con las que guarda relación RED HE GO, así como de entidades colaboradoras relacionadas con productos decoración, menaje, interiorismo, y más del sector de reformas y reparaciones del hogar, comercio o empresa. El consentimiento obtenido podrá ser revocado en cualquier momento.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        A tal efecto, el titular de los datos acepta que RED HE GO, pueda ceder datos recíprocamente, los datos obtenidos con las finalidades de tratamiento indicadas.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        La lectura y aceptación de la política de privacidad de RED HE GO (en adelante, la “<b>Política de Privacidad</b>”) es condición necesaria para la utilización de los servicios del Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Esta Política de Privacidad regula la recopilación y el uso de la información de los Usuarios del Sitio Web a partir de la fecha de entrada en vigor que aparece al final de los presentes Términos de Uso. No se aplica a cualquier otra URL a la que se acceda desde el Sitio Web, que tendrá su propia política en relación con la recopilación de datos y su uso.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Por favor, lea atentamente esta política de privacidad antes de empezar a utilizar el sitio web y facilitar cualquier tipo de información personal. Le ayudará a tomar decisiones informadas.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Si usa el Sitio Web es porque acepta que HE GO use su información y datos personales de acuerdo con esta Política de Privacidad. Si no está de acuerdo con ella, por favor no use el Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO siempre pedirá consentimiento al Usuario antes de utilizar sus datos para cualquier fin distinto de los que se describen en esta Política de Privacidad.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        En el cumplimiento de la Ley de datos Personales de sus Usuarios, HE GO cumple en todo momento con la legislación vigente en Perú y convenios y tratados internacionales y en concreto, con la Ley Nº 29733, del 02 de julio del 2011, de Protección de Datos (en adelante “<b>LPDP</b>”) y su reglamento de desarrollo. Para ello, adopta las medidas técnicas y organizativas necesarias para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales facilitados, habida cuenta del estado de la tecnología, la naturaleza de los datos y los riesgos a los que están expuestos.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO informa al Usuario que, a fin de mejorar la calidad de los servicios proporcionados y facilitar la realización de los mismos, se reserva el derecho de grabar las llamadas telefónicas, tanto entrantes como salientes, con el previo y expreso consentimiento del Usuario, de forma que la negativa a realizar dicha grabación facultará a HE GO para no aceptar la activación del servicio o, en su caso, proceder a la suspensión del mismo.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Información que recopilamos sobre el Usuario</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Sitio Web puede usarse por los Usuarios de distintas maneras, desde buscar solamente información hasta ser Usuarios registrados, recopilando distinta información según el caso:
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        a) Usuario no registrado
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        En este caso, el Sitio Web recopila, además de la dirección IP desde la que se accede, la siguiente información del Usuario (en adelante, “Información no Personal”):
                                    </p>
                                    <br>
                                    <ul>
                                        <li class="text-justify">Si se ha llegado al Sitio Web haciendo clic en un enlace o anuncio de otro sitio web.</li>
                                        <li class="text-justify">Almacenamiento de cookies en el disco duro del ordenador del Usuario, previo consentimiento del Usuario. HE GO utiliza las cookies para poder dar información más ajustada a los intereses del Usuario. Siempre se puede denegar el uso de cookies que no sean puramente técnicas o encaminadas a hacer posible la comunicación entre HE GO y el Usuario, si bien esto puede reducir la plena funcionalidad del Sitio Web y de otros sitios web que se visiten. Para tener más información sobre la utilización de cookies por parte de HE GO, consulte el apartado Política de cookies de nuestros Términos de Uso.</li>
                                        <li class="text-justify">Datos específicos sobre el dispositivo del Usuario (por ejemplo, el modelo de equipo, versión del sistema operativo, fallos, tipo de navegador, idioma, fecha y hora de la solicitud, URL de referencia, datos sobre la red móvil, incluyendo el número de teléfono, etc.).</li>
                                        <li class="text-justify">Datos sobre la ubicación física del Usuario mediante diferentes tecnologías, como por ejemplo mediante las señales de GPS enviadas por un dispositivo móvil, información sobre puntos de acceso Wi-Fi, antenas de telefonía móvil más cercanas, etc.</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        b) Usuario registrado
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO condiciona la utilización de algunos de los servicios del Sitio Web a la previa cumplimentación del correspondiente registro de Usuario, bien como Prestador de servicios, bien como Buscador de servicios, debiendo indicar como mínimo en los formularios de registro la información marcada con un asterisco sobre determinados datos personales (en adelante, “Información Personal”), como por ejemplo, nombre y apellidos, documento de identificacion, un nombre de usuario, una contraseña, dirección de e-mail, dirección postal, archivos de documentación personal, de vivienda, de carácter policial y número de teléfono.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        En este caso, HE GO recopilará la Información no personal descrita más arriba y también la Información Personal, incluyendo la dirección IP desde la que se accede. Además, el Usuario que se registre podrá incluir información no obligatoria adicional, como por ejemplo el género, fecha de nacimiento, la dirección de blog si lo tiene o la URL de su sitio web. Podemos almacenar toda esta información o parte de la misma en una cookie para que nuestro sistema le reconozca cada vez que visites el Sitio Web y guardar las preferencias del Usuario.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        En virtud de lo dispuesto en la LPDP, le informamos de que la cumplimentación de los formularios por el Usuario tiene carácter voluntario, si bien la falta de cumplimentación de los campos básicos limitará el acceso o uso del Sitio Web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Los datos personales que los Usuarios nos facilitan quedan incorporados y serán tratados en los ficheros titularidad de HE GO, con el fin de poder atender las peticiones de los Usuarios y prestar el servicio solicitado, así como para mantenerles informados, incluso por medios electrónicos, sobre cuestiones relativas a la actividad de la empresa y sus servicios, así como para el envío de comunicaciones comerciales sobre los sectores de reformas, obras, decoración, interiorismo, hogar y gran consumo, cuyo envío el Usuario consiente expresamente.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Usuario puede ejercer en cualquier momento los derechos de acceso, rectificación, cancelación y oposición de sus datos de carácter personal mediante correo electrónico dirigido a soporte@redhego.com.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        En la utilización de los datos incluidos en el fichero, HE GO se compromete a respetar la confidencialidad y a utilizarlos de acuerdo con la finalidad del fichero, así como a dar cumplimiento a su obligación de guardarlos y adaptar todas las medidas para evitar la alteración, pérdida, tratamiento o acceso no autorizado de acuerdo con lo establecido en la LPDP - Ley de Protección de Datos Personales – Ley Nº 29733 y su Reglamento
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Privacidad de menores de edad</em>
                                    </p>
                                    <p class="text-justify">
                                        El Sitio Web se dirige a mayores de edad. HE GO no recopila ni mantiene voluntariamente ningún tipo de Información Personal de menores de edad.
                                    </p>
                                    <p class="text-justify">
                                        Si HE GO tuviera conocimiento de que el Usuario es menor de edad, procederá a la cancelación inmediata de su Información Personal, de su información no personal y de su cuenta.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Utilización de los datos suministrados por los Usuarios</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO usará los datos de sus Usuarios con una finalidad comercial de personalización, operativa y estadística, y para el envío de comunicaciones comerciales a los Usuarios.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Usuario autoriza expresamente el almacenamiento de sus datos y la posibilidad de hacer estudios de marketing con ellos, para poder adecuar los servicios de HE GO a su perfil personal. Además, HE GO podrá ceder la Información Personal y la Información no Personal de los Clientes a los prestadores y terceros con el objeto de que el Cliente reciba el servicio solicitado.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO podrá conservar los datos de los Usuarios, aunque haya terminado toda relación con HE GO, por el tiempo necesario para cumplir con las obligaciones legales oportunas.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO podrá compartir y publicar en redes sociales el contenido de los servicios ofrecidos por los Prestadores, que hayan publicado en el Sitio Web, a efectos publicitarios. A tal efecto, el Prestador autoriza a HE GO a, de forma gratuita, libremente reproducir, distribuir, publicitar, adaptar o explotar por cualquier forma o medio dicho contenido publicado por el Prestador, renunciando a cualesquiera derechos de contenido económico que le pudieran corresponder, en concepto de compensación o cualquier otro título.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO utilizará la dirección de correo electrónico del Usuario para los siguientes fines, que el Usuario consiente expresamente:
                                    </p>
                                    <br>
                                    <ul>
                                        <li class="text-justify">Envío de boletines informativos y otras comunicaciones comerciales por correo electrónico sobre HE GO y los sectores de arquitectura, la decoración, el interiorismo, las obras y reformas y gran consumo. El Usuario podrá indicar su oposición a la recepción de tal boletín en cualquier momento.</li>
                                        <li class="text-justify">Pedir participación en encuestas que ayudan a mejorar el servicio prestado.</li>
                                        <li class="text-justify">Responder solicitudes de presupuestos de los Clientes.</li>
                                        <li class="text-justify">Notificar actividades de otros Usuarios (si alguien envía una opinión o comentario).</li>
                                        <li class="text-justify">Notificar cuestiones de servicio o cuenta (por ejemplo, restablecer la contraseña).</li>
                                        <li class="text-justify">Avisar de conducta impropia y, si es necesario, informar de la suspensión o supresión de la cuenta del Usuario.</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        Al acceder o utilizar el Sitio Web, los Usuarios consienten expresamente recibir estos mensajes de correo electrónico, aunque podrán darse de baja en el enlace especificado en cualquier comunicación que se envíe por correo electrónico, en su configuración personal en el Sitio Web o enviando un correo electrónico a soporte@redhego.com. No obstante, el Usuario no podrá cancelar determinada correspondencia nuestra, como los mensajes relativos a su cuenta.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Además, HE GO podrá utilizar la Información Personal para:
                                    </p>
                                    <br>
                                    <ul>
                                        <li>Recomendar servicios que pudieran ser interesantes.</li>
                                        <li>Garantizar que los anuncios que se vean como parte del uso del Sitio Web sean interesantes para el Usuario, según su ubicación geográfica y categoría de trabajos elegida.</li>
                                        <li>Atribuir al Usuario sus comentarios y opiniones.</li>
                                        <li>De cualquier otro modo, para mejorar su uso del Sitio Web.</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        <em>Como datos agregados</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO podrá utilizar la Información Personal de los Usuarios como parte de datos agregados para mostrar a terceras partes (incluidos anunciantes). También podrá compartir estadísticas y la información demográfica sobre los Usuarios y su utilización del Sitio Web con los proveedores de publicidad y programación, pero nada de esto permitirá a esos terceros identificar personalmente a un Usuario.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Con los proveedores de servicios</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Hay terceros que gestionan parte de los servicios del Sitio Web, como los anuncios, las encuestas, etc. A ellos HE GO les exige que cumplan esta Política de Privacidad en lo que les resulte aplicable y además ellos deberán tener la suya propia; no obstante, HE GO no será responsable del cumplimiento de dicha política.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Si HE GO es requerido</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Bajo algunas circunstancias HE GO podrá compartir la Información Personal, usarla, preservarla o divulgarla, como, por ejemplo:
                                    </p>
                                    <br>
                                    <ul>
                                        <li>Para cooperar con las autoridades competentes:</li>
                                        <ul>
                                            <ol>
                                                <li>Si creemos que es razonablemente necesario para satisfacer cualquier ley o proceso legal en cualquier parte del mundo y creemos que el hacerlo podrá disminuir nuestra responsabilidad o nos permitiría defender nuestros derechos legales; y sólo proporcionaremos la información requerida.</li>
                                                <li>Si creemos que esta acción es apropiada para hacer cumplir con los Términos y Condiciones de HE GO, incluida toda la investigación de posibles violaciones de ello.</li>
                                                <li>Si es necesario para detectar, prevenir, o de cualquier modo abordar y perseguir el fraude, seguridad o temas técnicos relacionados con HE GO.</li>
                                                <li>Si esa acción es adecuada para proteger los derechos, propiedad o seguridad de HE GO, sus empleados y los Usuarios.</li>
                                            </ol>
                                        </ul>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        En todos estos casos, HE GO podrá, por propia iniciativa o a requerimiento de terceros que demuestren un legítimo interés, facilitar la Información Personal del Profesional, cesión que el Profesional consiente expresamente.
                                    </p>
                                    <br>
                                    <ul>
                                        <li>Transacción corporativa. Si HE GO participa en una fusión, adquisición u otra transacción que implique la venta de todos o algunos de sus activos o valores con derechos de voto, la información del Usuario, incluida la Información Personal obtenida a través del Sitio Web, puede ser incluida en los activos transferidos o valores. Si esto ocurre, la Información Personal del Usuario seguirá siendo objeto de la presente Política de Privacidad.</li>
                                    </ul>
                                    <br>
                                    <p class="text-justify">
                                        <em>Terceros que recogen y reciben su información: Google Analytics</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        El Sitio Web utiliza Google Analytics, un servicio de análisis web proporcionado por Google, Inc (“Google”) una sociedad de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway Mountain View (California), CA 94043, Estados Unidos y que, según determina Google, se rige por las condiciones generales y la política de privacidad de Google accesibles en su web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Google Analytics utiliza cookies para ayudar a HE GO a analizar cómo los Usuarios utilizan la aplicación. La información generada por la cookie acerca del uso de un Usuario del Sitio Web (incluyendo su dirección IP) se archiva por Google (en su propio nombre y nunca en nombre ni por cuenta de HE GO) en sus servidores y HE GO no tiene acceso en ningún momento a esa información, sino únicamente a la información agregada que le facilita posteriormente Google y que no está asociada a ninguna dirección IP; además, Google utiliza una cookie distinta para cada sitio web y no realiza ningún tipo de seguimiento de los visitantes de varios sitios.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Con esa información, que se recopila de forma anónima y sin identificar a Usuarios individualmente, Google elabora informes de actividad para HE GO y la prestación de otros servicios relacionados con el uso de Internet.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Google podrá transmitir dicha información a terceros cuando sea necesario hacerlo por ley o cuando dichos terceros procesen la información en nombre de Google.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Al utilizar el Sitio Web, el Usuario consiente el tratamiento de información por Google de la forma y para los fines arriba indicados.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Para deshabilitar este tipo de cookie, algunos navegadores indicarán cuando se está enviando una cookie y permitirán rechazarlas caso por caso. Adicionalmente a rechazar cookies, el Usuario también puede instalar la herramienta Google AnalyticsOpt-outAdd-on en su navegador, que impide a Google Analytics la recopilación de información sobre sus visitas a sitios web.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Por último, informamos de que Google es quien determina la finalidad del tratamiento y uso de la información que capta Google Analytics, así como el funcionamiento y duración de las cookies.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Para saber más sobre estas cookies, visite el apartado Política de cookies de los Términos y Condiciones.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>Medidas de seguridad para proteger la información personal</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        HE GO se preocupa por defender el derecho a la intimidad de los Usuarios. Por eso, toma todas las medidas técnicas y organizativas necesarias para proteger la seguridad e integridad de la Información Personal frente a accesos no autorizados y también contra la alteración, pérdida o destrucción accidentales.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Así, el envío y la remisión de datos que se realice por el Usuario a través del Sitio Web o la información que éste remita, se encuentra protegida por técnicas de seguridad electrónica en la red. Asimismo, los datos suministrados y almacenados en las bases de datos de HE GO se encuentran igualmente protegidos por sistemas de seguridad que impiden el acceso de terceros no autorizados a los mismos. HE GO realiza sus mejores esfuerzos para disponer de los sistemas más actualizados para la eficacia de estos sistemas de seguridad.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Además, HE GO almacena la Información Personal durante tanto tiempo como sea imprescindible para ofrecer sus servicios y destruye periódicamente la que ya no necesita. HE GO cuenta con un archivo con copia de la información que le permite cumplir con las leyes o responder a procesos legales.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        A pesar de lo anterior, HE GO no puede garantizar la seguridad absoluta de la Información Personal, por lo que el Usuario debe colaborar y utilizar y en todo momento el sentido común sobre la Información Personal que publica.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        <em>El control del propio usuario sobre su información</em>
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Todos los datos que publica el Usuario lo hace bajo su propio riesgo. HE GO no puede garantizar que la Información publicada no será vista por personas no autorizadas. El Usuario entiende y reconoce que, incluso después de la eliminación, la Información puede permanecer visibles en caché y que se archivan las páginas si otros Usuarios han copiado o almacenado la Información.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Para ejercitar sus derechos de acceso, rectificación, oposición y cancelación de sus datos de carácter personal, en cumplimiento de lo dispuesto en la Ley deberá ponerse en contacto con el siguiente departamento y seguir las pautas indicadas.
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        RED HE GO
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        Departamento de Protección de Datos
                                    </p>
                                    <br>
                                    <p class="text-justify">
                                        contacto@redhego.com
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
