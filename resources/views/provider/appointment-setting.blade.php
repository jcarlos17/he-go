@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Administrar agenda</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Administrar agenda</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardappointmentsetting">
                                <div class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox">
                                            <div class="tg-dashboardtitle">
                                                <h2>Administrar agenda</h2>
                                                {{--<a class="tg-btnaddnew" href="dashboardappointmentsetting2.html">+ Add Custom Slots</a>--}}
                                            </div>
                                            <div class="tg-dashboardappointmentsettingbox">
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Lunes</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Lunes">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsLunes" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Lunes') as $schedule)
                                                            <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                        {{--<div class="tg-slots">--}}
                                                            {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Martes</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Martes">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsMartes" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Martes') as $schedule)
                                                                <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                    {{--<div class="tg-slots">--}}
                                                    {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Miércoles</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Miercoles">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsMiercoles" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Miercoles') as $schedule)
                                                                <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                    {{--<div class="tg-slots">--}}
                                                    {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Jueves</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Jueves">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsJueves" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Jueves') as $schedule)
                                                                <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                    {{--<div class="tg-slots">--}}
                                                    {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Viernes</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Viernes">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsViernes" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Viernes') as $schedule)
                                                                <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                    {{--<div class="tg-slots">--}}
                                                    {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Sábado</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Sabado">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsSabado" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Sabado') as $schedule)
                                                                <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                    {{--<div class="tg-slots">--}}
                                                    {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="tg-row">
                                                    <div class="tg-col">
                                                        <div class="tg-daybox">
                                                            <h3>Domingo</h3>
                                                            <span><a class="tg-btntext" href="javascript:void(0);" data-day="Domingo">Agregar</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-col">
                                                        <div id="slotsDomingo" class="tg-slots">
                                                            @foreach(auth()->user()->schedules('Domingo') as $schedule)
                                                                <div class="tg-slotsbox">
                                                                <span class="tg-radiotimeslot">
                                                                    <span>
                                                                        <em>{{ $schedule->complete }}</em>
                                                                        <a href="javascript:void(0);" data-delete="{{ url('schedule/'.$schedule->id.'/delete') }}" class="fa fa-close tg-btndelete"></a>
                                                                    </span>
                                                                </span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {{--<div class="tg-col">--}}
                                                    {{--<div class="tg-slots">--}}
                                                    {{--<div class="tg-description"><p>No Time Slot</p></div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                            <template id="templateSchedule">
                                                <div class="tg-slotsbox">
                                                    <span class="tg-radiotimeslot">
                                                        <span>
                                                            <em></em>
                                                            <a href="javascript:void(0);" data-delete="" class="fa fa-close tg-btndelete"></a>
                                                        </span>
                                                    </span>
                                                </div>
                                            </template>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <!--Theme Modal Box Start-->
    <div class="modal fade tg-socialModal" id="scheduleModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2 id="titleScheduleModal"></h2>
                </div>
                <div id="alert-category"></div>
                <div class="tg-modalbody">
                    <div id="formSchedule" class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <input type="hidden" id="hiddenDay" value="">
                        <div id="alertSchedule"></div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Hora inicio</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-clock"></i>
                                        <input type="time" id="startSchedule" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Hora fin</label>
                                    <div class="tg-inpuicon">
                                        <i class="lnr lnr-clock"></i>
                                        <input type="time" id="endSchedule" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn" id="btnAddSchedule" data-add="">Agregar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-day]', function () {
            var day = $(this).data('day');
            $('#hiddenDay').val(day);
            $('#titleScheduleModal').replaceWith('<h2 id="titleScheduleModal">Agregar horario - '+day+'</h2>');
            $('#btnAddSchedule').replaceWith('<button class="tg-btn" id="btnAddSchedule" data-add="/schedule/add">Agregar</button>');
            $('#scheduleModal').modal('show');
        });

        $(document).on('click', '[data-add]', function () {
            var urlSchedule = $(this).data('add');
            var day = $('#hiddenDay');
            var startSchedule = $('#startSchedule');
            var endSchedule = $('#endSchedule');
            var $formSchedule = $('#formSchedule');
            var scheduleModal = $('#scheduleModal');

            $.ajax({
                url: urlSchedule,
                type: 'POST',
                data:{
                    '_token':$formSchedule.find('[name=_token]').val(),
                    'start_time':startSchedule.val(),
                    'end_time':endSchedule.val(),
                    'day':day.val()
                },
                success:function (data){
                    var uploadedSchedule = $('#templateSchedule').html();
                    uploadedSchedule = uploadedSchedule.replace('<em></em>', '<em>'+data.complete+'</em>');
                    uploadedSchedule = uploadedSchedule.replace('data-delete=""', 'data-delete="/schedule/'+data.id+'/delete"');
                    $('#slots'+data.day).append(uploadedSchedule);

                    startSchedule.val("");
                    endSchedule.val("");
                    $('.alert-warning').remove();
                    scheduleModal.modal('hide');
                },
                error:function (data) {
                    $('.alert-warning').remove();
                    const $data = data.responseJSON;
                    const $alert = $('#alertSchedule');

                    if ($data.start_time)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.start_time +'</strong></span>' +
                            '</div>');
                    if ($data.end_time)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.end_time +'</strong></span>' +
                            '</div>');
                }
            });
        });

        $(document).on('click', '[data-delete]', function () {
            // request al servidor
            const urldeleteSchedule = $(this).data('delete');
            const $div = $(this).closest('.tg-slotsbox');
            $.ajax({
                type: 'GET',
                url: urldeleteSchedule,

                success:function (){
                    $div.remove();
                }
            });
        });
    </script>
@endsection
