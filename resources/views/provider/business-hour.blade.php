@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Horario de trabajo</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Horario de trabajo</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboarbusinesshours">
                                <div class="tg-themeform">
                                    <fieldset>
                                        {{ csrf_field() }}
                                        <div class="tg-dashboardbox tg-businesshours">
                                            <div class="tg-dashboardtitle">
                                                <h2>Horario de trabajo</h2>
                                            </div>
                                            {{--Emergency--}}
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Atención de emergencias las 24 horas</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="Emergencia" value="Emergencia" @if($user->attendEmergency($user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="Emergencia">Atiendo emergencias</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Lunes</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkLunes" value="Lunes" @if($user->attend('Lunes', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkLunes">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourLunes"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeLunes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeLunes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnLunes" class="tg-addtimeslot" data-day="Lunes" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursLunes">
                                                    @foreach($user->hours('Lunes', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Martes</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkMartes" value="Martes" @if($user->attend('Martes', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkMartes">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourMartes"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeMartes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeMartes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnMartes" class="tg-addtimeslot" data-day="Martes" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursMartes">
                                                    @foreach($user->hours('Martes', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Miércoles</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkMiercoles" value="Miercoles" @if($user->attend('Miercoles', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkMiercoles">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourMiercoles"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeMiercoles" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeMiercoles" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnMiercoles" class="tg-addtimeslot" data-day="Miercoles" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursMiercoles">
                                                    @foreach($user->hours('Miercoles', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Jueves</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkJueves" value="Jueves" @if($user->attend('Jueves', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkJueves">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourJueves"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeJueves" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeJueves" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnJueves" class="tg-addtimeslot" data-day="Jueves" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursJueves">
                                                    @foreach($user->hours('Jueves', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Viernes</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkViernes" value="Viernes" @if($user->attend('Viernes', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkViernes">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourViernes"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeViernes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeViernes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnViernes" class="tg-addtimeslot" data-day="Viernes" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursViernes">
                                                    @foreach($user->hours('Viernes', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Sábado</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkSabado" value="Sabado" @if($user->attend('Sabado', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkSabado">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourSabado"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeSabado" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeSabado" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnSabado" class="tg-addtimeslot" data-day="Sabado" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursSabado">
                                                    @foreach($user->hours('Sabado', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="tg-businesshourssbox">
                                                <div class="form-group">
                                                    <div class="tg-daychckebox">
                                                        <h3>Domingo</h3>
                                                        <div class="tg-checkbox">
                                                            <input type="checkbox" id="checkDomingo" value="Domingo" @if($user->attend('Domingo', $user->id)) checked @endif onchange="save(this.value, this.checked);">
                                                            <label for="checkDomingo">No atiendo este día</label>
                                                        </div>
                                                    </div>
                                                    <div id="alertHourDomingo"></div>
                                                </div>
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <label for="">Hora inicio:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="startTimeDomingo" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Hora fin:</label>
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="endTimeDomingo" class="form-control">
                                                        </div>
                                                    </div>
                                                    <button type="button" id="btnDomingo" class="tg-addtimeslot" data-day="Domingo" data-add-hour="{{ url('hour/add') }}" style="top: 29px;">+</button>
                                                </div>
                                                <div id="hoursDomingo">
                                                    @foreach($user->hours('Domingo', $user->id) as $hour)
                                                        <div class="tg-startendtime">
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->start }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="tg-inpuicon">
                                                                    <i class="lnr lnr-clock"></i>
                                                                    <input type="time" step="1800" class="form-control" disabled value="{{ $hour->end }}">
                                                                </div>
                                                            </div>
                                                            <button type="button" data-delete="{{ url('hour/'.$hour->id.'/delete') }}" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <template id="templateTime">
                                                <div class="tg-startendtime">
                                                    <div class="form-group">
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="start" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="tg-inpuicon">
                                                            <i class="lnr lnr-clock"></i>
                                                            <input type="time" step="1800" id="end" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                    <button type="button" data-delete="" class="tg-addtimeslot tg-deleteslot"><i class="lnr lnr-trash"></i></button>
                                                </div>
                                            </template>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-add-hour]', function () {

            const urlHour = $(this).data('add-hour');
            const day = $(this).data('day');
            const startTime = $('#startTime'+day);
            const endTime = $('#endTime'+day);
            var hourDay = $('#hours'+day);

            $.ajax({
                url: urlHour,
                type: 'POST',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'start_time':startTime.val(),
                    'end_time':endTime.val(),
                    'day':day
                },
                success:function (data){
                    var uploadedTime = $('#templateTime').html();
                    uploadedTime = uploadedTime.replace('<input type="time" step="1800" id="start" class="form-control" value="">', '<input type="time" step="1800" id="start" class="form-control" value="'+data.start+'" disabled>');
                    uploadedTime = uploadedTime.replace('<input type="time" step="1800" id="end" class="form-control" value="">', '<input type="time" step="1800" id="end" class="form-control" value="'+data.end+'" disabled>');
                    uploadedTime = uploadedTime.replace('data-delete=""', 'data-delete="/hour/'+data.id+'/delete"');
                    hourDay.append(uploadedTime);

                    startTime.val("");
                    endTime.val("");
                    $('.alert-warning').remove();
                },
                error:function (data) {
                    $('.alert-warning').remove();
                    const $data = data.responseJSON;
                    const $alert = $('#alertHour'+day);

                    if ($data.start_time)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.start_time +'</strong></span>' +
                            '</div>');
                    if ($data.end_time)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.end_time +'</strong></span>' +
                            '</div>');
                }
            });
        });

        $(document).on('click', '[data-delete]', function () {
            // request al servidor
            const urldeleteHour = $(this).data('delete');
            const $div = $(this).closest('.tg-startendtime');
            $.ajax({
                type: 'GET',
                url: urldeleteHour,

                success:function (){
                    $div.remove();
                }
            });
        });
        function save(value, checked) {
            $.ajax({
                type: 'POST',
                url: '/hour/attend',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'day':value,
                    'checked': checked
                },
                success:function (data){
                    // if(data.status === 1)
                    //     $('#btn'+data.day).prop('disabled', true);
                    // else
                    //     $('#btn'+data.day).prop('disabled', false);
                },
                error:function () {
                }
            });
        }
    </script>
@endsection
