@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Propuesta</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="{{ url('/proposal-sent') }}">Propuestas enviadas</a></li>
                        <li class="tg-active">Propuesta</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-haslayout" style="padding: 40px 0">
        <div class="tg-detailpage tg-jobdetail">
            <div class="tg-detailpagehead">
                <div class="container">
                    <div class="tg-bordertitle">
                        <a class="tg-btnprint" href="javascript:void(0);"><i class="lnr lnr-printer"></i></a>
                        <a class="tg-btn tg-btnedite" href="javascript:void(0);" style="margin-right: 10px"><i class="lnr lnr-envelope"></i></a>
                        <h2 style="font-style: bold">Has enviado la siguiente propuesta a:</h2>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="tg-detailpageheadcontent tg-border-bottom" style="margin: 40px 0; border-top: 1px solid #5dc560">
                                <div class="tg-companylogo"><img src="{{ $proposal->to->photo_selected }}" alt="image description"></div>
                                <div class="tg-companycontent">
                                    <div class="tg-title" style="margin-bottom: 10px">
                                        <h3>{{ $proposal->to->name }}</h3>
                                    </div>
                                    <ul class="tg-matadata">
                                        <li><span class="tg-stars"><span></span></span></li>
                                        <li style="color: #999">
                                            <i class="fa fa-thumbs-o-up"></i>
                                            <em>99% (1009 votes)</em>
                                        </li>
                                    </ul>
                                    <span class="tg-jobpostedby" style="margin: 5px 0;color: black">{{ $catSubcat }}</span>
                                    <span class="tg-jobpostedby" style="margin: 5px 0;color: black">{{ $location }}</span>
                                    <span class="tg-jobpostedby" style="margin: 5px 0;color: black">Enviado: {{ $proposal->send_date }}</span>
                                </div>
                                <a class="tg-btn" href="{{ url('/chat') }}">Iniciar chat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tg-twocolumns" class="tg-twocolumns">
                <div class="container">
                    <div id="tg-content" class="tg-content">
                        <div class="tg-companyfeatures">
                            <div class="tg-companyfeaturebox tg-introduction">
                                <div class="tg-companyfeaturetitle">
                                    <h3 class="text-center" style="font-size: 23px; margin-bottom: 20px">Propuesta proforma</h3>
                                    <h3 style="font-style: bold; margin-bottom: 10px">Título del trabajo a presupuestar:</h3>
                                    <h4 style="color: gray">{{ $proposal->offer->title }}</h4>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" disabled>{{ $proposal->offer->description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                        <span class="col-sm-8" style="margin-top:10px">Movilidad geográfica</span>
                                        <div class="form-horizontal col-sm-4">
                                            <input type="text" class="form-control" value="{{ $proposal->geographical_mobility == 1 ? 'Sí' : 'No' }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                        <span class="col-sm-8" style="margin-top:10px">Materiales necesarios incluidos</span>
                                        <div class="form-horizontal col-sm-4">
                                            <input type="text" class="form-control" value="{{ $proposal->necessary_material == 1 ? 'Sí' : 'No' }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                        <span class="col-sm-8" style="margin-top:10px">Garantía mínima de 30 dias</span>
                                        <div class="form-horizontal col-sm-4">
                                            <input type="text" class="form-control" value="{{ $proposal->minimum_guarantee == 1 ? 'Sí' : 'No' }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                        <span class="col-sm-8" style="margin-top:10px">Requiere adelanto (%)</span>
                                        <div class="form-horizontal col-sm-4">
                                            <input type="text" class="form-control" value="{{ $proposal->advancement }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                        <span class="col-sm-8" style="margin-top:10px">Cuento con seguro EPS/SCTR</span>
                                        <div class="form-horizontal col-sm-4">
                                            <input type="text" class="form-control" value="{{ $proposal->insurance == 1 ? 'Sí' : 'No' }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 20px">
                                        <span class="col-sm-8" style="margin-top:10px">Precio final estimado S/.</span>
                                        <div class="form-horizontal col-sm-4">
                                            <input type="text" class="form-control" value="{{ $proposal->final_price }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tg-dashboardbox tg-introduction">
                                <div class="tg-dashboardtitle">
                                    <h2>Detalles y desarrollo del servicio</h2>
                                </div>
                                <div class="tg-introductionbox">
                                    <div class="form-group">
                                        <textarea class="form-control" name="presentation" disabled>{{ $proposal->service_detail }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="tg-checkbox">
                                            <input type="checkbox" id="terms" {{ $proposal->personal_interview == 1 ? 'checked' : '' }} disabled>
                                            <label for="terms">Propongo una entrevista personal para una correcta valoración del trabajo demandado
                                                y la elaboración de un presupuesto final.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-dashboardtitle">
                                    <h2>Estado</h2>
                                </div>
                                <div class="tg-introductionbox">
                                    <div class="form-group">
                                        @if($proposal->status == 'Enviada')
                                            <span style="color: #42a5f5;">Propuesta en revisión: </span><span>{{ $proposal->time_elapsed }}</span>
                                        @endif
                                        @if($proposal->status == 'Rechazada')
                                            <span style="color: #ef5350;">Propuesta Rechazada: </span><span> No califica</span><br>
                                        @endif
                                        @if($proposal->status == 'Aceptada')
                                            <span style="color:#5dc560;">Propuesta Aceptada: </span><span>En desarrollo</span>
                                        @endif
                                    </div>
                                </div>
                                {{--<div class="form-group text-center">--}}
                                    {{--<button data-updatepresentation="" class="tg-btn-danger">Eliminar</button>--}}
                                    {{--<button data-updatepresentation="" class="tg-btn">Reenviar</button>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
