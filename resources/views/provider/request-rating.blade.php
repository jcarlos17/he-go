@extends('layouts.app')

@section('styles')
    <style>
        .tg-servicetitle {
            padding: 0;
        }
    </style>
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Solicitar calificación</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Solicitar calificación</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprofilesetting">
                                <div class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-basicinformation">
                                            <div class="tg-dashboardtitle">
                                                <h2 style="width: 100%;margin-bottom: 10px">Consigue valoraciones de una forma sencilla</h2>
                                                <p style="margin-bottom: 5px">Dispones de dos formas para solicitar comentarios a tus clientes:</p>
                                                <ul>
                                                    <li>Escribe el nombre y el email de tu cliente. Le enviaremos un correo electrónico</li>
                                                    <li>Selecciona a tu cliente de la lista de contactos. esta es la forma más fácil y directa</li>
                                                </ul>
                                            </div>
                                            <h6>Datos del cliente / usuario:</h6>
                                            <div id="alert-sent"></div>
                                            <div class="tg-basicinformationbox">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombres">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-4">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-4">
                                                        <div class="form-group">
                                                            <a href="#!" data-toggle="modal" data-target="#imageDocument" class="pull-right">Buscar en mi lista de contactos</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="form-group">
                                                            <button id="btnSent" data-sent="{{ url('request-rating') }}" class="tg-btn pull-right">Solicitar calificación</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tg-dashboardtitle" style="border-top: 1px #919191 solid; margin-top: 50px">
                                                <p style="margin: 20px 0 10px 0">Puede solicitar una valoración cada vez que:</p>
                                                <ul>
                                                    <li>Cada vez que finalices un trabajo o servicio</li>
                                                    <li>Cuando redactas un presupuesto informativo</li>
                                                    <li>Cuando orientas y aconsejas a través del chat u otros canales</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->

    <div class="modal fade tg-socialModal" id="imageDocument" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document" style="width: 850px">
            <div class="modal-content tg-modalcontent">
                <div class="tg-themeform tg-formrefinesearch">
                    <div class="tg-modalbody text-center">
                        <table class="tg-tablejoblidting">
                            <thead>
                                <tr>
                                    <td colspan="5" style="background-color: #bcbdc0; color: white; padding: 0">
                                        Mis contactos
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="tg-radio" style="padding-bottom: 5px">
                                            <input class="services" type="radio" id="1" name="type" value="Presupuesto">
                                            <label for="1"> </label>
                                        </div>
                                    </td>
                                    <td class="col-md-5">
                                        <div class="tg-servicetitle">
                                            <figure style="width: 40px;float: left;margin: 0 10px 0 0;border-radius: 50%;">
                                                <img src="{{ asset('images/photos/default.png') }}" alt="image description">
                                            </figure>
                                            <div class="tg-clientcontent">
                                                <h2>Carls Puigdemon</h2>
                                                <ul class="tg-matadata">
                                                    <li><span class="tg-stars"><span></span></span></li>
                                                    <li>
                                                        <i class="fa fa-thumbs-o-up"></i>
                                                        <em>99% (1009 votes)</em>
                                                    </li>
                                                </ul>
                                                <span>Guardado: 28/09/2019</span>
                                                <span>Distrito / Provincia / Departamento</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-md-3">
                                        <span>Publicaciones</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-suitcase"></i>
                                    </td>
                                    <td class="col-md-2">
                                        <span>Presupuestos</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-calculator"></i>
                                    </td>
                                    <td class="col-md-2">
                                        <span>C. Presentación</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-edit"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tg-radio" style="padding-bottom: 5px">
                                            <input class="services" type="radio" id="2" name="type" value="Presupuesto">
                                            <label for="2"> </label>
                                        </div>
                                    </td>
                                    <td class="col-md-5">
                                        <div class="tg-servicetitle">
                                            <figure style="width: 40px;float: left;margin: 0 10px 0 0;border-radius: 50%;">
                                                <img src="{{ asset('images/photos/default.png') }}" alt="image description">
                                            </figure>
                                            <div class="tg-clientcontent">
                                                <h2>Carls Puigdemon</h2>
                                                <ul class="tg-matadata">
                                                    <li><span class="tg-stars"><span></span></span></li>
                                                    <li>
                                                        <i class="fa fa-thumbs-o-up"></i>
                                                        <em>99% (1009 votes)</em>
                                                    </li>
                                                </ul>
                                                <span>Guardado: 28/09/2019</span>
                                                <span>Distrito / Provincia / Departamento</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-md-3">
                                        <span>Publicaciones</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-suitcase"></i>
                                    </td>
                                    <td class="col-md-2">
                                        <span>Presupuestos</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-calculator"></i>
                                    </td>
                                    <td class="col-md-2">
                                        <span>C. Presentación</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-edit"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="tg-radio" style="padding-bottom: 5px">
                                            <input class="services" type="radio" id="3" name="type" value="Presupuesto">
                                            <label for="3"> </label>
                                        </div>
                                    </td>
                                    <td class="col-md-5">
                                        <div class="tg-servicetitle">
                                            <figure style="width: 40px;float: left;margin: 0 10px 0 0;border-radius: 50%;">
                                                <img src="{{ asset('images/photos/default.png') }}" alt="image description">
                                            </figure>
                                            <div class="tg-clientcontent">
                                                <h2>Carls Puigdemon</h2>
                                                <ul class="tg-matadata">
                                                    <li><span class="tg-stars"><span></span></span></li>
                                                    <li>
                                                        <i class="fa fa-thumbs-o-up"></i>
                                                        <em>99% (1009 votes)</em>
                                                    </li>
                                                </ul>
                                                <span>Guardado: 28/09/2019</span>
                                                <span>Distrito / Provincia / Departamento</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-md-3">
                                        <span>Publicaciones</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-suitcase"></i>
                                    </td>
                                    <td class="col-md-2">
                                        <span>Presupuestos</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-calculator"></i>
                                    </td>
                                    <td class="col-md-2">
                                        <span>C. Presentación</span><br>
                                        00 <br>
                                        <i class="fa fa-2x fa-edit"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group text-left" style="margin-top: 20px">
                            <button data-add="#" class="tg-btn">Añadir contacto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-sent]', function () {
            let urlOpinion = $(this).data('sent');
            let name = $('#name');
            let email = $('#email');
            $('#btnSent').prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: urlOpinion,
                data:{
                    '_token':$('input[name=_token]').val(),
                    'name':name.val(),
                    'email':email.val(),
                },
                success:function (){
                    $('.alert-success').remove();
                    $('.alert-warning').remove();
                    let $alert = $('#alert-sent');
                    $alert.append('<div class="alert alert-success tg-alertmessage fade in">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-star"></i>' +
                        '<span><strong>Se envió correctamente su invitación</strong></span>' +
                        '</div>');
                    name.val("");
                    email.val("");

                    $('#btnSent').prop('disabled', false);
                },
                error:function (data) {
                    $('.alert-success').remove();
                    $('.alert-warning').remove();
                    let $data = data.responseJSON;

                    let $alert = $('#alert-sent');

                    if ($data.name)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.name +'</strong></span>' +
                            '</div>');

                    if ($data.email)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.email +'</strong></span>' +
                            '</div>');

                    $('#btnSent').prop('disabled', false);
                }
            });
        });
    </script>
@endsection
