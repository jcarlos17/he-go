@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Configuración de privacidad</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Configuración de privacidad</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprivacysettings">
                                <form class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-privacysettings">
                                            <div class="tg-dashboardtitle">
                                                <h2>Configuración de privacidad</h2>
                                            </div>
                                            <div class="tg-privacysettingsbox">
                                                @foreach ($settings as $setting)
                                                    <div class="tg-privacysetting">
                                                        <span>{{ $setting->name }}</span>
                                                        <div class="tg-iosstylcheckbox">
                                                            <input type="checkbox" id="{{ $setting->id }}" value="{{ $setting->id }}" onchange="save(this.value, this.checked);" {{ $setting->privacy_setting_exists ? 'checked' : '' }}>
                                                            <label for="{{ $setting->id }}" data-enable="Activo" data-disable="Inactivo"></label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        function save(value, checked) {
            $.ajax({
                type: 'POST',
                url: '/privacy-settings',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'option':value,
                    'checked': checked
                },
                success:function (){
                },
                error:function () {
                }
            });
        }
    </script>
@endsection
