@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Oferta de trabajo</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Oferta de trabajo</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Oferta de trabajo</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <main id="tg-main" class="tg-main tg-haslayout" style="padding: 90px 0">
        <div class="tg-detailpage tg-jobdetail">
            <div class="tg-detailpagehead">
                <div class="container">
                    <div class="row">
                        <div id="alert-sent-proposal"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="tg-detailpageheadcontent">
                                <div class="tg-companylogo"><a href="{{ url('perfil-publico/'.$offerJob->user_id) }}"><img src="{{ $offerJob->user->photo_selected }}" alt="image description"></a></div>
                                <div class="tg-companycontent">
                                    <ul class="tg-tags">
                                        <li><a class="tg-tag" href="#">{{ $offerJob->job_type }}</a></li>
                                    </ul>
                                    <div class="tg-title">
                                        <h1>{{ $offerJob->title }}</h1>
                                        <span class="tg-jobpostedby"><a href="{{ url('/perfil-publico/'.$offerJob->user_id) }}">{{ $offerJob->user->name }}</a></span>
                                    </div>
                                </div>
                                <a class="tg-btn-info" href="{{ url('chat') }}">Iniciar chat</a>
                                @if($offerJob->status != 5)
                                    @if($proposalExists)
                                        <a class="tg-btn" href="javascript:void(0);">{{ $offerJob->type_description }}</a>
                                    @else
                                        <a class="tg-btn" href="javascript:void(0);" data-toggle="modal" data-target="#showProposal">{{ $offerJob->type_description }}</a>
                                    @endif
                                @endif
                                {{--<a class="tg-btn" href="{{ url('/proposal') }}">Enviar propuesta</a>--}}
                            </div>
                            <ul class="tg-jobmatadata">
                                <li>
                                    <div class="tg-box tg-posteddate">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-calendar-full"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Fecha publicación</strong>
                                            <span>{{ $offerJob->publication_date }}</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="tg-box tg-applicantapplied">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-book"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Solicitantes</strong>
                                            <span>3479  solicitantes</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="tg-box tg-views">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-eye"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Vistas</strong>
                                            <span>Total vistas: 22508</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="tg-box tg-expiredate">
                                        <span class="tg-jobmataicon"><i class="lnr lnr-warning"></i></span>
                                        <div class="tg-jobmatacontent">
                                            <strong>Fecha de cierre</strong>
                                            <span>{{ $offerJob->expire_date_format }}</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tg-twocolumns" class="tg-twocolumns">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 pull-left">
                            <div id="tg-content" class="tg-content">
                                <div class="tg-companyfeatures">
                                    <div class="tg-companyfeaturebox tg-introduction">
                                        <div class="tg-companyfeaturetitle">
                                            <h3>Descripción de la Solicitud</h3>
                                        </div>
                                        <div class="tg-description">
                                            <p>{{ $offerJob->description }}</p>
                                        </div>
                                    </div>
                                    <div class="tg-companyfeaturebox tg-jobdetails">
                                        <div class="tg-companyfeaturetitle">
                                            <h3>Detalles del trabajo</h3>
                                        </div>
                                        <ul>
                                            <li>
                                                <span>Categoría profesional:</span>
                                                <span>{{ $offerJob->category->name }}</span>
                                            </li>
                                            <li>
                                                <span>Profesional en:</span>
                                                <span>{{ $offerJob->subcategory->name }}</span>
                                            </li>
                                            <li>
                                                <span>Experiencia:</span>
                                                <span>{{ $offerJob->experience }}</span>
                                            </li>
                                            <li>
                                                <span>Pago sugerido:</span>
                                                <span>{{ $offerJob->suggested_payment }}</span>
                                            </li>
                                            <li>
                                                <span>Tipo de proridad:</span>
                                                <span>{{ $offerJob->priority_type }}</span>
                                            </li>
                                            <li>
                                                <span>Tipo de empleo:</span>
                                                <span>{{ $offerJob->job_type }}</span>
                                            </li>
                                            <li>
                                                <span>Horario preferible:</span>
                                                <span>{{ $offerJob->schedule }}</span>
                                            </li>
                                            <li>
                                                <span>Movilidad geográfica:</span>
                                                <span>{{ $offerJob->geographical_mobility }}</span>
                                            </li>
                                            <li>
                                                <span>Idioma preferible:</span>
                                                <span>{{ $offerJob->language }}</span>
                                            </li>
                                            <li>
                                                <span>Departamento:</span>
                                                <span>{{ $offerJob->department->name }}</span>
                                            </li>
                                            <li>
                                                <span>Provincia:</span>
                                                <span>{{ $offerJob->province->name }}</span>
                                            </li>
                                            @if($offerJob->district_id)
                                                <li>
                                                    <span>Distrito:</span>
                                                    <span>{{ $offerJob->district->name }}</span>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="tg-companyfeaturebox tg-jobrequirments">
                                        <div class="tg-companyfeaturetitle">
                                            <h3>Requisitos del prestador de servicios</h3>
                                        </div>
                                        <div class="tg-description">
                                            <p>{{ $offerJob->abilities }}</p>
                                        </div>
                                        {{--<ul class="tg-themeliststyle tg-themeliststyletick">--}}
                                            {{--<li>Minimum 200hr Yoga Certification</li>--}}
                                            {{--<li>1+ years experience teaching regular group classes</li>--}}
                                            {{--<li>Current CPR</li>--}}
                                            {{--<li>Agree to 6 month minimum commitment</li>--}}
                                            {{--<li>Ability to approach challenges in a creative, positive and team-centered manner</li>--}}
                                            {{--<li>Strong communication and interpersonal skills</li>--}}
                                        {{--</ul>--}}
                                    </div>
                                    @if($offerJob->benefits->count() > 0)
                                        <div class="tg-companyfeaturebox tg-benefitsallowances">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Beneficios que se incluyen</h3>
                                            </div>
                                            <ul class="tg-themeliststyle tg-themeliststylecircletick">
                                                @foreach($offerJob->benefits as $key => $benefit)
                                                    <li>{{ $benefit->name }}</li>
                                                    @if(($key+1)%3 == 0)
                                                        <div class="clearfix"></div>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if($offerJob->images->count() > 0)
                                        <div class="tg-companyfeaturebox tg-gallery">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Imágenes</h3>
                                            </div>
                                            <ul>
                                                @foreach($offerJob->images as $image)
                                                    <li>
                                                        <div class="tg-galleryimgbox">
                                                            <figure>
                                                                <img src="{{ $image->image_url }}" alt="image description">
                                                                {{--<a class="tg-btngallery" href="{{ $gallery->image_url }}" data-rel="prettyPhoto[gallery]"><i class="lnr lnr-magnifier"></i></a>--}}
                                                            </figure>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pull-right">
                            <aside id="tg-sidebar" class="tg-sidebar">
                                <div class="tg-widget tg-widgetlocationandcontactinfo">
                                    {{--@include('job-offer.status', ['offerJob' => $offerJob, 'detail' => 1])--}}
                                    <div class="tg-mapbox">
                                        <div id="tg-joblocationmap" class="tg-locationmap"></div>
                                    </div>
                                    <div class="tg-contactinfobox">
                                        <ul class="tg-contactinfo">
                                            @foreach($offerJob->user->addresses as $address)
                                                <li>
                                                    <address>{{ $address->description_complete }}</address>
                                                </li>
                                            @endforeach
                                            <li>
                                                <i class="lnr lnr-phone-handset"></i>
                                                <span>{{ $offerJob->user->mobile }}</span>
                                            </li>
                                            <li>
                                                <i class="lnr lnr-phone"></i>
                                                <span>{{ $offerJob->user->phone }}</span>
                                            </li>
                                            <li>
                                                <i class="lnr lnr-apartment"></i>
                                                <span>{{ $offerJob->user->company_name }}</span>
                                            </li>
                                            <li>
                                                <i class="lnr lnr-envelope"></i>
                                                <span><a href="mailto:{{ $offerJob->user->email }}">{{ $offerJob->user->email }}</a></span>
                                            </li>
                                            @if($offerJob->user->website)
                                                <li>
                                                    <i class="lnr lnr-laptop"></i>
                                                    <span><a href="{{ $offerJob->user->website }}" target="_blank">{{ $offerJob->user->website }}</a></span>
                                                </li>
                                            @endif
                                        </ul>
                                        <ul class="tg-socialicons">
                                            <li class="tg-facebook"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->facebooklink : '' }}"><i class="fa fa-facebook"></i></a></li>
                                            <li class="tg-twitter"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->twitterlink : '' }}"><i class="fa fa-twitter"></i></a></li>
                                            <li class="tg-linkedin"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->linkedinlink : '' }}"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="tg-skype"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->skypelink : '' }}"><i class="fa fa-skype"></i></a></li>
                                            <li class="tg-googleplus"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->googlepluslink : '' }}"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="tg-pinterestp"><a href="{{ $offerJob->user->socialnetwork ? $offerJob->user->socialnetwork->pinterestlink : '' }}"><i class="fa fa-pinterest-p"></i></a></li>
                                        </ul>
                                        {{--<a class="tg-btn tg-btn-lg" href="javascript:void(0);">get direction</a>--}}
                                    </div>
                                </div>
                                <div class="tg-widget tg-widgetshare">
                                    <div class="tg-widgettitle">
                                        <h3>Comparte este trabajo</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul class="tg-socialicons">
                                            <li class="tg-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="tg-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="tg-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="tg-googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="tg-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                                            <li class="tg-vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                            <li class="tg-tumblr"><a href="#"><i class="fa fa-tumblr"></i></a></li>
                                            <li class="tg-yahoo"><a href="#"><i class="fa fa-yahoo"></i></a></li>
                                            <li class="tg-yelp"><a href="#"><i class="fa fa-yelp"></i></a></li>
                                            <li class="tg-pinterestp"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li class="tg-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                            <li class="tg-stumbleupon"><a href="#"><i class="fa fa-stumbleupon"></i></a></li>
                                            <li class="tg-reddit"><a href="#"><i class="fa fa-reddit"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tg-widget tg-widgetrelatedjobs">
                                    <div class="tg-widgettitle">
                                        <h3>Trabajos relacionados</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul>
                                            <li>
                                                <div class="tg-serviceprovidercontent">
                                                    <div class="tg-companylogo"><a href="#"><img src="images/logos/img-01.png" alt="image description"></a></div>
                                                    <div class="tg-companycontent">
                                                        <a class="tg-tag tg-tagjobtype" href="#">fix</a>
                                                        <div class="tg-title">
                                                            <h3><a href="#">Carpenter Required</a></h3>
                                                        </div>
                                                        <span class="tg-jobpostedby">By: <a href="javascript:void(0);">Blue Bird Organization</a></span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="tg-serviceprovidercontent">
                                                    <div class="tg-companylogo"><a href="#"><img src="images/logos/img-02.png" alt="image description"></a></div>
                                                    <div class="tg-companycontent">
                                                        <a class="tg-tag tg-tagjobtype" href="#">Internship</a>
                                                        <div class="tg-title">
                                                            <h3><a href="#">Electrician For Fixing Required</a></h3>
                                                        </div>
                                                        <span class="tg-jobpostedby">By: <a href="javascript:void(0);">On Fleek Spa &amp; Beauty Saloon</a></span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="tg-serviceprovidercontent">
                                                    <div class="tg-companylogo"><a href="#"><img src="images/logos/img-03.png" alt="image description"></a></div>
                                                    <div class="tg-companycontent">
                                                        <a class="tg-tag tg-tagjobtype" href="#">Temporary</a>
                                                        <div class="tg-title">
                                                            <h3><a href="#">Car Fixing Mechanic Required</a></h3>
                                                        </div>
                                                        <span class="tg-jobpostedby">By: <a href="javascript:void(0);">Eldridge Hemenway</a></span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal fade" id="showProposal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>{{ $offerJob->type_description }}</h2>
                </div>
                <div class="tg-modalbody">
                    <div class="tg-detailpage tg-jobdetail">
                        <div class="tg-detailpagehead">
                            <p style="margin-bottom: 20px;color: black">
                                Este paso es muy importante y hara que destaques de otras posibles ofertas que reciba tu futuro cliente, es por ello que debes de tomarte
                                unos minutos preparando una propuesta lo más formal y completa posible ya que mientras mas información incluyas más oportunidad tienes
                                de conseguir este trabajo. Aquí tienes todas las herramientas,
                            </p>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="tg-detailpageheadcontent tg-border-bottom" style="margin: 40px 0; border-top: 1px solid #5dc560">
                                        <div class="tg-companylogo"><img src="{{ $offerJob->user->photo_selected }}" alt="image description"></div>
                                        <div class="tg-companycontent">
                                            <div class="tg-title" style="margin-bottom: 10px">
                                                <h3>{{ $offerJob->user->name }}</h3>
                                            </div>
                                            <ul class="tg-matadata">
                                                <li><span class="tg-stars"><span></span></span></li>
                                                <li style="color: #999">
                                                    <i class="fa fa-thumbs-o-up"></i>
                                                    <em>99% (1009 votes)</em>
                                                </li>
                                            </ul>
                                            <span class="tg-jobpostedby" style="margin: 5px 0;color: black">{{ $catSubcat }}</span>
                                            <span class="tg-jobpostedby" style="margin: 5px 0;color: black">{{ $location }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tg-twocolumns" class="tg-twocolumns">
                            <div class="">
                                <div id="tg-content" class="tg-content">
                                    <div class="tg-companyfeatures">
                                        <div class="tg-companyfeaturebox tg-introduction">
                                            <div class="tg-companyfeaturetitle">
                                                <h3>Propuesta proforma</h3>
                                            </div>
                                            <div id="alert-proposal"></div>
                                            <div class="row">
                                                {{ csrf_field() }}
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: 20px 0">
                                                    <span class="col-sm-8">Movilidad geográfica</span>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="">
                                                            <div class="tg-radio">
                                                                <input type="radio" id="GM_Si" name="geographical_mobility" value="1">
                                                                <label for="GM_Si">Sí</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="">
                                                            <div class="tg-radio">
                                                                <input type="radio" id="GM_No" name="geographical_mobility" value="0" checked>
                                                                <label for="GM_No">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: 20px 0">
                                                    <span class="col-sm-8">Materiales necesarios incluidos</span>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="">
                                                            <div class="tg-radio">
                                                                <input type="radio" id="NM_Si" name="necessary_material" value="1">
                                                                <label for="NM_Si">Sí</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="">
                                                            <div class="tg-radio">
                                                                <input type="radio" id="NM_No" name="necessary_material" value="0" checked>
                                                                <label for="NM_No">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: 20px 0">
                                                    <span class="col-sm-8" style="margin-top:10px">Garantía mínima de 30 dias</span>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="">
                                                            <div class="tg-radio">
                                                                <input type="radio" id="MG_Si" name="minimum_guarantee" value="1">
                                                                <label for="MG_Si">Sí</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="">
                                                            <div class="tg-radio">
                                                                <input type="radio" id="MG_No" name="minimum_guarantee" value="0" checked>
                                                                <label for="MG_No">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: 20px 0">
                                                    <span class="col-sm-8" style="margin-top:10px">Requiere adelanto (%)</span>
                                                    <div class="form-horizontal col-sm-4">
                                                        <input type="text" class="form-control" name="advancement" placeholder="10, 20, 50,...">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: 20px 0">
                                                    <span class="col-sm-8" style="margin-top:10px">Cuento con seguro EPS/SCTR</span>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="tg-radio">
                                                            <input type="radio" id="I_Si" name="insurance" value="1">
                                                            <label for="I_Si">Sí</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-horizontal col-sm-2">
                                                        <div class="tg-radio">
                                                            <input type="radio" id="I_No" name="insurance" value="0" checked>
                                                            <label for="I_No">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: 20px 0">
                                                    <span class="col-sm-8">Precio final estimado S/.</span>
                                                    <div class="form-horizontal col-sm-4">
                                                        <input type="number" class="form-control" name="final_price" placeholder="1,000.00" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-introduction">
                                            <div class="tg-dashboardtitle">
                                                <h2>Detalles y desarrollo del servicio</h2>
                                            </div>
                                            <div class="tg-introductionbox">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="service_detail" placeholder="Se claro al expresar tus ideas, el tiempo que tardará el trabajo, el tipo de material que usarás, y sobre todo, no uses muchas frases técnicas que el cliente podría no entender o confundirse."></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="tg-checkbox">
                                                        <input type="checkbox" id="terms" name="personal_interview" value="1">
                                                        <label for="terms">Propongo una entrevista personal para una correcta valoración del trabajo demandado
                                                            y la elaboración de un presupuesto final.</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn-info" type="button">Vista previa</button>
                    <button id="btnSentProposal" class="tg-btn" style="float: right" data-proposal="{{ url('/oferta-trabajo/'.$offerJob->id.'/sentProposal') }}">Enviar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/proposal/sent.js') }}"></script>
@endsection

