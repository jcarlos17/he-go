@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Mis finanzas</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Mis finanzas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                @include('user.include.menu')
                <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h4 style="color: #5dc560; font-weight: 600; margin-bottom: 0">S/ 3,500.00</h4>
                                        <span style="font-weight: 600">Total 2018</span>
                                        <h5 style="color: #5ddace; font-weight: 600; margin-top: 5px; margin-bottom: 0">S/ 120.00</h5>
                                        <span>Total Octubre 2018</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <table style="border: hidden;">
                                            <tbody>
                                                <tr>
                                                    <th style="border-right: 2px solid #ddd;">
                                                        <img style="max-height: 40px; margin: 10px 0" src="{{ asset('images/icon_money.png') }}" alt="">
                                                        <h4 style="color: #5dc560; font-weight: 600; margin-bottom: 0">S/ 0.00</h4>
                                                        <span style="font-weight: 500">Pago en mano</span>
                                                    </th>
                                                    <th style="border-bottom: 2px solid #ddd;">
                                                        <img style="max-height: 40px; margin: 10px 0" src="{{ asset('images/icon_paypal.png') }}" alt="">
                                                        <h4 style="color: #5dc560; font-weight: 600; margin-bottom: 0">S/ 0.00</h4>
                                                        <span style="font-weight: 500">Paypal</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th style="border-top: 2px solid #ddd;">
                                                        <div class="pull-left" style="margin-top:10px; font-size: 13px">
                                                            <span style="font-weight: 600;">- Francisco Franco</span>
                                                            <small style="font-weight: 500" class="text-muted">01/10/2018</small>
                                                        </div>
                                                        <div class="pull-right" style="font-weight: 500; margin-top:10px; margin-right: 20px; font-size: 13px">
                                                            <span style="color: #5dc560;">S/ 50.00</span>
                                                            <a href="#">(Ver pago)</a>
                                                        </div>
                                                        <div style="border-top: 2px solid #5dc560; margin: 10em 1em 0 6em">
                                                            <span class="pull-left" style="font-weight: 500">Total</span>
                                                            <span style="color: #5dc560;">S/ 50.00</span>
                                                        </div>
                                                    </th>
                                                    <th style="border-left: 2px solid #ddd;">
                                                        <div class="pull-left" style="margin-top:10px; margin-left: 20px; font-size: 13px">
                                                            <span style="font-weight: 600;">- Alan García</span>
                                                            <small style="font-weight: 500" class="text-muted">01/10/2018</small>
                                                        </div>
                                                        <div class="pull-right" style="font-weight: 500; margin-top:10px; font-size: 13px">
                                                            <span style="color: #5dc560;">S/ 50.00</span>
                                                            <a href="#">(Ver pago)</a>
                                                        </div>
                                                        <div style="border-top: 2px solid #5dc560; margin: 10em 1em 0 6em">
                                                            <span class="pull-right" style="font-weight: 500">Total</span>
                                                            <span style="color: #5dc560;">S/ 50.00</span>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr style="border: 1px solid #ddd">
                                    <h5 style="margin-bottom: 15px"><i class="lnr lnr-cog"></i> Configuración mis finanzas</h5>
                                    <div class="col-sm-6" style="border-right: 2px solid #ddd">
                                        <div class="form-group" style="margin-left: 10px">
                                            <div class="tg-registeras" style="float: left;">
                                                <div class="tg-radio">
                                                    <input type="radio" id="business" name="type_pay" value="pago" onchange="btnPaypal(this.value)">
                                                    <label style="font-size: 13px" for="business">Activar pago en mano</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="padding-top: 10px;">
                                            <button data-feature="" class="tg-btn pull-right" style="font:400 13px/30px 'Work Sans', Arial, Helvetica, sans-serif; padding: 0 20px">Aceptar</button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" style="margin-left: 10px">
                                            <div class="tg-registeras" style="float: left;">
                                                <div class="tg-radio">
                                                    <input type="radio" id="professional" name="type_pay" value="paypal" onchange="btnPaypal(this.value)">
                                                    <label style="font-size: 13px" for="professional">Activar paypal</label>
                                                </div>
                                                <button id="btnAccountPaypal" class="tg-btn-info" style="font:400 12px/30px 'Work Sans', Arial, Helvetica, sans-serif; padding: 0 20px; margin-left: 10px; display: none">Configurar cuenta</button>
                                            </div>
                                        </div>
                                        <div class="form-group" style="padding-top: 10px;">
                                            <button data-feature="" class="tg-btn pull-right" style="font:400 13px/30px 'Work Sans', Arial, Helvetica, sans-serif; padding: 0 20px">Aceptar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        function btnPaypal(val) {
            if(val == 'paypal')
                $('#btnAccountPaypal').show();
            else
                $('#btnAccountPaypal').hide();

        }
    </script>
@endsection
