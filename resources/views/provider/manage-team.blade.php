@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Mi red de colaboradores</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Mi red de colaboradores</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardmanageteams">
                                <div class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-manageteam tg-ourteam">
                                            <div class="tg-dashboardtitle">
                                                <h2>Mi red de colaboradores</h2>
                                                <span><a href="javascript:void(0);" data-toggle="modal" data-target="#invitationModal">Invitar a futuros prestadores</a></span>
                                            </div>
                                            <div class="tg-manageteambox">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <img src="{{ asset('/images/icon_add_provider.png') }}" alt="Mi red de colaboradores">
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <p>Añade prestadores a tu red de colaboradores y crea tu propia red y estructura de soporte para intercambiar ideas,
                                                            proyectos, subcontratarse y/o formar equipo para asumir grandes proyectos.</p>
                                                        <div id="alertInvitation"></div>
                                                        <div class="form-group" style="margin-top: 20px">
                                                            <input type="text" id="emailMember" class="form-control" placeholder="Buscar por ID, nombre o email" style="width: 90%">
                                                            <a href="javascript:void(0);" class="tg-btn" data-invitation="{{ url('/invitation') }}">Enviar invitación</a>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <ul id="teamMembers" class="tg-teammembers">
                                                    @foreach($members as $member)
                                                        <li id="memberList">
                                                            <div class="tg-teammember">
                                                                <a class="tg-btndel" href="javascript:void(0);" data-delete="{{ url('member/'.$member->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                <figure><a href="javascript:void(0);"><img src="{{ $member->photo_selected }}" alt="{{ $member->name }}"></a></figure>
                                                                <div class="tg-memberinfo">
                                                                    <h5><a href="javascript:void(0);">{{ $member->name }}</a></h5>
                                                                    <a href="javascript:void(0);">{{ $member->email }}</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                <template id="templateMember">
                                                    <li id="memberList">
                                                        <div class="tg-teammember">
                                                            <a class="tg-btndel" href="javascript:void(0);" data-delete=""><i class="fa fa-close"></i></a>
                                                            <figure><a href="javascript:void(0);"><img src=""></a></figure>
                                                            <div class="tg-memberinfo">
                                                                <h5><a href="javascript:void(0);"></a></h5>
                                                                <a href="javascript:void(0);"></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </template>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <div class="modal fade tg-categoryModal" id="invitationModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document" style="max-width: 970px">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Invitación a futuros prestadores</h2>
                </div>
                <div class="tg-modalbody">
                    <div class="tg-themeform tg-formrefinesearch">
                        <div class="row">
                            <div class="col-sm-4 text-center">
                                <img src="{{ asset('/images/add_team.png') }}" alt="Invitación a futuros prestadores" style="max-width: 60%">
                            </div>
                            <div class="col-sm-8">
                                <p>Si conoces a alguien que tiene mucha experiencia y desempeña muy bien su trabajo y crees
                                que es necesario dar a conocer su trabajo, entonces invitalo a unirse y luego agregarlo a tu red de colaboradores.</p>
                                <div id="alertEmail"></div>
                                <div class="form-group">
                                    <input type="email" id="emailInvitation" class="form-control" placeholder="Agrega su email">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn pull-right" data-invitation-email="{{ url('invitation-email') }}">Enviar invitación</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // $(document).on('click', '[data-search]', function () {
        //     const urlSearch = $(this).data('search');
        //     const search = $('#searchMember');
        //     var teamMembers = $('#teamMembers');
        //
        //     $.ajax({
        //         url: urlSearch,
        //         type: 'GET',
        //         data:{
        //             'searchmember': search.val()
        //         },
        //         success:function (data){
        //             teamMembers.empty();
        //
        //             if(data.length > 0){
        //                 for(var i=0; i<data.length; i++){
        //                     var uploadedMember = $('#templateMember').html();
        //                     uploadedMember = uploadedMember.replace('src=""', 'src="'+data[i].photo_selected+'"');
        //                     uploadedMember = uploadedMember.replace('<h5><a href="javascript:void(0);"></a></h5>', '<h5><a href="javascript:void(0);">'+data[i].name+'</a></h5>');
        //                     uploadedMember = uploadedMember.replace('<a href="javascript:void(0);"></a>', '<a href="javascript:void(0);">'+data[i].email+'</a>');
        //                     uploadedMember = uploadedMember.replace('data-delete=""', 'data-delete="/member/'+data[i].id+'/delete"');
        //                     teamMembers.append(uploadedMember);
        //                 }
        //             }else{
        //                 teamMembers.append('<div class="alert alert-info tg-alertmessage fade in" style="margin-bottom: 0px;">' +
        //                     '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
        //                     '<i class="lnr lnr-flag"></i>' +
        //                     '<span><strong>No se encontraron resultados de su búsqueda</strong></span>' +
        //                     '</div>')
        //             }
        //
        //         },
        //     });
        // });

        $(document).on('click', '[data-invitation]', function () {
            const btnInvitation = $("[data-invitation]");
            const urlInvitation = $(this).data('invitation');
            const email = $('#emailMember');
            const alertInvitation = $('#alertInvitation');
            const teamMembers = $('#teamMembers');

            btnInvitation.hide();

            $.ajax({
                url: urlInvitation,
                type: 'GET',
                data:{
                    'email': email.val()
                },
                success:function (data){
                    btnInvitation.show();
                    email.val("");
                    $('.alert-info').remove();
                    $('.alert-warning').remove();

                    var uploadedMember = $('#templateMember').html();
                    uploadedMember = uploadedMember.replace('src=""', 'src="'+data.photo_selected+'"');
                    uploadedMember = uploadedMember.replace('<h5><a href="javascript:void(0);"></a></h5>', '<h5><a href="javascript:void(0);">'+data.name+'</a></h5>');
                    uploadedMember = uploadedMember.replace('<a href="javascript:void(0);"></a>', '<a href="javascript:void(0);">'+data.email+'</a>');
                    uploadedMember = uploadedMember.replace('data-delete=""', 'data-delete="/member/'+data.id+'/delete"');
                    teamMembers.append(uploadedMember);

                    alertInvitation.append('<div class="alert alert-info tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-flag"></i>' +
                        '<span><strong>Se envió correctamente su invitación</strong></span>' +
                        '</div>')
                },
                error:function (data) {
                    $('.alert-info').remove();
                    $('.alert-warning').remove();
                    var $data = data.responseJSON;

                    if ($data.email)
                        alertInvitation.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.email +'</strong></span>' +
                            '</div>');

                    btnInvitation.show();
                }
            });
        });

        $(document).on('click', '[data-invitation-email]', function () {
            const btnInvitation = $("[data-invitation-email]");
            const urlInvitation = $(this).data('invitation-email');
            const email = $('#emailInvitation');
            const modal = $('#invitationModal');
            const alertInvitation = $('#alertInvitation');

            btnInvitation.hide();

            $.ajax({
                url: urlInvitation,
                type: 'GET',
                data:{
                    'email': email.val()
                },
                success:function (data){
                    btnInvitation.show();
                    email.val("");
                    modal.modal('hide');
                    $('.alert-info').remove();
                    $('.alert-warning').remove();

                    alertInvitation.append('<div class="alert alert-info tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-flag"></i>' +
                        '<span><strong>Se envió correctamente su invitación vía email</strong></span>' +
                        '</div>')
                },
                error:function (data) {
                    $('.alert-warning').remove();
                    var $data = data.responseJSON;
                    var $alert = $('#alertEmail');

                    if ($data.email)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.email +'</strong></span>' +
                            '</div>');

                    btnInvitation.show();
                }
            });
        });

        $(document).on('click', '[data-delete]', function () {
            // request al servidor
            var $div = $(this).closest('#memberList');
            var urlDelete = $(this).data('delete');
            $.ajax({
                type: 'GET',
                url: urlDelete,

                success:function (data){
                        $div.remove();
                    }
            });
        });
    </script>
@endsection
