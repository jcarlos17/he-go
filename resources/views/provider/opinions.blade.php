@extends('layouts.app')

@section('banner')
    @guest
        <div class="tg-innerpagebanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-pagetitle">
                            <h1>Mis opiniones</h1>
                        </div>
                        <ol class="tg-breadcrumb">
                            <li><a href="/">Inicio</a></li>
                            <li class="tg-active">Mis opiniones</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="tg-innerloginbanner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-logintitle">
                            <h1>Mis opiniones</h1>
                        </div>
                        <div class="tg-loginsubtitle">
                            <a href="#!" onclick="window.history.back();">
                                <h1><i class="fa fa-mail-reply"></i> Volver atrás</h1>
                            </a>
                        </div>
                        <div class="tg-loginright">
                            <a href="{{ url('/insights') }}">
                                <h1>Panel de administración <i class="fa fa-mail-forward"></i></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardappointmentsetting">
                                <form class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-dashboardappointments">
                                            <div class="tg-dashboardtitle">
                                                <h2>Mis opiniones</h2>
                                            </div>
                                            <div class="tg-dashboardappointmentbox">
                                                @foreach($opinions as $opinion)
                                                    <div class="tg-dashboardappointment">
                                                        <div class="tg-servicetitle">
                                                            <figure>
                                                                <img src="{{ $opinion->user->photo_selected }}" alt="{{ $opinion->user->name }}">
                                                            </figure>
                                                            <div class="tg-clientcontent">
                                                                <h2><a href="javascript:void(0);" data-show="{{ url('opinions/'.$opinion->id.'/show') }}">{{ $opinion->user->name }}</a></h2>
                                                                <span id="descriptionStr{{$opinion->id}}">{{ $opinion->description_str }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="tg-btntimeedit">
                                                            <a href="#" class="tg-btnedite" data-edit="{{ url('opinions/'.$opinion->id.'/show') }}"><i class="lnr lnr-pencil"></i></a>
                                                            <a href="#" class="tg-btndel" data-toggle="modal"><i class="lnr lnr-trash"></i></a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->

    <!--Theme Modal Box Start*-->
    <div class="modal fade tg-appointmentapprovemodal tg-categoryModal-1" id="detailOpinion" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <span id="reported" class="text-danger pull-right"></span>
                    <h2>Detalle de la opinión</h2>
                </div>
                <div class="tg-modalbody">
                    <ul class="tg-invoicedetail">
                        <li><span>Nombre del usuario:</span><span id="username"></span></li>
                        <li><span>Comentario:</span><span id="description"></span></li>
                        <li><span>Fecha:</span><span id="date"></span></li>
                        <li><span>Hora:</span><span id="hour"></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Theme Modal Box End-->
    <div class="modal fade tg-appointmentapprovemodal tg-categoryModal-1" id="editOpinion" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Editar opinión</h2>
                </div>
                <div id="alert-opinions"></div>
                <div class="tg-modalbody">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input id="nameEdit" type="text" class="form-control" value="" disabled>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="description">Opinión</label>
                                <textarea class="form-control" id="descriptionEdit" rows="5" readonly></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="tg-checkbox">
                                    <input class="services" type="checkbox" id="reportedEdit" name="reportedEdit">
                                    <label for="reportedEdit"><span>Reportar este comentario </span> <small class="text-muted"> <i>(No se publicara hasta después de ser revisado por nuestro departamento)</i></small></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="">Responder públicamente este comentario <small class="text-muted"> <i>(solo si no marca la casilla anterior)</i></small></label>
                                <textarea class="form-control" id="answerEdit" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-modalfoot">
                    <button class="tg-btn tg-btnapproved" id="btnUpdate" data-update="" type="submit">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-show]', function () {
            // request al servidor
            const urlshow = $(this).data('show');
            let modal = $('#detailOpinion');
            let reported = $('#reported');
            let username = $('#username');
            let description = $('#description');
            let date = $('#date');
            let hour = $('#hour');
            $.ajax({
                type: 'GET',
                url: urlshow,

                success:function (data){
                    let text = data.reported ? 'En evaluación' : '';
                    reported.text(text);
                    username.text(data.user_name);
                    description.text(data.description);
                    date.text(data.date_format);
                    hour.text(data.hour_format);
                    modal.modal('show');
                }
            });
        });

        $(document).on('click', '[data-edit]', function () {
            // request al servidor
            const urlshow = $(this).data('edit');
            let modal = $('#editOpinion');
            let reported = $('#reportedEdit');
            let username = $('#nameEdit');
            let description = $('#descriptionEdit');
            let answer = $('#answerEdit');
            $.ajax({
                type: 'GET',
                url: urlshow,

                success:function (data){
                    let value = !!data.reported;
                    reported.prop('checked', value);
                    username.val(data.user_name);
                    description.val(data.description);
                    answer.val(data.answer);
                    $('#btnUpdate').attr("data-update",'/opinions/'+data.id+'/update');
                    modal.modal('show');
                }
            });
        });

        $(document).on('click', '[data-update]', function () {
            let urlOpinion = $(this).data('update');
            let modal = $('#editOpinion');
            let reported = $('#reportedEdit:checked');
            let description = $('#descriptionEdit');
            let answer = $('#answerEdit');
            $('#btnUpdate').prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: urlOpinion,
                data:{
                    '_token':$('input[name=_token]').val(),
                    'description':description.val(),
                    'reported': reported.val(),
                    'answer': answer.val()
                },
                success:function (data){
                    $('.alert-success').remove();
                    $('.alert-warning').remove();

                    $('#descriptionStr'+data.id).text(data.description_str);
                    $('#btnUpdate').prop('disabled', false);
                    modal.modal('hide');
                },
                error:function (data) {
                    $('.alert-success').remove();
                    $('.alert-warning').remove();
                    let $data = data.responseJSON;

                    let $alert = $('#alert-opinions');

                    if ($data.description)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 1px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.description +'</strong></span>' +
                            '</div>');

                    $('#btnUpdate').prop('disabled', false);
                }
            });
        });
    </script>
@endsection
