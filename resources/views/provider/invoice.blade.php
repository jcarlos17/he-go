@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Boletines de servicio</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Boletines de servicio</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardmanageservices">
                                <div class="tg-dashboardhead">
                                    <div class="tg-dashboardtitle">
                                        <h2>Boletines de servicio</h2>
                                    </div>
                                    <button class="tg-btnaddservices">Delete All</button>
                                </div>
                                <div class="tg-sortfilters">
                                    <div class="tg-sortfilter tg-sortby">
                                        <span>Sort By:</span>
                                        <div class="tg-select">
                                            <select>
                                                <option>days</option>
                                                <option>Type</option>
                                                <option>date</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tg-sortfilter tg-arrange">
                                        <span>Arrange:</span>
                                        <div class="tg-select">
                                            <select>
                                                <option>des</option>
                                                <option>Asc</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tg-sortfilter tg-show">
                                        <span>Show:</span>
                                        <div class="tg-select">
                                            <select>
                                                <option>11</option>
                                                <option>24</option>
                                                <option>all</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-dashboardservices">
                                    @foreach($appointments as $appointment)
                                        <div class="tg-dashboardservice">
                                            <div class="tg-servicetitle">
                                                <h2><a href="#" data-toggle="modal" data-target=".tg-categoryModal-P">{{ $appointment->service->title }}</a> <span>(En cita)</span></h2>
                                            </div>
                                            <div class="tg-btntimeedit">
                                                <span>{{ $appointment->service->price_value }}</span>
                                                <button class="tg-btnedite"><i class="lnr lnr-printer" data-show="{{ url('/invoice/'.$appointment->id.'/show') }}"></i></button>
                                                <button class="tg-btndel"><i class="lnr lnr-trash"></i></button>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
    <!--Theme Modal Box Start-->
    <div class="modal fade tg-invoicemodal" id="modalInvoice" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Detalle del servicio</h2>
                    <ul class="tg-btnaction">
                        <li class="tg-email"><a href="#" title="Enviar por email"><i class="lnr lnr-envelope"></i></a></li>
                        <li class="tg-printer"><a href="#" title="Imprimir"><i class="lnr lnr-printer"></i></a></li>
                        <li class="tg-pdf"><a href="#" title="Descargar en formato PDF"><i class="lnr lnr-cloud-download"></i></a></li>
                    </ul>
                </div>
                <div class="tg-modalbody">
                    <ul class="tg-invoicedetail">
                        <li><span>ID:</span><span id="idInvoice"></span></li>
                        <li><span>Cliente:</span><span id="clientInvoice"></span></li>
                        <li><span>Monto:</span><span id="mountInvoice"></span></li>
                        <li><span>Método pago:</span><span>--</span></li>
                        <li><span>Estado pago:</span><span>--</span></li>
                        <li><span>Fecha servicio:</span><span id="dateInvoice"></span></li>
                        <li><span>Hora:</span><span id="timeInvoice"></span></li>
                        {{--<li><span>Dirección:</span><span></span></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Theme Modal Box End-->
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-show]', function () {
            // request al servidor
            const urlShowInvoice = $(this).data('show');
            const idInvoice = $('#idInvoice');
            const clientInvoice = $('#clientInvoice');
            const mountInvoice = $('#mountInvoice');
            const dateInvoice = $('#dateInvoice');
            const timeInvoice = $('#timeInvoice');
            $.ajax({
                type: 'GET',
                url: urlShowInvoice,

                success:function (data){
                    idInvoice.replaceWith('<span id="idInvoice">'+data.id+'</span>');
                    clientInvoice.replaceWith('<span id="clientInvoice">'+data.interested_name+'</span>');
                    mountInvoice.replaceWith('<span id="mountInvoice">'+data.service_price+'</span>');
                    dateInvoice.replaceWith('<span id="dateInvoice">'+data.date+'</span>');
                    timeInvoice.replaceWith('<span id="timeInvoice">'+data.time+'</span>');

                    $('#modalInvoice').modal('show');
                }
            });
        });
    </script>
@endsection
