@extends('layouts.app')

@section('styles')
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Categorías</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Categorías</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprofilesetting">
                                <div class="tg-themeform">
                                    <fieldset>
                                        {{--categories--}}
                                        <div class="tg-dashboardbox tg-certificatesawards">
                                            <div id="alert-categories"></div>
                                            <div class="tg-dashboardtitle">
                                                <h2>Categorias</h2>
                                                <a class="tg-btnaddnew" href="#" data-toggle="modal" data-target="#categoryModal">Agregar nuevo</a>
                                            </div>
                                            <div id="uploadedCategories" class="tg-certificatesawardsbox">
                                                @foreach($categories as $category)
                                                    <div class="tg-certificatesaward">
                                                        <div class="tg-imgandtitle">
                                                            <figure id="ImageUrlText{{$category->id}}"><a href="#"><img src="{{ $category->image_url }}" style="height: 40px"></a></figure>
                                                            <h3 id="nameCategoryText{{$category->id}}"><a href="{{ url('admin/categories/'.$category->id.'/subcategories') }}">{{ $category->name }}</a></h3>
                                                        </div>
                                                        <div class="tg-btntimeedit">
                                                            <span id="iconText{{$category->id}}"><i class="{{ $category->icon->code }}"></i></span>
                                                            <button class="tg-btnedite" data-show="{{ url('admin/categories/'.$category->id.'/show') }}"><i class="lnr lnr-pencil"></i></button>
                                                            <button class="tg-btndel" data-delete="{{ url('admin/categories/'.$category->id.'/delete') }}"><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <template id="templateCategory">
                                                    <div class="tg-certificatesaward">
                                                        <div class="tg-imgandtitle">
                                                            <figure id=""><a href="#"><img src="" style="height: 40px"></a></figure>
                                                            <h3 id=""><a href=""></a></h3>
                                                        </div>
                                                        <div class="tg-btntimeedit">
                                                            <span id=""></span>
                                                            <button class="tg-btnedite" data-show=""><i class="lnr lnr-pencil"></i></button>
                                                            <button class="tg-btndel" data-delete=""><i class="lnr lnr-trash"></i></button>
                                                        </div>
                                                    </div>
                                                </template>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->

    <!--Theme Modal Box Start-->
    <div class="modal fade tg-socialModal" id="categoryModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Categorías</h2>
                </div>
                <div id="alert-category"></div>
                <div class="tg-modalbody">
                    <form id="formCategory" class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="name" type="text" class="form-control" name="name" placeholder="Nombre de la categoría" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="slug" type="text" class="form-control" name="slug" placeholder="Slug de la categoría" value="" required>
                                </div>
                            </div>
                        </div>
                        <div class="tg-upload">
                            <div class="tg-uploadhead">
                                <span>
                                    <h3>Subir imagen representativa</h3>
                                    <i class="fa fa-exclamation-circle"></i>
                                </span>
                                <i class="lnr lnr-upload"></i>
                            </div>
                            <div class="tg-box">
                                <label class="tg-fileuploadlabel" for="inputCategory">
                                    <i class="lnr lnr-cloud-upload"></i>
                                    <span>O arrastre su imagen aquí para subir</span>
                                    <input id="inputCategory" class="tg-fileinput" type="file" accept="image/*" onchange="loadFileI(event)">
                                </label>
                                <div class="tg-gallery">
                                    <div id="uploadedImageLoading" class="tg-galleryimages">

                                    </div>
                                </div>
                                <template id="templateImageLoading">
                                    <div class="tg-galleryimg tg-uploading">
                                        <figure>
                                            <img src="" alt="" style="height: 80px">
                                            <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                        </figure>
                                    </div>
                                </template>
                            </div>
                        </div>
                        <div class="tg-companyfeaturebox tg-amenities" style="padding: 20px 0">
                            <ul>
                                @foreach($icons as $key => $icon)
                                    <li style="width: 12.5%">
                                        <div class="tg-radio">
                                            <input class="services" type="radio" id="{{ $icon->id }}" name="icon_id" value="{{ $icon->id }}">
                                            <label for="{{ $icon->id }}"><span><i class="{{ $icon->code }}"></i></span></label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="tg-modalfoot">
                    <button data-category="{{ url('admin/categories/add') }}" class="tg-btn">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade tg-socialModal" id="categoryEditModal" tabindex="-1">
        <div class="modal-dialog tg-modaldialog" role="document">
            <div class="modal-content tg-modalcontent">
                <div class="tg-modalhead">
                    <h2>Editar categoría</h2>
                </div>
                <div id="alert-category-edit"></div>
                <div class="tg-modalbody">
                    <form id="formEditCategory" class="tg-themeform tg-formaddsocial">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="nameEdit" type="text" class="form-control" value="" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <input id="slugEdit" type="text" class="form-control" value="" required>
                                </div>
                            </div>
                        </div>
                        <div class="tg-upload">
                            <div class="tg-uploadhead">
                                <span>
                                    <h3>Subir imagen del certificado o premio</h3>
                                    <i class="fa fa-exclamation-circle"></i>
                                </span>
                                <i class="lnr lnr-upload"></i>
                            </div>
                            <div class="tg-box">
                                <label class="tg-fileuploadlabel" for="inputImgEdit">
                                    <i class="lnr lnr-cloud-upload"></i>
                                    <span>O arrastre la imagen aquí para subir</span>
                                    <em>(Subir sólo si quiere reemplazar la actual)</em>
                                    <input id="inputImgEdit" class="tg-fileinput" type="file" accept="image/*"  onchange="loadFileEdit(event)">
                                </label>
                                <div class="tg-gallery">
                                    <div class="tg-galleryimages">
                                        <div id="galleryEdit" class="tg-galleryimg">
                                            <figure>
                                                <img id="imageView" src="" alt="" style="height: 80px">
                                                <span id="loaderEdit"></span>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tg-companyfeaturebox tg-amenities" style="padding: 20px 0">
                            <ul>
                                @foreach($icons as $key => $icon)
                                    <li style="width: 12.5%">
                                        <div class="tg-radio">
                                            <input class="services" type="radio" id="edit{{ $icon->id }}" name="iconIdEdit" value="{{ $icon->id }}">
                                            <label for="edit{{ $icon->id }}"><span><i class="{{ $icon->code }}"></i></span></label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="tg-modalfoot">
                    <button id="btnCategoryEdit" data-edit="" class="tg-btn">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/category.js') }}"></script>
    <script>
        var loadFileI = function(event) {
            // var image = document.getElementById('imageLoading');
            var url = URL.createObjectURL(event.target.files[0]);
            $('.tg-uploading').remove();
            var templateImageLoading = $('#templateImageLoading').html();
            templateImageLoading = templateImageLoading.replace('src=""', 'src="'+url+'"');
            $('#uploadedImageLoading').append(templateImageLoading);
        };

        var loadFileEdit = function(event) {
            var url = URL.createObjectURL(event.target.files[0]);
            $('#galleryEdit').addClass("tg-uploading");
            $('#imageView').replaceWith('<img id="imageView" src="'+url+'" alt="" style="height: 80px">');
            $('#loaderEdit').replaceWith('<span id="loaderEdit" class="tg-loader"><i class="fa fa-spinner"></i></span>');
        };
    </script>
    <script>
        $('#name').on('input',function(e){
            var string = $('#name').val();
            var slug = string_to_slug(string);
            $('#slug').val(slug);
        });
        $('#nameEdit').on('input',function(e){
            var string = $('#nameEdit').val();
            var slug = string_to_slug(string);
            $('#slugEdit').val(slug);
        });
        function string_to_slug (str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();
            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }
            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;
        }
    </script>
@endsection
