@extends('layouts.app')

@section('styles')
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Getionar membresias</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Getionar membresias</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprofilesetting">
                                <div class="tg-themeform">
                                    <fieldset>
                                        <form class="tg-dashboardbox tg-location" method="POST">
                                            <div class="tg-dashboardtitle">
                                                <h2>Agregar membresia a prestador de servicios</h2>
                                            </div>
                                            <div id="alert-address"></div>
                                            <div class="tg-locationbox">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="provider_id" id="select_provider" required>
                                                                <option value="">Seleccione prestador</option>
                                                                @foreach ($providers as $provider)
                                                                    <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                        <span class="tg-select">
                                                            <select name="package" id="select_package" required>
                                                                <option value="">Seleccione plan</option>
                                                                <option value="60">Plan joven (6 meses)</option>
                                                                <option value="100">Plan joven (12 meses)</option>
                                                                <option value="150">Tarifa plana ilimitado (6 meses)</option>
                                                                <option value="210">Tarifa plana ilimitado (12 meses)</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                            <button class="tg-btn" type="submit">Agregar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tg-companyfeaturebox tg-ourteam">
                                                    <div class="tg-companyfeaturetitle">
                                                        <h3>Memebresias</h3>
                                                    </div>
                                                    <ul class="tg-teammembers">
                                                        @foreach($packages as $package)
                                                            <li>
                                                                <div class="tg-teammember">
                                                                    <figure><a href="javascript:void(0);"><img src="{{ $package->provider->photo_selected }}" alt="image description"></a></figure>
                                                                    <div class="tg-memberinfo">
                                                                        <h5><a href="#">{{ $package->provider->name }}</a></h5>
                                                                        <p>{{ $package->type }}</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </form>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
@endsection
