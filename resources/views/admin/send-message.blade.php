@extends('layouts.app')

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Enviar mensaje</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Enviar mensaje</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboarprivatemessages">
                                <form class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-dashboardbox tg-dashboardmessages">
                                            <div class="tg-dashboardtitle">
                                                <h2>Enviar mensaje</h2>
                                            </div>
                                            <div id="alertMessage"></div>
                                            <div class="tg-dashboardmessagesbox">
                                                <div class="tg-emailattachemnets">
                                                    <div class="tg-emailmessagebox">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <input type="text" name="subject" class="form-control" placeholder="Para: Todos los usuarios (prestadores y buscadores)" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="topicAdmin" placeholder="Asunto:">
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea placeholder="Escriba su mensaje aquí" id="messageAdmin"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tg-btnarea">
                                                    <a class="tg-btn" id="btnMessageAdmin" href="javascript:void(0);" data-message="{{ url('admin/send-message') }}">Enviar mensaje</a>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!--Main End-->
@endsection

@section('scripts')
    <script>
        $(document).on('click', '[data-message]', function () {

            const urlMessage = $(this).data('message');
            const topic = $('#topicAdmin');
            const message = $('#messageAdmin');
            const btnMessage = $('#btnMessageAdmin');
            const $alert = $('#alertMessage');

            btnMessage.prop('disabled', true);

            $.ajax({
                url: urlMessage,
                type: 'POST',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'topic':topic.val(),
                    'message':message.val(),
                },
                success:function (data){
                    btnMessage.prop('disabled', false);
                    topic.val("");
                    message.val("");
                    $('.alert-warning').remove();
                    $('.alert-success').remove();

                    $alert.append('<div class="alert alert-success tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                        '<i class="lnr lnr-star"></i>' +
                        '<span><strong>Se envió correctamente su mensaje a todos los usuarios</strong></span>' +
                        '</div>');
                },
                error:function (data) {
                    btnMessage.prop('disabled', false);
                    $('.alert-warning').remove();
                    $('.alert-success').remove();

                    const $data = data.responseJSON;
                    if ($data.topic)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.topic +'</strong></span>' +
                            '</div>');
                    if ($data.message)
                        $alert.append('<div class="alert alert-warning tg-alertmessage fade in" style="margin-bottom: 0px;">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close"><em class="lnr lnr-cross"></em></a>' +
                            '<i class="lnr lnr-bug"></i>' +
                            '<span><strong>'+ $data.message +'</strong></span>' +
                            '</div>');
                }
            });
        });
    </script>
@endsection
