@extends('layouts.app')

@section('styles')
@endsection

@section('banner')
    <div class="tg-innerpagebanner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-pagetitle">
                        <h1>Gestión de imágenes</h1>
                    </div>
                    <ol class="tg-breadcrumb">
                        <li><a href="/">Inicio</a></li>
                        <li class="tg-active">Gestión de imágenes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('main')
    <!--Main Start-->
    <main id="tg-main" class="tg-main tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <!--Menu Start-->
                    @include('user.include.menu')
                    <!--Menu End-->
                    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-dashboard tg-dashboardprofilesetting">
                                <div class="tg-themeform">
                                    <fieldset>
                                        <div class="tg-bordertitle">
                                            <h3 style="margin: 0">Prestador de servicios</h3>
                                        </div>
                                        <div class="tg-dashboardbox tg-uploadphotos">
                                            <div class="tg-dashboardtitle">
                                                <h2>Imágenes de la vista inicial</h2>
                                            </div>
                                            <div class="tg-uploadbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgTopInsight"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen superior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputTopInsight">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formTopInsight" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputTopInsight" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgTopInsight" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedTopInsight" class="tg-galleryimages">
                                                                            <div id="itemTopInsight" class="tg-galleryimg">
                                                                                <figure>
                                                                                    @if($imgTopInsight)
                                                                                    <img src="{{ $imgTopInsight->image_url }}" alt="{{ $imgTopInsight->type }}" style="height: 80px">
                                                                                        <figcaption>
                                                                                            <a data-delete-image="{{ url('/admin/images/'.$imgTopInsight->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                        </figcaption>
                                                                                    @endif
                                                                                </figure>
                                                                            </div>
                                                                    </div>
                                                                    <template id="templateTopInsight">
                                                                        <div id="itemTopInsight" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateTopInsightLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgBottomInsight"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen inferior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputBottomInsight">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formBottomInsight" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputBottomInsight" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgBottomInsight" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedBottomInsight" class="tg-galleryimages">
                                                                        <div id="itemBottomInsight" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgBottomInsight)
                                                                                    <img src="{{ $imgBottomInsight->image_url }}" alt="{{ $imgBottomInsight->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgBottomInsight->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateBottomInsight">
                                                                        <div id="itemBottomInsight" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateBottomInsightLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-uploadphotos">
                                            <div class="tg-dashboardtitle">
                                                <h2>Imágenes del perfil público</h2>
                                            </div>
                                            <div class="tg-uploadbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgTopPublicProfile"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen superior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputTopPublicProfile">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formTopPublicProfile" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputTopPublicProfile" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgTopPublicProfile" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedTopPublicProfile" class="tg-galleryimages">
                                                                        <div id="itemTopPublicProfile" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgTopPublicProfile)
                                                                                    <img src="{{ $imgTopPublicProfile->image_url }}" alt="{{ $imgTopPublicProfile->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgTopPublicProfile->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateTopPublicProfile">
                                                                        <div id="itemTopPublicProfile" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateTopPublicProfileLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgBottomPublicProfile"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen inferior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputBottomPublicProfile">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formBottomPublicProfile" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputBottomPublicProfile" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgBottomPublicProfile" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedBottomPublicProfile" class="tg-galleryimages">
                                                                        <div id="itemBottomPublicProfile" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgBottomPublicProfile)
                                                                                    <img src="{{ $imgBottomPublicProfile->image_url }}" alt="{{ $imgBottomPublicProfile->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgBottomPublicProfile->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateBottomPublicProfile">
                                                                        <div id="itemBottomPublicProfile" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateBottomPublicProfileLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-bordertitle">
                                            <h3 style="margin: 50px 0 0 0">Buscador de servicios</h3>
                                        </div>
                                        <div class="tg-dashboardbox tg-uploadphotos">
                                            <div class="tg-dashboardtitle">
                                                <h2>Imágenes de la vista inicial</h2>
                                            </div>
                                            <div class="tg-uploadbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgTopInsightBS"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen superior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputTopInsightBS">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formTopInsightBS" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputTopInsightBS" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgTopInsightBS" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedTopInsightBS" class="tg-galleryimages">
                                                                        <div id="itemTopInsightBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgTopInsightBS)
                                                                                    <img src="{{ $imgTopInsightBS->image_url }}" alt="{{ $imgTopInsightBS->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgTopInsightBS->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateTopInsightBS">
                                                                        <div id="itemTopInsightBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateTopInsightBSLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgBottomInsightBS"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen inferior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputBottomInsightBS">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formBottomInsightBS" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputBottomInsightBS" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgBottomInsightBS" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedBottomInsightBS" class="tg-galleryimages">
                                                                        <div id="itemBottomInsightBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgBottomInsightBS)
                                                                                    <img src="{{ $imgBottomInsightBS->image_url }}" alt="{{ $imgBottomInsightBS->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgBottomInsightBS->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateBottomInsightBS">
                                                                        <div id="itemBottomInsightBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateBottomInsightBSLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tg-dashboardbox tg-uploadphotos">
                                            <div class="tg-dashboardtitle">
                                                <h2>Imágenes del perfil público</h2>
                                            </div>
                                            <div class="tg-uploadbox">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgTopPublicProfileBS"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen superior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputTopPublicProfileBS">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formTopPublicProfileBS" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputTopPublicProfileBS" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgTopPublicProfileBS" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedTopPublicProfileBS" class="tg-galleryimages">
                                                                        <div id="itemTopPublicProfileBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgTopPublicProfileBS)
                                                                                    <img src="{{ $imgTopPublicProfileBS->image_url }}" alt="{{ $imgTopPublicProfileBS->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgTopPublicProfileBS->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateTopPublicProfileBS">
                                                                        <div id="itemTopPublicProfileBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateTopPublicProfileBSLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div id="alertImgBottomPublicProfileBS"></div>
                                                        <div class="tg-upload">
                                                            <div class="tg-uploadhead">
                                                                <span>
                                                                    <h3>Subir imagen inferior</h3>
                                                                    <i class="fa fa-exclamation-circle"></i>
                                                                </span>
                                                                <i class="lnr lnr-upload"></i>
                                                            </div>
                                                            <div class="tg-box">
                                                                <label class="tg-fileuploadlabel" for="inputBottomPublicProfileBS">
                                                                    <i class="lnr lnr-cloud-upload"></i>
                                                                    <span>O arrastre su imagen aquí para cargar</span>
                                                                    <form id="formBottomPublicProfileBS" style="display: none">
                                                                        {{ csrf_field() }}
                                                                        <input id="inputBottomPublicProfileBS" class="tg-fileinput" type="file" name="image" accept="image/*">
                                                                        <input type="hidden" value="imgBottomPublicProfileBS" name="type">
                                                                    </form>
                                                                </label>
                                                                <div class="tg-gallery tg-horizontalthemescrollbar">
                                                                    <div id="uploadedBottomPublicProfileBS" class="tg-galleryimages">
                                                                        <div id="itemBottomPublicProfileBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                @if($imgBottomPublicProfileBS)
                                                                                    <img src="{{ $imgBottomPublicProfileBS->image_url }}" alt="{{ $imgBottomPublicProfileBS->type }}" style="height: 80px">
                                                                                    <figcaption>
                                                                                        <a data-delete-image="{{ url('/admin/images/'.$imgBottomPublicProfileBS->id.'/delete') }}"><i class="fa fa-close"></i></a>
                                                                                    </figcaption>
                                                                                @endif
                                                                            </figure>
                                                                        </div>
                                                                    </div>
                                                                    <template id="templateBottomPublicProfileBS">
                                                                        <div id="itemBottomPublicProfileBS" class="tg-galleryimg">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <figcaption>
                                                                                    <a data-delete-image=""><i class="fa fa-close"></i></a>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                    <template id="templateBottomPublicProfileBSLoading">
                                                                        <div id="" class="tg-galleryimg tg-uploading">
                                                                            <figure>
                                                                                <img src="" alt="" style="height: 80px">
                                                                                <span class="tg-loader"><i class="fa fa-spinner"></i></span>
                                                                            </figure>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/images/topInsight.js') }}"></script>
    <script src="{{ asset('js/admin/images/bottomInsight.js') }}"></script>
    <script src="{{ asset('js/admin/images/topPublicProfile.js') }}"></script>
    <script src="{{ asset('js/admin/images/bottomPublicProfile.js') }}"></script>
    <script src="{{ asset('js/admin/images/topInsightBS.js') }}"></script>
    <script src="{{ asset('js/admin/images/bottomInsightBS.js') }}"></script>
    <script src="{{ asset('js/admin/images/topPublicProfileBS.js') }}"></script>
    <script src="{{ asset('js/admin/images/bottomPublicProfileBS.js') }}"></script>
    <script>
        $(document).on('click', '[data-delete-image]', function () {
            // request al servidor
            const $li= $(this).parent().parent();
            const urlDeleteBanner = $(this).data('delete-image');
            $.ajax({
                type: 'GET',
                url: urlDeleteBanner,

                success:function (){
                    $li.remove();
                }
            });
        });

    </script>
@endsection
