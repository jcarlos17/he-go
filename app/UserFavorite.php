<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFavorite extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userFavorite()
    {
        return $this->belongsTo(User::class, 'user_favorite_id');
    }
}
