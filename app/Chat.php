<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public function from()
    {
        return $this->belongsTo(User::class, 'from_id');
    }

    public function to()
    {
        return $this->belongsTo(User::class, 'to_id');
    }

    public function contact()
    {
        if($this->from_id == auth()->id())
            return $this->to();
        else
            return $this->from();
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class);
    }

    public function getLastMessageAttribute()
    {
        if($this->from_id == auth()->id()){
            $lastMessage = $this->conversations()->where('user_id', $this->to_id)->orderBy('created_At', 'desc')->first();
            if($lastMessage){
                return $lastMessage->message;
            }
            return '';
        }else{
            $lastMessage = $this->conversations()->where('user_id', $this->from_id)->orderBy('created_At', 'desc')->first();
            if($lastMessage){
                return $lastMessage->message;
            }
            return '';
        }
    }

    public function getLastHourAttribute()
    {
        if($this->from_id == auth()->id()){
            $lastMessage = $this->conversations()->where('user_id', $this->to_id)->orderBy('created_At', 'desc')->first();
            if($lastMessage){
                return $lastMessage->hour;
            }
            return '';
        }else{
            $lastMessage = $this->conversations()->where('user_id', $this->from_id)->orderBy('created_At', 'desc')->first();
            if($lastMessage){
                return $lastMessage->hour;
            }
            return '';
        }
    }
}
