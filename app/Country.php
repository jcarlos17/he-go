<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function deparments()
    {
        return $this->hasMany(Department::class);
    }
}
