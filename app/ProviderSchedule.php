<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderSchedule extends Model
{
    public function getStartFormatAttribute()
    {
        return date('H:i', strtotime($this->start));
    }

    public function getEndFormatAttribute()
    {
        return date('H:i', strtotime($this->end));
    }

    public function getCompleteAttribute()
    {
        return $this->start_format. ' - '. $this->end_format;
    }
}
