<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacySetting extends Model
{
    public function getPrivacySettingExistsAttribute()
    {
        $user = auth()->user();

        $settingExists = $user->privacysettings()->where('privacy_setting_id', $this->id)->exists();
        if(!$settingExists)
            return true;

        return $user->privacysettings()->where('privacy_setting_id', $this->id)->where('status', 1)->exists();
    }
}
