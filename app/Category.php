<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function getImageUrlAttribute()
    {
        if($this->image)
            return '/images/categories/'.$this->image;

        return '/images/categories/default.png';
    }

    public function icon()
    {
        return $this->belongsTo(Icon::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'category_id');
    }

    public function jobs()
    {
        return $this->hasMany(OfferJob::class);
    }
}
