<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class BusinessHour extends Model
{
    public function getStartFormatAttribute()
    {
        return date('h:i A', strtotime($this->start));
    }

    public function getEndFormatAttribute()
    {
        return date('h:i A', strtotime($this->end));
    }

    public function getCompleteAttribute()
    {
        return $this->start_format. ' - '. $this->end_format;
    }
}
