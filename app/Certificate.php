<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    public function getImageUrlAttribute()
    {
        return '/images/certificates/'.$this->image;
    }

    public function getDateAttribute()
    {
        return $this->created_at->toFormattedDateString();
    }
}
