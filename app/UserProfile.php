<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public function getPercentageAttribute()
    {
        $quantity = 0;
        if ($this->has_profile_picture){$quantity += 10;}
        if ($this->has_complete_basic_information){$quantity += 10;}
        if ($this->has_photo_identity_document){$quantity += 25;}
        if ($this->has_category_work){$quantity += 5;}
        if ($this->has_three_social_network){$quantity += 5;}
        if ($this->has_cover_letter){$quantity += 15;}
        if ($this->has_experience){$quantity += 10;}
        if ($this->has_photo_gallery){$quantity += 5;}
        if ($this->has_certificate_or_prize){$quantity += 5;}
        if ($this->has_professional_preparation){$quantity += 5;}
        if ($this->has_brochure){$quantity += 5;}

        return $quantity;
    }
}
