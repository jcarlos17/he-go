<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    public $dates = ['start_date', 'end_date'];

    public function getDateIntervalAttribute()
    {
        return $this->start_date->format('F Y').' - '.$this->end_date->format('F Y');
    }
}
