<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public function provider()
    {
        return $this->belongsTo(User::class, 'provider_id');
    }

    public function interested()
    {
        return $this->belongsTo(User::class, 'interested_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
