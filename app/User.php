<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    protected $fillable = [
        'name', 'last_name', 'phone', 'email', 'password', 'role', 'company_name', 'type_provider'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function opinions()
    {
        return $this->hasMany(Opinion::class, 'provider_id');
    }

    public function getProgressPercentageAttribute()
    {
        $profile = $this->profile()->exists();
        if (! $profile){
            return $this->profile()->create();
        }

        return $this->profile->percentage;
    }

    public function socialnetwork()
    {
        return $this->hasOne(SocialNetwork::class);
    }

    public function languages()
    {
        return $this->hasMany(Language::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function brochures()
    {
        return $this->hasMany(Brochure::class);
    }

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_user');
    }

    public function features()
    {
        return $this->hasMany(FeatureUser::class);
    }

    public function service_features()
    {
        return $this->belongsToMany(ServiceFeature::class, 'feature_user');
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class);
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function privacysettings()
    {
        return $this->hasMany(UserPrivacySetting::class);
    }

    public function tasks()
    {
        return $this->hasMany(UserTask::class);
    }

    public function photos()
    {
        return $this->hasMany(ProfilePhoto::class);
    }

    public function banners()
    {
        return $this->hasMany(ProfileBanner::class);
    }

    public function identities()
    {
        return $this->hasMany(IdentityPhoto::class)->where('type', 'dni');
    }

    public function public_services()
    {
        return $this->hasMany(IdentityPhoto::class)->where('type', 'ps');
    }

    public function favorites()
    {
        return $this->hasMany(UserFavorite::class);
    }

    public function internalMessages()
    {
        return $this->hasMany(InternalMessage::class, 'to_id');
    }

    public function jobs()
    {
        return $this->hasMany(OfferJob::class);
    }

    public function getTwoLastJobsAttribute()
    {
        return $this->jobs()->whereNotIn('status', ['Borrador','Pausado'])->take(2)->orderBy('created_at', 'desc')->get();
    }

    public function getJobsPublishedAttribute()
    {
        return $this->jobs()->whereNotIn('status', ['Borrador','Pausado'])->count();
    }

    public function countJobs($status) {

        return $this->jobs()->where('status', $status)->count();
    }

    public function getJobsClosedAttribute()
    {
        return $this->jobs()->where('status', 'Publicado')->where('expire_date', '<', Carbon::now())->count();
    }

    public function getPhotoSelectedAttribute()
    {
        $photoSelected = $this->photos()->where('selected', 1)->first();
        if ($photoSelected)
            return '/images/photos/'.$photoSelected->image;

        return '/images/photos/default.png';
    }

    public function getPhotoSelectedPdfAttribute()
    {
        $photoSelected = $this->photos()->where('selected', 1)->first();
        if ($photoSelected)
            return 'images/photos/'.$photoSelected->image;

        return 'images/photos/default.png';
    }

    public function getOnlyCompanyNameAttribute()
    {
        if($this->is_provider){
            if($this->company_name)
                return $this->company_name;
            else
                return 'Prestador de servicios independiente';
        }

        if($this->is_seeker){
            if($this->company_name)
                return $this->company_name;
            else
                return 'Usuario buscador de servicios';
        }


    }

    public function getLocationCompleteAttribute()
    {
        $firstAddres = $this->addresses()->first();
        if ($firstAddres){
            $location = $firstAddres->province->name.' / '.$firstAddres->department->name;
        }else{
            $location = '';
        }

        return $location;
    }

    public function getBannerSelectedAttribute()
    {
        $bannerSelected = $this->banners()->where('selected', 1)->first();
        if ($bannerSelected)
            return '/images/banners/'.$bannerSelected->image;

        return '/images/banners/default.jpg';
    }

    public function getIsAdminAttribute()
    {
        return $this->role == 0;
    }

    public function getIsProviderAttribute()
    {
        return $this->role == 1 || $this->role == 0;
    }

    public function getIsSeekerAttribute()
    {
        return $this->role == 2;
    }

    public function getVerifyIdentityAttribute()
    {
        $dateLimited = $this->created_at->addDays(7);
        $identityCount = $this->identities()->count();

        return $this->role == 1 && $dateLimited < Carbon::now() && $identityCount == 0;

    }

    public function hours($day, $userId) {

        return BusinessHour::where('user_id', $userId)->whereNotNull('start')->whereNotNull('end')
            ->where('status', 1)->where('day', $day)->orderBy('start')->get();
    }

    public function schedules($day) {

        return ProviderSchedule::where('provider_id', $this->id)->where('day', $day)->orderBy('start')->get();
    }

    public function attend($day, $userId) {

        if(!BusinessHour::where('user_id', $userId)->where('day', $day)->exists())
            return true;

        return BusinessHour::where('user_id', $userId)
            ->whereNull('start')->whereNull('end')->where('day', $day)->where('status', 0)->exists();
    }

    public function attendEmergency($userId) {

        return BusinessHour::where('user_id', $userId)
            ->whereNull('start')->whereNull('end')->where('day', 'Emergencia')->where('status', 0)->exists();
    }

    public function verifyPrivacity($id, $userId) {

        $user = User::find($userId);
        if ($user->is_seeker){
            return true;
        }

        $privacyExists = $user->privacysettings()->where('privacy_setting_id', $id)->exists();
        if(! $privacyExists){
            return true;
        }

        return $user->privacysettings()->where('privacy_setting_id', $id)->where('status', 1)->exists();
    }

    public function getHiddenPhotoSelectedAttribute()
    {
        if(auth()->check() && auth()->user()->is_provider){
            $photoSelected = $this->photos()->where('selected', 1)->first();
            if ($photoSelected)
                return '/images/photos/'.$photoSelected->image;
            else
                return '/images/icono-perfil.png';
        }else{
            return '/images/icono-perfil.png';
        }
    }

    public function getHiddenNameAttribute()
    {
        if(auth()->check() && auth()->user()->is_provider){
            return $this->name;
        }else{
            if ($this->name){
                $last_position = strlen($this->name);
                $name = $this->name;
                for($i=3; $i<$last_position; $i++){
                    $name = substr_replace($name, 'x',$i, 1);
                }

                return $name;
            }
            return '';
        }
    }

    public function getHiddenEmailAttribute()
    {
        if(auth()->check() && auth()->user()->is_provider){
            return $this->email;
        }else {
            if ($this->email) {
                $pos_arroba = strpos($this->email, '@');
                $last_position = strlen($this->email);
                $email = $this->email;
                for ($i = 3; $i < $pos_arroba; $i++) {
                    $email = substr_replace($email, 'x', $i, 1);
                }
                for ($i = $pos_arroba + 1; $i < $last_position; $i++) {
                    if ($email[$i] !== '.')
                        $email = substr_replace($email, 'x', $i, 1);
                }

                return $email;
            }

            return '';
        }
    }

    public function getHiddenMobileAttribute()
    {
        if(auth()->check() && auth()->user()->is_provider){
            return $this->mobile;
        }else {
            if ($this->mobile) {
                $mobile = $this->mobile;
                $last_position = strlen($this->mobile);
                for ($i = 3; $i < $last_position; $i++) {
                    $mobile = substr_replace($mobile, 'x', $i, 1);
                }

                return $mobile;
            }

            return '';
        }
    }

    public function getHiddenWebsiteAttribute()
    {
        if(auth()->check() && auth()->user()->is_provider){
            return $this->website;
        }else {
            if ($this->website) {
                $website = $this->website;
                $pos = strripos($this->website, '.');
                for ($i = 0; $i < $pos; $i++) {
                    $website = substr_replace($website, 'x', $i, 1);
                }

                return $website;
            }

            return '';
        }
    }

}
