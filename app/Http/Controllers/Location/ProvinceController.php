<?php

namespace App\Http\Controllers\Location;

use App\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    public function byDepartment($id)
    {
        return Province::where('department_id', $id)->orderby('name')->get();
    }
}
