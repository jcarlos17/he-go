<?php

namespace App\Http\Controllers\Location;

use App\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function byProvince($id)
    {
        return District::where('province_id', $id)->orderby('name')->get();
    }
}
