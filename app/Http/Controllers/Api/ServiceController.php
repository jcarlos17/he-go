<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function show($id, $option)
    {
        $user = User::find($id);

        $services = [];

        if($option == 'Free'){
            $services = $user->services()->where('type_value', 'G')->get();
        }

        if($option == 'Service'){
            $services = $user->services()->where('type_value', '!=', 'G')->get();
        }

        return $services;
    }
}
