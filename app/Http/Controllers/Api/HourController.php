<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HourController extends Controller
{
    public function show($id, Request $request)
    {
        $user = User::find($id);
        $date = Carbon::createFromFormat('d/m/Y', $request->date);
        $day = $date->format('l');

        if($day == 'Sunday'){$dia = 'Domingo';}
        if($day == 'Monday'){$dia = 'Lunes';}
        if($day == 'Tuesday'){$dia = 'Martes';}
        if($day == 'Wednesday'){$dia = 'Miercoles';}
        if($day == 'Thursday'){$dia = 'Jueves';}
        if($day == 'Friday'){$dia = 'Viernes';}
        if($day == 'Saturday'){$dia = 'Sabado';}

        $hours = $user->hours($dia, $id);

        $hoursInterval = [];

        foreach ($hours as $key => $hour)
             array_push($hoursInterval, $this->intervaloHora($hour->start, $hour->end));

        return $hoursInterval;
    }

    function intervaloHora($hora_inicio, $hora_fin, $intervalo = 30) {

        $hora_inicio = new DateTime( $hora_inicio );
        $hora_fin    = new DateTime( $hora_fin );
        $hora_fin->modify('+1 second'); // Añadimos 1 segundo para que nos muestre $hora_fin


        // Establecemos el intervalo en minutos
        $intervalo = new DateInterval('PT'.$intervalo.'M');

        // Sacamos los periodos entre las horas
        $periodo   = new DatePeriod($hora_inicio, $intervalo, $hora_fin);

        foreach( $periodo as $hora ) {

            // Guardamos las horas intervalos
            $horas[] =  $hora->format('H:i');
        }

        return $horas;
    }
}
