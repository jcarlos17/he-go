<?php

namespace App\Http\Controllers\Auth;

use App\ServiceProvider;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/insights';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'type_provider' => 'required_if:role,1',
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required_if:role,2',
            'company_name' => 'required_if:role,1',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'condition' => 'required'
        ],[
            'type_provider.required_if' => 'Es necesario seleccionar el tipo de prestador',
            'name.required' => 'Es necesario ingresar nombres',
            'last_name.required' => 'Es necesario ingresar apellidos',
            'email.required' => 'Es necesario ingresar un correo',
            'email.email' => 'El correo ingresado no tiene el formato correcto',
            'email.unique' => 'El correo ingresado ya se encuentra registrado',
            'password.required' => 'Es necesario ingresar una contraseña',
            'password.min' => 'La contraseña debe tener al menos 6 caracteres',
            'password.confirmed' => 'La contraseña de confirmación no coincide',
            'condition.required' => 'Es necesario aceptar los términos y condiciones',
            'phone.required_if' => 'Es necesario ingresar telefono móvil',
            'company_name.required_if' => 'Es necesario ingresar nombre de la empresa'
        ]);
    }


    protected function create(array $data)
    {
        if ($data['role'] == 1){
            $user = User::create([
                'type_provider' => $data['type_provider'],
                'name' => $data['name'],
                'last_name' => $data['last_name'],
                'company_name' => $data['company_name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
            ]);
        }else{
            $user = User::create([
                'name' => $data['name'],
                'last_name' => $data['last_name'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
            ]);
        }

        return $user;
    }
}
