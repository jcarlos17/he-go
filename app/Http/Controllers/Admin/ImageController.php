<?php

namespace App\Http\Controllers\Admin;

use App\AdminImage;
use App\ProfileBanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function index()
    {
        $admin = auth()->user();
        $imgTopInsight = AdminImage::where('type', 'imgTopInsight')->first();
        $imgBottomInsight = AdminImage::where('type', 'imgBottomInsight')->first();
        $imgTopPublicProfile = AdminImage::where('type', 'imgTopPublicProfile')->first();
        $imgBottomPublicProfile = AdminImage::where('type', 'imgBottomPublicProfile')->first();
        $imgTopInsightBS = AdminImage::where('type', 'imgTopInsightBS')->first();
        $imgBottomInsightBS = AdminImage::where('type', 'imgBottomInsightBS')->first();
        $imgTopPublicProfileBS = AdminImage::where('type', 'imgTopPublicProfileBS')->first();
        $imgBottomPublicProfileBS = AdminImage::where('type', 'imgBottomPublicProfileBS')->first();

        return view('admin.images.index', compact('admin', 'imgTopInsight', 'imgBottomInsight',
            'imgTopPublicProfile', 'imgBottomPublicProfile','imgTopInsightBS', 'imgBottomInsightBS',
            'imgTopPublicProfileBS', 'imgBottomPublicProfileBS'));
    }

    public function add(Request $request)
    {
        $type = $request->type;
        if ($type == 'imgTopInsight' || $type == 'imgTopInsightBS'){
            $rules = [
                'image' => 'required|image|dimensions:min_width=870,min_height=175'
            ];
            $messages = [
                'image.required' => 'Es necesario ingresar una imagen',
                'image.image' => 'El archivo debe ser formato imagen',
                'image.dimensions' => 'La imagen debe tener dimensiones como mínimo 870x175'
            ];
        }
        if ($type == 'imgBottomInsight' || $type == 'imgBottomInsightBS'){
            $rules = [
                'image' => 'required|image|dimensions:min_width=270,min_height=270'
            ];
            $messages = [
                'image.required' => 'Es necesario ingresar una imagen',
                'image.image' => 'El archivo debe ser formato imagen',
                'image.dimensions' => 'La imagen debe tener dimensiones como mínimo 270x270'
            ];
        }
        if ($type == 'imgTopPublicProfile' || $type == 'imgTopPublicProfileBS'){
            $rules = [
                'image' => 'required|image|dimensions:min_width=770,min_height=175'
            ];
            $messages = [
                'image.required' => 'Es necesario ingresar una imagen',
                'image.image' => 'El archivo debe ser formato imagen',
                'image.dimensions' => 'La imagen debe tener dimensiones como mínimo 770x175'
            ];
        }
        if ($type == 'imgBottomPublicProfile' || $type == 'imgBottomPublicProfileBS'){
            $rules = [
                'image' => 'required|image|dimensions:min_width=370,min_height=370'
            ];
            $messages = [
                'image.required' => 'Es necesario ingresar una imagen',
                'image.image' => 'El archivo debe ser formato imagen',
                'image.dimensions' => 'La imagen debe tener dimensiones como mínimo 370x370'
            ];
        }
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $file = $request->file('image');

        $image = AdminImage::where('type', $type)->first();
        if ($image){
            $pathDelete = public_path('images/adminImages/' . $image->image);
            File::delete($pathDelete);
        }else{
            $image = new AdminImage();
        }

        $extension = $file->getClientOriginalExtension();
        if ($type == 'imgTopInsight' || $type == 'imgTopInsightBS'){
            $file_name = uniqid(). '.' . $extension;
            $path = public_path('images/adminImages/' . $file_name);
            Image::make($file)->resize(870, 175)->save($path);
        }
        if ($type == 'imgBottomInsight' || $type == 'imgBottomInsightBS'){
            $file_name = uniqid(). '.' . $extension;
            $path = public_path('images/adminImages/' . $file_name);
            Image::make($file)->resize(270, 270)->save($path);
        }
        if ($type == 'imgTopPublicProfile' || $type == 'imgTopPublicProfileBS'){
            $file_name = uniqid(). '.' . $extension;
            $path = public_path('images/adminImages/' . $file_name);
            Image::make($file)->resize(770, 175)->save($path);
        }
        if ($type == 'imgBottomPublicProfile' || $type == 'imgBottomPublicProfileBS'){
            $file_name = uniqid(). '.' . $extension;
            $path = public_path('images/adminImages/' . $file_name);
            Image::make($file)->resize(370, 370)->save($path);
        }

        $image->admin_id = auth()->id();
        $image->image = $file_name;
        $image->type = $type;
        $image->save();

        $image->image_url = $image->getImageUrlAttribute();

        return $image;
    }

    public function delete($id)
    {
        $banner = AdminImage::find($id);
        $pathDelete = public_path('images/adminImages/' . $banner->image);
        File::delete($pathDelete);
        $banner->delete();

        return response()->json();
    }
}
