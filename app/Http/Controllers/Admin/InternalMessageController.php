<?php

namespace App\Http\Controllers\Admin;

use App\InternalMessage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InternalMessageController extends Controller
{
    public function show()
    {
        return view('admin.send-message');
    }

    public function send(Request $request)
    {
        $rules = [
            'topic' => 'required',
            'message' => 'required',
        ];
        $messages = [
            'topic.required' => 'Es necesario ingresar el asunto del mensaje',
            'message.required' => 'Es necesario ingresar la descripción del mensaje',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $topic = $request->topic;
        $message = $request->message;

        $userIds = User::where('role', '!=', 0)->pluck('id');

        foreach ($userIds as $userId){
            $sendMessage = new InternalMessage();
            $sendMessage->type = 'message_admin';
            $sendMessage->topic = $topic;
            $sendMessage->message = $message;
            $sendMessage->from_id = auth()->id();
            $sendMessage->to_id = $userId;
            $sendMessage->save();
        }

        return response()->json();
    }
}
