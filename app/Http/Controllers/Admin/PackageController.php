<?php

namespace App\Http\Controllers\Admin;

use App\Package;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    public function show()
    {
        $providers = User::where('role',1)->get();
        $packages = Package::all();
        return view('admin.packages.show', compact('providers', 'packages'));
    }

    public function add(Request $request)
    {
        $packageExists = Package::where('provider_id', $request->provider_id)->exists();
        if($packageExists)
            return back();

        $price = $request->package;
        if($price == '60' || $price == '100')
            $month = 6;
        else
            $month = 12;

        $package = new Package();
        $package->price = $price;
        $package->start = Carbon::now();
        $package->end = Carbon::now()->addMonths($month);
        $package->provider_id = $request->provider_id;
        $package->save();

        return back();
    }
}
