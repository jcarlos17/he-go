<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Icon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    public function show()
    {
        $categories = Category::where('category_id', NULL)->get();
        $icons = Icon::orderBy('code', 'asc')->get();

        return view('admin.categories.show', compact('categories', 'icons'));
    }

    public function add(Request $request)
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
            'image' => 'required',
            'icon_id' => 'required'
        ];
        $messages = [
          'name.required' => 'Es necesario ingresar nombre de la categoría',
          'slug.required' => 'Es necesario ingresar slug de la categoría',
          'image.required' => 'Es necesario ingresar imagen de la categoría',
          'icon_id.required' => 'Es necesario marcar un icono de la categoría'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $nameInput = $request->name;

        $validator->after(function($validator) use ($nameInput, $request) {
            $nameExists = Category::where('name', $nameInput)->exists();
            if ($nameExists) {
                $validator->errors()->add('name', 'El nombre ingresado ya se encuentra registrado');
            }
            if (! $request->hasFile('image')){
                $validator->errors()->add('image', 'Es necesario ingresar imagen de la categoría');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $category = new Category();
        $category->name = $nameInput;
        $category->slug = $request->slug;
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $file_name = uniqid() . '.' . $extension;
            $path = public_path('images/categories/' . $file_name);
            Image::make($file)
                ->fit(370, 270)
                ->save($path);
            $category->image = $file_name;
        }

        $category->icon_id = $request->icon_id;
        $category->save();

        $category->image_url = $category->getImageUrlAttribute();
        $category->icon_code = $category->icon->code;

        return response()->json($category);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
            'icon_id' => 'required'
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar nombre de la categoría',
            'slug.required' => 'Es necesario ingresar slug de la categoría',
            'icon_id.required' => 'Es necesario marcar un icono de la categoría'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $nameInput = $request->name;

        $validator->after(function($validator) use ($nameInput, $request, $id) {
            $nameExists = Category::where('name', $nameInput)->where('id','<>', $id)->exists();
            if ($nameExists) {
                $validator->errors()->add('name', 'El nombre ingresado ya se encuentra registrado');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $category = Category::find($id);
        $category->name = $nameInput;
        $category->slug = $request->slug;
        if ($request->hasFile('image')){
            $pathDelete = public_path('/images/categories/' . $category->image);
            File::delete($pathDelete);

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $file_name = uniqid() . '.' . $extension;
            $path = public_path('images/categories/' . $file_name);
            Image::make($file)
                ->fit(370, 270)
                ->save($path);
            $category->image = $file_name;
        }

        $category->icon_id = $request->icon_id;
        $category->save();

        $category->image_url = $category->getImageUrlAttribute();
        $category->icon_code = $category->icon->code;

        return response()->json($category);
    }

    public function showCategory($id)
    {
        $category = Category::find($id);
        $category->image_url = $category->getImageUrlAttribute();

        return response()->json($category);
    }

    public function delete($id)
    {
        $category = Category::find($id);
        if($category->subcategories()->count() > 0){
            $data = [];
            $data['success'] = true;
            $data['message'] = 'La categoría '.$category->name.' no puede ser eliminada porque tiene subcategorias';
            return $data;
        }
        $pathDelete = public_path('/images/categories/' . $category->image);
        File::delete($pathDelete);
        $category->delete();

        return response()->json();
    }
}
