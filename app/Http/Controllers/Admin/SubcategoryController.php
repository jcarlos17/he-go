<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Icon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class SubcategoryController extends Controller
{
    public function show($id)
    {
        $category = Category::find($id);
        $subcategories = Category::where('category_id', $id)->get();
        $icons = Icon::orderBy('code', 'asc')->get();

        return view('admin.subcategories.show', compact('category','subcategories', 'icons'));
    }

    public function add($id,Request $request)
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
            'image' => 'required',
            'icon_id' => 'required'
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar nombre de la subcategoría',
            'slug.required' => 'Es necesario ingresar slug de la subcategoría',
            'image.required' => 'Es necesario ingresar imagen de la subcategoría',
            'icon_id.required' => 'Es necesario marcar un icono de la subcategoría'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $nameInput = $request->name;

        $validator->after(function($validator) use ($nameInput, $request, $id) {
            $nameExists = Category::where('name', $nameInput)->where('category_id', $id)->exists();
            if ($nameExists) {
                $validator->errors()->add('name', 'El nombre ingresado ya se encuentra registrado');
            }
            if (! $request->hasFile('image')){
                $validator->errors()->add('image', 'Es necesario ingresar imagen de la subcategoría');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $subcategory = new Category();
        $subcategory->name = $nameInput;
        $subcategory->slug = $request->slug;
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $file_name = uniqid() . '.' . $extension;
            $path = public_path('images/categories/' . $file_name);
            Image::make($file)
                ->fit(370, 270)
                ->save($path);
            $subcategory->image = $file_name;
        }

        $subcategory->category_id = $id;
        $subcategory->icon_id = $request->icon_id;
        $subcategory->save();

        $subcategory->image_url = $subcategory->getImageUrlAttribute();
        $subcategory->icon_code = $subcategory->icon->code;

        return response()->json($subcategory);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
            'icon_id' => 'required'
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar nombre de la categoría',
            'slug.required' => 'Es necesario ingresar slug de la categoría',
            'icon_id.required' => 'Es necesario marcar un icono de la categoría'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $nameInput = $request->name;

        $validator->after(function($validator) use ($nameInput, $request, $id) {
            $nameExists = Category::where('name', $nameInput)->where('id','<>', $id)->exists();
            if ($nameExists) {
                $validator->errors()->add('name', 'El nombre ingresado ya se encuentra registrado');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $subcategory = Category::find($id);
        $subcategory->name = $nameInput;
        $subcategory->slug = $request->slug;
        if ($request->hasFile('image')){
            $pathDelete = public_path('/images/categories/' . $subcategory->image);
            File::delete($pathDelete);

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $file_name = uniqid() . '.' . $extension;
            $path = public_path('images/categories/' . $file_name);
            Image::make($file)
                ->fit(370, 270)
                ->save($path);
            $subcategory->image = $file_name;
        }

        $subcategory->icon_id = $request->icon_id;
        $subcategory->save();

        $subcategory->image_url = $subcategory->getImageUrlAttribute();
        $subcategory->icon_code = $subcategory->icon->code;

        return response()->json($subcategory);
    }

    public function showCategory($id)
    {
        $subcategory = Category::find($id);
        $subcategory->image_url = $subcategory->getImageUrlAttribute();

        return response()->json($subcategory);
    }

    public function delete($id)
    {
        $subcategory = Category::find($id);
        $pathDelete = public_path('/images/categories/' . $subcategory->image);
        File::delete($pathDelete);
        $subcategory->delete();

        return response()->json();
    }
}
