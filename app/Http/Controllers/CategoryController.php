<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show()
    {
        $categories = Category::where('category_id', NULL)->paginate(12);
        $searchCategories = Category::where('category_id', NULL)->get();
        return view('categories', compact('categories', 'searchCategories'));
    }

    public function byCategory($id)
    {
        return Category::where('category_id', $id)->get();
    }
}
