<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TermsConditionController extends Controller
{
    public function show()
    {
        return view('terms_conditions');
    }
}
