<?php

namespace App\Http\Controllers;

use App\OfferJob;
use Illuminate\Http\Request;

class LastJobController extends Controller
{
    public function show()
    {
        $lastJobs = OfferJob::whereIn('status', [1,2,3])->orderBy('created_at', 'desc')->paginate(10);
        return view('last-job', compact('lastJobs'));
    }
}
