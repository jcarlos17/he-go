<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Http\Controllers\Controller;


class ProfilePercentageController extends Controller
{
    public function show()
    {
        $user = User::find(auth()->id());

        return $user->progress_percentage;
    }
}
