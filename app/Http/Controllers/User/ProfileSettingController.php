<?php

namespace App\Http\Controllers\User;

use App\AdminImage;
use App\Category;
use App\Department;
use App\ServiceFeature;
use App\User;
use App\Http\Controllers\Controller;


class ProfileSettingController extends Controller
{
    public function show()
    {
        $user = auth()->user();

        $categories = Category::whereNull('category_id')->get();

        $userCategories = $user->categories()->whereNull('categories.category_id')->get();

        foreach ($userCategories as $category)
            $category->userSubcategories = $this->getSubcategoriesForCategory($category, $user);

        $features = ServiceFeature::all();
        $departments = Department::all();

        return view('user.profile-setting',
            compact('user', 'categories', 'features', 'departments',
            'userCategories'
                ));
    }

    public function getSubcategoriesForCategory($category, $user)
    {
        return $user->categories()->where('categories.category_id', $category->id)->get();
    }
}
