<?php

namespace App\Http\Controllers\User;

use App\InternalMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function sendContact($toId, Request $request)
    {
        $rules = [
            'topic' => 'required',
            'message' => 'required',
        ];
        $messages = [
            'topic.required' => 'Es necesario ingresar el asunto para contactar al usuario',
            'message.required' => 'Es necesario ingresar la descripción para contactar al usuario',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $from = auth()->user();

        $sendMessage = new InternalMessage();
        $sendMessage->type = 'message_contact';
        $sendMessage->topic = $request->topic;
        $sendMessage->message = $request->message;
        $sendMessage->from_id = $from->id;
        $sendMessage->to_id = $toId;
        $sendMessage->save();

        $sendMessage->from_email = $from->email;
        $sendMessage->from_phone = $from->phone ? $from->phone : '--';

        return $sendMessage;
    }

    public function sendClaim($toId, Request $request)
    {
        $rules = [
            'topic' => 'required',
            'message' => 'required',
        ];
        $messages = [
            'topic.required' => 'Es necesario ingresar el tema del reclamo',
            'message.required' => 'Es necesario ingresar la descripción del reclamo',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $from = auth()->user();
        $sendMessage = new InternalMessage();
        $sendMessage->type = 'message_claim';
        $sendMessage->topic = $request->topic;
        $sendMessage->message = $request->message;
        $sendMessage->from_id = $from->id;
        $sendMessage->to_id = $toId;
        $sendMessage->save();

        $sendMessage->from_email =  $from->email;
        $sendMessage->from_phone = $from->phone ? $from->phone : '--';

        return $sendMessage;


    }
}
