<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'link' => 'required|active_url'
        ];
        $messages = [
            'link.required' => 'Es necesario ingresar un link',
            'link.active_url' => 'El link ingresado no tiene el formato correcto'
        ];

        $linkInput = $request->input('link');

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($linkInput, $id) {
            $linkExists = Video::where('link', $linkInput)->where('user_id', $id)->exists();
            if ($linkExists) {
                $validator->errors()->add('link', 'El link ingresado ya se encuentra registrado');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $video = new Video();
        $video->link = $linkInput;
        $video->user_id = $id;
        $video->save();

        return response()->json($video);
    }

    public function delete($video_id)
    {
        $video = Video::find($video_id);
        $video->delete();
        return response()->json();
    }
}
