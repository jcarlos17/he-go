<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PresentationController extends Controller
{
    public function update(Request $request)
    {
        $rules = [
            'presentation' => 'min:15'
        ];
        $messages = [
            'presentation.min' => 'La carta de presentación debe tener al menos 15 caracteres'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $user = User::find(auth()->id());
        $user->presentation = $request->input('presentation');
        $user->save();

        $lenght = strlen($user->presentation);
        $userProfile = UserProfile::where('user_id', auth()->id())->first();
        if($lenght >= 150)
            $userProfile->has_cover_letter = 1;
        else
            $userProfile->has_cover_letter = 0;

        $userProfile->save();

        return response()->json($user);
    }
}
