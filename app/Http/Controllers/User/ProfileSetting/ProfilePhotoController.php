<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\ProfilePhoto;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProfilePhotoController extends Controller
{
    public function update(Request $request)
    {
        $rules = [
            'image' => 'required|image'
        ];
        $messages = [
            'image.required' => 'Es necesario ingresar una imagen',
            'image.image' => 'El archivo debe ser formato imagen'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $user = auth()->user();
        $validator->after(function($validator) use ($user) {
            $countPhotos = $user->photos()->count();
            if ($countPhotos >= 5) {
                $validator->errors()->add('image', 'Se acepta un máximo de 5 imagenes');
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $inputImage = $request->file('image');
        $extension = $inputImage->getClientOriginalExtension();
        $file_name = uniqid() . '.' . $extension;

        $path = public_path('images/photos/' . $file_name);

        Image::make($inputImage)
            ->fit(100, 100)
            ->save($path);

        $photoUser = new ProfilePhoto();
        $photoUser->image = $file_name;
        $photoUser->user_id = $user->id;
        $photoUser->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->has_profile_picture = 1;
        $userProfile->save();

        $photoUser->photo_url = $photoUser->getPhotoUrlAttribute();

        return $photoUser;
    }

    public function select($id)
    {
        $user = auth()->user();
        $user->photos()->update(['selected' => 0]);

        $photoSelected = ProfilePhoto::find($id);
        $photoSelected->selected = 1;
        $photoSelected->save();

        $user->photo_selected = $user->getPhotoSelectedAttribute();
        return response()->json($user);
    }

    public function delete($photo_id)
    {
        $photo = ProfilePhoto::find($photo_id);
        $pathDelete = public_path('/images/photos/' . $photo->image);
        File::delete($pathDelete);
        $photo->delete();

        $photoCount = ProfilePhoto::where('user_id', auth()->id())->count();
        if ($photoCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_profile_picture = 0;
            $userProfile->save();
        }

        $user = auth()->user();
        $user->photo_selected = $user->getPhotoSelectedAttribute();

        return response()->json($user);
    }
}
