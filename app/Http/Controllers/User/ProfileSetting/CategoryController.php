<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\CategoryUser;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'category_id' => 'required',
        ];
        $messages = [
            'category_id.required' => 'Es necesario seleccionar una categoría.',
        ];

        $category_id = $request->input('category_id');

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($category_id, $id) {
            $categoryExists = CategoryUser::where('category_id', $category_id)->where('user_id', $id)->exists();
            if ($categoryExists) {
                $validator->errors()->add('category_id', 'La categoría seleccionada ya se encuentra registrada');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $categoryUser = new CategoryUser();
        $categoryUser->category_id = $category_id;
        $categoryUser->user_id = $id;
        $categoryUser->save();
        $categoryUser->category_name = $categoryUser->category->name;
        $categoryUser->r_category_id = $categoryUser->category->category_id;

        $userProfile = UserProfile::where('user_id', $id)->first();
        $userProfile->has_category_work = 1;
        $userProfile->save();

        return response()->json($categoryUser);
    }

    public function addSubcat(Request $request, $id)
    {
        $rules = [
            'category_id' => 'required',
        ];
        $messages = [
            'category_id.required' => 'Es necesario seleccionar una subcategoría.',
        ];

        $category_id = $request->input('category_id');

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($category_id, $id) {
            $categoryExists = CategoryUser::where('category_id', $category_id)->where('user_id', $id)->exists();
            if ($categoryExists) {
                $validator->errors()->add('category_id', 'La subcategoría seleccionada ya se encuentra registrada');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $categoryUser = new CategoryUser();
        $categoryUser->category_id = $category_id;
        $categoryUser->user_id = $id;
        $categoryUser->save();
        $categoryUser->category_name = $categoryUser->category->name;
        $categoryUser->r_category_id = $categoryUser->category->category_id;

        return response()->json($categoryUser);
    }

    public function delete($category_id)
    {
        $user = auth()->user();
        $category = CategoryUser::where('category_id',$category_id)->first();
        $subcatExists = $this->getSubcategoriesForCategory($category_id, $user);
        if($subcatExists){
            $data = [];
            $data['success'] = true;
            $data['message'] = 'No puede eliminar esta categoría porque contiene subcategorías';
            return $data;
        }

        $category->delete();

        $categoryCount = CategoryUser::where('user_id', auth()->id())->count();
        if ($categoryCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_category_work = 0;
            $userProfile->save();
        }

        return response()->json();
    }

    public function deleteSubcat($category_id)
    {
        $category = CategoryUser::where('category_id',$category_id)->first();
        $category->delete();

        return response()->json();
    }

    public function getSubcategoriesForCategory($category_id, $user)
    {
        return $user->categories()->where('categories.category_id', $category_id)->exists();
    }
}
