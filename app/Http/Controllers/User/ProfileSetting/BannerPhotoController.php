<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\ProfileBanner;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class BannerPhotoController extends Controller
{
    public function update(Request $request)
    {
        $rules = [
            'image' => 'required|image|dimensions:min_width=1350,min_height=210'
        ];
        $messages = [
            'image.required' => 'Es necesario ingresar una imagen',
            'image.image' => 'El archivo debe ser formato imagen',
            'image.dimensions' => 'La imagen debe tener como mínimo dimensiones 1350x210'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $user = auth()->user();
        $validator->after(function($validator) use ($user) {
            $countPhotos = $user->banners()->count();
            if ($countPhotos >= 5) {
                $validator->errors()->add('image', 'Se acepta un máximo de 5 imagenes');
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $user = User::find(auth()->id());
        $inputImage = $request->file('image');
        $extension = $inputImage->getClientOriginalExtension();
        $file_name = uniqid() . '.' . $extension;

        $path = public_path('images/banners/' . $file_name);

        Image::make($inputImage)
            ->fit(1350, 210)
            ->save($path);

        $userBanner = new ProfileBanner();
        $userBanner->image = $file_name;
        $userBanner->user_id = $user->id;
        $userBanner->save();

        $userBanner->banner_url = $userBanner->getBannerUrlAttribute();

        return $userBanner;
    }

    public function select($id)
    {
        $user = auth()->user();
        $user->banners()->update(['selected' => 0]);

        $bannerSelected = ProfileBanner::find($id);
        $bannerSelected->selected = 1;
        $bannerSelected->save();

        $user->banner_selected = $user->getBannerSelectedAttribute();
        return response()->json($user);
    }

    public function delete($banner_id)
    {
        $banner = ProfileBanner::find($banner_id);
        $pathDelete = public_path('/images/banners/' . $banner->image);
        File::delete($pathDelete);
        $banner->delete();

        $user = auth()->user();
        $user->banner_selected = $user->getBannerSelectedAttribute();

        return response()->json($user);
    }
}
