<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Gallery;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'file' => 'required|image'
        ];
        $messages = [
            'file.required' => 'Es necesario ingresar una imagen',
            'file.image' => 'El archivo debe ser formato imagen'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $file_name = uniqid() . '.' . $extension;

        $path = public_path('images/galleries/' . $file_name);

        Image::make($file)
            ->fit(92, 92)
            ->save($path);

        $image = new Gallery();
        $image->user_id = $id;
        $image->image = $file_name;
        $image->save();

        $userProfile = UserProfile::where('user_id', $id)->first();
        $userProfile->has_photo_gallery = 1;
        $userProfile->save();

        $image->image_url = $image->getImageUrlAttribute();

        return $image;
    }

    public function delete($gallery_id)
    {
        $gallery = Gallery::find($gallery_id);
        $pathDelete = public_path('/images/galleries/' . $gallery->image);
        File::delete($pathDelete);
        $gallery->delete();

        $galleryCount = Gallery::where('user_id', auth()->id())->count();
        if ($galleryCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_photo_gallery = 0;
            $userProfile->save();
        }

        return response()->json();
    }
}
