<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\FeatureUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeatureController extends Controller
{
    public function add(Request $request)
    {
        $featureUserExists = FeatureUser::where('user_id', auth()->id())->exists();
        if ($featureUserExists)
            FeatureUser::where('user_id', auth()->id())->delete();

        $features = $request->input('service');
        foreach ($features as $feature){
            $add_service = new FeatureUser();
            $add_service->service_feature_id= $feature;
            $add_service->user_id = auth()->id();
            $add_service->save();
        }

        return response()->json();
    }
}
