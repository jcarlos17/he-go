<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Qualification;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class QualificationController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'institution' => 'required',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d'
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título',
            'institution.required' => 'Es necesario ingresar el nombre de la institución',
            'start_date.required' => 'Es necesario ingresar la fecha de inicio',
            'start_date.date_format' => 'La fecha de inicio no tiene el formato correcto',
            'end_date.required' => 'Es necesario ingresar la fecha de fin',
            'end_date.date_format' => 'La fecha de fin no tiene el formato correcto'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $qualification = new Qualification();
        $qualification->title = $request->input('title');
        $qualification->institution = $request->input('institution');
        $qualification->start_date = $request->input('start_date');
        $qualification->end_date = $request->input('end_date');
        $qualification->user_id = $id;
        $qualification->save();

        $qualification->date_interval = $qualification->getDateIntervalAttribute();

        $userProfile = UserProfile::where('user_id', $id)->first();
        $userProfile->has_professional_preparation = 1;
        $userProfile->save();

        return $qualification;
    }

    public function show($id)
    {
        $qualification = Qualification::find($id);

        $qualification->start_date_format = date("Y-m-d", strtotime($qualification->start_date));
        $qualification->end_date_format = date("Y-m-d", strtotime($qualification->end_date));

        return response()->json($qualification);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'institution' => 'required',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d'
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título',
            'institution.required' => 'Es necesario ingresar el nombre de la institución',
            'start_date.required' => 'Es necesario ingresar la fecha de inicio',
            'start_date.date_format' => 'La fecha de inicio no tiene el formato correcto',
            'end_date.required' => 'Es necesario ingresar la fecha de fin',
            'end_date.date_format' => 'La fecha de fin no tiene el formato correcto'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $qualification = Qualification::find($id);
        $qualification->title = $request->input('title');
        $qualification->institution = $request->input('institution');
        $qualification->start_date = $request->input('start_date');
        $qualification->end_date = $request->input('end_date');
        $qualification->save();

        $qualification->date_interval = $qualification->getDateIntervalAttribute();

        return $qualification;
    }

    public function delete($qualification_id)
    {
        $qualification = Qualification::find($qualification_id);
        $qualification->delete();

        $qualificationCount = Qualification::where('user_id', auth()->id())->count();
        if ($qualificationCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_professional_preparation = 0;
            $userProfile->save();
        }

        return response()->json();
    }
}
