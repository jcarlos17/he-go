<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LanguageController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'language' => 'required'
        ];
        $messages = [
            'language.required' => 'Es necesario seleccionar un lenguaje.'
        ];

        $description = $request->input('language');

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($description, $id) {
            $languageExists = Language::where('language', $description)->where('user_id', $id)->exists();
            if ($languageExists) {
                $validator->errors()->add('language', 'El lenguaje seleccionado ya se encuentra registrado');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $language = new Language();
        $language->language = $description;
        $language->user_id = $id;
        $language->save();

        return response()->json($language);
    }

    public function delete($laguange_id)
    {
        $laguange = Language::find($laguange_id);
        $laguange->delete();

        return response()->json();
    }
}
