<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Experience;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExperienceController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'company' => 'required',
            'description' => 'nullable',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'nullable|date_format:Y-m-d'
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título',
            'company.required' => 'Es necesario ingresar el nombre de la empresa',
            'start_date.required' => 'Es necesario ingresar la fecha de inicio',
            'start_date.date_format' => 'La fecha de inicio no tiene el formato correcto',
            'end_date.date_format' => 'La fecha de fin no tiene el formato correcto'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $experience = new Experience();
        $experience->title = $request->input('title');
        $experience->company = $request->input('company');
        $experience->description = $request->input('description');
        $experience->start_date = $request->input('start_date');
        $experience->end_date = $request->input('end_date');
        $experience->user_id = $id;
        $experience->save();

        if ($experience->end_date)
            $experience->date_interval = $experience->start_date->format('F Y').' - '.$experience->end_date->format('F Y');
        else
            $experience->date_interval = $experience->start_date->format('F Y').' - To Date';

        $userProfile = UserProfile::where('user_id', $id)->first();
        $userProfile->has_experience = 1;
        $userProfile->save();

        return $experience;
    }

    public function show($experience_id)
    {
        $experience = Experience::find($experience_id);

        $experience->start_date_format = date("Y-m-d", strtotime($experience->start_date));
        if($experience->end_date)
            $experience->end_date_format = date("Y-m-d", strtotime($experience->end_date));
        else
            $experience->end_date_format = NULL;

        return response()->json($experience);
    }

    public function update(Request $request, $experience_id)
    {
        $rules = [
            'title' => 'required',
            'company' => 'required',
            'description' => 'nullable',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'nullable|date_format:Y-m-d'
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título',
            'company.required' => 'Es necesario ingresar el nombre de la empresa',
            'start_date.required' => 'Es necesario ingresar la fecha de inicio',
            'start_date.date_format' => 'La fecha de inicio no tiene el formato correcto',
            'end_date.date_format' => 'La fecha de fin no tiene el formato correcto'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $experience = Experience::find($experience_id);
        $experience->title = $request->input('title');
        $experience->company = $request->input('company');
        $experience->description = $request->input('description');
        $experience->start_date = $request->input('start_date');
        $experience->end_date = $request->input('end_date');
        $experience->save();

        if ($experience->end_date)
            $experience->date_interval = $experience->start_date->format('F Y').' - '.$experience->end_date->format('F Y');
        else
            $experience->date_interval = $experience->start_date->format('F Y').' - To Date';

        return $experience;
    }

    public function delete($experience_id)
    {
        $experience = Experience::find($experience_id);
        $experience->delete();

        $experienceCount = Experience::where('user_id', auth()->id())->count();
        if ($experienceCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_experience = 0;
            $userProfile->save();
        }

        return response()->json();
    }
}
