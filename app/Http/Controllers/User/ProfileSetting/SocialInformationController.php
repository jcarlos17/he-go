<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\SocialNetwork;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SocialInformationController extends Controller
{
    public function update(Request $request, $id)
    {
        $rules = [
            'facebooklink' => 'nullable|active_url',
            'twitterlink' => 'nullable|active_url',
            'linkedinlink' => 'nullable|active_url',
            'skypelink' => 'nullable|active_url',
            'googlepluslink' => 'nullable|active_url',
            'pinterestlink' => 'nullable|active_url',

        ];
        $messages = [
            'facebooklink.active_url' => 'El link de Facebook no tiene el formato correcto',
            'twitterlink.active_url' => 'El link de Twitter no tiene el formato correcto',
            'linkedinlink.active_url' => 'El link de Linkedin no tiene el formato correcto',
            'skypelink.active_url' => 'El link de Skype no tiene el formato correcto',
            'googlepluslink.active_url' => 'El link de Google Plus no tiene el formato correcto',
            'pinterestlink.active_url' => 'El link de Pinterest no tiene el formato correcto'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $userSocialLink = SocialNetwork::where('user_id',$id)->first();
        if (! $userSocialLink)
            $userSocialLink = new SocialNetwork();

        $userSocialLink->facebooklink = $request->input('facebooklink');
        $userSocialLink->twitterlink = $request->input('twitterlink');
        $userSocialLink->linkedinlink = $request->input('linkedinlink');
        $userSocialLink->skypelink = $request->input('skypelink');
        $userSocialLink->googlepluslink = $request->input('googlepluslink');
        $userSocialLink->pinterestlink = $request->input('pinterestlink');
        $userSocialLink->user_id = $id;
        $userSocialLink->save();

        $count = $this->getCountSocial($userSocialLink);
        $userProfile = UserProfile::where('user_id', $id)->first();
        if($count >= 3)
            $userProfile->has_three_social_network = 1;
        else
            $userProfile->has_three_social_network = 0;

        $userProfile->save();

        return response()->json($userSocialLink);
    }

    public function getCountSocial($user)
    {
        $count = 0;
        if ($user->facebooklink){$count += 1;}
        if ($user->twitterlink){$count += 1;}
        if ($user->linkedinlink){$count += 1;}
        if ($user->skypelink){$count += 1;}
        if ($user->googlepluslink){$count += 1;}
        if ($user->pinterestlink){$count += 1;}
        return $count;
    }
}
