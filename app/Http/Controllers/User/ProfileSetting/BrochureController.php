<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Brochure;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class BrochureController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'file' => 'required|mimes:doc,docx,pdf,jpg,jpeg,png,svg'
        ];
        $messages = [
            'file.required' => 'Es necesario ingresar un archivo',
            'file.mimes' => 'El archivo debe ser formato doc, docx, pdf, jpg, jpeg, png, svg'
        ];

        $archive = $request->file('file');
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $extension = $archive->getClientOriginalExtension();
        $archiveName = uniqid().'.'.$extension;
        $path = public_path().'/images/brochureFiles';

        $archive->move($path, $archiveName);

        $archive = new Brochure();
        $archive->file = $archiveName;
        $archive->user_id = $id;
        $archive->save();

        $userProfile = UserProfile::where('user_id', $id)->first();
        $userProfile->has_brochure = 1;
        $userProfile->save();

        $archive->brochure_url = $archive->getBrochureUrlAttribute();
        $archive->view_url = $archive->getViewUrlAttribute();

        return $archive;
    }

    public function delete($brochure_id)
    {
        $brochure = Brochure::find($brochure_id);
        $pathDelete = public_path('/images/brochureFiles/' . $brochure->file);
        File::delete($pathDelete);
        $brochure->delete();

        $brochureCount = Brochure::where('user_id', auth()->id())->count();
        if ($brochureCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_brochure = 0;
            $userProfile->save();
        }

        return response()->json();
    }
}
