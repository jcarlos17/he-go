<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BasicInformationController extends Controller
{
    public function update(Request $request, $id)
    {
        $rules = [
            'website' => 'nullable|active_url',
            'email' => 'required|email|unique:users,email,'.$id,
            'identity_type' => 'required_with:identity_document',
            'identity_document' => 'required_with:identity_type',
        ];
        $messages = [
            'identity_type.required_with' => 'Es necesario seleccionar un tipo de documento si ingresó un documento de identificación',
            'identity_document.required_with' => 'El documento de identificación es obligatorio si hay un tipo de documento seleccionado',
            'website.active_url' => 'El sitio web ingresado no tiene el formato correcto',
            'email.required' => 'Es necesario ingresar un correo',
            'email.email' => 'El correo no tiene el formato correcto',
            'email.unique' => 'El correo ya existe en nuestros registros'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $user = User::find($id);
        $user->name = $request->input('name');
        $user->last_name = $request->input('last_name');
        $user->phone = $request->input('phone');
        $user->mobile = $request->input('mobile');
        $user->company_name = $request->input('company_name');
        if($user->is_admin)
            $user->email = $request->email;
        $user->website = $request->input('website');
        $user->identity_type = $request->input('identity_type');
        $user->identity_document = $request->input('identity_document');
        $user->save();

        $userProfile = UserProfile::where('user_id', auth()->id())->first();
        if ($user->name && $user->last_name && $user->phone && $user->mobile && $user->website && $user->identity_type && $user->identity_document)
            $userProfile->has_complete_basic_information = 1;
        else
            $userProfile->has_complete_basic_information = 0;

        $userProfile->save();

        return response()->json($user);
    }
}
