<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'description' => 'required',
            'department_id' => 'required',
            'province_id' => 'required',
            'district_id' => 'nullable',
        ];
        $messages = [
            'description.required' => 'Es necesario ingresar una dirección',
            'department_id.required' => 'Es necesario seleccionar un departamento',
            'province_id.required' => 'Es necesario seleccionar una provincia',
        ];

        $addressInput = $request->input('description');
        $departmentId = $request->input('department_id');
        $provinceId = $request->input('province_id');
        $districtId = $request->input('district_id');

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($addressInput, $id) {
            $addressExists = Address::where('description', $addressInput)->where('user_id', $id)->exists();
            if ($addressExists) {
                $validator->errors()->add('description', 'La dirección ingresada ya se encuentra registrado');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $address = new Address();
        $address->description = $addressInput;
        $address->department_id = $departmentId;
        $address->province_id = $provinceId;
        $address->district_id = $districtId;
        $address->user_id = $id;
        $address->save();

        $address->description_complete = $address->getDescriptionCompleteAttribute();

        return response()->json($address);
    }

    public function delete($address_id)
    {
        $address = Address::find($address_id);
        $address->delete();
        return response()->json();
    }
}
