<?php

namespace App\Http\Controllers\User\ProfileSetting;

use App\Certificate;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class CertificateController extends Controller
{
    public function add(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'description' => 'nullable',
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título del certificado o premio',
        ];

        $fileExists = $request->hasFile('file');

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->after(function($validator) use ($fileExists) {
            if (! $fileExists) {
                $validator->errors()->add('file', 'Es necesario ingresar una imagen del certificado o premio');
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $file_name = uniqid() . '.' . $extension;

        $path = public_path('images/certificates/' . $file_name);

        Image::make($file)
            ->fit(170, 170)
            ->save($path);

        $certificate = new Certificate();
        $certificate->title = $request->input('title');
        $certificate->description = $request->input('description');
        $certificate->user_id = $id;
        $certificate->image = $file_name;
        $certificate->save();

        $certificate->date = $certificate->created_at->toFormattedDateString();
        $certificate->image_url = $certificate->getImageUrlAttribute();

        $userProfile = UserProfile::where('user_id', $id)->first();
        $userProfile->has_certificate_or_prize = 1;
        $userProfile->save();

        return $certificate;
    }

    public function show($certificate_id)
    {
        $certificate = Certificate::find($certificate_id);
        $certificate->date = $certificate->getDateAttribute();
        $certificate->image_url = $certificate->getImageUrlAttribute();
        return response()->json($certificate);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'description' => 'nullable',
            'file' => 'nullable',
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título del certificado o premio',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $certificate = Certificate::find($id);

        if ($request->hasFile('file')){
            $pathDelete = public_path('/images/certificates/' . $certificate->image);
            File::delete($pathDelete);

            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $file_name = uniqid() . '.' . $extension;

            $path = public_path('images/certificates/' . $file_name);

            Image::make($file)
                ->fit(170, 170)
                ->save($path);

            $certificate->image = $file_name;
        }
        $certificate->title = $request->input('title');
        $certificate->description = $request->input('description');
        $certificate->save();
        $certificate->date = $certificate->getDateAttribute();
        $certificate->image_url = $certificate->getImageUrlAttribute();

        return $certificate;
    }

    public function delete($certificate_id)
    {
        $certificate = Certificate::find($certificate_id);
        $pathDelete = public_path('/images/certificates/' . $certificate->image);
        File::delete($pathDelete);
        $certificate->delete();

        $certificateCount = Certificate::where('user_id', auth()->id())->count();
        if ($certificateCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_certificate_or_prize = 0;
            $userProfile->save();
        }

        return response()->json();
    }
}
