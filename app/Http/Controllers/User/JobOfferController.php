<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Department;
use App\OfferJob;
use App\OfferJobBenefit;
use App\OfferJobImage;
use App\OfferPause;
use App\Proposal;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class JobOfferController extends Controller
{
    public function index(Request $request)
    {
        $status = $request->status;
        $query = OfferJob::where('user_id', auth()->id());

        if(strlen($status) > 0)
            $query = $query->where('status', $status);

        $offerJobs = $query->orderBy('created_at', 'desc')->get();

        return view('job-offer.index', compact('offerJobs', 'status'));
    }

    public function default()
    {
        $offerJob = new OfferJob();
        $offerJob->user_id = auth()->id();
        $offerJob->save();

        return response()->json($offerJob);
    }

    public function create($id)
    {
        $user = auth()->user();
        $offer = OfferJob::find($id);
        $departments = Department::all();
        $categories = Category::where('category_id', NULL)->get();
        return view('job-offer.create', compact('user', 'offer', 'departments', 'categories'));
    }

    public function store($id, Request $request)
    {
        if ($request->save) {
            $rules = [
                'title' => 'required|string|min:5|max:30',
                'expire_date' => 'required',
                'type' => 'required',
                'description' => 'required|min:250|max:2500',
                'abilities' => 'required|min:250|max:2500',
                'category_id' => 'required',
                'subcategory_id' => 'required',
                'priority_type' => 'required',
                'schedule' => 'required',
                'job_type' => 'required',
                'geographical_mobility' => 'required',
                'experience' => 'required',
                'language' => 'required',
                'department_id' => 'required',
                'province_id' => 'required',
                'district_id' => 'required_unless:department_id,7',
                'suggested_payment' => 'required',
            ];
            $messages = [
                'title.required' => 'Es necesario ingresar el título de la oferta de trabajo.',
                'title.min' => 'El título debe tener como mínimo 5 caracteres.',
                'title.max' => 'El título debe tener como máximo 30 caracteres.',
                'type.required' => 'Es necesario seleccionar una opción.',
                'expire_date.required' => 'Es necesario ingresar una fecha de caducidad.',
                'description.required' => 'Es necesario ingresar la descripción.',
                'description.min' => 'La descripción debe tener como mínimo 250 caracteres.',
                'description.max' => 'La descripción debe tener como máximo 2500 caracteres.',
                'abilities.required' => 'Es necesario ingresar las habilidades del prestador.',
                'abilities.min' => 'Las habilidades del prestador debe tener como mínimo 250 caracteres.',
                'abilities.max' => 'Las habilidades del prestador debe tener como máximo 2500 caracteres.',
                'category_id.required' => 'Es necesario seleccionar una categoria profesional.',
                'subcategory_id.required' => 'Es necesario seleccionar una subcategoria profesional.',
                'priority_type.required' => 'Es necesario seleccionar un tipo de prioridad.',
                'schedule.required' => 'Es necesario seleccionar un horario preferible.',
                'job_type.required' => 'Es necesario seleccionar un tipo de empleo.',
                'geographical_mobility.required' => 'Es necesario seleccionar una movilidad geográfica.',
                'experience.required' => 'Es necesario seleccionar una experiencia.',
                'language.required' => 'Es necesario seleccionar un idioma preferible.',
                'department_id.required' => 'Es necesario seleccionar un departamento.',
                'province_id.required' => 'Es necesario seleccionar una provincia.',
                'district_id.required_unless' => 'Es necesario seleccionar un distrito.',
                'suggested_payment.required' => 'Es necesario seleccionar un pago sugerido.',

            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails())
                return response()->json($validator->errors(), 422);

            $validator->after(function($validator) use ($request) {
                $request->expire_date;
                $dateToday = Carbon::today();
                $maxDateExpired = Carbon::today()->addDays(30);

                if ($request->expire_date < $dateToday) {
                    $validator->errors()->add('expire_date', 'La fecha de caducidad debe ser igual o posterior a la fecha actual');
                }
                if ($request->expire_date > $maxDateExpired) {
                    $validator->errors()->add('expire_date', 'La máxima duración de la oferta es de 30 días');
                }
            });

            if ($validator->fails())
                return response()->json($validator->errors(), 422);
        }

        $offerJob = OfferJob::find($id);
        $offerJob->type = $request->type;
        $offerJob->title = $request->title;
        $offerJob->expire_date = $request->expire_date;
        $offerJob->description = $request->description;
        $offerJob->abilities = $request->abilities;
        $offerJob->attached_images = $request->attached_images == 'on' ? 1 : 0;
        $offerJob->category_id = $request->category_id;
        $offerJob->subcategory_id = $request->subcategory_id;
        $offerJob->priority_type = $request->priority_type;
        $offerJob->schedule = $request->schedule;
        $offerJob->job_type = $request->job_type;
        $offerJob->geographical_mobility = $request->geographical_mobility;
        $offerJob->experience = $request->experience;
        $offerJob->language = $request->language;
        $offerJob->department_id = $request->department_id;
        $offerJob->province_id = $request->province_id;
        $offerJob->district_id = $request->district_id;
        $offerJob->suggested_payment = $request->suggested_payment;
        $offerJob->status = $request->save ? 1 : 0;

        $limit = $request->limit;
        if($limit)
            $offerJob->limit = $limit;

        $offerJob->save();

        return response()->json($offerJob);
    }

    public function edit($id)
    {
        $user = auth()->user();
        $offer = OfferJob::find($id);
        $departments = Department::all();
        $categories = Category::where('category_id', NULL)->get();
        return view('job-offer.edit', compact('user', 'offer', 'departments', 'categories'));
    }

    public function update($id, Request $request)
    {
        if ($request->save) {
            $rules = [
                'title' => 'required|string|min:5|max:30',
                'type' => 'required',
                'expire_date' => 'required',
                'description' => 'required|min:250|max:2500',
                'abilities' => 'required|min:250|max:2500',
                'category_id' => 'required',
                'subcategory_id' => 'required',
                'priority_type' => 'required',
                'schedule' => 'required',
                'job_type' => 'required',
                'geographical_mobility' => 'required',
                'experience' => 'required',
                'language' => 'required',
                'department_id' => 'required',
                'province_id' => 'required',
                'district_id' => 'required_unless:department_id,7',
                'suggested_payment' => 'required',
            ];
            $messages = [
                'title.required' => 'Es necesario ingresar el título de la oferta de trabajo.',
                'title.min' => 'El título debe tener como mínimo 5 caracteres.',
                'title.max' => 'El título debe tener como máximo 30 caracteres.',
                'type.required' => 'Es necesario seleccionar una opción.',
                'expire_date.required' => 'Es necesario ingresar una fecha de caducidad.',
                'description.required' => 'Es necesario ingresar la descripción.',
                'description.min' => 'La descripción debe tener como mínimo 250 caracteres.',
                'description.max' => 'La descripción debe tener como máximo 2500 caracteres.',
                'abilities.required' => 'Es necesario ingresar las habilidades del prestador.',
                'abilities.min' => 'Las habilidades del prestador debe tener como mínimo 250 caracteres.',
                'abilities.max' => 'Las habilidades del prestador debe tener como máximo 2500 caracteres.',
                'category_id.required' => 'Es necesario seleccionar una categoria profesional.',
                'subcategory_id.required' => 'Es necesario seleccionar una subcategoria profesional.',
                'priority_type.required' => 'Es necesario seleccionar un tipo de prioridad.',
                'schedule.required' => 'Es necesario seleccionar un horario preferible.',
                'job_type.required' => 'Es necesario seleccionar un tipo de empleo.',
                'geographical_mobility.required' => 'Es necesario seleccionar una movilidad geográfica.',
                'experience.required' => 'Es necesario seleccionar una experiencia.',
                'language.required' => 'Es necesario seleccionar un idioma preferible.',
                'department_id.required' => 'Es necesario seleccionar un departamento.',
                'province_id.required' => 'Es necesario seleccionar una provincia.',
                'district_id.required_unless' => 'Es necesario seleccionar un distrito.',
                'suggested_payment.required' => 'Es necesario seleccionar un pago sugerido.',

            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails())
                return response()->json($validator->errors(), 422);

            $validator->after(function($validator) use ($request) {
                $dateToday = Carbon::today();
                $maxDateExpired = Carbon::today()->addDays(30);

                if ($request->expire_date < $dateToday) {
                    $validator->errors()->add('expire_date', 'La fecha de caducidad debe ser igual o posterior a la fecha actual');
                }
                if ($request->expire_date > $maxDateExpired) {
                    $validator->errors()->add('expire_date', 'La máxima duración de la oferta es de 30 días');
                }
            });

            if ($validator->fails())
                return response()->json($validator->errors(), 422);
        }

        $offerJob = OfferJob::find($id);
        $offerJob->type = $request->type;
        $offerJob->title = $request->title;
        $offerJob->expire_date = $request->expire_date;
        $offerJob->description = $request->description;
        $offerJob->abilities = $request->abilities;
        $attachedImages = $request->attached_images == 'on' ? 1 : 0;
        $offerJob->attached_images = $attachedImages;
        $offerJob->category_id = $request->category_id;
        $offerJob->subcategory_id = $request->subcategory_id;
        $offerJob->priority_type = $request->priority_type;
        $offerJob->schedule = $request->schedule;
        $offerJob->job_type = $request->job_type;
        $offerJob->geographical_mobility = $request->geographical_mobility;
        $offerJob->experience = $request->experience;
        $offerJob->language = $request->language;
        $offerJob->department_id = $request->department_id;
        $offerJob->province_id = $request->province_id;
        $offerJob->district_id = $request->district_id;
        $offerJob->suggested_payment = $request->suggested_payment;
        $offerJob->status = $request->save ? 1 : 0;

        $limit = $request->limit;
        if($limit)
            $offerJob->limit = $limit;

        $offerJob->save();

        return response()->json($offerJob);
    }

    public function detail($id)
    {
        $offerJob = OfferJob::find($id);

        //
        $firstCategory = $offerJob->user->categories()->where('categories.category_id', NULL)->first();
        if ($firstCategory){
            $fisrtSubcategory = $offerJob->user->categories()->where('categories.category_id', $firstCategory->id)->first();
            $catSubcat = $fisrtSubcategory->name.' / '.$firstCategory->name;
        }else{
            $catSubcat = '';
        }
        $firstAddres = $offerJob->user->addresses()->first();
        if ($firstAddres){
            $location = $firstAddres->province->name.' / '.$firstAddres->department->name;
        }else{
            $location = '';
        }
        //
        $proposalExists = Proposal::where('from_id', auth()->id())->where('offer_job_id', $id)->exists();

        return view('job-offer.detail', compact('offerJob', 'location', 'catSubcat', 'proposalExists'));
    }

    public function stream($id)
    {
        $offerJob = OfferJob::findOrFail($id);

        $pdf = PDF::loadView('job-offer.pdf', ['offerJob' =>$offerJob])
            ->setPaper('a4');

        return $pdf->stream($offerJob->id.'.pdf');
    }

    public function pause($id, Request $request)
    {
        $offerJob = OfferJob::find($id);
        $offerJob->status = 3;
        $offerJob->save();

        $offerPause = new OfferPause();
        $offerPause->duration = $request->duration;
        $offerPause->end_pause_date = Carbon::now()->addDays($request->duration);
        $offerPause->offer_job_id = $id;
        $offerPause->save();

        return back();
    }

    public function showPause($id)
    {
        $offerJob = OfferJob::find($id);
        $offerPause = $offerJob->pause;
        $date = $offerPause->end_pause_date;

        $offerPause->year = $date->year;
        $offerPause->month = $date->month;
        $offerPause->day = $date->day;
        $offerPause->hour = $date->hour;
        $offerPause->minute = $date->minute;
        $offerPause->second = $date->second;

        return response()->json($offerPause);
    }

    public function restore($id)
    {
        $offerJob = OfferJob::find($id);
        $offerJob->status = 1;
        $offerJob->save();

        $offerJob->pause()->delete();

        return back();
    }

    public function delete($id)
    {
        $offerJob = OfferJob::find($id);
        $offerJob->images()->delete();
        $offerJob->benefits()->delete();
        $offerJob->proposals()->delete();
        $offerJob->pause()->delete();
        $offerJob->delete();

        return back();
    }

    public function addBenefit(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar un beneficio',
        ];

        $name = $request->name;

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($name, $id) {
            $addressExists = OfferJobBenefit::where('name', $name)->where('offer_job_id', $id)->exists();
            if ($addressExists) {
                $validator->errors()->add('name', 'El beneficio ingresado ya se encuentra registrado');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $benefit = new OfferJobBenefit();
        $benefit->name = $name;
        $benefit->offer_job_id = $id;
        $benefit->save();

        return response()->json($benefit);
    }

    public function deleteBenefit($benefit_id)
    {
        $benefit = OfferJobBenefit::find($benefit_id);
        $benefit->delete();
        return response()->json();
    }

    public function addImage(Request $request, $id)
    {
        dd($request->all());
        $rules = [
            'image' => 'nullable|image'
        ];
        $messages = [
            'image.image' => 'El archivo debe ser formato imagen'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $offer = OfferJob::find($id);
        $validator->after(function($validator) use ($offer) {
            $countPhotos = $offer->images()->count();
            if ($countPhotos >= 7) {
                $validator->errors()->add('image', 'Se acepta un máximo de 7 imágenes');
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $inputImage = $request->file('image');
        $extension = $inputImage->getClientOriginalExtension();
        $file_name = uniqid() . '.' . $extension;

        $path = public_path('images/offer-job/' . $file_name);

        Image::make($inputImage)
            ->fit(100, 100)
            ->save($path);

        $image = new OfferJobImage();
        $image->image = $file_name;
        $image->offer_job_id = $id;
        $image->save();

        $image->image_url = $image->getImageUrlAttribute();

        return $image;
    }

    public function selectImage($offer_id, $id)
    {
        $offer = OfferJob::find($offer_id);
        $offer->images()->update(['selected' => 0]);

        $imageSelected = OfferJobImage::find($id);
        $imageSelected->selected = 1;
        $imageSelected->save();

        return response()->json();
    }

    public function deleteImage($image_id)
    {
        $image = OfferJobImage::find($image_id);
        $pathDelete = public_path('/images/offer-job/' . $image->image);
        File::delete($pathDelete);
        $image->delete();

        return response()->json();
    }
}
