<?php

namespace App\Http\Controllers\User;

use App\Opinion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OpinionController extends Controller
{
    public function send($providerId, $userId, Request $request)
    {
        $rules = [
            'description' => 'required',
        ];
        $messages = [
            'description.required' => 'Es necesario ingresar una descripción',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $opinion = new Opinion();
        $opinion->title = $request->title;
        $opinion->description = $request->description;
        $opinion->user_id = $userId;
        $opinion->provider_id = $providerId;
        $opinion->save();

        return $opinion;
    }
}
