<?php

namespace App\Http\Controllers\User;

use App\Chat;
use App\Conversation;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pusher;
use function Sodium\compare;

class ChatController extends Controller
{
    public function show()
    {
        $chats = Chat::where('from_id', auth()->id())->orWhere('to_id', auth()->id())->get();

        $firstChat = Chat::where('from_id', auth()->id())->orWhere('to_id', auth()->id())->orderBy('created_at','desc')->first();

        $lastId = [];
        if($firstChat){
            $lastId = $firstChat->id;
    }

        return view('user.chat', compact('chats', 'lastId'));
    }

    public function store(Request $request)
    {
        $fromId = auth()->id();
        $toId = $request->to_id;
        if($fromId == $toId)
            return back();

        $firstChat = Chat::whereIn('from_id', [$fromId,$toId])->WhereIn('to_id', [$fromId,$toId])->first();

        if($firstChat){
            $chatId = $firstChat->id;
            return redirect('/chat')->with(compact('chatId'));
        }

        $chat = new Chat();
        $chat->from_id = $fromId;
        $chat->to_id = $toId;
        $chat->save();

        $chatId = $chat->id;

        return redirect('/chat')->with(compact('chatId'));
    }

    public function conversation($id)
    {
        $chat = Chat::find($id);

        $chat->conversations;

        foreach ($chat->conversations as $conversation)
        {
            $conversation->user_photo_selected = $conversation->user->photo_selected;
            $conversation->user_name = $conversation->user->name;
            $conversation->user_auth = $conversation->user_id == auth()->id() ? 1 : 0;
            $conversation->date = $conversation->getDateAttribute();
        }

        $chat->contact_name = $chat->contact->name;
        $chat->contact_phone = $chat->contact->phone ? $chat->contact->phone : '';
        $chat->contact_location = $chat->contact->location_complete;
        $chat->contact_photo_selected = $chat->contact->photo_selected;

        return $chat;
    }

    public function message(Request $request, $id)
    {
        $message = new Conversation();
        $message->message = $request->message;
        $message->user_id = auth()->id();
        $message->chat_id = $id;
        $message->save();

        $message->date = $message->getDateAttribute();

//        event(new MessageSent($message));
        $options = array(
            'cluster' => 'us2',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            '01bb61e5ad5bc975923f',
            '75b7da328e4a84ac7070',
            '623514',
            $options
        );

        $pusher->trigger('my-channel', 'my-event', $message);
        return $message;
    }
}
