<?php

namespace App\Http\Controllers\User;

use App\Mail\ProposalAccepted;
use App\Mail\ProposalRejected;
use App\OfferJob;
use App\Proposal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ProposalReceivedController extends Controller
{
    public function index()
    {
        $proposals = Proposal::where('to_id', auth()->id())->get();
        return view('proposal-received.index', compact('proposals'));
    }

    public function show($id)
    {
        $proposal = Proposal::find($id);
        //
        $offer = $proposal->offer;
        if($offer->status == 1) {
            $offer->status = 2;
            $offer->save();
        }
        //
        $fisrtCategory = $proposal->from->categories()->where('categories.category_id', null)->first();
        if ($fisrtCategory){
            $fisrtSubcategory = $proposal->from->categories()->where('categories.category_id', $fisrtCategory->id)->first();
            $catSubcat = $fisrtSubcategory->name.' / '.$fisrtCategory->name;
        }else{
            $catSubcat = '';
        }
        $firstAddres = $proposal->from->addresses()->first();
        if ($firstAddres){
            $location = $firstAddres->province->name.' / '.$firstAddres->department->name;
        }else{
            $location = '';
        }
        //
        $proposal->from_photo_selected = $proposal->from->photo_selected;
        $proposal->from_name = $proposal->from->name;
        $proposal->cat_subcat = $catSubcat;
        $proposal->location = $location;
        $proposal->send_date = $proposal->getSendDateAttribute();
        $proposal->offer_title = $proposal->offer->title;
        $proposal->offer_description = $proposal->offer->description;
        $proposal->geographical_mobility = $proposal->geographical_mobility == 1 ? 'Sí' : 'No';
        $proposal->necessary_material = $proposal->necessary_material == 1 ? 'Sí' : 'No';
        $proposal->minimum_guarantee = $proposal->minimum_guarantee == 1 ? 'Sí' : 'No';
        $proposal->insurance = $proposal->insurance == 1 ? 'Sí' : 'No';
        $proposal->personal_interview = $proposal->personal_interview == 1 ? 'checked' : '';
        return $proposal;
    }

    public function reject($id, Request $request)
    {
        $proposal = Proposal::find($id);
        $proposal->status = 'Rechazada';
        $proposal->reject_reason = $request->reject_reason;
        $proposal->save();

        //Proposal rejected
        $from = $proposal->from;
        Mail::to($from)->send(new ProposalRejected($proposal));

        return redirect('/proposal-received');
    }

    public function accept($id)
    {
        $proposal = Proposal::find($id);
        $proposal->status = 'Aceptada';
        $proposal->save();

        $offer = OfferJob::find($proposal->offer_job_id);
        $offer->status = 5;
        $offer->save();

        //Proposal accepted
        $from = $proposal->from;
        Mail::to($from)->send(new ProposalAccepted($proposal));

        return redirect('/proposal-received');
    }
}
