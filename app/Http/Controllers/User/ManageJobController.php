<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageJobController extends Controller
{
    public function show()
    {
        return view('user.manage-job');
    }
}
