<?php

namespace App\Http\Controllers\User;

use App\UserFavorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavouriteListingController extends Controller
{
    public function add($favorite_id)
    {
        $favorite = new UserFavorite();
        $favorite->user_favorite_id = $favorite_id;
        $favorite->user_id = auth()->id();
        $favorite->save();

        return $favorite;
    }
    public function show()
    {
        $user = auth()->user();

        return view('user.favourite-listing', compact('user'));
    }

    public function delete($id)
    {
        $favorite = UserFavorite::find($id);
        $favorite->delete();

        return $favorite;
    }
}
