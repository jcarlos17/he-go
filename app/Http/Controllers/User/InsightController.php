<?php

namespace App\Http\Controllers\User;

use App\AdminImage;
use App\UserTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InsightController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $imgTopInsight = AdminImage::where('type', 'imgTopInsight')->first();
        $imgTopInsightBS = AdminImage::where('type', 'imgTopInsightBS')->first();
        return view('user.insight', compact('user', 'imgTopInsight', 'imgTopInsightBS'));
    }

    public function add(Request $request)
    {
        $rules = [
            'description' => 'required',
        ];
        $messages = [
            'description.required' => 'Es necesario ingresar una descripción',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $description = $request->description;

        $task = new UserTask();
        $task->user_id = auth()->id();
        $task->description = $description;
        $task->save();

        return response()->json($task);
    }

    public function completed(Request $request, $id)
    {
        $checked = $request->checked;

        $taskCompleted = UserTask::find($id);
        if ($checked == 'true')
            $taskCompleted->date_completed = Carbon::now();
        else
            $taskCompleted->date_completed = NULL;

        $taskCompleted->save();

        return response()->json($taskCompleted);
    }

    public function delete($id)
    {
        $task = UserTask::find($id);
        $task->delete();

        return response()->json();
    }
}
