<?php

namespace App\Http\Controllers\User;

use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        if($user->is_seeker){
            $appointments = Appointment::where('interested_id', auth()->id())->orWhere('provider_id', auth()->id())->orderBy('id', 'desc')->get();
        }else{
            $appointments = Appointment::where('option', 'Reservar cita')->where(function($query){
                    $query->where('interested_id', auth()->id())->orWhere('provider_id', auth()->id());
                })->orderBy('id', 'desc')->get();
        }
        return view('user.appointment', compact('appointments'));
    }

    public function show($id)
    {
        $appointment = Appointment::find($id);

        $appointment->interested_name = $appointment->interested->name;
        $appointment->service_title = $appointment->service->title;

        return $appointment;
    }
}
