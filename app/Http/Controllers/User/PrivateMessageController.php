<?php

namespace App\Http\Controllers\User;

use App\InternalMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivateMessageController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $messages = $user->internalMessages;

        return view('user.private-message', compact('messages'));
    }

    public function show($id)
    {
        $message = InternalMessage::find($id);

        return response()->json(view('user.include.detail_message',compact('message'))->render());

    }
}
