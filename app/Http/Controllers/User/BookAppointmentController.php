<?php

namespace App\Http\Controllers\User;

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookAppointmentController extends Controller
{
    public function store($id, Request $request)
    {
        $option = $request->option;
        if($option === 'Service')
            $optionDescription = 'Contratar un servicio';
        else
            $optionDescription = 'Reservar cita';

        $date = Carbon::createFromFormat('d/m/Y', $request->date);
        $time = $request->time;
        $service = $request->service;
        $information = $request->information;

        $appointment = new Appointment();
        $appointment->date = $date;
        $appointment->time = $time;
        $appointment->option = $optionDescription;
        $appointment->information = $information;

        $appointment->service_id = $service;
        $appointment->interested_id = auth()->id();
        $appointment->provider_id = $id;
        $appointment->save();

        return $appointment;
    }
}
