<?php

namespace App\Http\Controllers\User;

use App\User;
use App\UserDeactivated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SecuritySettingController extends Controller
{
    public function show()
    {
        return view('user.security-setting');
    }

    public function update(Request $request)
    {
        $rules =[
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ];

        $messages=[
            'current_password.required' => 'Es necesario ingresar la contraseña actual',
            'password.required' => 'Es necesario ingresar la nueva contraseña',
            'password.confirmed' => 'La confirmación de contraseña no coincide',
            'password.min' => 'La contraseña debe ser como mínimo 6 letras',
        ];
        $user = auth()->user();

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function($validator) use ($request, $user) {
            if (! password_verify($request->current_password, $user->password)) {
                $validator->errors()->add('password_confirmation', 'La contraseña actual no coincide');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json($user);
    }

    public function deactivated(Request $request, $id)
    {
        $rules =[
            'password' => 'required|confirmed',
            'reason' => 'required',
            'justification' => 'required|min:200'
        ];

        $messages=[
            'reason.required' => 'Es necesario seleccionar un motivo del ¿por qué te vas?',
            'justification.required' => 'Es necesario ingresar una justificación',
            'justification.min' => 'La justificación debe tener como mínimo 200 caracteres',
            'password.required' => 'Es necesario ingresar la contraseña actual',
            'password.confirmed' => 'La confirmación de contraseña no coincide',
        ];

        $user = User::find($id);
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $validator->after(function($validator) use ($request, $user) {
            if (! password_verify($request->password, $user->password)) {
                $validator->errors()->add('password_confirmation', 'La contraseña actual no coincide');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $userDeactivated = new UserDeactivated();
        $userDeactivated->reason = $request->reason;
        $userDeactivated->justification = $request->justification;
        $userDeactivated->user_id = $id;
        $userDeactivated->save();

        $user->delete();

        return response()->json();
    }
}
