<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function show(Request $request)
    {
        $queryText = $request->input('queryText');

        if ($queryText && strlen($queryText) >= 3)
            return Category::whereNotNull('category_id')->where('name', 'like', "%$queryText%")->get([
                'id', 'name as text'
            ]);
        else
            return Category::whereNotNull('category_id')->orderBy('name')->take(0)->get([
                'id', 'name as text'
            ]);
    }

    public function showCategory($id)
    {
        $subcategory = Category::find($id);
        $category = Category::find($subcategory->category_id);

        return $category;
    }
}
