<?php

namespace App\Http\Controllers\Provider;

use App\Mail\SendInvitation;
use App\ProviderMember;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ManageTeamController extends Controller
{
    public function show()
    {
        $memberIds = ProviderMember::where('provider_id', auth()->id())->pluck('member_id');

        $members = User::whereIn('id', $memberIds)->get();

        return view('provider.manage-team', compact('members'));
    }

    public function search(Request $request)
    {
        $searchMember = $request->searchmember;
        $memberIds = ProviderMember::where('provider_id', auth()->id())->pluck('member_id');

        $query = User::whereIn('id', $memberIds);

        if ($searchMember) {// when search by date
            $query = $query->where('name','LIKE', '%'.$searchMember.'%')
                ->orWhere('email','LIKE', '%'.$searchMember.'%')
                ->orWhere('id','LIKE', '%'.$searchMember.'%');
        }
        $members = $query->get();

        foreach ($members as $member)
            $member->photo_selected = $member->getPhotoSelectedAttribute();


        return $members;
    }

    public function delete($member_id)
    {
        $providerMember = ProviderMember::where('member_id', $member_id)->where('provider_id', auth()->id())->first();
        $providerMember->delete();

        return $providerMember;
    }

    public function invitation(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];
        $messages = [
            'email.required' => 'Es necesario ingresar el email',
            'email.email' => 'El email ingresado no tiene el formato correcto',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $validator->after(function($validator) use ($request) {
            $provider = User::where('email', $request->email)->where('role', 1)->first();
            if (!$provider) {
                $validator->errors()->add('email', 'El email ingresado no se encuentra registrado.');
            }else{
                $memberExists = ProviderMember::where('member_id', $provider->id)->where('provider_id', auth()->id())->exists();
                if ($memberExists){
                    $validator->errors()->add('email', 'El email ingresado ya se encuentra registrado en su red.');
                }
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $provider = User::where('email', $request->email)->where('role', 1)->first();

        $member = new ProviderMember();
        $member->member_id = $provider->id;
        $member->provider_id = auth()->id();
        $member->save();

        $provider->photo_selected = $provider->getPhotoSelectedAttribute();

        return $provider;
    }

    public function invitationEmail(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];
        $messages = [
            'email.required' => 'Es necesario ingresar el email',
            'email.email' => 'El email ingresado no tiene el formato correcto',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $email = $request->email;

        Mail::to($email)->send(new sendInvitation());

        return $email;
    }
}
