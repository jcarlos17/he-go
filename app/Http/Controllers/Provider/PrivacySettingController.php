<?php

namespace App\Http\Controllers\Provider;

use App\PrivacySetting;
use App\UserPrivacySetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivacySettingController extends Controller
{
    public function show()
    {
        $settings = PrivacySetting::all();
        return view('provider.privacy-setting', compact('settings'));
    }

    public function update(Request $request)
    {
        $option = $request->option;
        $checked = $request->checked;

        $status = $checked=='true' ? 1 : 0;

        $userPrivacySettings = UserPrivacySetting::where('user_id', auth()->id())
            ->where('privacy_setting_id', $option)->first();
        if (! $userPrivacySettings){
            $userPrivacySettings = new UserPrivacySetting();
            $userPrivacySettings->user_id = auth()->id();
            $userPrivacySettings->privacy_setting_id = $option;
            $userPrivacySettings->status = $status;
        }else{
            $userPrivacySettings->status = $status;
        }
        $userPrivacySettings->save();

        return response()->json();
    }
}
