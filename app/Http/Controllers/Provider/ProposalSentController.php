<?php

namespace App\Http\Controllers\Provider;

use App\Proposal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProposalSentController extends Controller
{
    public function index()
    {
        $proposals = Proposal::where('from_id', auth()->id())->get();
        return view('provider.proposal-sent.index', compact('proposals'));
    }

    public function show($id)
    {
        $proposal = Proposal::find($id);
        //
        $fisrtCategory = $proposal->to->categories()->where('categories.category_id', null)->first();
        if ($fisrtCategory){
            $fisrtSubcategory = $proposal->to->categories()->where('categories.category_id', $fisrtCategory->id)->first();
            $catSubcat = $fisrtSubcategory->name.' / '.$fisrtCategory->name;
        }else{
            $catSubcat = '';
        }
        $firstAddres = $proposal->to->addresses()->first();
        if ($firstAddres){
            $location = $firstAddres->province->name.' / '.$firstAddres->department->name;
        }else{
            $location = '';
        }
        //
        $proposal->to_photo_selected = $proposal->to->photo_selected;
        $proposal->to_name = $proposal->to->name;
        $proposal->cat_subcat = $catSubcat;
        $proposal->location = $location;
        $proposal->send_date = $proposal->getSendDateAttribute();
        $proposal->offer_title = $proposal->offer->title;
        $proposal->offer_description = $proposal->offer->description;
        $proposal->geographical_mobility = $proposal->geographical_mobility == 1 ? 'Sí' : 'No';
        $proposal->necessary_material = $proposal->necessary_material == 1 ? 'Sí' : 'No';
        $proposal->minimum_guarantee = $proposal->minimum_guarantee == 1 ? 'Sí' : 'No';
        $proposal->insurance = $proposal->insurance == 1 ? 'Sí' : 'No';
        $proposal->personal_interview = $proposal->personal_interview == 1 ? 'checked' : '';
        $proposal->time_elapsed = $proposal->getTimeElapsedAttribute();
        return $proposal;
    }
}
