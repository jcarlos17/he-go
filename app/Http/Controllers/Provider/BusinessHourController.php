<?php

namespace App\Http\Controllers\Provider;

use App\BusinessHour;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BusinessHourController extends Controller
{
    public function show()
    {
        $user = User::find(auth()->id());
        return view('provider.business-hour', compact('user'));
    }

    public function attend(Request $request)
    {
        $day = $request->day;
        $checked = $request->checked;

        $status = $checked=='true' ? 0 : 1;

        $attend = BusinessHour::whereNull('start')
            ->whereNull('end')->where('day', $day)->where('user_id', auth()->id())->first();

        if(!$attend){
            $attend = new BusinessHour();
        }

        $attend->day = $day;
        $attend->status = $status;
        $attend->user_id = auth()->id();
        $attend->save();

        return $attend;
    }

    public function add(Request $request)
    {
        $rules = [
            'start_time' => 'required',
            'end_time' => 'required',
        ];
        $messages = [
            'start_time.required' => 'Es necesario ingresar la hora de inicio correctamente.',
            'end_time.required' => 'Es necesario ingresar la hora de fin correctamente.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $validator->after(function($validator) use ($request) {
            $startInput = $request->start_time;
            $endInput = $request->end_time;
            $dayInput = $request->day;
            $userId = auth()->id();
            if ($request->start_time > $request->end_time) {
                $validator->errors()->add('start_time', 'La hora inicio debe ser menor que la hora fin');
            }

            $hours = BusinessHour::where('user_id', $userId)->where('day', $dayInput)
                ->whereNotNull('start')->whereNotNull('end')->where('status', 1)->get();

            foreach ($hours as $hour){
                $start = $hour->start;
                $end = $hour->end;
                if (($startInput>$start && $startInput<$end) || ($endInput>$start && $endInput<$end) || ($startInput<$start && $endInput>$end)) {
                    $validator->errors()->add('start_time', 'El rango ingresado se cruza con un rango existente');
                }
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $hour = new BusinessHour();
        $hour->day = $request->day;
        $hour->start = $request->start_time;
        $hour->end = $request->end_time;
        $hour->status = 1;
        $hour->user_id = auth()->id();
        $hour->save();

        return $hour;
    }

    public function delete($id)
    {
        $hour = BusinessHour::find($id);
        $hour->delete();

        return $hour;
    }
}
