<?php

namespace App\Http\Controllers\Provider;

use App\IdentityPhoto;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    public function updateDni(Request $request)
    {
        $rules = [
            'file' => 'required|mimes:svg,jpg,png,jpeg,docx,pdf,doc'
        ];
        $messages = [
            'file.required' => 'Es necesario ingresar una imagen',
            'file.mimes' => 'El archivo debe ser formato: jpg, jpeg, png, svg, doc, docx, pdf.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $user = auth()->user();
        $validator->after(function($validator) use ($user) {
            $countPhotos = $user->identities()->count();
            if ($countPhotos >= 10) {
                $validator->errors()->add('file', 'Se acepta un máximo de 10 archivos');
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $inputFile = $request->file('file');
        $extension = $inputFile->getClientOriginalExtension();
        $fileName = uniqid().'.'.$extension;

        $path = public_path().'/images/identityFiles';
        $inputFile->move($path, $fileName);

        $identityPhoto = new IdentityPhoto();
        $identityPhoto->file = $fileName;
        $identityPhoto->type = 'dni';
        $identityPhoto->user_id = $user->id;
        $identityPhoto->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->has_photo_identity_document = 1;
        $userProfile->save();

        $identityPhoto->doc_identity_url = $identityPhoto->getDocIdentityUrlAttribute();
        $identityPhoto->view_doc_url = $identityPhoto->getViewDocUrlAttribute();

        return $identityPhoto;
    }

    public function deleteDni($identity_id)
    {
        $identity = IdentityPhoto::find($identity_id);
        $pathDelete = public_path('/images/identityFiles/' . $identity->file);
        File::delete($pathDelete);
        $identity->delete();

        $photoCount = IdentityPhoto::where('user_id', auth()->id())->count();
        if ($photoCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_photo_identity_document = 0;
            $userProfile->save();
        }

        return response()->json();
    }

    public function updatePs(Request $request)
    {
        $rules = [
            'file' => 'required|mimes:svg,jpg,png,jpeg,docx,pdf,doc'
        ];
        $messages = [
            'file.required' => 'Es necesario ingresar una imagen',
            'file.mimes' => 'El archivo debe ser formato: jpg, jpeg, png, svg, doc, docx, pdf.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $user = auth()->user();
        $validator->after(function($validator) use ($user) {
            $countPhotos = $user->public_services()->count();
            if ($countPhotos >= 10) {
                $validator->errors()->add('file', 'Se acepta un máximo de 10 archivos');
            }
        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);


        $inputFile = $request->file('file');
        $extension = $inputFile->getClientOriginalExtension();
        $fileName = uniqid().'.'.$extension;

        $path = public_path().'/images/identityFiles';
        $inputFile->move($path, $fileName);

        $identityPhoto = new IdentityPhoto();
        $identityPhoto->file = $fileName;
        $identityPhoto->type = 'ps';
        $identityPhoto->user_id = $user->id;
        $identityPhoto->save();

        $userProfile = UserProfile::where('user_id', $user->id)->first();
        $userProfile->has_photo_identity_document = 1;
        $userProfile->save();

        $identityPhoto->doc_identity_url = $identityPhoto->getDocIdentityUrlAttribute();
        $identityPhoto->view_doc_url = $identityPhoto->getViewDocUrlAttribute();

        return $identityPhoto;
    }

    public function deletePs($identity_id)
    {
        $identity = IdentityPhoto::find($identity_id);
        $pathDelete = public_path('/images/identityFiles/' . $identity->file);
        File::delete($pathDelete);
        $identity->delete();

        $photoCount = IdentityPhoto::where('user_id', auth()->id())->count();
        if ($photoCount == 0){
            $userProfile = UserProfile::where('user_id', auth()->id())->first();
            $userProfile->has_photo_identity_document = 0;
            $userProfile->save();
        }

        return response()->json();
    }
}
