<?php

namespace App\Http\Controllers\Provider;

use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    public function index()
    {
        $appointments = Appointment::where('option', 'Contratar un servicio')->where('provider_id', auth()->id())->orderBy('id', 'desc')->get();

        return view('provider.invoice', compact('appointments'));
    }

    public function show($id)
    {
        $appointment = Appointment::find($id);

        $appointment->interested_name = $appointment->interested->name;
        $appointment->service_price = $appointment->service->price;

        return $appointment;
    }
}
