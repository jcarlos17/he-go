<?php

namespace App\Http\Controllers\Provider;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ManageServiceController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        return view('user.manage-service', compact('user'));
    }

    public function showService($id)
    {
        $service = Service::find($id);
        return response()->json($service);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'type_value' => 'required',
            'price' => 'numeric|required_if:type_value,PH|required_if:type_value,HFO|required_if:type_value,PV|nullable',
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título',
            'type_value.required' => 'Seleccione un tipo de valor',
            'description.required' => 'Es necesario ingresar una descripción',
            'price.required_if' => 'Es necesario ingresar un precio'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $service = new Service();
        $service->title = $request->input('title');
        $service->description = $request->input('description');
        $service->type_value = $request->input('type_value');
        if ($request->type_value != 'G')
            $service->price = $request->input('price');

        if ($request->appointment == 1)
            $service->appointment = 1;
        else
            $service->appointment = 0;

        $service->user_id = auth()->id();
        $service->save();

        $service->by_appointment = $service->getByAppointmentAttribute();
        $service->price_value = $service->getPriceValueAttribute();

        return $service;
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'type_value' => 'required',
            'price' => 'numeric|required_if:type_value,PH|required_if:type_value,HFO|required_if:type_value,PV|nullable',
        ];
        $messages = [
            'title.required' => 'Es necesario ingresar el título',
            'type_value.required' => 'Seleccione un tipo de valor',
            'description.required' => 'Es necesario ingresar una descripción',
            'price.required_if' => 'Es necesario ingresar un precio'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $service = Service::find($id);
        $service->title = $request->input('title');
        $service->description = $request->input('description');
        $service->type_value = $request->input('type_value');
        if ($request->type_value != 'G')
            $service->price = $request->input('price');

        if ($request->appointment == 1)
            $service->appointment = 1;
        else
            $service->appointment = 0;

        $service->user_id = auth()->id();
        $service->save();

        $service->by_appointment = $service->getByAppointmentAttribute();
        $service->price_value = $service->getPriceValueAttribute();

        return $service;
    }

    public function delete($service_id)
    {
        $service = Service::find($service_id);
        $service->delete();

        return response()->json();
    }
}
