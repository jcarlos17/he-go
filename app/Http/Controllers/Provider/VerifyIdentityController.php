<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerifyIdentityController extends Controller
{
    public function show()
    {
       return view('provider.verify_identity');
    }
}
