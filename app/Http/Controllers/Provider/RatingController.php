<?php

namespace App\Http\Controllers\Provider;

use App\Mail\InvitationCalification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('provider.request-rating', compact('user'));
    }

    public function sent(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar el nombre',
            'email.required' => 'Es necesario ingresar el correo',
            'email.email' => 'Ingrese un correo válido',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $data = [];
        $data['email'] = $request->email;
        $data['name'] = $request->name;
        $data['id'] = auth()->id();

        Mail::to($data['email'])->send(new InvitationCalification($request->name, auth()->id()));

        return $data;
    }
}
