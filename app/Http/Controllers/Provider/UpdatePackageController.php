<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdatePackageController extends Controller
{
    public function show()
    {
        return view('user.update-package');
    }
}
