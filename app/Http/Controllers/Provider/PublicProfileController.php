<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicProfileController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        return view('public-profile.index', compact('user'));
    }
}
