<?php

namespace App\Http\Controllers\Provider;

use App\Mail\ProposalReceived;
use App\Mail\ProposalSent;
use App\OfferJob;
use App\Proposal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class JobOfferController extends Controller
{
    public function show($id)
    {
        $offerJob = OfferJob::find($id);

        //
        $firstCategory = $offerJob->user->categories()->where('categories.category_id', NULL)->first();
        if ($firstCategory){
            $fisrtSubcategory = $offerJob->user->categories()->where('categories.category_id', $firstCategory->id)->first();
            $catSubcat = $fisrtSubcategory->name.' / '.$firstCategory->name;
        }else{
            $catSubcat = '';
        }
        $firstAddres = $offerJob->user->addresses()->first();
        if ($firstAddres){
            $location = $firstAddres->province->name.' / '.$firstAddres->department->name;
        }else{
            $location = '';
        }
        //
        $proposalExists = Proposal::where('from_id', auth()->id())->where('offer_job_id', $id)->exists();

        return view('provider.offer-job', compact('offerJob', 'location', 'catSubcat', 'proposalExists'));
    }

    public function sentProposal($id, Request $request)
    {
        $rules= [
            'geographical_mobility' => 'required',
            'necessary_material' => 'required',
            'minimum_guarantee' => 'required',
            'insurance' => 'required',
            'advancement' => 'required',
            'final_price' => 'required',
            'service_detail' => 'required|min:120|max:1000',
        ];

        $messages = [
            'geographical_mobility.required' => 'Es necesario marcar movilidad geográfica',
            'necessary_material.required' => 'Es necesario marcar materiales necesarios incluidos',
            'minimum_guarantee.required' => 'Es necesario marcar garantia mínima de 30 días',
            'insurance.required' => 'Es necesario marcar si cuenta con seguro EPS/SCTR',
            'advancement.required' => 'Es necesario ingresar % de adelanto',
            'final_price.required' => 'Es necesario ingresar el precio final estimado',
            'service_detail.required' => 'Es necesario ingresar detalles y desarrollo del servicio',
            'service_detail.min' => 'El detalle y desarrollo del servicio debe tener como mínimo 120 caracteres',
            'service_detail.max' => 'El detalle y desarrollo del servicio debe tener como máximo 1000 caracteres'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $offer = OfferJob::find($id);

        $validator->after(function($validator) use ($offer, $id) {

            if ($offer->status == 5) {
                $validator->errors()->add('geographical_mobility', 'LA oferta ha sido cerrada. No puede enviar propuestas');
            }
        });

        if ($validator->fails())
            return response()->json($validator->errors(), 422);


        $proposal = new Proposal();
        $proposal->geographical_mobility = $request->geographical_mobility;
        $proposal->necessary_material = $request->necessary_material;
        $proposal->minimum_guarantee = $request->minimum_guarantee;
        $proposal->insurance = $request->insurance;
        $proposal->advancement = $request->advancement;
        $proposal->final_price = $request->final_price;
        $proposal->service_detail = $request->service_detail;
        $proposal->personal_interview = $request->personal_interview == '1' ? true : false;
        $proposal->status = 'Enviada';
        $proposal->from_id = auth()->id();
        $proposal->to_id = $offer->user_id;
        $proposal->offer_job_id = $offer->id;
        $proposal->save();

        //Proposal sent
        $from = $proposal->from;
        Mail::to($from)->send(new ProposalSent($proposal));
        //Proposal received
        $to = $proposal->to;
        Mail::to($to)->send(new ProposalReceived($proposal));

        return response()->json($proposal);
    }

//    public function test()
//    {
//        $proposal = Proposal::find(1) ;
//
//        $date = Carbon::now()->format('d/m/Y - h:m a');
//        return view('emails.proposal_received', compact('proposal', 'date'));
//    }
}
