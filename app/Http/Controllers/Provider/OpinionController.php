<?php

namespace App\Http\Controllers\Provider;

use App\Opinion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OpinionController extends Controller
{
    public function index()
    {
        $provider = auth()->user();
        $opinions = $provider->opinions;

        return view('provider.opinions', compact('opinions'));
    }

    public function show($id)
    {
        $opinion = Opinion::findOrFail($id);

        return $opinion;
    }

    public function update($id, Request $request)
    {
        $rules = [
            'description' => 'required',
        ];
        $messages = [
            'description.required' => 'Es necesario ingresar una descripción',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $opinion = Opinion::findOrFail($id);
        $opinion->title = $request->title;
        $opinion->description = $request->description;
        $opinion->reported = $request->reported == 'on' ? 1 : 0;
        $opinion->answer = $request->answer;
        $opinion->save();

        return $opinion;
    }
}
