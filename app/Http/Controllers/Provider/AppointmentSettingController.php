<?php

namespace App\Http\Controllers\Provider;

use App\ProviderSchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AppointmentSettingController extends Controller
{
    public function show()
    {
        return view('provider.appointment-setting');
    }

    public function add(Request $request)
    {
        $rules = [
            'start_time' => 'required',
            'end_time' => 'required',
        ];
        $messages = [
            'start_time.required' => 'Es necesario ingresar la hora de inicio correctamente.',
            'end_time.required' => 'Es necesario ingresar la hora de fin correctamente.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $validator->after(function($validator) use ($request) {
            $startInput = $request->start_time;
            $endInput = $request->end_time;
            $dayInput = $request->day;
            $providerId = auth()->id();
            if ($startInput > $endInput) {
                $validator->errors()->add('start_time', 'La hora inicio debe ser menor que la hora fin');
            }
            $schedules = ProviderSchedule::where('provider_id', $providerId)->where('day', $dayInput)->get();

            foreach ($schedules as $schedule){
                $start = $schedule->start;
                $end = $schedule->end;
                if (($startInput>$start && $startInput<$end) || ($endInput>$start && $endInput<$end) || ($startInput<$start && $endInput>$end)) {
                    $validator->errors()->add('start_time', 'El horario ingresado se cruza con un horario existente');
                }
            }

        });
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $schedule = new ProviderSchedule();
        $schedule->day = $request->day;
        $schedule->start = $request->start_time;
        $schedule->end = $request->end_time;
        $schedule->provider_id = auth()->id();
        $schedule->save();

        $schedule->complete = $schedule->getCompleteAttribute();

        return $schedule;
    }

    public function delete($id)
    {
        $schedule = ProviderSchedule::find($id);
        $schedule->delete();

        return $schedule;
    }
}
