<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchServiceController extends Controller
{
    public function show()
    {
        return view('search-service');
    }
}
