<?php

namespace App\Http\Controllers;

use App\Address;
use App\BusinessHour;
use App\Category;
use App\CategoryUser;
use App\Department;
use App\Language;
use App\User;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    public function show(Request $request)
    {
        $id = $request->id;
        $username = $request->username;
        $email = $request->email;
        $subcategoryId = $request->subcategory_id;
//        $categoryId = $request->category_id;
        $language = $request->language;
        $departmentId = $request->department_id;
        $provinceId = $request->province_id;
        $districtId = $request->district_id;
        $emergency = $request->emergency;

        $query = User::where('role', 1);

        if ($emergency) {
            $userIds = BusinessHour::whereNull('start')->whereNull('end')->where('day', 'Emergencia')->where('status', 0)->pluck('user_id');
            $query = $query->whereIn('id', $userIds);
        }

        if($subcategoryId){
            $subcategory = Category::findOrFail($subcategoryId);
            $userCategoryIds = CategoryUser::where('category_id', $subcategoryId)->pluck('user_id');
            $query = $query->whereIn('id', $userCategoryIds);
        }
        if ($language){
            $userLanguageIds = Language::where('language', $language)->pluck('user_id');
            $query = $query->whereIn('id', $userLanguageIds);
        }

        if($departmentId && $provinceId && $districtId){
            $userAddressIds = Address::where('department_id', $departmentId)
                ->where('province_id', $provinceId)->where('district_id', $districtId)->pluck('user_id');
            $query = $query->whereIn('id', $userAddressIds);

        }
        if($departmentId && $provinceId){
            $userAddressIds = Address::where('department_id', $departmentId)->where('province_id', $provinceId)->pluck('user_id');
            $query = $query->whereIn('id', $userAddressIds);
        }

        if($departmentId && $provinceId && $language){
            $userAddressIds = $userAddressIds->toArray();
            $userLanguageIds = $userLanguageIds->toArray();
            $ids = array_intersect($userAddressIds, $userLanguageIds);
            $query = $query->whereIn('id', $ids);
        }

        if($id){
            $query = $query->where('id', $id);
        }
        if($username){
            $query = $query->where('name', 'like', "%$username%");
        }
        if($email){
            $query = $query->where('email', $email);
        }


        $providers = $query->paginate(9);
        $departments = Department::all();
        $categories = Category::where('category_id', NULL)->get();
        return view('providers', compact('categories', 'departments', 'providers',
            'subcategoryId', 'subcategory', 'username', 'id', 'emergency'));
    }
}
