<?php

namespace App\Http\Controllers;

use App\Department;
use App\Mail\MessageContactReceived;
use App\MessageContact;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function show()
    {
        $departments = Department::orderBy('name', 'ASC')->get();
        return view('contact', compact('departments'));
    }

    public function send(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'theme' => 'required',
            'description' => 'required',
            'department_id' => 'required',
            'province_id' => 'required',
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar nombres y apellidos',
            'email.required' => 'Es necesario ingresar un correo',
            'email.email' => 'Es correo ingresado no tiene el formato correcto',
            'phone.required' => 'Es necesario ingresar un número telefónico',
            'theme.required' => 'Es necesario seleccionar un asunto',
            'description.required' => 'Es necesario ingresar un mensaje',
            'department_id.required' => 'Es necesario seleccionar un departamento',
            'province_id.required' => 'Es necesario seleccionar una provincia'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $messageContact = new MessageContact();
        $messageContact->name = $request->name;
        $messageContact->email = $request->email;
        $messageContact->phone = $request->phone;
        $messageContact->theme = $request->theme;
        $messageContact->description = $request->description;
        $messageContact->country_id = 1;
        $messageContact->department_id = $request->department_id;
        $messageContact->province_id = $request->province_id;
        $messageContact->district_id = $request->district_id;
        $messageContact->save();

        $admin = User::where('role',0)->get();

        Mail::to($admin)->send(new MessageContactReceived($messageContact));

        return response()->json($messageContact);
    }

//    public function test()
//    {
//        $messageContact = MessageContact::find(16) ;
//
//        $date = Carbon::now()->format('d/m/Y - h:m a');
//        return view('emails.message_to_admin', compact('messageContact', 'date'));
//    }
}
