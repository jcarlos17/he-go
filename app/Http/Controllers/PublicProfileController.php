<?php

namespace App\Http\Controllers;

use App\AdminImage;
use App\User;
use App\UserFavorite;
use Illuminate\Http\Request;

class PublicProfileController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        $imgTopPublicProfile = AdminImage::where('type', 'imgTopPublicProfile')->first();
        $imgBottomPublicProfile = AdminImage::where('type', 'imgBottomPublicProfile')->first();
        $imgTopPublicProfileBS = AdminImage::where('type', 'imgTopPublicProfileBS')->first();
        $imgBottomPublicProfileBS = AdminImage::where('type', 'imgBottomPublicProfileBS')->first();

        $favoriteExists = UserFavorite::where('user_favorite_id', $id)->where('user_id', auth()->id())->exists();

        $userCategories = $user->categories()->whereNull('categories.category_id')->get();
        foreach ($userCategories as $category)
            $category->userSubcategories = $this->getSubcategoriesForCategory($category, $user);

        return view('public-profile.index', compact('user', 'userCategories', 'favoriteExists',
            'imgTopPublicProfile', 'imgBottomPublicProfile', 'imgTopPublicProfileBS', 'imgBottomPublicProfileBS'));
    }

    public function getSubcategoriesForCategory($category, $user)
    {
        return $user->categories()->where('categories.category_id', $category->id)->get();
    }
}
