<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class DocIdentityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $dateLimited = $user->created_at->addDays(7);
        $identityCount = $user->identities()->count();
        if ($user->role == 1 && $dateLimited > Carbon::now() && $identityCount == 0)
            return redirect('profile-settings')
                ->with('notification', 'Por favor complete su documento de identidad para poder continuar.');

        return $next($request);
    }
}
