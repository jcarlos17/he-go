<?php

namespace App\Http\Middleware;

use Closure;

class ProviderMiddleware
{

    public function handle($request, Closure $next)
    {
        if (auth()->user()->role != 0 && auth()->user()->role != 1 ) // not a provider
            return redirect('insights');

        return $next($request);
    }
}
