<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{

    public function handle($request, Closure $next)
    {
        if (auth()->user()->role != 0 ) // not an admin
            return redirect('insights');

        return $next($request);
    }
}
