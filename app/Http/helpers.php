<?php

use App\AdminImage;
use App\Category;


function categories()
{
    return Category::withCount('jobs')->orderBy('jobs_count', 'desc')->take(6)->get();
}

function imgBottomInsight()
{
    return AdminImage::where('type', 'imgBottomInsight')->first();
}
function imgBottomInsightBS()
{
    return AdminImage::where('type', 'imgBottomInsightBS')->first();
}