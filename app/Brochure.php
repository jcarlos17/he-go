<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brochure extends Model
{
    public function getBrochureUrlAttribute()
    {
        $name = explode(".", $this->file);
        $extension = $name[1];
        $jpg = strcasecmp ($extension, 'jpg');
        $jpeg = strcasecmp ($extension, 'jpeg');
        $png = strcasecmp ($extension, 'png');
        $svg = strcasecmp ($extension, 'svg');
        $pdf = strcasecmp ($extension, 'pdf');
        $doc = strcasecmp ($extension, 'doc');
        $docx = strcasecmp ($extension, 'docx');

        if ($this->file){
            if ($jpg === 0 || $jpeg === 0 || $png === 0 || $svg === 0)
                return '/images/brochureFiles/'.$this->file;
            elseif($pdf === 0)
                return '/images/brochureFiles/icono_pdf.png';
            elseif ($doc === 0 || $docx === 0)
                return '/images/brochureFiles/icono_word.png';
        }
        return '/images/brochureFiles/default.png';
    }

    public function getViewUrlAttribute()
    {
        return '/images/brochureFiles/'.$this->file;
    }
}
