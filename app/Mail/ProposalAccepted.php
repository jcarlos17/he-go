<?php

namespace App\Mail;

use App\Proposal;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProposalAccepted extends Mailable
{
    use Queueable, SerializesModels;

    public $proposal;

    public function __construct(Proposal $proposal)
    {
        $this->proposal = $proposal;
    }


    public function build()
    {
        $this->subject("Su propuesta fue aceptada");
        return $this->view('emails.proposal_accepted');
    }
}
