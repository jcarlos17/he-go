<?php

namespace App\Mail;

use App\Proposal;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProposalSent extends Mailable
{
    use Queueable, SerializesModels;


    public $proposal;

    public function __construct(Proposal $proposal)
    {
        $this->proposal = $proposal;
    }


    public function build()
    {
        $this->subject("Propuesta enviada correctamente");
        return $this->view('emails.proposal_sent');
    }
}
