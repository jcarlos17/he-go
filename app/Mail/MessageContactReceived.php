<?php

namespace App\Mail;

use App\MessageContact;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageContactReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $messageContact;

    public function __construct(MessageContact $messageContact)
    {
        $this->messageContact = $messageContact;
    }

    public function build()
    {
        $this->subject("Nuevo mensaje de la sección contactos");

        return $this->view('emails.message_to_admin');
    }
}
