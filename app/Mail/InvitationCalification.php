<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitationCalification extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $id;

    public function __construct($name, $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    public function build()
    {
        $this->subject("Invitación a dar una valoración en HeGo");
        return $this->view('emails.invitation_calification');
    }
}
