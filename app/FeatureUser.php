<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureUser extends Model
{
    protected $table = 'feature_user';

    public function user()
    {
        return $this->belongsTo(User::class );
    }

    public function feature()
    {
        return $this->belongsTo(Category::class );
    }
}
