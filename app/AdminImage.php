<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminImage extends Model
{
    public function getImageUrlAttribute()
    {
        return '/images/adminImages/'.$this->image;
    }
}
