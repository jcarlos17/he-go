<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageContact extends Model
{
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getLocationCompleteAttribute()
    {
        if ($this->district()->exists())
            return $this->department->name.' / '.$this->province->name.' / '.$this->district->name;

        return $this->department->name.' / '.$this->province->name;
    }

    public function getDateFormatAttribute()
    {
       return $this->created_at->format('d/m/Y - h:m a');
    }
}
