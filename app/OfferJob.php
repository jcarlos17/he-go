<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OfferJob extends Model
{
    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Category::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function benefits()
    {
        return $this->hasMany(OfferJobBenefit::class);
    }

    public function images()
    {
        return $this->hasMany(OfferJobImage::class);
    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }

    public function pause()
    {
        return $this->hasOne(OfferPause::class);
    }

    public function getIsPublishedAttribute()
    {
        return $this->status == 1 && $this->expire_date > Carbon::now();
    }
    public function getIsClosedAttribute()
    {
        return $this->status == 1 && $this->expire_date < Carbon::now();
    }

    public function getImageSelectedAttribute()
    {
        $imageSelected = $this->images()->where('selected', 1)->first();
        if ($imageSelected)
            return '/images/offer-job/'.$imageSelected->image;

        return '/images/offer-job/default.png';
    }

    public function getLocationAttribute()
    {
        if($this->department_id && $this->province_id){
            if($this->district_id){
                return $this->department->name.', '.$this->province->name.', '.$this->district->name;
            }else{
                return $this->department->name.', '.$this->province->name;
            }
        }

        return '';
    }

    public function getJobTypeNameAttribute()
    {
        if($this->status == 3)
            return 'Pausado';

        if ($this->category_id)
            return $this->job_type;

        return 'Borrador';
    }

    public function getExpireDateformatAttribute()
    {
        if ($this->status == 1 || $this->status == 5)
            return date('d/m/Y', strtotime($this->expire_date));

        return '';
    }

    public function getCategoryCompleteAttribute()
    {
        if ($this->status == 1 || $this->status == 5)
            return $this->subcategory->name.', '.$this->category->name;

        return '';
    }

    public function getDepProvAttribute()
    {
        if ($this->status == 1 || $this->status == 5)
            return ucwords (strtolower($this->department->name.', '.$this->province->name));

        return '';
    }

    public function getPublicationDateAttribute()
    {
        if ($this->status == 1 || $this->status == 5)
            return $this->created_at->format('d/m/Y h:i a');

        return '';
    }

    public function getTypeDescriptionAttribute()
    {
        if ($this->type == 'Presupuesto')
            return 'Enviar presupuesto';

        return 'Aplicar ahora';
    }

    public function getStatusDescriptionAttribute()
    {
        switch ($this->status) {
            case (5) :
                return 'Solicitud completada';
                break;
            case (4) :
                return 'Solicitud cancelada';
                break;
            case (3) :
                return 'Solicitud pausada';
                break;
            case (2) :
                return 'Estudiando candidatos';
                break;
            case (1) :
                return 'Solicitud activa';
                break;

            default :
                return 'Borrador';
                break;

        }
    }


}
