<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileBanner extends Model
{
    public function getBannerUrlAttribute()
    {
        if ($this->image)
            return '/images/banners/'.$this->image;

        return '/images/banners/default.jpg';
    }
}
