<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    protected $appends = ['description_str', 'date_format', 'hour_format', 'user_name'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function provider()
    {
        return $this->belongsTo(User::class, 'provider_id');
    }

    public function getUserNameAttribute()
    {
        return $this->user->name;
    }

    public function getDescriptionStrAttribute()
    {
        return substr($this->description, 0, 40).'...';
    }

    public function getDateFormatAttribute()
    {
        return $this->created_at->format('d-m-Y');
    }

    public function getHourFormatAttribute()
{
    return $this->created_at->format('h:i:s');
}
}
