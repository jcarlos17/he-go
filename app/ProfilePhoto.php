<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilePhoto extends Model
{
    public function getPhotoUrlAttribute()
    {
        if ($this->image)
            return '/images/photos/'.$this->image;

        return '/images/photos/default.png';
    }
}
