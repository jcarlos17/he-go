<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public function getImageUrlAttribute()
    {
        return '/images/galleries/'.$this->image;
    }
}
