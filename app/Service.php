<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function getPriceValueAttribute()
    {
        if ($this->type_value == 'HFO')
            return 'S/ '.$this->price.'/ Fin Obra';

        if ($this->type_value == 'PH')
            return 'S/ '.$this->price.'/ Hora';

        if ($this->type_value == 'PV')
            return 'S/ '.$this->price.'/ Visita';

        return 'Gratuito';
    }

    public function getByAppointmentAttribute()
    {
        if ($this->appointment)
            return '(Requiere cita previa)';

        return '';
    }
}
