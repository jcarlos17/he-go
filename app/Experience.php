<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    public $dates = ['start_date', 'end_date'];

    public function getDateIntervalAttribute()
    {
        if ($this->end_date)
            return $this->start_date->format('F Y').' - '.$this->end_date->format('F Y');

        return $this->start_date->format('F Y').' - To Date';
    }
}
