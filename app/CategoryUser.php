<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryUser extends Model
{
    protected $table = 'category_user';

    public function user()
    {
        return $this->belongsTo(User::class );
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
