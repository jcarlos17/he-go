<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    public function from()
    {
        return $this->belongsTo(User::class, 'from_id');
    }

    public function to()
    {
        return $this->belongsTo(User::class, 'to_id');
    }

    public function offer()
    {
        return $this->belongsTo(OfferJob::class, 'offer_job_id');
    }

    public function getSendDateAttribute()
    {
        return $this->created_at->format('d/m/Y h:i A');
    }

    public function getTimeElapsedAttribute()
    {
        $fecha1 = $this->created_at;//fecha inicial
        $fecha2 = Carbon::now();//fecha de cierre

        $intervalo = $fecha1->diff($fecha2);

        return $intervalo->format('%d días, %H horas, %i minutos, %s segundos');
    }
}
