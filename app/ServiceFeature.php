<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceFeature extends Model
{
    public function getUserHasServiceAttribute()
    {
        return FeatureUser::where('user_id', auth()->id())->where('service_feature_id', $this->id)->exists();
    }
}
