<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferPause extends Model
{
    protected $dates = [
        'end_pause_date'
    ];
}
