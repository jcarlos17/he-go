<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function provider()
    {
        return $this->belongsTo(User::class);
    }

    public function getTypeAttribute()
    {
        if($this->price == '60')
            return 'Plan joven (6 meses)';
        elseif ($this->price == '100')
            return 'Plan joven (12 meses)';
        elseif ($this->price == '150')
            return 'Tarfia plana ilimitado (6 meses)';
        else
            return 'Tarfia plana ilimitado (12 meses)';
    }
}
