<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function getDescriptionCompleteAttribute()
    {
        if ($this->district()->exists())
            return $this->description.' - '.$this->district->name.', '.$this->province->name.', '.$this->department->name;

        return $this->description.' - '.$this->province->name.', '.$this->department->name;
    }
}
