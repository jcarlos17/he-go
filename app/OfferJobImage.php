<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferJobImage extends Model
{
    public function getImageUrlAttribute()
    {
        if ($this->image)
            return '/images/offer-job/'.$this->image;

        return '/images/offer-job/default.png';
    }

    public function getImageUrlPdfAttribute()
    {
        if ($this->image)
            return 'images/offer-job/'.$this->image;

        return 'images/offer-job/default.png';
    }
}
