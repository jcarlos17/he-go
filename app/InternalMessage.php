<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalMessage extends Model
{
    public function from()
    {
        return $this->belongsTo(User::class);
    }

    public function to()
    {
        return $this->belongsTo(User::class);
    }

    public function getTopicPresentationAttribute()
    {
        return strlen($this->topic) > 25 ? substr($this->topic, 0, 25) . '...': $this->topic;
    }

    public function getMessagePresentationAttribute()
    {
        return strlen($this->message) > 58 ? substr($this->message, 0, 58) . '...': $this->message;
    }

    public function getHourAttribute()
    {
        return $this->created_at->format('h:i a');
    }
}
