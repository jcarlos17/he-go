<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Web API
Route::get('/department/{id}/provinces', 'Location\ProvinceController@byDepartment');

Route::get('/province/{id}/districts', 'Location\DistrictController@byProvince');
Route::get('/category/{id}/subcategories', 'CategoryController@byCategory');
Route::get('/subcategories/{id}/category', 'SubcategoryController@showCategory');
Route::get('/subcategories', 'SubcategoryController@show');
Route::get('/user/{id}/{option}/services', 'Api\ServiceController@show');
Route::get('/provider/{id}/hours', 'Api\HourController@show');
