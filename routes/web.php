<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contact', 'ContactController@show');
Route::post('/contact/message/send', 'ContactController@send');
//Route::get('/test', 'ContactController@test');

Route::get('/about', 'AboutController@show');
Route::get('/categories', 'CategoryController@show');
Route::get('/providers', 'ProviderController@show');
Route::get('/ultimos-trabajos', 'LastJobController@show');
Route::get('/search-service', 'SearchServiceController@show');
Route::get('/service-provider', 'ServiceProviderController@show');
Route::get('/terms-conditions', 'TermsConditionController@show');
Route::get('/privacy-policy', 'PrivacyPolicyController@show');
//public profile
Route::get('/perfil-publico/{id}', 'PublicProfileController@index');

Route::group(['middleware' => ['auth','admin'], 'namespace' => 'Admin'], function () {
    //categories
    Route::get('admin/categories', 'CategoryController@show');
    Route::post('admin/categories/add', 'CategoryController@add');
    Route::get('admin/categories/{id}/show', 'CategoryController@showCategory');
    Route::post('admin/categories/{id}/update', 'CategoryController@update');
    Route::get('admin/categories/{id}/delete', 'CategoryController@delete');
    //subcategories
    Route::get('admin/categories/{id}/subcategories', 'SubcategoryController@show');
    Route::post('admin/categories/{cat_id}/subcategories/add', 'SubcategoryController@add');
    Route::get('admin/categories/subcategories/{id}/show', 'SubcategoryController@showCategory');
    Route::post('admin/categories/subcategories/{id}/update', 'SubcategoryController@update');
    Route::get('admin/categories/subcategories/{id}/delete', 'SubcategoryController@delete');
    //images
    Route::get('admin/images', 'ImageController@index');
    Route::post('admin/images/add', 'ImageController@add');
    Route::get('admin/images/{id}/delete', 'ImageController@delete');
    //send message
    Route::get('admin/send-message', 'InternalMessageController@show');
    Route::post('admin/send-message', 'InternalMessageController@send');
    //packages
    Route::get('admin/packages', 'PackageController@show');
    Route::post('admin/packages', 'PackageController@add');

});

Route::group(['middleware' => ['auth','provider'], 'namespace' => 'Provider'], function () {

    //documents
    Route::post('doc-identity/update', 'DocumentController@updateDni');
    Route::get('doc-identity/{id}/delete', 'DocumentController@deleteDni');
    Route::post('public-service/update', 'DocumentController@updatePs');
    Route::get('public-service/{id}/delete', 'DocumentController@deletePs');


    Route::get('/verify-identity', 'VerifyIdentityController@show');

    //privacy - settings
    Route::get('/privacy-settings', 'PrivacySettingController@show');
    Route::post('/privacy-settings', 'PrivacySettingController@update');

    Route::group(['middleware' => 'doc_identity'], function () {
        //business hour
        Route::get('/business-hours', 'BusinessHourController@show');
        Route::post('/hour/add', 'BusinessHourController@add');
        Route::post('/hour/attend', 'BusinessHourController@attend');
        Route::get('/hour/{id}/delete', 'BusinessHourController@delete');
        //manage - services
        Route::get('/manage-services', 'ManageServiceController@show');
        Route::get('service/{id}/show', 'ManageServiceController@showService');
        Route::post('/services/add', 'ManageServiceController@store');
        Route::post('/services/{id}/update', 'ManageServiceController@update');
        Route::get('/services/{id}/delete', 'ManageServiceController@delete');
        //team
        Route::get('/manage-team', 'ManageTeamController@show');
//    Route::get('/manage-team/search', 'ManageTeamController@search');
        Route::get('/member/{member_id}/delete', 'ManageTeamController@delete');
        //invitation
        Route::get('/invitation', 'ManageTeamController@invitation');
        Route::get('/invitation-email', 'ManageTeamController@invitationEmail');
        //invoices
        Route::get('/invoices', 'InvoiceController@index');
        Route::get('/invoice/{id}/show', 'InvoiceController@show');
        //
        Route::get('/update-package', 'UpdatePackageController@show');
        //my-finances
        Route::get('/finances', 'FinanceController@index');

        //offer job
        Route::get('/oferta-trabajo/{id}', 'JobOfferController@show');
        Route::post('/oferta-trabajo/{id}/sentProposal', 'JobOfferController@sentProposal');
//    Route::get('/test', 'JobOfferController@test');

        //opinions
        Route::get('/opinions', 'OpinionController@index');
        Route::get('/opinions/{id}/show', 'OpinionController@show');
        Route::post('/opinions/{id}/update', 'OpinionController@update');
        //appointment
        Route::get('/appointment/{id}', 'AppointmentController@show');
        //appointment setting
        Route::get('/appointment-settings', 'AppointmentSettingController@show');
        Route::post('/schedule/add', 'AppointmentSettingController@add');
        Route::get('/schedule/{id}/delete', 'AppointmentSettingController@delete');
        //proposal sent
        Route::get('/proposal-sent', 'ProposalSentController@index');
        Route::get('/proposal-sent/{id}/show', 'ProposalSentController@show');
        //Request rating
        Route::get('request-rating', 'RatingController@index');
        Route::post('request-rating', 'RatingController@sent');
    });
});

Route::group(['middleware' => 'auth', 'namespace' => 'User'], function () {

    //insights user
    Route::get('/insights', 'InsightController@show');
    //Profile percentage
    Route::get('/profile/percentage', 'ProfilePercentageController@show');
    //Profile Settings
    Route::get('/profile-settings', 'ProfileSettingController@show');

    Route::group(['namespace' => 'ProfileSetting'], function () {
        //basic information
        Route::post('/basic-information/{user_id}/edit', 'BasicInformationController@update');
    });

    Route::group(['middleware' => 'doc_identity'], function () {
        //send message contact - claim
        Route::post('/message-contact/{to_id}/send', 'MessageController@sendContact');
        Route::post('/message-claim/{to_id}/send', 'MessageController@sendClaim');
        //chat
        Route::get('/chat', 'ChatController@show');
        Route::post('/chat/new', 'ChatController@store');
        Route::get('/chat/{id}/conversation', 'ChatController@conversation');
        Route::post('/chat/{id}/message', 'ChatController@message');

        Route::get('/gestionar-trabajos', 'JobOfferController@index');
        //post job offer
        Route::post('/publicar-oferta-trabajo', 'JobOfferController@default');
        Route::get('/publicar-oferta-trabajo/{id}', 'JobOfferController@create');
        Route::post('/publicar-oferta-trabajo/{id}', 'JobOfferController@store');
        Route::post('/publicar-oferta-trabajo/{id}/benefit/add', 'JobOfferController@addBenefit');
        Route::get('/publicar-oferta-trabajo/benefit/{id}/delete', 'JobOfferController@deleteBenefit');
        Route::post('/publicar-oferta-trabajo/{id}/image/add', 'JobOfferController@addImage');
        Route::get('/publicar-oferta-trabajo/{offer_id}/image/{id}/select', 'JobOfferController@selectImage');
        Route::get('/publicar-oferta-trabajo/image/{id}/delete', 'JobOfferController@deleteImage');
        //offer job
        Route::get('/oferta-trabajo/{id}/edit', 'JobOfferController@edit');
        Route::post('/oferta-trabajo/{id}/edit', 'JobOfferController@update');
        Route::post('/oferta-trabajo/{id}/pause', 'JobOfferController@pause');
        Route::get('/oferta-trabajo/{id}/restore', 'JobOfferController@restore');
        Route::get('/oferta-trabajo/{id}/delete', 'JobOfferController@delete');
        Route::get('/oferta-trabajo/{id}/show-pause', 'JobOfferController@showPause');
        Route::get('/oferta-trabajo/{id}/detalle', 'JobOfferController@detail');
        Route::get('/oferta-trabajo/{id}/pdf', 'JobOfferController@stream');

//    Route::get('/proposal', 'JobOfferController@proposal');

        Route::group(['namespace' => 'ProfileSetting'], function () {
            //photo profile
            Route::group(['prefix' => 'photo'], function () {
                Route::post('update', 'ProfilePhotoController@update');
                Route::get('{id}/select', 'ProfilePhotoController@select');
                Route::get('{id}/delete', 'ProfilePhotoController@delete');
            });
            //photo banner
            Route::group(['prefix' => 'banner'], function () {
                Route::post('update', 'BannerPhotoController@update');
                Route::get('{id}/select', 'BannerPhotoController@select');
                Route::get('{id}/delete', 'BannerPhotoController@delete');
            });
            //categoriees
            Route::group(['prefix' => 'categories'], function () {
                Route::post('{user_id}/add', 'CategoryController@add');
                Route::post('subcat/{user_id}/add', 'CategoryController@addSubcat');
                Route::get('{category_id}/delete', 'CategoryController@delete');
                Route::get('subcat/{category_id}/delete', 'CategoryController@deleteSubcat');
            });
            //social information
            Route::post('/social-information/{user_id}/edit', 'SocialInformationController@update');
            //presentation
            Route::post('/presentation/edit', 'PresentationController@update');
            //address
            Route::group(['prefix' => 'address'], function () {
                Route::post('{user_id}/add', 'AddressController@add');
                Route::get('{address_id}/delete', 'AddressController@delete');
            });
            //languages
            Route::group(['prefix' => 'languages'], function () {
                Route::post('{user_id}/add', 'LanguageController@add');
                Route::get('{language_id}/delete', 'LanguageController@delete');
            });
            //experience
            Route::group(['prefix' => 'experiences'], function () {
                Route::post('{user_id}/add', 'ExperienceController@add');
                Route::post('{experience_id}/update', 'ExperienceController@update');
                Route::get('{experience_id}/delete', 'ExperienceController@delete');
            });
            Route::get('/experience/{experience_id}/show', 'ExperienceController@show');
            //certificates
            Route::group(['prefix' => 'certificates'], function () {
                Route::post('{user_id}/add', 'CertificateController@add');
                Route::post('{certificate_id}/update', 'CertificateController@update');
                Route::get('{certificate_id}/delete', 'CertificateController@delete');
            });
            Route::get('/certificate/{certificate_id}/show', 'CertificateController@show');
            //qualifications
            Route::group(['prefix' => 'qualifications'], function () {
                Route::post('{user_id}/add', 'QualificationController@add');
                Route::post('{qualification_id}/update', 'QualificationController@update');
                Route::get('{qualification_id}/delete', 'QualificationController@delete');
            });
            Route::get('/qualification/{qualification_id}/show', 'QualificationController@show');
            //galleries
            Route::group(['prefix' => 'galleries'], function () {
                Route::post('{user_id}/add', 'GalleryController@add');
                Route::get('{category_id}/delete', 'GalleryController@delete');
            });
            //videos
            Route::group(['prefix' => 'videos'], function () {
                Route::post('{user_id}/add', 'VideoController@add');
                Route::get('{video_id}/delete', 'VideoController@delete');
            });
            //brochures
            Route::group(['prefix' => 'brochures'], function () {
                Route::post('{user_id}/add', 'BrochureController@add');
                Route::get('{brochure_id}/delete', 'BrochureController@delete');
            });
            //features
            Route::post('/features/add', 'FeatureController@add');
        });

        Route::post('/tasks/add', 'InsightController@add');
        Route::post('/tasks/{id}/completed', 'InsightController@completed');
        Route::get('/tasks/{id}/delete', 'InsightController@delete');

        //proposal received
        Route::get('/proposal-received', 'ProposalReceivedController@index');
        Route::get('/proposal-received/{id}/show', 'ProposalReceivedController@show');
        Route::post('/proposal-received/{id}/reject', 'ProposalReceivedController@reject');
        Route::get('/proposal-received/{id}/accept', 'ProposalReceivedController@accept');
        //internalMessage
        Route::get('/private-messages', 'PrivateMessageController@index');
        Route::get('/private-message/{id}/show', 'PrivateMessageController@show');
        //appointments
        Route::get('/appointments', 'AppointmentController@index');
        Route::get('/appointment/{id}/show', 'AppointmentController@show');

        //favorite
        Route::get('/favourite-listings', 'FavouriteListingController@show');
        Route::get('/favorite/{favorite_id}/add', 'FavouriteListingController@add');
        Route::get('/favorite/{id}/delete', 'FavouriteListingController@delete');

        Route::get('/security-settings', 'SecuritySettingController@show');

        //security - settings
        Route::post('/password/change', 'SecuritySettingController@update');
        Route::get('/deactivated/{id}', 'SecuritySettingController@deactivated');

        //book appointment
        Route::post('/reserve/{provider_id}/appointment', 'BookAppointmentController@store');
    });

    //send opinion
    Route::post('/opinions/{prov_id}/{user_id}', 'OpinionController@send');
});