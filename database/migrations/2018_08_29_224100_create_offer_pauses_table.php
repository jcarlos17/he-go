<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferPausesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_pauses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('duration');
            $table->dateTime('end_pause_date');

            $table->integer('offer_job_id')->unsigned();
            $table->foreign('offer_job_id')->references('id')->on('offer_jobs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_pauses');
    }
}
