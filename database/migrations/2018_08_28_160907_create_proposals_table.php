<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('geographical_mobility');
            $table->boolean('necessary_material');
            $table->boolean('minimum_guarantee');
            $table->boolean('insurance');
            $table->string('advancement');
            $table->decimal('final_price');
            $table->text('service_detail');
            $table->boolean('personal_interview')->default(0);
            $table->string('status'); //Enviada - Rechazada - Aceptada
            $table->text('reject_reason')->nullable();

            $table->integer('from_id')->unsigned();
            $table->foreign('from_id')->references('id')->on('users');
            $table->integer('to_id')->unsigned();
            $table->foreign('to_id')->references('id')->on('users');
            $table->integer('offer_job_id')->unsigned();
            $table->foreign('offer_job_id')->references('id')->on('offer_jobs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
