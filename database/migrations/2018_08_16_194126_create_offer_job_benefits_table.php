<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferJobBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_job_benefits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // FK
            $table->integer('offer_job_id')->unsigned()->nullable();
            $table->foreign('offer_job_id')->references('id')->on('offer_jobs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_job_benefits');
    }
}
