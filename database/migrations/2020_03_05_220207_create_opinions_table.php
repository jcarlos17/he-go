<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('answer')->nullable();

            $table->boolean('reported')->default(false);

            $table->integer('user_id')->unsigned(); // De
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('provider_id')->unsigned(); // A
            $table->foreign('provider_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinions');
    }
}
