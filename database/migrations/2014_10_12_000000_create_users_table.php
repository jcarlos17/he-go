<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->smallInteger('role'); // 0:Admin - 1:Provider - 2:Seeker
            $table->string('password');

            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('company_name')->nullable();
            $table->string('type_provider')->nullable();
            $table->string('website')->nullable();
            $table->text('presentation')->nullable();

            $table->string('identity_type')->nullable();
            $table->string('identity_document')->nullable();
            $table->string('identity_image')->nullable(); // image identity

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('users');
    }
}
