<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->date('expire_date')->nullable();
            $table->text('description')->nullable();
            $table->text('abilities')->nullable();
            $table->string('priority_type')->nullable();
            $table->string('schedule')->nullable();
            $table->string('job_type')->nullable();
            $table->string('geographical_mobility')->nullable();
            $table->string('experience')->nullable();
            $table->string('language')->nullable();
            $table->string('suggested_payment')->nullable();
            $table->integer('status')->default(0); //Borrador - Publicado - Pausado - Cerrado
            $table->string('type')->nullable(); // Presupuesto - Carta
            $table->integer('limit')->default(4);
            $table->boolean('attached_images')->default(false);

            // FK
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->foreign('subcategory_id')->references('id')->on('categories');

            // FK
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            // FK
            $table->integer('department_id')->unsigned()->nullable();
            $table->foreign('department_id')->references('id')->on('departments');
            // FK
            $table->integer('province_id')->unsigned()->nullable();
            $table->foreign('province_id')->references('id')->on('provinces');
            // FK
            $table->integer('district_id')->unsigned()->nullable();
            $table->foreign('district_id')->references('id')->on('districts');
            // FK
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_jobs');
    }
}
