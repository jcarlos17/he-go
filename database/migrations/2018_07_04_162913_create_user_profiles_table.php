<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('has_profile_picture')->default(0);
            $table->boolean('has_photo_identity_document')->default(0);
            $table->boolean('has_complete_basic_information')->default(0);
            $table->boolean('has_category_work')->default(0);
            $table->boolean('has_three_social_network')->default(0);
            $table->boolean('has_cover_letter')->default(0);
            $table->boolean('has_experience')->default(0);
            $table->boolean('has_photo_gallery')->default(0);
            $table->boolean('has_certificate_or_prize')->default(0);
            $table->boolean('has_professional_preparation')->default(0);
            $table->boolean('has_brochure')->default(0);

            // FK
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
