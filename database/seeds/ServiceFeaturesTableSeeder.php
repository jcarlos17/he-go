<?php

use App\ServiceFeature;
use Illuminate\Database\Seeder;

class ServiceFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceFeature::create(['name' => 'Entrego factura', 'icon' => 'lnr-file-empty']);
        ServiceFeature::create(['name' => 'Garantía en mis trabajos', 'icon' => 'lnr-thumbs-up']);
        ServiceFeature::create(['name' => 'Hago visitas a domicilio', 'icon' => 'lnr-home']);
        ServiceFeature::create(['name' => 'Atiendo urgencias las 24 horas', 'icon' => 'lnr-history']);
        ServiceFeature::create(['name' => 'Consultas gratuitas y sin compromiso', 'icon' => 'lnr-heart']);
        ServiceFeature::create(['name' => 'Dispongo de personal de trabajo', 'icon' => 'lnr-user']);
        ServiceFeature::create(['name' => 'Soy freelance', 'icon' => 'lnr-smile']);
        ServiceFeature::create(['name' => 'Personal uniformado e identificado', 'icon' => 'lnr-shirt']);
        ServiceFeature::create(['name' => 'Atención personalizada', 'icon' => 'lnr-pointer-up']);
        ServiceFeature::create(['name' => 'Dispongo de local/oficina/taller', 'icon' => 'lnr-location']);
        ServiceFeature::create(['name' => 'Acepto pago con tarjeta', 'icon' => 'lnr-cart']);
        ServiceFeature::create(['name' => 'Movilidad geográfica nacional', 'icon' => 'lnr-earth']);
        ServiceFeature::create(['name' => 'Dispongo de seguro individual contra accidentes', 'icon' => 'lnr-heart-pulse']);
    }
}
