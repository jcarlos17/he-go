<?php

use App\PrivacySetting;
use Illuminate\Database\Seeder;

class PrivacySettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PrivacySetting::create(['type' => 'make_profile_photo_public', 'name' => 'Foto de perfil público']);
        PrivacySetting::create(['type' => 'make_banner_photo_public', 'name' => 'Foto de banner público']);
        PrivacySetting::create(['type' => 'appointment_option', 'name' => 'Opción de cita']);
        PrivacySetting::create(['type' => 'contact_details', 'name' => 'Detalles del contacto']);
        PrivacySetting::create(['type' => 'business_hours', 'name' => 'Horas de trabajo']);
        PrivacySetting::create(['type' => 'show_services', 'name' => 'Mostrar servicios']);
        PrivacySetting::create(['type' => 'show_team', 'name' => 'Mostrar equipo']);
        PrivacySetting::create(['type' => 'show_gallery', 'name' => 'Mostrar galería']);
        PrivacySetting::create(['type' => 'show_videos', 'name' => 'Mostrar videos']);
    }
}
