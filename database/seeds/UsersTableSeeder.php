<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'last_name' => 'He Go',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 0
        ]);

        User::create([
            'name' => 'Prestador',
            'last_name' => 'He Go',
            'email' => 'prestador@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 1
        ]);

        User::create([
            'name' => 'Buscador',
            'last_name' => 'He Go',
            'email' => 'buscador@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 2
        ]);
    }
}
