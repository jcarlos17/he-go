<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Country, provinces and locations
        $path = public_path('data/division_geografica_Peru.sql');
        DB::unprepared(file_get_contents($path));
    }
}
