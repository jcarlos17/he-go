<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat1 = Category::create(['name' => 'REFORMAS Y REPARACIONES', 'icon_id' => 3]);
        Category::create(['name' => 'ALBAÑIL', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'CARPINTERIA DE MADERA', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'CARPINTERIA DE METAL', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'CARPINTERIA DE ALUMINIO', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'CERRAJERO', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'VIDRIERO/CRISTALERO PEQUEÑAS REPARACIONES', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'VIDRIERO/CRISTALERO ATENCION A EMPRESAS', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'ELECTRICISTA', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'GASFITERO', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'FONTANERO', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'SOLDADOR', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'AIRE ACONDICIONADO', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'PINTOR', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'DRYWALL', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'YESERO', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'PARQUET', 'category_id' => $cat1->id, 'icon_id' => 3]);
        Category::create(['name' => 'MARMOL', 'category_id' => $cat1->id, 'icon_id' => 3]);

        $cat2 = Category::create(['name' => 'PRESTACIÓN DE SERVICIOS', 'icon_id' => 3]);
        Category::create(['name' => 'COCINERO / CHEFF', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LIMPIEZA / MANTENIMIENTO', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LIMPIEZA / MANTENIMIENTO DEL HOGAR', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LIMPIEZA / MANTENIMIENTO A EMPRESAS', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LIMPIEZA / MANTENIMIENTO A OFICINAS', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LAVANDERIA', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LAVANDERIA PRENDAS DE VESTIR Y MÁS', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'LAVANDERIA ALFOMBRAS Y TAPICES', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'CUIDADO DE PERSONAS', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'CUIDADO DE NIÑOS / CANGOORO', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'CUIDADO DE PERSONAS MAYORES', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR A DOMICILIO', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR REFUERZO ACADÉMICO', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR DE BAILE', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR DE YOGA', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR LENGUA EXTRANJERA', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR DE ZUMBA', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROFESOR DE AEROBIC', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'COSTUREROS', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'COSTUREROS DE PRENDAS A DOMICILIO', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'COSTUREROS INDUSTRIALES', 'category_id' => $cat2->id, 'icon_id' => 3]);
        Category::create(['name' => 'CUIDADOR DE MASCOTAS', 'category_id' => $cat2->id, 'icon_id' => 3]);

        $cat3 = Category::create(['name' => 'BELLEZA Y ESTÉTICA', 'icon_id' => 3]);
        Category::create(['name' => 'MANICURA', 'category_id' => $cat3->id, 'icon_id' => 3]);
        Category::create(['name' => 'PEDICURA', 'category_id' => $cat3->id, 'icon_id' => 3]);
        Category::create(['name' => 'PELUQUERO', 'category_id' => $cat3->id, 'icon_id' => 3]);
        Category::create(['name' => 'NUTRICIONISTA', 'category_id' => $cat3->id, 'icon_id' => 3]);
        Category::create(['name' => 'ESTILISTA', 'category_id' => $cat3->id, 'icon_id' => 3]);
        Category::create(['name' => 'PERSONAL SHOPPER', 'category_id' => $cat3->id, 'icon_id' => 3]);

        $cat4 = Category::create(['name' => 'DISEÑO GRÁFICO Y WEB E INFORMÁTICA', 'icon_id' => 3]);
        Category::create(['name' => 'DISEÑADOR GRAFICO', 'category_id' => $cat4->id, 'icon_id' => 3]);
        Category::create(['name' => 'DISEÑADOR WEB', 'category_id' => $cat4->id, 'icon_id' => 3]);
        Category::create(['name' => 'PROGRAMADOR', 'category_id' => $cat4->id, 'icon_id' => 3]);
        Category::create(['name' => 'MANTENIMIENTO INFORMATICO Y REDES', 'category_id' => $cat4->id, 'icon_id' => 3]);

        $cat5 = Category::create(['name' => 'CÁMARA Y VIDEO', 'icon_id' => 3]);
        Category::create(['name' => 'FOTOGRAFO', 'category_id' => $cat5->id, 'icon_id' => 3]);
        Category::create(['name' => 'CAMAROGRAFO', 'category_id' => $cat5->id, 'icon_id' => 3]);
        Category::create(['name' => 'AFINES', 'category_id' => $cat5->id, 'icon_id' => 3]);

        $cat6 = Category::create(['name' => 'MECÁNICA', 'icon_id' => 3]);
        Category::create(['name' => 'RAPIDA EN ZONA', 'category_id' => $cat6->id, 'icon_id' => 3]);
        Category::create(['name' => 'EN GENERAL', 'category_id' => $cat6->id, 'icon_id' => 3]);
        Category::create(['name' => 'NEUMATICOS', 'category_id' => $cat6->id, 'icon_id' => 3]);
        Category::create(['name' => 'ELECTRICISTA DEL AUTOMOVIL', 'category_id' => $cat6->id, 'icon_id' => 3]);
        Category::create(['name' => 'TAPICEROS', 'category_id' => $cat6->id, 'icon_id' => 3]);

        $cat7 = Category::create(['name' => 'CATERING Y EVENTOS', 'icon_id' => 3]);
        Category::create(['name' => 'CAMAREROS', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'AZAFATA', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'SERVICIO DE CATERING', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'ORGANIZACION DE EVENTOS Y CELEBRACIONES', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'DJS', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'ANIMADORES DE EVENTOS', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'PAYASOS', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'MIMO', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'BAILADORES', 'category_id' => $cat7->id, 'icon_id' => 3]);
        Category::create(['name' => 'ALQUILER DE LOCALES PARA EVENTOS', 'category_id' => $cat7->id, 'icon_id' => 3]);

        $cat8 = Category::create(['name' => 'FREELANCE', 'icon_id' => 3]);
        Category::create(['name' => 'DECORADOR', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'MODELO', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'PUBLICISTA', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'ACTOR', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'DISEÑO DE INTERIORES', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'REDACCION Y PERIODISMO', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'TRADUCTORES', 'category_id' => $cat8->id, 'icon_id' => 3]);
        Category::create(['name' => 'PRODUCTOR DE CINE Y MEDIA', 'category_id' => $cat8->id, 'icon_id' => 3]);

        $cat9 = Category::create(['name' => 'TATUADORES', 'icon_id' => 3]);
        $cat10 = Category::create(['name' => 'CARIDAD Y ONGs', 'icon_id' => 3]);
        Category::create(['name' => 'VOLUNTARIADO', 'category_id' => $cat10->id, 'icon_id' => 3]);
        Category::create(['name' => 'DONACION DE ENSERES Y ARTICULOS', 'category_id' => $cat10->id, 'icon_id' => 3]);

        $cat11 = Category::create(['name' => 'TRANSPORTE, CARGO Y MUDANZAS', 'icon_id' => 3]);
        Category::create(['name' => 'MUDANZAS', 'category_id' => $cat11->id, 'icon_id' => 3]);
        Category::create(['name' => 'SERVICIO PRIVADO DE TRANSPORTE DE PERSONAS', 'category_id' => $cat11->id, 'icon_id' => 3]);
        Category::create(['name' => 'TRANSPORTE DE CARGA LIGERA', 'category_id' => $cat11->id, 'icon_id' => 3]);
        Category::create(['name' => 'TRANSPORTE DE CARGA PESADA', 'category_id' => $cat11->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONDUCTOR PARTICULAR', 'category_id' => $cat11->id, 'icon_id' => 3]);
        Category::create(['name' => 'OPERARIO DE MAQUINARIA PESADA', 'category_id' => $cat11->id, 'icon_id' => 3]);
        Category::create(['name' => 'CARGADOR/PORTEADOR', 'category_id' => $cat11->id, 'icon_id' => 3]);

        $cat12 = Category::create(['name' => 'ALQUILER DE VEHÍCULOS / EMBARCACIONES', 'icon_id' => 3]);
        Category::create(['name' => 'AUTOS', 'category_id' => $cat12->id, 'icon_id' => 3]);
        Category::create(['name' => 'CAMIONES', 'category_id' => $cat12->id, 'icon_id' => 3]);
        Category::create(['name' => 'FURGONETAS', 'category_id' => $cat12->id, 'icon_id' => 3]);
        Category::create(['name' => 'BARCOS', 'category_id' => $cat12->id, 'icon_id' => 3]);

        $cat13 = Category::create(['name' => 'VIAJES Y AVENTURA', 'icon_id' => 3]);
        Category::create(['name' => 'VIAJES DE AVENTURAS', 'category_id' => $cat13->id, 'icon_id' => 3]);
        Category::create(['name' => 'VENTA DE PASAJES AEREOS', 'category_id' => $cat13->id, 'icon_id' => 3]);
        Category::create(['name' => 'SERVICIOS DE TOURS', 'category_id' => $cat13->id, 'icon_id' => 3]);
        Category::create(['name' => 'GUIA TURISTICO', 'category_id' => $cat13->id, 'icon_id' => 3]);

        $cat14 = Category::create(['name' => 'PC Y LAPTOPS', 'icon_id' => 3]);
        Category::create(['name' => 'VENTA ', 'category_id' => $cat14->id, 'icon_id' => 3]);
        Category::create(['name' => 'REPARACION A DOMICILIO', 'category_id' => $cat14->id, 'icon_id' => 3]);
        Category::create(['name' => 'SERVICIO TECNICO', 'category_id' => $cat14->id, 'icon_id' => 3]);

        $cat15 = Category::create(['name' => 'CELULARES Y TABLETS', 'icon_id' => 3]);
        Category::create(['name' => 'REPARACION DE CELULARES', 'category_id' => $cat15->id, 'icon_id' => 3]);
        Category::create(['name' => 'REPARACION DE TABLETS', 'category_id' => $cat15->id, 'icon_id' => 3]);

        $cat16 = Category::create(['name' => 'ESPECIALISTAS Y PROFESIONES', 'icon_id' => 3]);
        Category::create(['name' => 'ABOGADOS', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'ARQUITECTOS / TECNICOS', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'SUPERVISOR DE PROYECTOS', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONTADORES', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'MEDICOS', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'VETERINARIA', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'PAISAJISTAS', 'category_id' => $cat16->id, 'icon_id' => 3]);
        Category::create(['name' => 'DELINEANTES', 'category_id' => $cat16->id, 'icon_id' => 3]);

        $cat17 = Category::create(['name' => 'CUIDADOS DE LA SALUD', 'icon_id' => 3]);
        Category::create(['name' => 'TECNICOS DE ENFERMERIA', 'category_id' => $cat17->id, 'icon_id' => 3]);
        Category::create(['name' => 'ENFERMERIA', 'category_id' => $cat17->id, 'icon_id' => 3]);
        Category::create(['name' => 'FISIOTERAPEUTA', 'category_id' => $cat17->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONSULTA MEDICA', 'category_id' => $cat17->id, 'icon_id' => 3]);
        Category::create(['name' => 'ENTRENADOR PERSONAL', 'category_id' => $cat17->id, 'icon_id' => 3]);

        $cat18 = Category::create(['name' => 'ASTROLOGÍA - FENG SHUI', 'icon_id' => 3]);
        Category::create(['name' => 'SEERVICIOS GENERALES', 'category_id' => $cat18->id, 'icon_id' => 3]);
        Category::create(['name' => 'LECTURA DE CARTAS Y TAROT', 'category_id' => $cat18->id, 'icon_id' => 3]);
        Category::create(['name' => 'HOROSCOPO', 'category_id' => $cat18->id, 'icon_id' => 3]);
        Category::create(['name' => 'FENG SHUI', 'category_id' => $cat18->id, 'icon_id' => 3]);

        $cat19 = Category::create(['name' => 'MANTENIMIENTO', 'icon_id' => 3]);
        Category::create(['name' => 'CONTROL DE PLAGAS', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'JARDINERO', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'PULIR SUELOS', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'MANTENIMIENTO DE ACENSORES', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'TELEFONILLOS Y PORTEROS AUTOMATICOS', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'MANTENIMIENTO DE PISCINAS', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'ALARMAS Y ANTENAS', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'EXTINTORES', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'DOMOTICA', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'TOLDOS', 'category_id' => $cat19->id, 'icon_id' => 3]);
        Category::create(['name' => 'LETREROS Y ROTULOS', 'category_id' => $cat19->id, 'icon_id' => 3]);

        $cat20 = Category::create(['name' => 'CONSTRUCCIÓN Y MANTENIMIENTO', 'icon_id' => 3]);
        Category::create(['name' => 'CONSTRUCCION DE CASAS', 'category_id' => $cat20->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONSTRUCCION DE GARAJES', 'category_id' => $cat20->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONSTRUCCION DE SAUNAS', 'category_id' => $cat20->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONSTRUCCION DE CASAS PREFABRICADAS', 'category_id' => $cat20->id, 'icon_id' => 3]);
        Category::create(['name' => 'CONSTRUCCION DE PISCINAS / JACUZZIS', 'category_id' => $cat20->id, 'icon_id' => 3]);
        Category::create(['name' => 'DERRIBOS Y EXCAVACIONES', 'category_id' => $cat20->id, 'icon_id' => 3]);
    }
}
